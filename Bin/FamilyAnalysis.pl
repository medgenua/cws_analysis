#!/usr/bin/perl

################
#keep track of runtime 
$now = time;
#;LOAD MODULES #
################
use Number::Format;
use DBI;
use Getopt::Std;
use POSIX 'ceil';
use Scalar::Util qw(looks_like_number);

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

# print the host running the job #
$hostname = `hostname`;
print "Running Job on $hostname";

###############################
# SET NUMBER FORMATTING STYLE #
###############################
my $de = new Number::Format(-thousands_sep =>',',-decimal_point => '.');

##########################
# COMMAND LINE ARGUMENTS #
##########################
# D = database (eg CNVanalysis-hg18)
# p = pid
# s = sid
my $sid;
my $pid;
getopts('D:p:s:',\%opts);
if ($opts{'s'}) {
	$sid = $opts{'s'};
}
else {
	die('No sample specified');
}
if ($opts{'p'}) {
	$pid = $opts{'p'};
}
else {
	die('No project specified');
}


###########################
# CONNECT TO THE DATABASE #
###########################
my $db;
if ($opts{'D'}) {
	$db = $opts{'D'};
}
else {
	die('No database specified');
}
$connectionInfo="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfo,$userid,$userpass);
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfo,$userid,$userpass) ;
}

$dbh->{mysql_auto_reconnect} = 1;

## if coming from addped.pl, the pid does not correspond to a real pid, so get and compare from db. 
$filepid = $pid;
$sth = $dbh->prepare("SELECT COUNT(idsamp) FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
$sth->execute();
my ($count) = $sth->fetchrow_array();
$sth->finish();
if ($count == 0) {
	$sth = $dbh->prepare("SELECT trackfromproject FROM sample WHERE id = '$sid'");
	$sth->execute();
	($pid) = $sth->fetchrow_array();
	$sth->finish();
	if ($pid == 0) {
		$sth = $dbh->prepare("SELECT idproj FROM projsamp WHERE idsamp = '$sid' ORDER BY idproj ASC LIMIT 1");
		$sth->execute();
		($pid) = $sth->fetchrow_array();
		$sth->finish();
	}
}
else {
	print "Sample '$sid' is present in provided project '$pid'. Assuming this is a valid hit.\n";
}

#####################################
## CHANGE TO CNVanalysis DIRECTORY ##
#####################################
chdir($scriptdir);

##########################
## INITIALISE VARIABLES ##
##########################
my @cnvs;
my @fcnvs;
my @mcnvs;
my $mdata = 0;
my $fdata = 0;
my %samples;
my %extrasamples;
@nrs=(1..22);
push(@nrs,'X');
my %chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "M" } = 25;


sub calculateMargin {
    my ($lstart, $lstop, $resolution) = @_;

    my $default_margin = 250000;
    my $rounding_number = 10000;

    if ($resolution eq 'low') {
	    $default_margin = 1000000;
    	$rounding_number = 40000;
    }

    my $lsize = $lstop - $lstart;
    if ($lsize > 2 * ($default_margin)) {
        return ceil($lsize / (2*$rounding_number)) * $rounding_number;
    }
    else {
        return $default_margin;
    }
}

##########################################
## GET ALL SAMPLES FROM CURRENT PROJECT ##
##########################################
## in project itself
$query = "SELECT idsamp FROM projsamp WHERE idproj = '$pid'";
my $sth = $dbh->prepare($query);
$sth->execute();
while (my @row = $sth->fetchrow_array()) {
	$samples{$row[0]} = 1;
}
$sth->finish();
## in extra datafiles
for ($i = 0; $i<= 99; $i++) {   # datafile.0 = main datafile for CNVanalysis.
	#if (-e "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extra.$i.cols") {
	if (-e "$datadir/$filepid.datafile.$i.cols") {
		open IN, "$datadir/$filepid.datafile.$i.cols";
		#open IN, "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extra.$i.cols";
		while (<IN>) {
			chomp($_);
			my @p = split(/=/,$_);
			$extrasamples{$p[0]}{'cols'} = $p[1];
			#$extrasamples{$p[0]}{'file'} = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extra.$i.txt";
			$extrasamples{$p[0]}{'file'} = "$datadir/$filepid.datafile.$i.txt";
		} 
		close IN;
	}
}
###################################
## GET FAMILY INFO FROM DATABASE ##
###################################
my $query = "SELECT father, father_project, mother, mother_project FROM parents_relations WHERE id = '$sid'";
my $prsth = $dbh->prepare($query);
$prsth->execute();
my @res = $prsth->fetchrow_array();
my $fid = $res[0];
my $fatherpid = $res[1];
my $mid = $res[2];
my $motherpid = $res[3];
$prsth->finish();

#####################################
## GET / CREATE DATAFILE FOR CHILD ##
#####################################
# todo, is this needed?

#########################################################################
## get cnvs of the child  (from any project,so info is always correct) ##
######################################################################### 
my $cnvquery = "SELECT id, chr, start, stop, largestart, largestop FROM aberration WHERE sample = '$sid' ORDER BY chr";
my $cnvsth = $dbh->prepare($cnvquery);
$cnvsth->execute();
$cnvrows = $cnvsth->rows();
while (my @row = $cnvsth->fetchrow_array()) {
	push(@cnvs,[@row]);
} 
my $nrcnvs = scalar(@cnvs);
$cnvsth->finish();
#############################
## COMPARE CNVS TO PARENTS ##
#############################
my @parents = ($fid, $mid);
foreach my $parent (@parents) {
#if ($fid != 0) {
	next if ($parent == 0);
	## parent samples/project details
	my $query = "SELECT s.chip_dnanr, s.intrack, s.trackfromproject, s.gender, ps.datatype, ps.swgs_resolution FROM sample s JOIN projsamp ps ON s.id = ps.idsamp WHERE id = '$parent'";
	$parsth = $dbh->prepare($query);
	$parsth->execute();
	my @res = $parsth->fetchrow_array();
	my $parent_name = $res[0];
	my $parent_datatype = $res[4];
	my $swgsresolution = 'high'; # use high resolution as default (matches original array margins)
	if ($res[5]) {
		$swgsresolution = $res[5];
	}
	# print "parent $parent_name datatype: <<$parent_datatype>>\n";
	# print "parent name ($parent): $parent_name\n";
	my $ppid; # get the project id for father cnv's 
	if ($res[3] eq 'Male' && $fatherpid != 0 && $fatherpid == $pid) {		# father reference project already listed.
		$ppid = $fatherpid;
		# print "in option 1 $ppid vs $pid\n";
		#if ($ppid == $pid) {
			#print "## father in project,\n";
			#$fdata = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.$fid.txt";
			$fdata = "$datadir/$filepid.$parent.txt";
		#}
	}
	elsif($res[3] eq 'Female' && $motherpid != 0 && $motherpid == $pid) {
		$ppid = $motherpid;
		# print "in option 2\n";
		#if ($ppid == $pid) {
			$fdata = "$datadir/$filepid.$parent.txt";
		#}
	}
	elsif (exists($samples{$parent})) {
		# print "in option 3\n";
		#print "# father in current project, but not yet in parental relations database\n";
		$ppid = $pid;
		#$fdata = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.$fid.txt";
		$fdata = "$datadir/$filepid.$parent.txt";
	}
	elsif ($res[1] == 1) {	# is parent listed in track? then use that project
		# print "in option 4\n";
		$ppid = $res[2];	
		# perhaps present in extra files		
		if (exists($extrasamples{$parent})) {
			#$fdata = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extrasample.$fid.txt";
			$fdata = "$datadir/$filepid.extrasample.$parent.txt";
			#print "father in extra file\n";
		}
	}
	else {		# just use most recent project... (can be changed manually later on)
		# print "in option 5\n";
		my $ppsth = $dbh->prepare("SELECT idproj FROM projsamp WHERE idsamp = '$parent' ORDER BY idproj DESC LIMIT 1");
		$ppsth->execute();
		my @pprow = $ppsth->fetchrow_array();
		$ppid = $pprow[0];
		$ppsth->finish();
		# perhaps present in extra files		
		if (exists($extrasamples{$parent})) {
			#$fdata = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extrasample.$fid.txt";
			# @Geert: waarom andere filenaam $filepid.extrasample.$parent.txt vs $filepid.$parent.txt?
			$fdata = "$datadir/$filepid.extrasample.$parent.txt";
			#print "father in extra file\n";
		}

	}
	$parsth->finish();
	## create file if needed
	print "fdata for $res[3] parent : $fdata\n";
	unless ($fdata eq '0' || -e $fdata) {
		#$fdata = "$datadir/$filepid.extrasample.$fid.txt";
		## create file lock while creating the datafile
		#open DUMMY, ">>/var/tmp/datafiles/CNV-WebStore/runtime/dummy";
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$parent}{'cols'};
		my $datafile = $extrasamples{$parent}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$parent.txt && cp /tmp/$filepid.extrasample.$parent.txt $fdata && rm -f /tmp/$filepid.extrasample.$parent.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}

	# update relations table
	if ($res[3] eq 'Male') {
		print "setting father project\n";
		$dbh->do("UPDATE parents_relations SET father_project='$ppid' WHERE id = '$sid' AND father = '$parent'");
	}
	elsif($res[3] eq 'Female') {
		print "setting mother project\n";
		$dbh->do("UPDATE parents_relations SET mother_project='$ppid' WHERE id = '$sid' AND mother = '$parent'");
	}
	# now get cnvs from parent
	my $fcnvquery = "SELECT id, chr, start, stop, cn FROM aberration WHERE idproj = '$ppid' AND sample = '$parent'";
	my $fcnvsth = $dbh->prepare($fcnvquery);
	$fcnvsth->execute();
	my @fcnvs;
	while (my @row = $fcnvsth->fetchrow_array()) {
		push(@fcnvs,[@row]);
	} 
	$fcnvsth->finish();
 	my $nrfcnvs = scalar(@fcnvs);
	# now compare to child
	my $datachr = 0;
	my %fdatapoints = ();
	my @positions;	
	for ($j = 0; $j < $nrcnvs; $j++) { # number of cnvs in child
		# use largestart and largestop from child cnv	 
		my $margin = calculateMargin($cnvs[$j][4], $cnvs[$j][5], $swgsresolution);
		my $from = $cnvs[$j][2] - $margin;
		my $to = $cnvs[$j][3] + $margin;
		# my $from = $cnvs[$j][2] - 250000;
		# my $to = $cnvs[$j][3] + 250000;
		if ($from < 0) {
			$from = 0;
		}
		my $fcnv_list = '';
		my $fcnvid;
		# first check if it is not already stored for this parent!
		for ($k = 0;$k < $nrfcnvs; $k++) {
			# check chr
			my $print = 0;
			if ($cnvs[$j][1] != $fcnvs[$k][1]) {
				#print "different chromosome\n";
				next;
			}
			# check if 5' child inside parent
			elsif ($from >= $fcnvs[$k][2] && $from <= $fcnvs[$k][3]) {
				$fcnvid .= $fcnvs[$k][0] . ",";
				$print = 1;
			}
			# check if 3' child inside parent
			elsif ($to >= $fcnvs[$k][2] && $to <= $fcnvs[$k][3]) {
				$fcnvid .= $fcnvs[$k][0] . ",";
				$fcnv_list .= "$fcnvs[$k][2]|$fcnvs[$k][3]|$fcnvs[$k][4],";
				$print = 1;
			}
			# check if parent inside child
			elsif ($from <= $fcnvs[$k][2] && $to >= $fcnvs[$k][3]) {
				$fcnvid .= $fcnvs[$k][0] . ",";
				$fcnv_list .= "$fcnvs[$k][2]|$fcnvs[$k][3]|$fcnvs[$k][4],";
				$print = 1;
				#next;
			}
		}
			
		$currchr = $chromhash{ $cnvs[$j][1]};
		# first check if region exists already (avoid double storage...)
		my $pcsth = $dbh->prepare("SELECT pr.id, pd.content FROM parents_regions pr JOIN parents_datapoints pd ON pr.id = pd.id WHERE chr = '$cnvs[$j][1]' AND start = '$from' AND stop = '$to' AND sid = '$parent'");
		$pcsth->execute();
		my $pcrows = $pcsth->rows();
		my $newid;
		$fcnvid =~ s/,$//;
		if ($pcrows > 0) {
			my @pcrow = $pcsth->fetchrow_array();
			$newid = $pcrow[0];
			$content = $pcrow[1];
			$pcsth->finish();
			$dbh->do("INSERT INTO parent_offspring_cnv_relations (aid, parent_seen, parent_aid, parent_sid,ParentalCNVs) VALUES ('$cnvs[$j][0]', '0', '$newid', '$parent','$fcnvid')");
			if ($fdata ne '0' && $content eq 'NA') {
				## was data already loaded for this chr? 
				if ($currchr ne $datachr) {
					%fdatapoints = ();
					$datachr = $currchr;
					open DATA, "$fdata";
					my $header = <DATA>;
					while (my $line = <DATA>) {
						chomp($line);
						my @parts = split(/\t/,$line);
						if ($parts[1] eq $datachr) {
							if (looks_like_number(sprintf($parts[3]))) {
								$val1 = sprintf("%.3f",$parts[3]);
							}
							else {
								$val1 = $parts[3];
							}
							if (looks_like_number(sprintf($parts[4]))) {
								$val2 = sprintf("%.3f",$parts[4]);
							}
							else {
								$val2 = $parts[4];
							}
							$fdatapoints{$parts[2]} = "$parts[2]" . "_" . $val1 . "_" . $val2 . "_";
						}
					}
					close DATA;
					@positions = keys(%fdatapoints);
					@positions = sort {$a <=> $b} @positions;
				}
				## get needed probes from positions array. 
				my $store = '';
				foreach(@positions) {
					my $pos = $_;
					if ($pos >= $from && $pos <= $to) {
						$store = $store . $fdatapoints{$pos} ;
					}
					elsif ($pos > $to) {
						last;
					}
				}
				close DATA;
				$store =~ s/_$//;
				$dbh->do("UPDATE parents_datapoints SET content = '$store', datatype = '$parent_datatype' WHERE id = '$newid'");
			}
			elsif($fdata eq '0' && $content eq 'NA' && $fcnvid ne '') {
				# no datafile : use datapoints from cnvs in parent
				$fcnvid =~ s/,$//;# substr($fcnvid,0,-1);
				my $dr = $dbh->selectall_arrayref("SELECT content,structure FROM `datapoints` WHERE id IN ($fcnvid)");
				my $c = '';
				foreach my $r (@$dr) {
					# needed structure : just use as is.
					if ($r->[1] eq '1110') {
						$c .= $r->[0]."_";
					}
					# need pos/logR/baf
					elsif (substr($r->[1],0,3) eq '111') {
						my @v = split(/_/,$r->[0]);
						my $incr = 0;
						for (my $p = 0; $p <=4; $p++) {
							if (substr($r->[1],$p,1) eq '1') {
								$incr++;
							}
						}
						for (my $p = 0; $p < scalar(@v);$p = $p+$incr) {
							$c .= $v[$p]."_".$v[$p+1]."_".$v[$p+2]."_";
						}
					}
				}
				if ($c ne '') { 
					$c =~ s/_$//;
					$dbh->do("UPDATE parents_datapoints SET content = '$c', datatype = '$parent_datatype' WHERE id = '$newid'");
				}
			}
		}
		else {
			my $store = '';
			if ($currchr eq 'X') {
				print "new region for $res[3] parent ($pstart:$pend) for sid:$sid : $currchr:$from-$to\n";
				print "  => fdata : $fdata\n";
			}
			if ($fdata ne '0') { # check if fdata available and insert data if so.
				if ($currchr eq 'X') {
					print "get data from file for : $currchr:$from-$to\n";
				}
				## was data already loaded for this chr? 
				if ($currchr ne $datachr) {
					#print "Loading data for father into tmp array\n";
					%fdatapoints = ();
					$datachr = $currchr;
					open DATA, "$fdata";
					my $header = <DATA>;
					while (my $line = <DATA>) {
						chomp($line);
						my @parts = split(/\t/,$line);
						if ($parts[1] eq $datachr) {
							if (looks_like_number(sprintf($parts[3]))) {
								$val1 = sprintf("%.3f",$parts[3]);
							}
							else {
								$val1 = $parts[3];
							}
							if (looks_like_number(sprintf($parts[4]))) {
								$val2 = sprintf("%.3f",$parts[4]);
							}
							else {
								$val2 = $parts[4];
							}
							$fdatapoints{$parts[2]} = "$parts[2]" . "_" . $val1 . "_" . $val2 . "_";
						}
					}
					close DATA;
					@positions = keys(%fdatapoints);
					@positions = sort {$a <=> $b} @positions;
				}
				## get needed probes from positions array. 
				
				foreach(@positions) {
					my $pos = $_;
					if ($pos >= $from && $pos <= $to) {
						$store = $store . $fdatapoints{$pos} ;
					}
					elsif ($pos > $to) {
						last;
					}
				}
				#close DATA;
				$store =~ s/_$//;
			}
			elsif ($fcnvid ne '') {
				# no datafile : use datapoints from cnvs in parent
				$fcnvid =~ s/,$//;# substr($fcnvid,0,-1);
				my $dr = $dbh->selectall_arrayref("SELECT content,structure FROM `datapoints` WHERE id IN ($fcnvid)");
				my $c = '';
				foreach my $r (@$dr) {
					# needed structure : just use as is.
					if ($r->[1] eq '1110') {
						$c .= $r->[0]."_";
					}
					# need pos/logR/baf
					elsif (substr($r->[1],0,3) eq '111') {
						my @v = split(/_/,$r->[0]);
						my $incr = 0;
						for (my $p = 0; $p <=4; $p++) {
							if (substr($r->[1],$p,1) eq '1') {
								$incr++;
							}
						}
						for (my $p = 0; $p < scalar(@v);$p = $p+$incr) {
							$c .= $v[$p]."_".$v[$p+1]."_".$v[$p+2]."_";
						}
					}
				}
				if ($c ne '') { 
					$c =~ s/_$//;
					$store = $c;
				}
				else {
					$store = 'NA';
				}
			}
			else {
				$store = 'NA';
			}
			$timer = time();
			$dbh->do("LOCK TABLE `parents_datapoints` LOW_PRIORITY WRITE");
			$timer = time();
			my $abquery = "INSERT INTO parents_datapoints (content, datatype) VALUES ('$store', '$parent_datatype')";
			$dbh->do($abquery);
			my $newid = $dbh->last_insert_id( undef, undef, undef, undef );
			$dbh->do("UNLOCK TABLES");
			$dbh->do("INSERT DELAYED INTO parents_regions (id, chr, start, stop, sid) VALUES ('$newid', '$cnvs[$j][1]', '$from', '$to', '$parent')");
			$dbh->do("INSERT DELAYED INTO parent_offspring_cnv_relations (aid, parent_seen, parent_aid, parent_sid,ParentalCNVs) VALUES ('$cnvs[$j][0]', '0', '$newid', '$parent','$fcnvid')");	
		}
	}
}
########################################
## SET STATUS TO COMPLETE IN DATABASE ##
########################################
$dbh->do("UPDATE `CNVanalysis-TMP`.`FamilyAnalysis` SET status = 1 WHERE pid = '$filepid' AND sid = '$sid'");

## update runtime stats.
$now = time - $now;
$dbh->do("INSERT INTO `GenomicBuilds`.`RuntimeStatistics` (process,chiptypeid,argument,time) VALUES ('FamilyAnalysis','$chipid','0','$now')");


exit;

#################
## SUBROUTINES ##
#################
sub bin_search {
    my ($list, $tgt) = @_;

    return 0       if $tgt < $list->[0];
    return $#$list if $tgt > $list->[-1];

    my ($beg, $end, $val) = (0, $#$list, undef);
    while ($beg <= $end) {
        my $mid = int(($beg + $end) / 2);
        $val = $list->[$mid];
        if ($val > $tgt) {
            $end = $mid - 1;
        }
        elsif ($val < $tgt) {
            $beg = $mid + 1;
        }
        else {
            return $mid;
        }
    }
    return -1;
}

