#!/usr/bin/perl

## keep track of runtime 
$now = time;

################
# LOAD MODULES #
################
use Number::Format;
use DBI;
use Getopt::Std;
#use Schedule::DRMAAc qw/ :all /;
use XML::Simple;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

# print hostname
$hostname = `hostname`;
print "Running Job on $hostname";

###############################
# SET NUMBER FORMATTING STYLE #
###############################
my $de = new Number::Format(-thousands_sep =>',',-decimal_point => '.');

##########################
# COMMAND LINE ARGUMENTS #
##########################
# D = Use specific database (mandatory, full name ! eg CNVanalysis-hg18 )
# p = prefix (project id)
# u = user id
# s = sample id
# r = random identifier
getopts('D:p:s:r:',\%opts);
	
# COMMAND LINE ARGUMENTS
my $db = $opts{'D'};
my $pid = $opts{'p'};
my $sid = $opts{'s'};
my $rand = $opts{'r'};

#########################
## Connect to database ##
######################### 
$connectionInfo="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfo,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfo,$userid,$userpass) ;
}
$dbh->{mysql_auto_reconnect} = 1;
#Create connection to local mysql server
#$local = DBI->connect("dbi:mysql:CNVanalysis-TMP:127.0.0.1",$userid,$userpass);
$local = DBI->connect("dbi:mysql:CNVanalysis-TMP:$host",$userid,$userpass);
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($local)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	#$local = DBI->connect("dbi:mysql:CNVanalysis-TMP:127.0.0.1",$userid,$userpass) ;
	$local = DBI->connect("dbi:mysql:CNVanalysis-TMP:$host",$userid,$userpass);
}
$local->{mysql_auto_reconnect} = 1;

#####################################
## CHANGE TO CNVanalysis DIRECTORY ##
#####################################
chdir($scriptdir);

########################
## GET SAMPLE DETAILS ##
########################
my $query = "SELECT chip_dnanr,gender FROM sample WHERE id='$sid'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @result = $sth->fetchrow_array();
my $CurrSample = $result[0];
my $CurrGender = $result[1];
	

#########################
## GET PROJECT DETAILS ##
#########################
$query = "SELECT chiptypeid, chiptype, asymFilter,minsnp,minmethods,userID FROM project WHERE id='$pid'";
$sth = $dbh->prepare($query);
$sth->execute();
@result = $sth->fetchrow_array();
$sth->finish();
my $chiptypeid = $result[0];
my $chiptype = $result[1];
my $asymstring = $result[2];
my $minsnp = $result[3];
my $minmethods = $result[4];
my $uid = $result[5];
# asymettric filtering details
my ($asym, $asymmethod, $asymminscore, $asymtype) = split(/\|/,$asymstring);
## process Y => from settings, now hardcoded by VICE?
$Ymethod = 'VanillaICE';


# get idx 
$query = "SELECT idx FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'";
$sth = $dbh->prepare($query);
$sth->execute();
my ($CurrIndex) = $sth->fetchrow_array();
$sth->finish();
# print info
if ($asym == 1) {
	print "Formatting results for sample $CurrSample. \n";
	print " - Asymmetric filtering by $asymmethod ($asymtype score >$asymminscore). \n";
	print " - GenomeStudio index: $CurrIndex\n";
}
else {
	print "Formatting results for sample $CurrSample \n";
	print " - GenomeStudio index: $CurrIndex\n";
}

######################
## GET USED METHODS ##
######################
$query = "SELECT Method, Version FROM `project_x_method` WHERE pid = '$pid'";
$sth = $dbh->prepare($query);
$sth->execute();
my %algos;
my %firstchars;
while (my @row = $sth->fetchrow_array()) {
	$algos{$row[0]}{'version'} = $row[1];
	my $fc = lc(substr($row[0],0,1));
	while (defined($firstchars{$fc})) {
		$fc = &NextChar($fc);
	}
	$firstchars{$fc} = $row[0];
	$algos{$row[0]}{'fc'} = $fc;
	#$algos{$row[0]}{'results'} = ();
}
$sth->finish();
my @results = ();

############################
## create local variables ##
############################
my @types ;
@types = ("CNV Bin: Min 0 to Max 0.5" , "CNV Bin: Min 0.5 to Max 1.5" , "CNV Bin: Min 1.5 to Max 2.5" , "CNV Bin: Min 2.5 to Max 3.5", "CNV Bin: Min 3.5 to Max 4.5", "RECURRENT CNV");
@nrs=(1..22);
push(@nrs,'X','Y');
my %chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y"; 
%dc = ('0'=>'','1'=>'Path','2'=>'Path','3'=>'UV','4'=>'Ben','5'=>'F.P',6=>'Path_Rec',''=>'','Path'=>'Pathogenic','UV'=>'Unknown Significance','Ben'=>'Benign','F.P'=>'False Positive','Path_Rec'=>'Recessive Pathogenic');

###########################
## READ IN SETTINGS FILE ##
###########################
my $xml = new XML::Simple;
#my $settingsfile = "datafiles/CNV-WebStore/runtime/$rand.settings.xml";
my $settingsfile = "$datadir/$rand.settings.xml";
my $settings = $xml->XMLin($settingsfile);
my $plink = $settings->{general}->{plink};
my $doY = $settings->{general}->{doY};
# get column positions of datafile . 
my $inputcolstring = $settings->{general}->{inputcols};
my @inputcols = split(/\|/,$inputcolstring);
my $colidx = -1;
my $NameCol = -1;
my $ChrCol = -1;
my $PosCol = -1;
my $LogRCol = -1;
my $BafCol = -1;
my $GTCol = -1;
foreach (@inputcols) {
	$colidx++;
	if ($_ eq '%probename%') {
		$NameCol = $colidx;
	}
	elsif ($_ eq '%chromosome%') {
		$ChrCol = $colidx;
	}
	elsif ($_ eq '%position%') {
		$PosCol = $colidx;
	}
	elsif ($_ eq '%samplelogr%') {
		$LogRCol = $colidx;
	}
	elsif ($_ eq '%samplebaf%') {
		$BafCol = $colidx;
	}
	elsif ($_ eq '%samplegt%') {
		$GTCol = $colidx;
	}
}
my $structure = '1' ;  #poscol = mandatory
if ($LogRCol == -1) {
	$structure .= '0';
}
else {
	$structure .= '1';
}
if ($BafCol == -1) {
	$structure .= '0';
}
else {
	$structure .= '1';
}
if ($GTCol == -1) {
	$structure .= '0';
}
else {
	$structure .= '1';
}
##############################
## replacement placeholders ##
##############################
my $runtimedir = "$scriptdir/Analysis_Methods/Runtime/";
$replace{'runtimedir'} = $runtimedir;
$replace{'samplename'} = $CurrSample;
$replace{'gender'} = $CurrGender;
#$replace{'inputfile'} = $file;
#$replace{'build'} = $shortdb;
$replace{'pid'} = $pid;
$replace{'sid'} = $sid;
$replace{'chiptype'} = $chiptype;
$replace{'scriptdirreg'} = $scriptdirreg;


###########################
## BUILD PROBE INFO HASH ##
###########################

## new approach: build from datafile => always correct positions.
print "Reading in probe positions from datafile\n";
open IN, "$datadir/$pid.$sid.txt"; 
my $head = <IN>;
my %ProbeByPos;  # {chr}{pos} => name
my @ProbePos;	 # [chr][...] => positions => binary search this for flanking, get name from hash. 
while (my $line = <IN>) {
	my @p = split(/\t/,$line);
	$ProbeByPos{$chromhash{$p[1]}}{$p[2]} = $p[0]; # {chr}{position} = {name}
}
close IN;
print "Sorting Positions on chromosome: ";
foreach my $chr (1 .. 24) {
	print "$chr - ";
	my @array = keys %{$ProbeByPos{$chr}};
	@array = sort { $a <=> $b } @array;
	$ProbePos[$chr] = \@array;
}

## old approach: build from database
#my $snpquery = "SELECT position,name FROM probelocations WHERE chromosome = ? AND chiptype = '$chiptypeid' ORDER BY position ASC";
#my $sth = $dbh->prepare($snpquery);
#my %ProbeByPos;  # {chr}{pos} => name
#my @ProbePos;	 # [chr][...] => positions => binary search this for flanking, get name from hash. 
#print "Fetching probe info for chromosome:";
#foreach my $chr (1 .. 24) {   # loop chromosomes
#        print " $chr";
#        my $array_ref = $dbh->selectcol_arrayref($sth,{Columns=>[1,2]},$chr);
#        my %hash = @$array_ref;
#        my @array = keys %hash;
#        $ProbeByPos{$chr} = \%hash;
#        @array = sort { $a <=> $b } @array;
#        $ProbePos[$chr] = \@array;
#}
print "\n";

######################
## LOAD CLASSIFIERS ##
######################
my $csth = $dbh->prepare("SELECT c.id, c.Name,c.Gender,c.WholeGenome,c.CN,c.dc,c.scn,c.scnd,c.sdc,c.snr,c.sbigger,c.ssmaller,c.soinner,c.soouter,uc.Type,uc.TrackOnly, uc.IncludeParents,uc.IncludeControls,uc.MinSNP FROM `Classifier` c JOIN `Users_x_Classifiers` uc ON c.id = uc.cid WHERE uc.uid = '$uid' AND uc.Type IN (1,3)");
my %classifiers = ();
$csth->execute();
while (my $row = $csth->fetchrow_hashref()) {
	## skip classifier not matching the sample gender
	if ($row->{'Gender'} !~ m/$CurrGender/) {
		next;
	}
	if ($row->{'WholeGenome'} == 1) {
		if (!defined($classifiers{'wg'})) {
			$classifiers{'wg'} = ();
		}
		#convert some to arrays.
		my %gh = map {$_ => 1} split(/,/,$row->{'Gender'});
		$row->{'Gender'} = \%gh;
		my %sh = map {$_ => 1 } split(/,/,$row->{'CN'});
		$row->{'CN'} = \%sh;
		push(@{$classifiers{'wg'}} ,$row);
	}
	else {
		# get regions.
		$cid = $row->{'id'};
		$sqsth = $dbh->prepare("SELECT chr, start, stop FROM `Classifier_x_Regions` WHERE cid = $cid");
		$sqsth->execute();
		my @regions = ();
		while (my $sr = $sqsth->fetchrow_hashref()) {
			push(@regions, $sr);
		}
		if (!defined($classifiers{'local'})) {
			$classifiers{'local'} = ();
		}
		$sqsth->finish();
		#convert some to arrays.
		my %gh = map {$_ => 1} split(/,/,$row->{'Gender'});
		$row->{'Gender'} =  \%gh ;
		my %sh = map {$_ => 1 } split(/,/,$row->{'CN'});
		$row->{'CN'} = \%sh;
		$row->{'regions'} = \@regions;
		#$classifiers['local'][] = array('id'=>$row['id'],'Name'=>$row['Name'],'Gender'=>$row['Gender'],'CN'=>$row['CN'],'dc'=>$row['dc'],'scn'=>$row['scn'],'scnd'=>$row['scnd'],'sdc'=>$row['sdc'],'snr'=>$row['snr'],'sbigger'=>$row['sbigger'],'ssmaller'=>$row['ssmaller'],'soinner'=>$row['soinner'],'soouter'=>$row['soouter'],'TrackOnly' => $row['TrackOnly'],'IncludeParents'=>$row['IncludeParents'],'IncludeControls'=>$row['IncludeControls'],'MinSNP'=>$row['MinSNP'],'regions'=>$regions);
		push(@{$classifiers{'local'}},$row);
	
	}
}
$csth->finish();
############################
## READ IN METHOD RESULTS ##
############################
my %allresults;
my %alltable;
my %allxml;
for my $method (keys(%algos)) {
	$allresults{$method}= ();
	$allxml{$method} = "\n";
	$alltable{$method} = '';
	#my $firstchar = substr($method,0,1);
	my $firstchar = $algos{$method}{'fc'};
	# get configuration hash
	my $configuration = $settings->{$method}->{configuration};
	# get cnvfile 
	#my $cnvfile = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.$sid.$method.cnv";
	my $cnvfile = "$datadir/$pid.$sid.$method.cnv";
	my $format = $configuration->{output}->{cnv_format};
	my $nrheader = $configuration->{output}->{cnv_header};
	my $minconf = $configuration->{general}->{min_conf};
	# get column structure
	my %columns;
	if ($format eq 'tabular') {
		my $nrcol = $configuration->{output}->{cnv_cols};
		for ($i = 1; $i<=$nrcol; $i++) {
			next if (!exists($configuration->{output}->{"cnv_$i"}));
			$columns{$configuration->{output}->{"cnv_$i"}} = ($i - 1) ;	
		}

	
		# open cnv_file
		open IN, $cnvfile;
		# skip header lines
		for ($i = 1; $i<=$nrheader;$i++) {
			$skip = <IN>;
		}
		# scan & store items
		while (<IN>) {
			my $currline = $_;
			chomp($currline);
			my @line = split(/\t/, $currline);
			## check for correct number of columns (issue mainly with vanillaICE)
			if (scalar(@line) < $nrcol) {
				# check next line
				my $next = <IN>;
				my $linelength = length($next);
				chomp($next);
				my @p = split(/\t/,$next);
				if (scalar(@p) + scalar(@line) == $nrcol) {
					# add both lines
					push(@line,@p);
				}
				else {
					# print message. put current line back on filehandle
					print "ERROR: Following line for method '$method' was discarded due to incorrect number of columns\n";
					print "$currline\n";
					seek(IN, -$linelength, 1); # rewind filehandle to reprocess '$next'
					next;
				}
			}
			my $CurrChr = $line[$columns{'chromosome'}];
			$CurrChr =~ s/^\s+//;
			if ($CurrChr !~ m/\d+/) {
				$CurrChr = $chromhash{ $CurrChr };
			}
			my $CurrStart = $line[$columns{'startposition'}];
			$CurrStart =~ s/^\s+//;
			my $CurrEnd = $line[$columns{'stopposition'}];
			$CurrEnd =~ s/^\s+//;
			my $CurrCN = $line[$columns{'cn'}];
			$CurrCN =~ s/^\s+//;
			my $CurrSNP = $line[$columns{'probes'}];
			$CurrSNP =~s/^\s+//;
			my $CurrScore = $line[$columns{'confidence'}];
			$CurrScore =~ s/^\s+//;
			my $CurrSize = $CurrEnd - $CurrStart + 1;
			if ($CurrStart eq '' || $CurrEnd eq '') {
				print "Method '$method' : Reading results file failed on chr $CurrChr\n";
				print "$currline\n";
				next;
			}
			## probenames : from hash
			my $startprobe = $ProbeByPos{$CurrChr}{$CurrStart};
			my $endprobe = $ProbeByPos{$CurrChr}{$CurrEnd};
			#my $pquery = "SELECT name FROM probelocations WHERE chromosome = '$CurrChr' AND chiptype = '$chiptypeid' AND position IN ($CurrStart, $CurrEnd) ORDER BY position ASC";
			#my $psth = $dbh->prepare($pquery);
			#$psth->execute();
			#my ($startprobe) = $psth->fetchrow_array();
			#my ($endprobe) = $psth->fetchrow_array();
			#$psth->finish();

			if (($CurrSNP >= $minsnp) && ($CurrScore >=$minconf)) { #skip all too short/unreliable calls 
				push(@{$allresults{$method}}, [ $CurrChr, $CurrStart, $CurrEnd, $CurrCN, $CurrScore, $CurrSNP, "$firstchar" ]);
				$allxml{$method} .= "<bookmark>\n<sample_id>$CurrSample \[$CurrIndex\]</sample_id>\n<bookmark_type>$types[$CurrCN]</bookmark_type>\n<entry_date></entry_date>\n<chr_num>$CurrChr</chr_num>\n<base_start_pos>$CurrStart</base_start_pos>\n<base_end_pos>$CurrEnd</base_end_pos>\n<author />\n<value>$CurrCN</value>\n<comment><![CDATA[ Confidence: $CurrScore, N.SNP: $CurrSNP]]></comment>\n</bookmark>\n";
				$alltable{$method} .= "$CurrSample\t$CurrChr\t$CurrStart\t$CurrEnd\t$CurrSize\t$startprobe\t$endprobe\t$CurrCN\t$CurrSNP\t$CurrScore\n";
			}
		}
		close IN;
	}
	elsif($format eq 'regex') {
		my $regex = $configuration->{output}->{cnv_regex};
		# replace placeholders
		while ($regex =~ m/%(.*?)%/) {
			$item = $1;
			$regex =~ s/^(.*?)%($item)%(.*)/$1$replace{$item}$3/;
		}
		my $nrhits = $configuration->{output}->{cnv_regex_hits};
		for ($i = 1; $i<=$nrhits; $i++) {
			next if (!exists($configuration->{output}->{"cnv_$i"}));
			$columns{$configuration->{output}->{"cnv_$i"}} = ($i - 1) ;
		}
		open IN, $cnvfile;
		# skip header lines
		for ($i = 1; $i<=$nrheader;$i++) {
			$skip = <IN>;
		}
		while (<IN>) {
			my $line = $_;
			chomp($line);
			my @matches =  $line =~ m/$regex/i;
			my $CurrChr = $matches[$columns{'chromosome'}];
			$CurrChr =~ s/^\s+//;
			if ($CurrChr !~ m/\d+/) {
				$CurrChr = $chromhash{ $CurrChr };
			}
			$startcol = $columns{'startposition'};
			my $CurrStart = $matches[$columns{'startposition'}];
			$CurrStart =~ s/^\s+//;
			my $CurrEnd = $matches[$columns{'stopposition'}];
			$CurrEnd =~ s/^\s+//;
			my $CurrCN = $matches[$columns{'cn'}];
			$CurrCN =~ s/^\s+//;
			my $CurrSNP = $matches[$columns{'probes'}];
			$CurrSNP =~s/^\s+//;
			my $CurrScore = $matches[$columns{'confidence'}];
			$CurrScore =~ s/^\s+//;
			my $CurrSize = $CurrEnd - $CurrStart + 1;
			my $startprobe = $ProbeByPos{$CurrChr}{$CurrStart};
			my $endprobe = $ProbeByPos{$CurrChr}{$CurrEnd};
			#my $pquery = "SELECT name FROM probelocations WHERE chromosome = '$CurrChr' AND chiptype = '$chiptypeid' AND position IN ($CurrStart, $CurrEnd) ORDER BY position ASC";
			#my $psth = $dbh->prepare($pquery);
			#$psth->execute();
			#my ($startprobe) = $psth->fetchrow_array();
			#my ($endprobe) = $psth->fetchrow_array();
			#$psth->finish();
			if (($CurrSNP >= $minsnp) && ($CurrScore >=$minconf)) { #skip all too short/unreliable calls
				push(@{$allresults{$method}}, [ $CurrChr, $CurrStart, $CurrEnd, $CurrCN, $CurrScore, $CurrSNP, "$firstchar" ]);
				$allxml{$method} .= "<bookmark>\n<sample_id>$CurrSample \[$CurrIndex\]</sample_id>\n<bookmark_type>$types[$CurrCN]</bookmark_type>\n<entry_date></entry_date>\n<chr_num>$CurrChr</chr_num>\n<base_start_pos>$CurrStart</base_start_pos>\n<base_end_pos>$CurrEnd</base_end_pos>\n<author />\n<value>$CurrCN</value>\n<comment><![CDATA[ Confidence: $CurrScore, N.SNP: $CurrSNP]]></comment>\n</bookmark>\n";
				$alltable{$method} .= "$CurrSample\t$CurrChr\t$CurrStart\t$CurrEnd\t$CurrSize\t$startprobe\t$endprobe\t$CurrCN\t$CurrSNP\t$CurrScore\n";
			}
		}
		close IN;
	}
	my $retnr = scalar(@{$allresults{$method}});
	if ($retnr eq '') {
		$retnr = 0;
	}
	print "$method : $retnr regions retained\n";
	# sort results
	@{$allresults{$method}} = sort {$a->[0] <=> $b->[0] || $a->[1] <=> $b->[1] } @{$allresults{$method}};
}
# CREATE TABLE
$random = int(rand(100000));
$tablename = "Format$random";
$tabledet = "Formatdet$random";
$tabletmp = "Formattmp$random";
$randfile = "/tmp/$random.table";
$tsth = $local->prepare("SHOW TABLES WHERE `Tables_in_CNVanalysis-TMP` = '$tablename' OR `Tables_in_CNVanalysis-TMP` = '$tabledet' OR `Tables_in_CNVanalysis-TMP` = '$tabletmp'");
$tsth->execute();
$nrrows = $tsth->rows();
$tsth->finish();
while ($nrrows > 0) {
	#new random number
	$random = int(rand(100000));
	$tablename = "Format$random";
	$tabledet = "Formatdet$random";
	$tabletmp = "Formattmp$random";
	$randfile = "/tmp/$random.table";	

	#check for tables
	$tsth = $local->prepare("SHOW TABLES WHERE `Tables_in_CNVanalysis-TMP` = '$tablename' OR `Tables_in_CNVanalysis-TMP` = '$tabledet' OR `Tables_in_CNVanalysis-TMP` = '$tabletmp'");
	$tsth->execute();
	$nrrows = $tsth->rows();
	$tsth->finish();
}	
print "using tables: $tablename and $tabledet\n";
$query = "CREATE TABLE $tablename (ID INT NOT NULL PRIMARY KEY,entry TINYTEXT)";
$sth = $local->prepare($query);
$sth->execute();
$sth->finish();
$query = "ALTER TABLE $tablename DELAY_KEY_WRITE = 1";
$sth = $local->prepare($query);
$sth->execute();
$sth->finish();

$query = "CREATE TABLE $tabledet (ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, cn INT, start BIGINT, stop BIGINT, score VARCHAR(30), algo VARCHAR(1))";
$sth = $local->prepare($query) ;
$sth->execute();
$sth->finish();
$query = "CREATE TABLE $tabletmp (IDtmp INT NOT NULL PRIMARY KEY,entrytmp TINYTEXT)";
$sth = $local->prepare($query);
$sth->execute();
$sth->finish();
$query = "ALTER TABLE $tabletmp DELAY_KEY_WRITE = 1";
$sth = $local->prepare($query);
$sth->execute();
$sth->finish();


######################################
## STORE EVERYTHING IN THE DATABASE ##
######################################
my $tresh = 7500;
my @options = ('del', 'dup');
foreach(@nrs) {   # loop chromosomes
  my $currchr = $_;
  my $nrChr = $chromhash{ $currchr };
  my $cytoq = "SELECT stop FROM cytoBand WHERE chr = '$nrChr' AND name LIKE '". '%p%' . "' ORDER BY stop DESC LIMIT 1";
  my $csth = $dbh->prepare($cytoq);
  $csth->execute();
  my @crow = $csth->fetchrow_array();
  my $lastp = $crow[0];
  foreach (@options) {  # loop del/dup
	my $pdone = 0;
	my $typ = $_;
	my $query = "TRUNCATE $tablename";
	$local->do($query);
	$query = "TRUNCATE $tabledet";
	$local->do($query);
	my %cnvhash = (); 
	# STORE
	for my $method (keys(%algos)) {	# loop used algorithms
		#open OUT, ">$randfile" ;
		my @regions = @{$allresults{$method}};
		
		for my $i (0..$#regions) {
			my $array = \@{$regions[$i]};
				
			if ($array->[0] != $nrChr) {
				next;
			}
			if ($typ eq 'del' && $array->[3] >= 2) {
				next;
			}
			if ($nrChr == 23 ) {
				# in females, dup is cn >= 3
				if ($CurrGender eq 'Female' && $typ eq 'dup' && $array->[3] < 3) {
					next;
				}
				# in males, dup is cn >= 2
				elsif ($CurrGender eq 'Male' && $typ eq 'dup' && $array->[3] < 2) {
					next;
				}
				# in females del is cn < 2
				#if ($CurrGender eq 'Female' && $typ eq 'del' && $array->[3] >= 2) {
				#	next;
				#}
				# in males, del < 1
				#elsif ($CurrGender eq 'Male' && $typ eq 'del' && $array->[3] >= 1) {
				#	next;
				#}


			}
			elsif ($nrChr == 24) {
				if ($doY != 1) {
					next;
				}
				# skip Y in females
				if ($CurrGender eq 'Female') {
					next;
				}
				## males: dup is cn > 2 (should be normalized on males only, so CN=2 is normal, although it represent just one copy !!
				elsif ($typ eq 'dup' && $array->[3] <= 2) {
					next;
				}
			}
			else {
				if ($typ eq 'dup' && $array->[3] < 3) {
					next;
				} 
			}
			my $startpos = $array->[1];
			my $stoppos = $array->[2];
			my $algochar = $array->[6];
			## asymmetric voting
			if ($asym == 1 && $method eq "$asymmethod" && ("$typ" eq "$asymtype" || "$asymtype" eq "any") && $array->[4] > $asymminscore) {
				$algochar = $algochar.$algochar;
			}
			## processing Y hack => now possible for VICE only 
			if ($doY == 1 && $method eq "$Ymethod" && $nrChr == 24) {
				$algochar = $algochar.$algochar;
			}
			# find start and stop index of positions in the ProbePos array.
			my $startidx = bin_search(\@{$ProbePos[$nrChr]},$startpos);
			my $stopidx = bin_search(\@{$ProbePos[$nrChr]},$stoppos);

			#$snpquery = "SELECT position FROM probelocations WHERE (position BETWEEN $startpos AND $stoppos) AND chromosome = '$nrChr' AND chiptype = '$chiptypeid'";
			#my $ary_ref = $dbh->selectcol_arrayref($snpquery);  
			my $cn = $array->[3];
			for (my $idx = $startidx; $idx <= $stopidx; $idx++) {
				#store in hash in stead of file=>db
				$cnvhash{$ProbePos[$nrChr]->[$idx]} = $cnvhash{$ProbePos[$nrChr]->[$idx]} . $algochar;
				#print OUT "-".$ProbePos[$nrChr]->[$idx]."-,-$algochar-\n";
			}
			#foreach (@$ary_ref) {
			#	print OUT "-$_-,-$algochar-\n";
			#}
			# unset asym/doY here
			$algochar = substr($algochar,0,1);
			$query = "INSERT INTO $tabledet (start, stop, cn, score, algo) VALUES ('$startpos', '$stoppos', '$cn', '".$array->[4]."', '$algochar') ";
			$local->do($query);
		}
		

	}	
	if (keys(%cnvhash) == 0) {
		next;
	}
	
	# get idx of first and last snp in cnv
	my @cnvpos = keys(%cnvhash);
	@cnvpos = sort { $a <=> $b } @cnvpos;
	my $firstsnp = $cnvpos[0];
	my $lastsnp = $cnvpos[-1];
	my $startidx = bin_search(\@{$ProbePos[$nrChr]},$firstsnp);
	my $stopidx = bin_search(\@{$ProbePos[$nrChr]},$lastsnp);
	my $found = 0;
	my $fstart = 0;
	my $fstop = 0;
	my %sb;   # seen by hash
	my $prevsnpcn;

	#construct details array
	my @details; 
	my $squery = "SELECT cn, start, stop FROM $tabledet ";
	$ssth = $local->prepare($squery);
	$ssth->execute();
	while (my @srow = $ssth->fetchrow_array() ) {
		my $cnvstart = $srow[1];
		my $cnvstop = $srow[2];
		my $cnvcn = $srow[0];
		push(@details, [$cnvstart, $cnvstop, $cnvcn ] );
	}
	$ssth->finish();
	for (my $idx = $startidx ; $idx <= $stopidx; $idx++) {
		my $csnppos = $ProbePos[$nrChr]->[$idx];
		if (length($cnvhash{$csnppos}) >= $minmethods) {
			# valid position, calculate mean CN on this snp.
			my $csumcn = 0;
			my $ccnidx = 0;
			foreach (@details) {
				if ($$_[0] <= $csnppos && $$_[1] >= $csnppos) {
					$csumcn = $csumcn + $$_[2];
					$ccnidx++;
				}
			}
			if ($ccnidx == 0) {
				print "Note: Unable to calculate average copynumber at pos $csnppos. Setting to 1\n";
				$snpcn = 1;
			}
			else {
				$snpcn = int(($csumcn/$ccnidx)+0.5);
			}
			# if found is 0, previous snp was not included in CNV
			if ($found == 0) {
				$fstart = $csnppos;
				$fstop = $csnppos;
				$found = 1;
				$prevsnpcn = $snpcn;	
				for (my $j = 0; $j<length($cnvhash{$csnppos});$j++) {
					$sb{substr($cnvhash{$csnppos},$j,1)} = 1;
				}
			}
			# if extension of previous CNV, check for same CN, if not, store
			elsif ($prevsnpcn != $snpcn) {
				my @topush = ( $currchr, $fstart, $fstop, $prevsnpcn);
				my $string = "";
				foreach $k (keys (%sb)) {
					my $squery = "SELECT score FROM $tabledet WHERE algo = '$k' AND ( ('$fstart' BETWEEN start AND stop) OR ('$fstop' BETWEEN start AND stop) OR (start >= '$fstart' AND stop <= '$fstop'))";
					$ssth = $local->prepare($squery);
					$ssth->execute();
					my @srow = $ssth->fetchrow_array();
					$ssth->finish();
					my $score = $srow[0];
					if ($score eq '') {
						next;
					}
					push(@topush, ($k,$score));
					$string .= "$k;$score - "; 
				}
				push(@results, [ @topush ]);
				print  "found $currchr:$fstart-$fstop : cn: $prevsnpcn : $string\n";
				$found = 1;
				$fstart = $csnppos;
				$fstop = $csnppos;
				$prevsnpcn = $snpcn;
			}
			# if the same cn but continues from p to q => split it up.
			elsif ($pdone == 0 && $csnppos > $lastp) {
				my @topush = ( $currchr, $fstart, $fstop, $prevsnpcn);
				my $string = "";
				foreach $k (keys (%sb)) {
					my $squery = "SELECT score FROM $tabledet WHERE algo = '$k' AND ( ('$fstart' BETWEEN start AND stop) OR ('$fstop' BETWEEN start AND stop) OR (start >= '$fstart' AND stop <= '$fstop'))";
					$ssth = $local->prepare($squery);
					$ssth->execute();
					my @srow = $ssth->fetchrow_array();
					$ssth->finish();
					my $score = $srow[0];
					if ($score eq '') {
						next;
					}
					push(@topush, ($k,$score));
					$string .= "$firstchars{$k};$score - "; 
				}
				push(@results, [ @topush ]);
				print  "found $currchr:$fstart-$fstop : cn: $prevsnpcn : $string\n";
				$found = 1;
				$fstart = $csnppos;
				$fstop = $csnppos;
				$prevsnpcn = $snpcn;
				$pdone = 1;
			}
			# same cn, same chromosome arm, just extend current region
			else {
				$fstop = $csnppos;
				for (my $j = 0; $j<length($cnvhash{$csnppos});$j++) {
					$sb{substr($cnvhash{$csnppos},$j,1)} = 1;
				}
			}
		}
		else {
			if ($found == 1) {		
				my @topush = ( $currchr, $fstart, $fstop, $prevsnpcn);
				my $string = "";
				foreach $k (keys (%sb)) {
					my $squery = "SELECT score FROM $tabledet WHERE algo = '$k' AND ( ('$fstart' BETWEEN start AND stop) OR ('$fstop' BETWEEN start AND stop) OR (start >= '$fstart' AND stop <= '$fstop'))";
					$ssth = $local->prepare($squery);
					$ssth->execute();
					my @srow = $ssth->fetchrow_array();
					$ssth->finish();
					my $score = $srow[0];
					if ($score eq '') {
						next;
					}
					push(@topush, ($k,$score));
					$string .= "$firstchars{$k};$score - "; 
		  
				}
				push(@results, [ @topush ]);
				$found = 0;
				print  "found $currchr:$fstart-$fstop : cn: $prevsnpcn : $string\n";
			}
			
		}
		if ($pdone == 0 && $csnppos > $lastp) {
			$pdone = 1;
		}

	}	
	if ($found == 1) {
		my @topush = ( $currchr, $fstart, $fstop, $prevsnpcn);
		$string = "";
		foreach $k (keys (%sb)) {
			my $squery = "SELECT score, cn FROM $tabledet WHERE algo = '$k' AND ( ('$fstart' BETWEEN start AND stop) OR ('$fstop' BETWEEN start AND stop) OR (start >= '$fstart' AND stop <= '$fstop'))";
			$ssth = $local->prepare($squery);
			$ssth->execute();
			my @srow = $ssth->fetchrow_array();
			$ssth->finish();
			my $score = $srow[0];
			if ($score eq '') {
				next;
			}
			push(@topush, ($k,$score));
			$string .= "$firstchars{$k};$score - ";
		}
		push(@results, [ @topush ]);
		print  "found $currchr:$fstart-$fstop : cn: $prevsnpcn : $string\n";
	}
   }	
}
#######################################
# STORE RETAINED RESULTS IN DATABASE ##
#######################################
my $nrabs = scalar(@results);
my $multistring = '';
my $tablestring = '';
for my $ri (0..$#results) {
	my $CurrChr = $results[$ri][0];
	my $CurrStart = $results[$ri][1];
	my $CurrEnd = $results[$ri][2];
	my $CurrCN = $results[$ri][3];
	my $CurrSize = $CurrEnd - $CurrStart +1;
	# Check nr of probes
	my $startidx = bin_search(\@{$ProbePos[$chromhash{$CurrChr}]},$CurrStart);
	my $stopidx = bin_search(\@{$ProbePos[$chromhash{$CurrChr}]},$CurrEnd);
	my $nrsnps = $stopidx - $startidx + 1;
	if ($nrsnps < $minsnp) {
		$nrabs--;
		next;
	} 
	# check nr of affected genes
	my $genequery = "SELECT Count(DISTINCT symbol) FROM genes WHERE chr='$chromhash{ $CurrChr }' AND (((start BETWEEN $CurrStart AND $CurrEnd) OR (end BETWEEN $CurrStart AND $CurrEnd)) OR (start <= $CurrStart AND end >= $CurrEnd))";
	$genesth = $dbh->prepare($genequery);
	$genesth->execute();
	my @countgenes = $genesth->fetchrow_array();
	$genesth->finish(); 
	my $nrgenes = $countgenes[0];
	# Check Maximal genomic region
	# FIND next 5' probe
	if ($startidx > 0) {
		$largestart = $ProbePos[$chromhash{$CurrChr}]->[($startidx - 1)];
	}
	else {
		$largestart = 'ter';
	}
	# FIND next 3' probe
	if ($stopidx < scalar(@{$ProbePos[$chromhash{$CurrChr}]}) - 1) {
		$largestop = $ProbePos[$chromhash{$CurrChr}]->[($stopidx + 1)];
	}
	else {
		$largestop = 'ter';
	}
	################
	## CLASSIFIER ##
	################
	$suggestedclass = 99;
	$appliedrule = '';
	$appliedid = '';
	if (defined($classifiers{'wg'})) {
		foreach my $details (@{$classifiers{'wg'}}) {
			# check cn needed in classifier
			next if (!defined($details->{'CN'}->{$CurrCN}));
			# check minSNP
			next if ($details->{'MinSNP'} > $nrsnps);
			# compose reference regions query
			my $where;
			if ($details->{'TrackOnly'} == 1) {
				$where .= " AND s.intrack = 1 AND a.idproj = s.trackfromproject";
			}
			if ($details->{'IncludeParents'} == 0) {
				$where .= " AND p.collection <> 'Parents'";
			}
			if ($details->{'IncludeControls'} == 0) {
				$where .= " AND p.collection <> 'Controls'";
			}
			my $rsth =  $dbh->prepare("SELECT chr,start,stop,cn,class FROM `aberration` a JOIN `project` p JOIN `projectpermission` pp JOIN `sample` s ON a.idproj = p.id AND a.sample = s.id AND a.idproj = pp.projectid WHERE pp.userid = '$uid' AND a.sample != '$sid' AND chr = '".$chromhash{ $CurrChr }."' AND ( (start BETWEEN $CurrStart AND $CurrEnd) OR (stop BETWEEN $CurrStart AND $CurrEnd) OR (start <= $CurrStart AND stop >= $CurrEnd)) $where");
			$rsth->execute();
			my $matches = 0;
			while (my $rrow = $rsth->fetchrow_hashref()) {
				## same cn needed ?
				if ($details->{'scn'} == 1) {
					if ($details->{'scnd'} eq 'exact' && $rrow->{'cn'} != $CurrCN) {
						next;
					}
					elsif ( $details->{'scnd'} eq 'type') {
						next if ($rrow->{'cn'} >= 2 && $CurrCN < 2) ;
						next if ($rrow->{'cn'} <= 2 && $CurrCN > 2) ;
						next if ($rrow->{'cn'} == 2 && $CurrCN != 2) ;
					}
				}
				## same dc needed ?
				if ($rrow->{'class'} == 2) {
					$rrow->{'class'} = 1;
				}
				next if ($details->{'sdc'} == 1 && $rrow->{'class'} != $details->{'dc'}) ;
				## size constraints
				## bigger ?
				next if ($rrow->{'start'} < $CurrStart && $rrow->{'stop'} > $CurrEnd && $details->{'sbigger'} != -1 && $details->{'sbigger'} < ($CurrEnd-$CurrStart+1)/ ($rrow->{'stop'} - $rrow->{'start'}+1));
				## smaller
				next if ($rrow->{'start'} > $CurrStart && $rrow->{'stop'} < $CurrEnd && $details->{'ssmaller'} >  ($CurrEnd-$CurrStart+1)/ ($rrow->{'stop'} - $rrow->{'start'}+1));
				## overlap/extend
				$ovstart = ($CurrStart,$rrow->{'start'})[$CurrStart < $rrow->{'start'} ];
				$ovend = ($CurrEnd,$rrow->{'stop'})[$CurrEnd > $rrow->{'stop'}];	
				# ratio inner
				next if ($details->{'soinner'} > ($ovend-$ovstart+1)/($rrow->{'stop'} - $rrow->{'start'} + 1)) ;
				# size extending 5'.
				next if ($CurrStart < $rrow->{'start'} && $CurrEnd <= $rrow->{'stop'} && ($rrow->{'start'} - $CurrStart + 1) > $details->{'soouter'});
				# size extending 3'.
				next if ($rrow->{'start'} <= $CurrStart && $rrow->{'stop'} < $CurrEnd && ($CurrEnd - $rrow->{'stop'} + 1) > $details->{'soouter'});
				$matches++;
				if ($matches >= $details->{'snr'}) {
					last;
				}

			}
			$rsth->finish();
			if ($matches >= $details->{'snr'}) {
				# ok. suggest if lower class.
				if ($details->{'dc'} < $suggestedclass) {
					$suggestedclass = $details->{'dc'};
					$appliedrule = $details->{'Name'};
					$appliedid = $details->{'id'};
					#print "\t => Applied Rule '$appliedrule' (class: $suggestedclass)!\n";
				}
			}
	

		}

	}
	if (defined($classifiers{'local'})) {
		foreach my $details (@{$classifiers{'local'}}) {
			
			# check cn needed in classifier
			next if (!defined($details->{'CN'}->{$CurrCN}));
			# check minSNP
			next if ($details->{'MinSNP'} > $nrsnps);
			# matching regions in classifier ?
			my $ok = 0;
			foreach my $region (@{$details->{'regions'}}) {
				# correct chr? && overlap ?
				next if ($chromhash{ $CurrChr } != $region->{'chr'} || $region->{'stop'} < $CurrStart || $region->{'start'} > $CurrEnd) ;
				## passed one region => ok.
				$ok = 1;
				last;
			}
			next if ($ok == 0);
			# CASE 1 : minimal number of matches == 0 ==> consider the classifier region as only reference CNV, only look at sizes
			if ($details->{'snr'} == 0) {
				$match = 0;
				foreach my $region (@{$details->{'regions'}}) {
					$ovstart = ($CurrStart,$region->{'start'})[$CurrStart < $region->{'start'}];
					$ovend = ($CurrEnd,$region->{'stop'})[$CurrEnd > $region->{'stop'}];
					#print "ovstart : $ovstart ; $ovend : $ovend\n";
					# correct chr? && overlap ?
					if ($chromhash{ $CurrChr } != $region->{'chr'} || $region->{'stop'} < $CurrStart || $region->{'start'} > $CurrEnd) {
						next; #continue;
					}
					# size constraints.
					# region < cnv
					if ($region->{'start'} > $CurrStart && $region->{'stop'} < $CurrEnd && $details->{'sbigger'} != -1 && $details->{'sbigger'} < ($CurrEnd-$CurrStart+1)/ ($region->{'stop'} - $region->{'start'}+1)) {
						next;#continue;
					}
					# region > cnv
					if ($region->{'start'} < $CurrStart && $region->{'stop'} > $CurrEnd && $details->{'ssmaller'} >  (($CurrEnd-$CurrStart+1)/ ($region->{'stop'} - $region->{'start'} +1))) {
						next;#continue;
					}
					# overlap/extend.
					# required ratio inner
					if ($details->{'soinner'} > ($ovend-$ovstart+1)/($region->{'stop'} - $region->{'start'} + 1)) {
						next; #continue;
					}
					# allowed size extending 5'.
					if ($CurrStart < $region->{'start'} && $CurrEnd <= $region->{'stop'} && ($region->{'start'} - $CurrStart + 1) > $details->{'soouter'}) {
						next; #continue;
					}
					# allowed size extending 3'.
					if ($region->{'start'} <= $CurrStart && $region->{'stop'} < $CurrEnd && ($CurrEnd - $region->{'stop'} + 1) > $details->{'soouter'}) {
						next; #continue;
					}
					# here : ok
					$match = 1;

				}
				if ($match == 1) {
					# ok. suggest if lower class.
					if ($details->{'dc'} < $suggestedclass) {
						$suggestedclass = $details->{'dc'};
						$appliedrule = $details->{'Name'};
						$appliedid = $details->{'id'};
					
					}
				}

			}
			else {
				# compose reference regions query
				my $where;
				if ($details->{'TrackOnly'} == 1) {
					$where .= " AND s.intrack = 1 AND a.idproj = s.trackfromproject";
				}
				if ($details->{'IncludeParents'} == 0) {
					$where .= " AND p.collection <> 'Parents'";
				}
				if ($details->{'IncludeControls'} == 0) {
					$where .= " AND p.collection <> 'Controls'";
				}
				my $rsth =  $dbh->prepare("SELECT chr,start,stop,cn,class FROM `aberration` a JOIN `project` p JOIN `projectpermission` pp JOIN `sample` s ON a.idproj = p.id AND a.sample = s.id AND a.idproj = pp.projectid WHERE pp.userid = '$uid' AND a.sample != '$sid' AND chr = '".$chromhash{ $CurrChr }."' AND ( (start BETWEEN $CurrStart AND $CurrEnd) OR (stop BETWEEN $CurrStart AND $CurrEnd) OR (start <= $CurrStart AND stop >= $CurrEnd)) $where");
				$rsth->execute();
				my $matches = 0;

				while (my $rrow = $rsth->fetchrow_hashref()) {
					## same cn needed ?
					if ($details->{'scn'} == 1) {
						if ($details->{'scnd'} eq 'exact' && $rrow->{'cn'} != $CurrCN) {
							next;
						}
						elsif ( $details->{'scnd'} eq 'type') {
							next if ($rrow->{'cn'} >= 2 && $CurrCN < 2) ;
							next if ($rrow->{'cn'} <= 2 && $CurrCN > 2) ;
							next if ($rrow->{'cn'} == 2 && $CurrCN != 2) ;
						}
					}
					## same dc needed ?
					if ($rrow->{'class'} == 2) {
						$rrow->{'class'} = 1;
					}
					next if ($details->{'sdc'} == 1 && $rrow->{'class'} != $details->{'dc'}) ;
					## size constraints
					## bigger ?
					next if ($rrow->{'start'} < $CurrStart && $rrow->{'stop'} > $CurrEnd && $details->{'sbigger'} != -1 && $details->{'sbigger'} < ($CurrEnd-$CurrStart+1)/ ($rrow->{'stop'} - $rrow->{'start'}+1));
					## smaller
					next if ($rrow->{'start'} > $CurrStart && $rrow->{'stop'} < $CurrEnd && $details->{'ssmaller'} >  ($CurrEnd-$CurrStart+1)/ ($rrow->{'stop'} - $rrow->{'start'}+1));
					## overlap/extend
					$ovstart = ($CurrStart,$rrow->{'start'})[$CurrStart < $rrow->{'start'} ];
					$ovend = ($CurrEnd,$rrow->{'stop'})[$CurrEnd > $rrow->{'stop'}];	
					# ratio inner
					next if ($details->{'soinner'} > ($ovend-$ovstart+1)/($rrow->{'stop'} - $rrow->{'start'} + 1)) ;
					# size extending 5'.
					next if ($CurrStart < $rrow->{'start'} && $CurrEnd <= $rrow->{'stop'} && ($rrow->{'start'} - $CurrStart + 1) > $details->{'soouter'});
					# size extending 3'.
					next if ($rrow->{'start'} <= $CurrStart && $rrow->{'stop'} < $CurrEnd && ($CurrEnd - $rrow->{'stop'} + 1) > $details->{'soouter'});
					$matches++;
					if ($matches >= $details->{'snr'}) {
						last;
					}

				}
				$rsth->finish();
				if ($matches >= $details->{'snr'}) {
					# ok. suggest if lower class.
					if ($details->{'dc'} < $suggestedclass) {
						$suggestedclass = $details->{'dc'};
						$appliedrule = $details->{'Name'};
						$appliedid = $details->{'id'};
						
					}
				}
			}
		}
	}
	# Construct output and update tables
	$tablestring .= "$CurrSample\t$CurrChr\t$CurrStart\t$CurrEnd\t$CurrSize\t$CurrCN\t$nrsnps\t$nrgenes";
	my $string = "";
	my $seenby = "";
	my $sublength = $#{$results[$ri]};
	for my $idx (4..$sublength) {
		#$tablestring .= "\t$results[$ri][$idx]";
		if ($results[$ri][$idx] =~ /^(\d+\.?\d*|\.\d+)$/ ) { # is numeric?
			$string .= "(".sprintf("%.3f",$results[$ri][$idx]).")";
			$seenby .= "(".sprintf("%.3f",$results[$ri][$idx]).")";
			$tablestring  .= "\t".sprintf("%.3f",$results[$ri][$idx]);
		}
		else {
			if ($results[$ri][$idx] =~ m/NaN/i ) {
				$string .= "(NaN)";
				$seenby .= "(NaN)";
				$tablestring  .= "\t(NaN)";
			}
			else {
				$string .= " \n$firstchars{ $results[$ri][$idx] } ";
				$seenby .= " - $firstchars{ $results[$ri][$idx] } ";
				$tablestring .= "\t$firstchars{ $results[$ri][$idx] }";
			}
		}

	}
	if ($suggestedclass < 99) {
		$tablestring .= $dc{$suggestedclass}."\t$appliedrule"; 
	}
	else {
		$tablestring .= "\t";
	}
	$tablestring .=  "\n";
	$multistring .= "<bookmark>\n<sample_id>$CurrSample \[$CurrIndex\]</sample_id>\n<bookmark_type>$types[$CurrCN]</bookmark_type>\n<entry_date></entry_date>\n<chr_num>$CurrChr</chr_num>\n<base_start_pos>$CurrStart</base_start_pos>\n<base_end_pos>$CurrEnd</base_end_pos>\n<author />\n<value>$CurrCN</value>\n<comment><![CDATA[ NrSNP's: $nrsnps\nSeen by (conf):$string";
	if ($suggestedclass < 99) {
		$multistring .= "\nSuggested Class: ".$dc{$dc{$suggestedclass}}."\nApplied Rule: $appliedrule";
	}
	$multistring .= "]]></comment>\n</bookmark>\n";
	# EXTRACT PROBE LEVEL DATA 
	my $from  = $CurrStart - 250000;
	my $to = $CurrEnd + 250000;
	open DATA, "$datadir/$pid.$sid.txt";
	my $header = <DATA>;
	# datafiles follows general input-column format (as extracted in main project, not true for family analysis/UPD !).
	# supported : logR, Baf, GType
	my $store;
	my $sum = 0;
	my $nrp = 0;
	if ($ChrCol == -1 || $PosCol == -1 ){
		print "Datafile is missing chr/position information for sample $CurrSample\n";
	}
	while (my $line = <DATA>) {
		chomp($line);
		my @parts = split(/\t/,$line);
		if ($parts[$ChrCol] eq $CurrChr && $parts[$PosCol] >= $from && $parts[$PosCol] <= $to) {
			if ($parts[$PosCol] >= $CurrStart && $parts[$PosCol] <= $CurrEnd) {
				$nrp++;
				if ($LogRCol != -1) {
					$sum = $sum + $parts[$LogRCol];
				}
			}
			# create string to store datapoints
			$store = $store . '_'.$parts[$PosCol] ;
			if ($LogRCol != -1 ) {
				$store .=  "_".sprintf("%.3f",$parts[$LogRCol]);
			}
			if ($BafCol != -1 ) {
				$store .=  "_".sprintf("%.3f",$parts[$BafCol]);
			}
			if ($GTCol != -1 ) {
				$store .= "_" . $parts[$GTCol];
			}
		}
	}
	close DATA;
	# remove leading/trailing underscores
	$store =~ s/_$//;
	$store =~ s/^_//;
	# calculate average logR in CNV
	my $avgLogR;
	if ($nrp == 0) {
		$avgLogR = 0;
	}
	else {
		$avgLogR = $sum / $nrp;
		$avgLogR = sprintf("%.3f",$avgLogR);
	}
	# STORE THE CNV INFO
	my $query = "INSERT INTO aberration (chr, start, stop, sample, idproj, cn, seenby, nrsnps, nrgenes, largestart, largestop, avgLogR, size) VALUES ('$chromhash{ $CurrChr }', '$CurrStart', '$CurrEnd', '$sid', '$pid', '$CurrCN', '$seenby', '$nrsnps', '$nrgenes', '$largestart', '$largestop', '$avgLogR','$CurrSize')";	
	$dbh->do($query);
	my $aid = $dbh->last_insert_id( undef, undef, undef, undef );
	## lock table for large insert. 
	$dbh->do("LOCK TABLE `datapoints` LOW_PRIORITY WRITE");
	my $abquery = "INSERT INTO datapoints (id, content, structure) VALUES ('$aid', '$store','$structure')";
	$dbh->do("UNLOCK TABLES");
	$dbh->do($abquery);
	# update class if classified.
	if ($suggestedclass < 99) {
		## aberrations table
		$dbh->do("UPDATE  `aberration` SET class = '$suggestedclass' WHERE id = '$aid'");
		## add to log.
		my $logentry = 'Diagnostic Class Set (automatically) to '.$dc{$suggestedclass};
		my $arguments = " Applied Rule : $appliedrule";
		my $isth = $dbh->prepare("INSERT INTO `log` (sid,pid,aid,uid,entry,arguments) VALUES (?,?,?,?,?,?)");
		$isth->execute($sid,$pid,$aid,$uid,$logentry,$arguments);
		$isth->finish();	
	}	
}

###########################
## PROCESS PLINK RESULTS ##
###########################
if ($plink == 1) {
	$method = "PlinkHomoz";
	my $firstchar = substr($method,0,1);
	# read in configuration file
	my $configfile = "$scriptdir/Analysis_Methods/Configuration/$method.xml";
	my $configuration = $xml->XMLin($configfile);
	# get cnvfile 
	my $version = $configuration->{general}->{version};
	$algos{$method}{'version'} = $version;
	my $cnvfile = "$datadir/$pid.$sid.$method.cnv";
	my $format = $configuration->{output}->{cnv_format};
	my $nrheader = $configuration->{output}->{cnv_header};
	#my $minconf = $configuration->{general}->{min_conf};
	# get column structure
	my %columns;
	my $regex = $configuration->{output}->{cnv_regex};
	# replace placeholders
	while ($regex =~ m/%(.*?)%/) {
		$item = $1;
		$regex =~ s/^(.*?)%($item)%(.*)/$1$replace{$item}$3/;
	}
	my $nrhits = $configuration->{output}->{cnv_regex_hits};
	for ($i = 1; $i<=$nrhits; $i++) {
		next if (!exists($configuration->{output}->{"cnv_$i"}));
		$columns{$configuration->{output}->{"cnv_$i"}} = ($i - 1) ;
	}
	open IN, $cnvfile;
	# skip header lines
	for ($i = 1; $i<=$nrheader;$i++) {
		$skip = <IN>;
	}
	my $nrloh = 0;
	while (<IN>) {
		$nrloh++;
		my $line = $_;
		chomp($line);
		my @matches =  $line =~ m/$regex/i;
		my $CurrChr = $matches[$columns{'chromosome'}];
		$CurrChr =~ s/^\s+//;
		# skip zero ?
		next if ($CurrChr eq '0');
		if ($CurrChr !~ m/\d+/) {
			$CurrChr = $chromhash{ $CurrChr };
		}
		$startcol = $columns{'startposition'};
		my $CurrStart = $matches[$columns{'startposition'}];
		$CurrStart =~ s/^\s+//;
		my $CurrEnd = $matches[$columns{'stopposition'}];
		$CurrEnd =~ s/^\s+//;
		my $CurrSNP = $matches[$columns{'probes'}];
		$CurrSNP =~s/^\s+//;
		my $CurrScore = $matches[$columns{'confidence'}];
		$CurrScore =~ s/^\s+//;
		# process entry
		#push(@{$allresults{$method}}, [ $CurrChr, $CurrStart, $CurrEnd, $CurrCN, $CurrScore, $CurrSNP, "$firstchar" ]);
		#my $CurrChr = $results[$ri][0];
		my $CurrCN = 2;
		my $CurrSize = $CurrEnd - $CurrStart +1;
		# check nr of affected genes
		my $genequery = "SELECT Count(DISTINCT symbol) FROM genes WHERE chr='$CurrChr' AND (((start BETWEEN $CurrStart AND $CurrEnd) OR (end BETWEEN $CurrStart AND $CurrEnd)) OR (start <= $CurrStart AND end >= $CurrEnd))";
		$genesth = $dbh->prepare($genequery);
		$genesth->execute();
		my ($nrgenes) = $genesth->fetchrow_array();
		$genesth->finish();
		# get number of probes
		my $startidx = bin_search(\@{$ProbePos[$CurrChr]},$CurrStart);
		my $stopidx = bin_search(\@{$ProbePos[$CurrChr]},$CurrEnd);

		# Check Maximal genomic region
		# FIND next 5' probe
		if ($startidx > 0) {
			$largestart = $ProbePos[$CurrChr]->[($startidx - 1)];
		}
		else {
			$largestart = 'ter';
		}
		# FIND next 3' probe
		if ($stopidx < scalar(@{$ProbePos[$CurrChr]}) - 1) {
			$largestop = $ProbePos[$CurrChr]->[($stopidx + 1)];
		}
		else {
			$largestop = 'ter';
		}
		# Construct output and update tables
		#$tablestring .= "$CurrSample\t$CurrChr\t$CurrStart\t$CurrEnd\t$CurrSize\t$CurrCN\t$nrsnps\t$nrgenes";
		#$tablestring .= "\tPlink\t$CurrScore";
		my $string = " \nPlink ($CurrScore) ";
		my $seenby = "Plink ($CurrScore)";
	
		#$tablestring .=  "\n";
		# EXTRACT PROBE LEVEL DATA 
		my $from  = $CurrStart - 250000;
		my $to = $CurrEnd + 250000;
		#open DATA, "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.$sid.txt";
		open DATA, "$datadir/$pid.$sid.txt";
		my $header = <DATA>;
		my $store;
		my $sum = 0;
		my $nrp = 0;
		my $startprobe = '-';
		my $endprobe = '-';
		while (my $line = <DATA>) {
			chomp($line);
			my @parts = split(/\t/,$line);
			my $dataCurrChr = $parts[$ChrCol];
	                if ($dataCurrChr !~ m/\d+/) {
        	                $dataCurrChr = $chromhash{ $dataCurrChr };
               		}
			if ($dataCurrChr eq $CurrChr && $parts[$PosCol] >= $from && $parts[$PosCol] <= $to) {
				if ($parts[$PosCol] >= $CurrStart && $parts[$PosCol] <= $CurrEnd) {
					$nrp++;
					if ($LogRCol != -1) {
						$sum = $sum + $parts[$LogRCol];
					}
				}
				if ($parts[$PosCol] == $CurrStart) {
					$startprobe = $parts[$NameCol];
				}
				elsif ($parts[$PosCol] == $CurrEnd) {
					$endprobe = $parts[$NameCol];
				}
				# create string to store datapoints
				$store = $store . $parts[$PosCol] ;
				if ($LogRCol != -1 ) {
					$store .=  "_".sprintf("%.3f",$parts[$LogRCol]);
				}
				if ($BafCol != -1 ) {
					$store .=  "_".sprintf("%.3f",$parts[$BafCol]);
				}
				if ($GTCol != -1 ) {
					$store .= "_" . $parts[$GTCol];
				}
				$store .= "_";
			}
		}
		close DATA;
		$store =~ s/_$//;
		my $avgLogR;
		if ($nrp == 0) {
			$avgLogR = 0;
		}
		else {
			$avgLogR = $sum / $nrp;
			$avgLogR = sprintf("%.3f",$avgLogR);
		}
		# STORE THE CNV INFO
		my $query = "INSERT INTO aberration_LOH (chr, start, stop, sample, idproj, seenby, nrsnps, nrgenes, largestart, largestop, avgLogR, size) VALUES ('$CurrChr', '$CurrStart', '$CurrEnd', '$sid', '$pid', '$seenby', '$nrp', '$nrgenes', '$largestart', '$largestop', '$avgLogR','$CurrSize')";	
		print "$query\n";
		$dbh->do($query);
		my $aid = $dbh->last_insert_id( undef, undef, undef, undef );
		$dbh->do("LOCK TABLE `datapoints_LOH` LOW_PRIORITY WRITE");
		my $abquery = "INSERT INTO datapoints_LOH (id, content, structure) VALUES ('$aid', '$store', '$structure')";
		$dbh->do($abquery);	
		$dbh->do("UNLOCK TABLES");	
		# add to xml
		$allxml{$method} .= "<bookmark>\n<sample_id>$CurrSample \[$CurrIndex\]</sample_id>\n<bookmark_type>$types[$CurrCN]</bookmark_type>\n<entry_date></entry_date>\n<chr_num>$CurrChr</chr_num>\n<base_start_pos>$CurrStart</base_start_pos>\n<base_end_pos>$CurrEnd</base_end_pos>\n<author />\n<value>$CurrCN</value>\n<comment><![CDATA[ Confidence: $CurrScore, N.SNP: $nrp]]></comment>\n</bookmark>\n";
		$alltable{$method} .= "$CurrSample\t$CurrChr\t$CurrStart\t$CurrEnd\t$CurrSize\t$startprobe\t$endprobe\tLOH\t$nrp\tNaN\n";
		
	}
	close IN;

	print "$method : $nrloh regions retained\n";
	
}

#######################################
## UPDATE QUALITY CONTROL PARAMETERS ##
#######################################
#my $qcfile = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.$sid.PennCNVas.qc";
my $qcfile = "$datadir/$pid.$sid.PennCNVas.qc";
my $out = `cat "$qcfile" | grep "quality summary"`;
$out =~ m/LRR_SD=(.+)\sBAF_mean(.+)\sBAF_SD=(.+)\sBAF_DRIFT(.+)\sWF=(.+)\sGCWF=/;
$LRRSD = $1;
$BAFSD = $3;
$WAVE = $5;
my $update = "UPDATE projsamp SET LRRSD='$LRRSD', BAFSD='$BAFSD', WAVE='$WAVE' WHERE idsamp='$sid' AND idproj='$pid'";
$dbh->do($update);

#####################
## CHECK MOSAICISM ##
#####################
my ($indication, $bmessage);
my $walltime = '-l walltime=30:00';
# qc file for QuantiSNP2 contains bad headers, strip them!
if (defined($algos{"QuantiSNPv2"})) {
   ## estimate wall time for BAFSegmentation 
   if ($setwalltime == 1) {
		my $query = "SELECT AVG(time) FROM `GenomicBuilds`.`RuntimeStatistics` WHERE process = 'BAFSEG' AND chiptypeid = '$chiptypeid'";
		my $sth = $dbh->prepare($query);
		$sth->execute();
		my ($seconds) = $sth->fetchrow_array();
		$sth->finish();
		if ($seconds > 0) {
			$walltime = '-l walltime=' . int($seconds);
			#print "Setting estimated wall time for INput file creation as $walltime 
		}
   	}
	else {
		$walltime = '';
   	}	

	$cleanqcfile =  "sed -i '1 d ; s/Gender\t//' $datadir/$pid.$sid.QuantiSNPv2.qc";
	my $qcfile = "$datadir/$pid.$sid.QuantiSNPv2.qc";
	system($cleanqcfile);
	$bafcheck = "Rscript $scriptdir/Bin/BAFcheck.R $pid $sid $qcfile $CurrGender";
	if (-e "/tmp/$pid.$sid.bafcheck.out") {
		unlink("/tmp/$pid"."$sid"."bafcheck.out");
	}
	system($bafcheck);
	
	if (-e "/tmp/$pid.$sid.bafcheck.out") {
		open BAFIN, "/tmp/$pid.$sid.bafcheck.out";
		while (<BAFIN>) {
			my $bline = $_;
			chomp($bline);
			my @bparts = split(/\t/,$bline);
			$bmessage .= "$bparts[1]   ";
			$indication .= "$bparts[1]\t$bparts[0]\n";
		}
		close BAFIN;
		unlink("/tmp/$pid"."$sid"."bafcheck.out");
		print "Indication of Mosaicism: Running BAFSEG  ($bmessage)\n" if ($bmessage);
	}
}
if (! $indication) {
	print "No indication of Mosaicism: Running BAFSEG anyway\n";
	$indication = 'no indication';
}

## Run BafSeg regardless QuantiSNP indication but store indication info
$sth = $dbh->prepare("INSERT INTO `CNVanalysis-TMP`.`BAFSEG` (pid, sid, status, indications) VALUES (?,?,?,?)");
$sth->execute($pid,$sid,0,$indication);
$sth->finish();

## create the job script
my $jobname = "$pid.$sid.BAFSegmentation";
my $jobfile = "$qsubscripts/$pid/$jobname.sh";
open OUT, ">$jobfile";
&PBSWriteHeader("OUT", $pid, $jobname, 1, 1, $walltime, $scriptdir);
&PBSWriteCommand("OUT", "perl $scriptdir/Bin/RunBafSeg.pl -p $pid -s $sid -b $BAFSD");
&PBSWriteEnd("OUT");
close OUT;

## submit job
my $jobid = `qsub $jobfile`;
chomp($jobid);
while ($jobid !~ m/^\d+\..*/) {
	sleep 1;
	$jobid = `qsub $jobfile`;
}


#######################
## CLEAN UP THE MESS ##
#######################
$query = "DROP TABLE $tablename";
$local->do($query);
$query = "DROP TABLE $tabledet";
$local->do($query);
$query = "DROP TABLE $tabletmp";
$local->do($query);
unlink($randfile);

#################################
# OUTPUT RESULT TO MAIN PROGRAM #
#################################
# main output (= multi, no matter of number of algos)
$query = "INSERT INTO `CNVanalysis-TMP`.`Formatting` (pid,sid,Method, Version, nrabs,xml,tab) VALUES (?,?,'Multi','2.0',?,?,?)";
$sth = $dbh->prepare($query);
$sth->execute($pid,$sid,$nrabs,$multistring,$tablestring);
$sth->finish();
# seperate method results. 
for my $method (keys(%algos)) {
	my $version = $algos{$method}{'version'};
	my $xmlstring = $allxml{$method};
	my $tablestring = $alltable{$method};
	$query = "INSERT INTO `CNVanalysis-TMP`.`Formatting` (pid,sid,Method, Version, nrabs,xml,tab) VALUES (?,?,'$method','$version',?,?,?)";
	$sth = $dbh->prepare($query);
	$sth->execute($pid,$sid,$nrabs,$xmlstring,$tablestring);
	$sth->finish();
}


####################################
## INSERT COMPLETION NOTICE IN DB.##
####################################
$now = time - $now;
print "Analysis finished\n";
printf("\n\nTotal running time: %02d:%02d:%02d\n\n", int($now / 3600), int(($now % 3600) / 60),
int($now % 60));
$dbh->do("INSERT INTO `GenomicBuilds`.`RuntimeStatistics` (process,chiptypeid,argument,time) VALUES ('FormatResults','$chiptypeid','$nrabs','$now')");



#################
## SUBROUTINES ##
#################
sub NextChar {
	my $char = shift;
	my $alpha = "abcdefghijklmnopqrstuvwxyz";
	$alpha =~ m/$char/;
	my $charpos = $-[0];
	my $nextchar = ($char eq 'z' ? 'a' : substr($alpha, $charpos + 1,1));
	return $nextchar;
}

sub bin_search {
	my ($list, $tgt) = @_;

	return 0       if $tgt < $list->[0];
	return $#$list if $tgt > $list->[-1];

	my ($beg, $end, $val) = (0, $#$list, undef);
	while ($beg <= $end) {
		my $mid = int(($beg + $end) / 2);
		$val = $list->[$mid];
		if ($val > $tgt) {
			$end = $mid - 1;
		}
		elsif ($val < $tgt) {
			$beg = $mid + 1;
		}
		else {
			return $mid;
		}
	}
	return -1;
}


sub PBSWriteCommand {
	my ($filehandle, $run_command) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	my $echo_command = $run_command;
	print $filehandle "echo 'Command:'\n";
	print $filehandle "echo '========'\n";
	$echo_command =~ s/"/\\"/g;
	print $filehandle "echo \"$echo_command\"\n";
	print $filehandle "$run_command\n";

}

sub PBSWriteHeader {
	my ($filehandle, $projectid, $job, $cpu, $mem, $time, $dir) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "#!/usr/bin/env bash\n";
	print $filehandle "#PBS -m a\n";
	print $filehandle "#PBS -M $adminemail\n";
	print $filehandle "#PBS -d $dir\n";
	print $filehandle "#PBS -l nodes=1:ppn=$cpu,mem=$mem"."g\n";
	print $filehandle "#PBS -N $job\n";
	print $filehandle "#PBS -o $cjo/$scriptuser/$projectid/$job.o.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -e $cjo/$scriptuser/$projectid/$job.e.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -V\n";
	if ($queuename ne '') {
		print $filehandle "#PBS -q $queuename\n";
	}
	if ($hpcaccount ne '') {
		print $filehandle "#PBS -A $hpcaccount\n";
	}
	if ($setwalltime == 1 && $time) {
		print $filehandle "#PBS $time\n";
	}
	#if ($afterok) {
	#	print $filehandle " #PBS -W depend=afterok:$afterok\n";
	#}

	print $filehandle "\necho 'Running on : ' `hostname`\n";
	print $filehandle "echo 'Start Time : ' `date`\n\n";
	print $filehandle "echo 'setting path: $path'\n";
	print $filehandle "export PATH=$path\n\n";

}

sub PBSWriteEnd {
	my $filehandle = shift;
	if (tell( $filehandle ) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "\n\necho 'End Time : ' `date`\n";
	print $filehandle "printf 'Execution Time = \%dh:\%dm:\%ds\\n' \$((\$SECONDS/3600)) \$((\$SECONDS%3600/60)) \$((\$SECONDS%60))\n";

}
