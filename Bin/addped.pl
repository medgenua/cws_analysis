#!/usr/bin/perl
################
# LOAD MODULES #
################
use Number::Format;
use DBI;
use Getopt::Std;
use File::Spec::Functions;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

#############
# SOME VARS #
#############
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
our %samples ;
our %samplelog;
our %samplebaf;
our %samplegt ;
our %NameCol ;
my %ChrCol ;
my %PosCol ;
my %children;
my %projects ;
my $minsize ;
$minsize =0;
my @parents; # these have to be processed last!


$|++;

####################
# COMMAND LINE ARG #
####################
# r = random number
# m = number of SNP datafiles 
# n = number of sWGS datafiles 
# s = name of sWGS samples (order should match the increment number (datafile with idx 0 = first sample in list, idx 1 = second in the list...)
# D = database to use (-hg18 style)
getopts('r:m:n:s:D:', \%opts);  # option are in %opts

###########################
# CONNECT TO THE DATABASE #
###########################
my $db ;
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbhgb)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass) ;
	}

	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "Assuming build ".$row[1]."\n";
	$gbsth->finish();
	$dbhgb->disconnect;	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

$dbh->{mysql_auto_reconnect} = 1;
## Random number for file ids
my $random = $opts{'r'};
if ($random eq '') {
	die ("Random identifier is mandatory!");
}
## Check mandatory Pedigree file
#$pedfile = "$scriptdir/datafiles/ped_$random.pedfile";
$pedfile = "$datadir/ped_$random.pedfile";
if (-e $pedfile) {
	print "Pedfile found ($pedfile)\n";
}
else {
	die("Pedfile Not found (specified as '$pedfile')");
}
## check line endings in pedigree table
$line = `head -n 1 $pedfile`;
my $convert = 0;
if( $line =~ m/\r\n$/ ) {
	print "Pedigree Table : Detected DOS file format: converting to UNIX\n";
	$dump = system("cd /tmp/ && dos2unix -n $pedfile $random && mv -f $random $pedfile");
}
elsif( $line =~ m/\n\r$/ ) {
	print "Pedigree Table : Detected DOS file format: converting to UNIX\n";
	$dump = system("cd /tmp/ && dos2unix -n $pedfile $random && mv -f $random $pedfile");
}
elsif( $line =~ m/\r$/ ) {
	print "Pedigree Table : Detected MAC file format: converting to UNIX\n";
	open IN, "$pedfile";
	open OUT, ">/tmp/tmpfile.txt";
	while (<IN>) {
		$_ =~ s/\r/\n/gi ;     # replace returns with newlines
		print OUT $_ ;
	}
	close OUT;
	close IN;
	$dump = `mv  \"/tmp/tmpfile.txt \"$pedfile\"`;
}

## GET FILES
my $nrsnpfiles = $opts{'m'};
my $nrswgsfiles = $opts{'n'};
my @swgssamples = split(',', $opts{'s'});

my @datafiles ;
my $datafile ;
print "Scanning headers from supplied data files\n";
if ($nrsnpfiles >= 0) {
	## we will run dos2unix on them on the HPC, so create drmaa
	for ($i = 0;$i<=$nrsnpfiles;$i++) {
		my $file = "$datadir/ped_$random.snpdatafile.$i";
		my $ofile = "$datadir/$random.datafile.$i.txt";
		## PBS jobs removed here; run this in sequence locally
		my $out = `dos2unix -n '$file' '$ofile'`;
		## rename datafiles to 'extra' for CheckUPD/FamilyAnalysis programs from main flow
		$file = $ofile;	
		# replace XY with X as chromosome entries, replace comma with dot as decimal seperator
		$dump = system("cd $datadir && sed -i '1n; s/,/\./g ; s/XY/X/g' '$file'");
		## fix invalid sample names
		$dump = system("cd $datadir && sed -i '1 s/[^A-Za-z0-9\\-\\_\\.\\t]/_/g; s/Log_R_Ratio/Log R Ratio/g ; s/B_Allele_Freq/B Allele Freq/g' '$file'");
		push(@datafiles,$file); 
		#print "Scanning column headers for file $file\n";
		open IN, "$file" or die("Could not open datafile for reading\n");
		my $header = <IN>;
		close IN;
		chomp($header);
		my @columns = split(/\t/,$header);
		my $awkcol = 0;
		foreach (@columns) {	
			$awkcol++;
			if ($_ =~ m/^([\w\-_]+).Log/ ) {
				$samples{$1} = $i;
				$samplelog{$1} = $awkcol;
			}
			elsif ($_ =~ m/^([\w\-_]+).B All/ ) {
				$samples{$1} = $i;
				$samplebaf{$1} = $awkcol;
			}
			elsif ($_ =~ m/^([\w\-_]+).GType/ ) {
				$samples{$1} = $i;
				$samplegt{$1} = $awkcol;
			}
			elsif ($_ =~ m/^Name/ ) {
				$NameCol{$i} = $awkcol;
			}
			elsif ($_ =~ m/^Chr/ ) {
				$ChrCol{$i} = $awkcol;
			}
			elsif ($_ =~ m/^Position/ ) {
				$PosCol{$i} = $awkcol;
			}
		}
	}
}

if (scalar @swgssamples > 0) {
	for ($i = 0;$i<scalar @swgssamples;$i++) {
		my $samplename = $swgssamples[$i];
		$j = scalar(@datafiles) + $i; # make one list of increments with snp + swgs files	
		my $file = "$datadir/ped_$random.swgsdatafile.$i";
		my $tmpfile = "$datadir/$random.datafile.$j.tmp.txt";
		my $ofile = "$datadir/$random.datafile.$j.txt";

		## PBS jobs removed here; run this in sequence locally
		my $out = `dos2unix -n '$file' '$tmpfile'`;
		# remove prefix chr from chromosome
		# $dump = system("sed -i '1n; s/chr//g '$file'");

		open INPUT, $tmpfile or die "Couldn't open $tmpfile, $!";
		open OUTPUT, ">$ofile" or die "Couldn't open $ofile, $!";

		my $header = <INPUT>;
		print OUTPUT "bin\tchr\tposition\tLogR\tZscore\n";
		my $linecount = 0;
		my $pos;
		while(<INPUT>) {
			$linecount++;
			chomp;
			my ($chr, $start, $stop, $logr, $z, $qc) = split('\t', $_);
			$chr =~ s/chr//;
			# take the middle of the bin as the position (to be compatible with the Db and interface)
			$pos = ($start + $stop) / 2;
			print OUTPUT "Bin$linecount\t$chr\t$pos\t$logr\t$z\n";
		}

		# $file = $ofile;	
		push(@datafiles,$ofile);
		$samples{$samplename} = $j;
		$samplelog{$samplename} = 4;
		$samplebaf{$samplename} = 5;
		$NameCol{$j} = 1;
		$ChrCol{$j} = 2;
		$PosCol{$j} = 3;

	}
}


## create extra.col files

foreach (keys(%samples)) {
	my $fileidx = $samples{$_};
	my $chrcol = $ChrCol{$fileidx};
	my $poscol = $PosCol{$fileidx};
	my $namecol = $NameCol{$fileidx};
	my $lr = $samplelog{$_};
	my $baf = $samplebaf{$_};
	my $gt;
	if (exists $samplegt{$_}) {
		$gt = $samplegt{$_};
	}
	# get sample ID.
	my $r = $dbh->selectcol_arrayref("SELECT id FROM `sample` WHERE `chip_dnanr` = '$_'");
	my $sid = $r->[0];
	# skip unknown
	next if ($sid eq '');
	## checkUPD reads from .extra. files
	open OUT, ">>$datadir/$random.datafile.$fileidx.cols";
	if ($gt) {
		print OUT "$sid=\$$namecol,\$$chrcol,\$$poscol,\$$lr,\$$baf,\$$gt\n";
	}
	else {
		print OUT "$sid=\$$namecol,\$$chrcol,\$$poscol,\$$lr,\$$baf\n";
	}
	close OUT;
}



if (scalar(@datafiles) > 0) {
	print scalar(@datafiles) . " datafiles found.\n";
	$usedata = 1;
}
else {
	$usedata = 0;
	print "No datafiles specified!\n";
}


################
# SCAN PEDFILE #
################
print "Updating Family relations from pedigree file \n";
open PED, "$pedfile" or die("Could not open pedigreefile");
$head = <PED>;
while (<PED>) {
	#print "Looking at line: $_";
	chomp($_);
	if ($_ eq '') {
		next;
	}
	my @members = split(/\t|;|,/,$_);
	for ($i = 0; $i < scalar(@members); $i++) {
		$members[$i] =~ s/^\s+|\s+$//g;
		$members[$i] =~ s/[^A-Za-z0-9\-_]/_/g;
	}

	my $child;
	my $father;
	my $fathername;
	my $mother;
	my $mothername;
	if ($members[0] ne '') {  		# first entry must be child
		my $sqsth = $dbh->prepare("SELECT id,trackfromproject FROM sample WHERE chip_dnanr = '$members[0]'");
		$sqsth->execute();
		my $sqrows = $sqsth->rows();
		if ($sqrows == 0) {		# not present in db, ignoring (for now)
			$sqsth->finish();
			print "$members[0] is not present in the database! Skipping this entry\n";
			next;
		}
		else {				# in db, get id
			my @sqrow = $sqsth->fetchrow_array();
			$child = $sqrow[0];
			my $pid = $sqrow[1];
			if ($pid != 0) {
				$projects{$child} = $pid;
			}
			else {
				my $defp = $dbh->prepare("SELECT idproj FROM projsamp WHERE idsamp = '$child' ORDER BY idproj ASC LIMIT 1");
				$defp->execute();
				($projects{$child}) = $defp->fetchrow_array();
				$defp->finish();
			}
			$children{$child} = 1;
		}
		$sqsth->finish();
	}
	else {
		# child is needed as key
		next;
	}
	if ($members[1] ne ''){
		my $sqsth = $dbh->prepare("SELECT id, gender FROM sample WHERE chip_dnanr = '$members[1]'");
		$sqsth->execute();
		my $sqrows = $sqsth->rows();
		if ($sqrows == 0) {		# not present in db, ignoring (for now)
			print "$members[1] is not listed in the database! Ignoring this parent\n";
		}
		else {				# in db, get id
			my @sqrow = $sqsth->fetchrow_array();
			if ($sqrow[1] eq 'Male') {
				$father = $sqrow[0];
				$fathername = $members[1];
			}
			else {
				$mother = $sqrow[0];
				$mothername = $members[1];
			}
		}
		$sqsth->finish();
	}
	if ($members[2] ne ''){
		my $sqsth = $dbh->prepare("SELECT id,gender FROM sample WHERE chip_dnanr = '$members[2]'");
		$sqsth->execute();
		my $sqrows = $sqsth->rows();
		if ($sqrows == 0) {		# not present in db, ignoring (for now)
			print "$members[2] is not listed in the database! Ignoring this parent\n";
		}
		else {				# in db, get id
			my @sqrow = $sqsth->fetchrow_array();
			if ($sqrow[1] eq 'Male') {
				$father = $sqrow[0];
				$fathername = $members[2];
			}
			else {
				$mother = $sqrow[0];
				$mothername = $members[2];
			}
		}
		$sqsth->finish();
	}
	$checksth = $dbh->prepare("SELECT id, father, mother, father_project, mother_project, mendelian_errors FROM parents_relations WHERE id = '$child'");
	$checksth->execute();
	$crv = $checksth->rows();
	if ($crv == 0) {	# new trio
		#print "$_ is a new trio\n";
		$dbh->do("INSERT INTO parents_relations (id, father, mother) VALUES ('$child', '$father', '$mother')");
	}
	else {			# existing trio
		my @crow = $checksth->fetchrow_array();
		# mendelian problem present? 
		my $me = $crow[5];
		my $metype = '';
		if ($me ne '') {
			my @p = split(/@@@/,$me);
			$metype = $p[0];
		}
		# father
		my $oldfather = $crow[1];
		my $updated = 0;
		# update the father in relations only if different from listed (chip_dnanr) AND included in this project (benefit of WG-data)
		#if ($oldfather != $father && $father != '' && exists $samples{$fathername} ) {
		if ($oldfather != $father && $father != '' ) {
			if ($metype eq 'Non-Paternity') {
				$me = '';
			}
			$dbh->do("UPDATE parents_relations SET father = '$father', father_project = '0', mendelian_errors = '$me' WHERE id = '$child'");
			$updated = 1;
		}
		# mother
		my $oldmother = $crow[2];
		# update the mother in relations only if different from listed (chip_dnanr) AND included in this project
		#if ($oldmother != $mother && $mother != '' && exists $samples{$mothername}) {
		if ($oldmother != $mother && $mother != '') {
			if ($metype eq 'Non-Maternity') {
				$me = '';
			}
			$dbh->do("UPDATE parents_relations SET mother = '$mother', mother_project = '0', mendelian_errors = '$me' WHERE id = '$child'");
			$updated = 1;
		}
		if ($updated == 1) {
			print "REMARK : Parents of $members[0] were updated !\n";
		}
		# SHOULD DATA FROM PARENTS INFO BE DELETED ? no, if changed back afterwards, datapoints are still stored.  
	}
	$checksth->finish();
}


###############################
# submit family analysis jobs #
###############################
my %queued; 
foreach $currid (sort keys %children) {
	# check if family relations are available
	my $query = "SELECT father, mother FROM parents_relations WHERE id = '$currid'";
	my $prsth = $dbh->prepare($query);
	$prsth->execute();
	my $rv = $prsth->rows();
	if ($rv == "0") {
		# Only children here, based on first column of provided pedigree file! 
		next;		
	}
	else {
		# entry found, queue it 
		my @prrow = $prsth->fetchrow_array();
		my $fatherid = $prrow[0];
		my $motherid = $prrow[1];
		$children{$currid} = 1;
		my $pid = $projects{$currid};
		#$famqueue->enqueue($currid); 
		print "Submitting child (dbid = $currid) to HPC\n";
		# queue by db_id of child
		$dbh->do("INSERT INTO `CNVanalysis-TMP`.`FamilyAnalysis` (pid, sid, status) VALUES ('$random', '$currid', '0')");
		$queued{$currid} = 1;

		## create the job script
		my $jobname = "ped_$random.$currid.FamilyAnalysis";
		my $qsubdir = catfile($qsubscripts, $random);
		if (! -d $qsubdir) { mkdir $qsubdir};
		my $jobfile = catfile($qsubdir, $jobname . ".sh");
		open OUT, ">$jobfile";
		&PBSWriteHeader("OUT", '', $jobname, 1, 1, undef, $scriptdir);
		&PBSWriteCommand("OUT", "perl $scriptdir/Bin/FamilyAnalysis.pl -D $db -p $random -s $currid");
		&PBSWriteEnd("OUT");
		close OUT;

		## submit job
		my $jobid = `qsub $jobfile`;
		chomp($jobid);
		while ($jobid !~ m/^\d+\..*/) {
			sleep 1;
			$jobid = `qsub $jobfile`;
		}

	}
	$prsth->finish();
}


########################
# submit UPD analysis  #
########################
open PED, "$pedfile";
my $headline = <PED>;
while (<PED>) {
	chomp($_);
	my @members = split(/\t|;|,/,$_);
	for ($i = 0; $i < scalar(@members); $i++) {
		$members[$i] =~ s/^\s+|\s+$//g;
		$members[$i] =~ s/[^A-Za-z0-9\-_]/_/g;
	}
	## check if all columns are filled.
	if ($members[0] eq '' || $members[1] eq '' || $members[2] eq '') {
		print "Skipping pedigree entry due to missing values\n";
		next;
	}
	## reset variables
	my $childname;
	my $childid;
	my $fatherid;
	my $fathername;
	my $motherid;
	my $mothername;
	## first column is child, get details
	$childname = $members[0];
	my $sqsth = $dbh->prepare("SELECT s.`id`, ps.`datatype` FROM `sample` s JOIN `projsamp` ps ON s.`id` = ps.`idsamp` WHERE s.`chip_dnanr` = '".$members[0]."'");
	$sqsth->execute();
	my $sqrows = $sqsth->rows();
	if ($sqrows == 0) {		
		# not present in db, needs to be !
		print "Offspring '".$members[0] . "' is not present in the database, UPD-check not possible\n";
		next;
	}
	else {	
		# in db, get id
		my @sqrow = $sqsth->fetchrow_array();
		$childid = $sqrow[0];
		if ($sqrow[1] eq 'swgs') {
			print "Offspring '".$members[0] . "' is an sWGS sample, UPD-check not possible\n";
			next;
		}
	}
	$sqsth->finish();
	if (!defined($samples{$childname})) {
		# data not available, skip !
		print "No data found for offspring '$childname', UPD-analysis not possible\n";
		next;
	}
	## second column: check ID and datafile
	my $sqsth = $dbh->prepare("SELECT s.`id`, ps.`datatype` FROM `sample` s JOIN `projsamp` ps ON s.`id` = ps.`idsamp` WHERE chip_dnanr = '".$members[1]."'");
	$sqsth->execute();
	my $sqrows = $sqsth->rows();
	if ($sqrows == 0) {		
		# not present in db, needs to be !
		print "Parent '".$members[1] . "' of '$childname' is not present in the database, UPD-analysis not possible \n";
		next;
	}
	else {
		# check if it is not a sWGS sample
		my @sqrow = $sqsth->fetchrow_array();
		if ($sqrow[1] eq 'swgs') {
			print "Parent '".$members[1] . "' is an sWGS sample, UPD-check not possible\n";
			next;
		}

	}
	$sqsth->finish();
	if (!defined($samples{$members[1]})) {
		# no datafile found for parent 1
		print "No data found for Parent '".$members[1]."' of '$childname', UPD-analysis not possible\n";
		next;
	}

	# third column: check ID and datafile
	my $sqsth = $dbh->prepare("SELECT s.`id`, ps.`datatype` FROM `sample` s JOIN `projsamp` ps ON s.`id` = ps.`idsamp` WHERE chip_dnanr = '".$members[2]."'");
	$sqsth->execute();
	my $sqrows = $sqsth->rows();
	if ($sqrows == 0) {		
		# not present in db, needs to be !
		print "Parent '".$members[2] . "' of '$childname' is not present in the database, UPD-analysis not possible\n";
		next;
	}
	else {	
		# check if it is not a sWGS sample
		my @sqrow = $sqsth->fetchrow_array();
		$childid = $sqrow[0];
		if ($sqrow[1] eq 'swgs') {
			print "Parent '".$members[2] . "' is an sWGS sample, UPD-check not possible\n";
			next;
		}
	}

	$sqsth->finish();
	if (!defined($samples{$members[2]})) {
		print "No data found for Parent '".$members[2]."' of '$childname', UPD-analysis not possible\n";
		next;
	}
	my $pid = $projects{$childid};
	# all data present, submit job to queue.
	$dbh->do("INSERT INTO `CNVanalysis-TMP`.`UPD` (pid, sid, status) VALUES ('$random', '$childid', '0')");
	print "Submitting sample $childname (ID: $childid) for UPD\n";

	## create the job script
	my $jobname = "ped.$childid.UPDAnalysis";
	my $jobfile = "$qsubscripts/$random/$jobname.sh";
	open OUT, ">$jobfile";
	&PBSWriteHeader("OUT", '', $jobname, 1, 1, undef, $scriptdir);
	&PBSWriteCommand("OUT", "perl $scriptdir/Bin/CheckUPD.pl -D $db -p $random -s $childid");
	&PBSWriteEnd("OUT");
	close OUT;

	## submit job
	my $jobid = `qsub $jobfile`;
	chomp($jobid);
	while ($jobid !~ m/^\d+\..*/) {
		sleep 1;
		$jobid = `qsub $jobfile`;
	}


}
		
############################################
## WAIT FOR THE FAMILY/UPD JOBS TO FINISH ##
############################################
$continue = 1;
while ($continue == 1) {
	my $todo = 0;
	$sth = $dbh->prepare("SELECT sid, status FROM `CNVanalysis-TMP`.`FamilyAnalysis` WHERE pid = '$random'");
	$sth->execute();
	## check family analysis
	while (my @row = $sth->fetchrow_array()) {
		if ($row[1] == 1) {
			my $currsid = $row[0];
			$dbh->do("DELETE FROM `CNVanalysis-TMP`.`FamilyAnalysis` WHERE pid = '$random' AND sid = '$currsid'");
		}
		else {
			$todo++;
		}
	}
	$sth->finish();
	## check upd
	$sth = $dbh->prepare("SELECT sid, status FROM `CNVanalysis-TMP`.`UPD` WHERE pid = '$random'");
	$sth->execute();
	while (my @row = $sth->fetchrow_array()) {
		if ($row[1] == 1) {
			my $currsid = $row[0];
			$dbh->do("DELETE FROM `CNVanalysis-TMP`.`UPD` WHERE pid = '$random' AND sid = '$currsid'");
		}
		else {
			$todo++;
		}
	}
	$sth->finish();
	## items left?
	if ($todo > 0) {
		sleep 30;
	}
	else {
		$continue = 0;
	}
}

############
# CLEAN UP #
############
print "Cleaning up\n";
system("rm -f $datadir/ped_$random.*");
system("rm -f $datadir/ped.$random.*");
system("rm -f $datadir/$random.*");
print " All Done.\n";


###################
### SUBROUTINES ###
###################
sub PBSWriteCommand {
	my ($filehandle, $run_command) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	my $echo_command = $run_command;
	print $filehandle "echo 'Command:'\n";
	print $filehandle "echo '========'\n";
	$echo_command =~ s/"/\\"/g;
	print $filehandle "echo \"$echo_command\"\n";
	print $filehandle "$run_command\n";

}

sub PBSWriteHeader {
	my ($filehandle, $projectid, $job, $cpu, $mem, $time, $dir) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "#!/usr/bin/env bash\n";
	print $filehandle "#PBS -m a\n";
	print $filehandle "#PBS -M $adminemail\n";
	print $filehandle "#PBS -d $dir\n";
	print $filehandle "#PBS -l nodes=1:ppn=$cpu,mem=$mem"."g\n";
	print $filehandle "#PBS -N $job\n";
	print $filehandle "#PBS -o $cjo/$scriptuser/$projectid/$job.o.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -e $cjo/$scriptuser/$projectid/$job.e.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -V\n";
	if ($queuename ne '') {
		print $filehandle "#PBS -q $queuename\n";
	}
	if ($hpcaccount ne '') {
		print $filehandle "#PBS -A $hpcaccount\n";
	}
	if ($setwalltime == 1 && $time) {
		print $filehandle "#PBS $time\n";
	}
	#if ($afterok) {
	#	print $filehandle " #PBS -W depend=afterok:$afterok\n";
	#}

	print $filehandle "\necho 'Running on : ' `hostname`\n";
	print $filehandle "echo 'Start Time : ' `date`\n\n";
	print $filehandle "echo 'setting path: $path'\n";
	print $filehandle "export PATH=$path\n\n";
}

sub PBSWriteEnd {
	my $filehandle = shift;
	if (tell( $filehandle ) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "\n\necho 'End Time : ' `date`\n";
	print $filehandle "printf 'Execution Time = \%dh:\%dm:\%ds\\n' \$((\$SECONDS/3600)) \$((\$SECONDS%3600/60)) \$((\$SECONDS%60))\n";

}
