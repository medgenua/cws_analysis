#!/usr/bin/perl
## keep track of runtime 
$now = time;
$|++; # no buffering => output immediately to spool files.
################
# LOAD MODULES #
################
use Number::Format;
use DBI;
use Getopt::Std;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

# print the host running the job #
$hostname = `hostname`;
print "Running Job on $hostname";

###############################
# SET NUMBER FORMATTING STYLE #
###############################
my $de = new Number::Format(-thousands_sep =>',',-decimal_point => '.');

##########################
# COMMAND LINE ARGUMENTS #
##########################
# D = database (eg CNVanalysis-hg18)
# p = pid
# s = sid
my $sid;
my $pid;
getopts('D:p:s:',\%opts);
if ($opts{'s'}) {
	$sid = $opts{'s'};
}
else {
	die('No sample specified');
}
if ($opts{'p'}) {
	$pid = $opts{'p'};
}
else {
	die('No project specified');
}


###########################
# CONNECT TO THE DATABASE #
###########################
my $db;
if ($opts{'D'}) {
	$db = $opts{'D'};
}
else {
	die('No database specified');
}
$connectionInfo="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfo,$userid,$userpass);
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfo,$userid,$userpass) ;
}
$dbh->{mysql_auto_reconnect} = 1;

## if coming from addped.pl, the pid does not correspond to a real pid, so get and compare from db. 
$filepid = $pid;
$sth = $dbh->prepare("SELECT COUNT(idsamp) FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
$sth->execute();
#$sth->finish();
my ($count) = $sth->fetchrow_array();
$sth->finish();
if ($count == 0) {
	print "sample '$sid' not found in provided project '$pid'. Fetching other project id from database\n";
	$sth = $dbh->prepare("SELECT trackfromproject FROM sample WHERE id = '$sid'");
	$sth->execute();
	($pid) = $sth->fetchrow_array();
	$sth->finish();
	if ($pid == 0) {
		$sth = $dbh->prepare("SELECT idproj FROM projsamp WHERE idsamp = '$sid' ORDER BY idproj ASC LIMIT 1");
		$sth->execute();
		($pid) = $sth->fetchrow_array();
		$sth->finish();
	}
}
else {
	print "Sample '$sid' is present in provided project '$pid'. Assuming this is a valid hit.\n";
}
#######################################
## GET PROJECT DETAILS FROM DATABASE ##
#######################################
my $query = "SELECT chiptype,chiptypeid FROM project WHERE id = '$pid'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @array = $sth->fetchrow_array();
my $chiptype = $array[0];
my $chiptypeid = $array[1];
$sth->finish();


#####################################
## CHANGE TO CNVanalysis DIRECTORY ##
#####################################
chdir($scriptdir);

##########################
## INITIALISE VARIABLES ##
##########################
my %samples;
my %extrasamples;
my $mfile = 0;
my $ffile = 0;
my $ofile = 0;
my $minsize = 3; # keep fragments of at least 3 informative probes
@nrs=(1..22);
push(@nrs,'X');
my %chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 


##########################################
## GET ALL SAMPLES FROM CURRENT PROJECT ##
##########################################
## in project itself
$query = "SELECT idsamp FROM projsamp WHERE idproj = '$pid'";
my $sth = $dbh->prepare($query);
$sth->execute();
while (my @row = $sth->fetchrow_array()) {
	$samples{$row[0]} = 1;
}
$sth->finish();
## in structure of datafiles (0 = main during CNVanalysis)
for ($i = 0; $i<= 99; $i++) {
	#if (-e "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extra.$i.cols") {
	if (-e "$datadir/$filepid.datafile.$i.cols") {
		print "Reading column structure from $datadir/$filepid.datafile.$i.cols\n";
		#open IN, "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extra.$i.cols";
		open IN, "$datadir/$filepid.datafile.$i.cols";
		while (<IN>) {
			chomp($_);
			my @p = split(/=/,$_);
			$extrasamples{$p[0]}{'cols'} = $p[1];
			#print "Storing : $p[0] ==> $p[1]\n";
			#$extrasamples{$p[0]}{'file'} = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extra.$i.txt";
			$extrasamples{$p[0]}{'file'} = "$datadir/$filepid.datafile.$i.txt";
		} 
		close IN;
	}
}

###################################
## GET FAMILY INFO FROM DATABASE ##
###################################
## family relations
my $query = "SELECT father, father_project, mother, mother_project FROM parents_relations WHERE id = '$sid'";
my $prsth = $dbh->prepare($query);
$prsth->execute();
my @res = $prsth->fetchrow_array();
my $fid = $res[0];
my $fatherpid = $res[1];
my $mid = $res[2];
my $motherpid = $res[3];
$prsth->finish();
## sample names
$query = "SELECT chip_dnanr FROM sample WHERE id = ?";
$sth = $dbh->prepare($query);
$sth->execute($sid);
@res = $sth->fetchrow_array();
$child = $res[0];
$sth->execute($fid);
@res = $sth->fetchrow_array();
$father = $res[0];
$sth->execute($mid);
@res = $sth->fetchrow_array();
$mother = $res[0];
$sth->finish();

################################
## CREATE MISSING INPUT FILES ##
################################
## offspring
if (exists($samples{$sid})) {
	## in current project, so file exists
	#$ofile = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.$sid.txt";
	$ofile = "$datadir/$filepid.$sid.txt";
	print "Using datafile from provided project: $ofile\n";
	#print "using samples-ofile: $ofile\n";
	unless (-e $ofile) {
		print "Creating datafile for offspring (in samples, could happen if pid got updated to sample not in track.):\n";
		## create file lock while creating the datafile
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$sid}{'cols'}; ## also contains samples from main project.
		my $datafile = $extrasamples{$sid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$sid.txt && cp /tmp/$filepid.extrasample.$sid.txt $ofile && rm -f /tmp/$filepid.extrasample.$sid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}

}
elsif (exists($extrasamples{$sid})) {
	## in extra files
	$ofile = "$datadir/$filepid.extrasample.$sid.txt";
	unless (-e $ofile) {
		## create file lock while creating the datafile
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$sid}{'cols'};
		my $datafile = $extrasamples{$sid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$sid.txt && cp /tmp/$filepid.extrasample.$sid.txt $ofile && rm -f /tmp/$filepid.extrasample.$sid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}
}
else {
	print "Could not determine the correct file for the child\n";
	print "  - pid: $pid\n";
	print "  - filepid : $filepid\n";
	print "  - mid : $sid\n";
	print "  - name: $child\n";
}

## father
if (exists($samples{$fid})) {
	## in current project, so file exists
	#$ffile = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.$fid.txt";
	$ffile = "$datadir/$filepid.$fid.txt";
	unless (-e $ffile) {
		## create file lock while creating the datafile
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$fid}{'cols'};
		my $datafile = $extrasamples{$fid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$fid.txt && cp /tmp/$filepid.extrasample.$fid.txt $ffile && rm -f /tmp/$filepid.extrasample.$fid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}

}
elsif (exists($extrasamples{$fid})) {
	## in extra files
	#$ffile = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extrasample.$fid.txt";
	$ffile = "$datadir/$filepid.extrasample.$fid.txt";
	unless (-e $ffile) {
		## create file lock while creating the datafile
		#open DUMMY, ">>/var/tmp/datafiles/CNV-WebStore/runtime/dummy";
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$fid}{'cols'};
		my $datafile = $extrasamples{$fid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$fid.txt && cp /tmp/$filepid.extrasample.$fid.txt $ffile && rm -f /tmp/$filepid.extrasample.$fid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}
}
else {
	print "Could not determine the correct file for the father\n";
	print "  - pid: $pid\n";
	print "  - filepid : $filepid\n";
	print "  - mid : $fid\n";
	print "  - name: $father\n";
}

## mother
if (exists($samples{$mid})) {
	## in current project, so file exists
	#$mfile = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.$mid.txt";
	$mfile = "$datadir/$filepid.$mid.txt";
	unless (-e $mfile) {
		## create file lock while creating the datafile
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$mid}{'cols'};
		my $datafile = $extrasamples{$mid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$mid.txt && cp /tmp/$filepid.extrasample.$mid.txt $mfile && rm -f /tmp/$filepid.extrasample.$mid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}
}
elsif (exists($extrasamples{$mid})) {
	## in extra files
	#$mfile = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extrasample.$mid.txt";
	$mfile = "$datadir/$filepid.extrasample.$mid.txt";
	unless (-e $mfile) {
		## create file lock while creating the datafile
		#open DUMMY, ">>/var/tmp/datafiles/CNV-WebStore/runtime/dummy";
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$mid}{'cols'};
		my $datafile = $extrasamples{$mid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$mid.txt && cp /tmp/$filepid.extrasample.$mid.txt $mfile && rm -f /tmp/$filepid.extrasample.$mid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}
}
else {
	print "Could not determine the correct file for the mother\n";
	print "  - pid: $pid\n";
	print "  - filepid : $filepid\n";
	print "  - mid : $mid\n";
	print "  - name: $mother\n";
}

################################
## LOAD USABLE DATA INTO HASH ##
################################
print "Loading Data.\n";
my %offspring;
open IN, "$ofile" or die("could not open file '$ofile'");
$headline = <IN>;
while (<IN>) {
	chomp($_);
	my @pieces = split(/\t/,$_);
	my $chr = $pieces[1];
	my $pos = $pieces[2];
	my $gt = $pieces[5];
	my $log = $pieces[3];
	if ($gt =~ m/Na|NC|\-/i) {
		next;
	}
	$offspring{$chr}{$pos}{'gt'} = $gt;
	$offspring{$chr}{$pos}{'logR'} = $log;	
}
close IN;

my %father;
open IN, "$ffile" or die("could not open file '$ffile'");
$headline = <IN>;
while (<IN>) {
	chomp($_);
	my @pieces = split(/\t/,$_);
	my $chr = $pieces[1];
	my $pos = $pieces[2];
	my $gt = $pieces[5];
	my $log = $pieces[3];
	if ($gt =~ m/Na|NC|\-/i || !exists($offspring{$chr}{$pos})) {
		next;
	}
	$father{$chr}{$pos}{'gt'} = $gt;	
	$father{$chr}{$pos}{'logR'} = $log;
}
close IN;

my %mother;
open IN, "$mfile" or die("could not open file '$mfile'");
$headline = <IN>;
while (<IN>) {
	chomp($_);
	my @pieces = split(/\t/,$_);
	my $chr = $pieces[1];
	my $pos = $pieces[2];
	my $gt = $pieces[5];
	my $log = $pieces[3];
	if ($gt =~ m/Na|NC|\-/i || !exists($offspring{$chr}{$pos})) {
		next;
	}
	$mother{$chr}{$pos}{'gt'} = $gt;	
	$mother{$chr}{$pos}{'logR'} = $log;
}
close IN;

###############################
## START COMPARING GENOTYPES ##
###############################
print "Comparing Genotypes\n";
my $X = 0;  # nr of informative probes
$typeoccs{'UPI-P'};# = 0;
$typeoccs{'UPI-M'};# = 0;
$typeoccs{'MI-S'};# = 0; 
$typeoccs{'MI-D'};# = 0;
$typeoccs{'BPI'};# = 0;
my %positions;
my %segments;
my $nrmi = 0;  # keep track of mendelian errors => check for wrong parents
for ($chr = 1; $chr <= 22; $chr++) {
	$Xchrom{$chr} = 0;
	$typeoccs{$chr}{'UPI-P'};
	$typeoccs{$chr}{'UPI-M'};
	$typeoccs{$chr}{'MI-S'}; 
	$typeoccs{$chr}{'MI-D'};
	$typeoccs{$chr}{'BPI'};

	my %chash = %{$offspring{$chr}};
	my %fhash = %{$father{$chr}};
	my %mhash = %{$mother{$chr}};
	my $stretchlength = 0;
	my $stretchtype = '';
	my $stretchstart = '';
	my $stretchstop = '';
	my $stretchpos = '';
	my $prevstretchtype = '';
	my $prevstretchlength = 0;
	my $prevstretchstart;
	my $prevstretchstop;
	my $isofound = 0;
	for $pos (sort {$a<=>$b} keys(%chash)) {
		my $currtype = '';
		my $cgt = $chash{$pos}{'gt'};
		my $fgt = $fhash{$pos}{'gt'};
		my $mgt = $mhash{$pos}{'gt'};
		# table see snptrio paper
		if ($fgt eq 'AA') {
			if ($mgt eq 'AA') {
				if ($cgt eq 'AB') { $currtype = 'MI-S'; }
				elsif ($cgt eq 'BB') {  $currtype = 'MI-D';}
				else { next; } # uniformative
			}
			elsif ($mgt eq 'AB') {
				if ($cgt eq 'BB') { $currtype = 'UPI-M';$isofound++;}
				else { next; } # uniformative / no GT
			}
			elsif ($mgt eq 'BB') {
				if ($cgt eq 'AA') { $currtype = 'UPI-P'; }
				elsif ($cgt eq 'AB') { $currtype = 'BPI';}
				elsif ($cgt eq 'BB') { $currtype = 'UPI-M';}
				else { next; } # uniformative / no GT
			}
			else {
				# no genotype, should not happen
				next;
			}
		}
		elsif ($fgt eq 'AB') {
			if ($mgt eq 'AA') {
				if ($cgt eq 'BB') { $currtype = 'UPI-P';$isofound++;}
				else { next; } # uniformative / no GT
			}
			elsif ($mgt eq 'AB') {
				next; # no informative combinations
			}
			elsif ($mgt eq 'BB') {
				if ($cgt eq 'AA') { $currtype = 'UPI-P';$isofound++;}
				else { next; } # uniformative / no GT
			}
			else {
				# no genotype, should not happen
				next;
			}
		}
		elsif ($fgt eq 'BB') {
			if ($mgt eq 'AA') {
				if ($cgt eq 'AA') { $currtype = 'UPI-M'; }
				elsif ($cgt eq 'AB') { $currtype = 'BPI';}
				elsif ($cgt eq 'BB') { $currtype = 'UPI-P';}
				else { next; } # uniformative / no GT
			}
			elsif ($mgt eq 'AB') {
				if ($cgt eq 'AA') { $currtype = 'UPI-M'; $isofound++;}
				else { next;} #uninformative / no GT
			}
			elsif ($mgt eq 'BB') {
				if ($cgt eq 'AA') { $currtype = 'MI-D'; }
				elsif ($cgt eq 'AB') { $currtype = 'MI-S';}
				else { next; } # uninformative
			}
			else {
				# no genotype, should not happen
				next;
			}
		}
		else {
			# no genotype, should not happen
			next;
		}
		# if it gets here, an informative probe was found
		$X++;	
		$Xchrom{$chr}++;
		if ($currtype =~ m/MI/) {
			$nrmi++;
			open MI, ">>/tmp/$filepid.$child.Mendelian.Inconsistensies.txt";
			print MI "$chr;$pos;$currtype;$cgt;$fgt;$mgt\n";
			close MI;
		}

		# same type detected (extension)
		if ($stretchtype eq $currtype) {
			$stretchlength++;
			$typeoccs{$chr}{$currtype}++;
			$typeoccs{$currtype}++;
			$stretchstop = $pos;
			$stretchpos .= "$pos,";
			next;
		}
		if ($stretchlength >= $minsize) {
			# size is ok, keep fragment ...
			if ($isofound > 0) {
				$type = 'i';
			}
			else {
				$type = 'h';
			}
			$stretchpos = substr($stretchpos,0,-1);
			$positions{$stretchstart."_".$stretchstop} = $stretchpos;
			push @{$segments{$chr}}, [$stretchtype, $stretchlength, $stretchstart, $stretchstop, $type]; 
		}
		else {
			# store as mismatch for merging?
			if ($isofound > 0) {
				$type = 'i';

			}
			else {
				$type = 'h';

			}
			$stretchpos = substr($stretchpos,0,-1);
			$positions{$stretchstart."_".$stretchstop} = $stretchpos;
			push @{$fails{$chr}}, [$stretchtype, $stretchlength, $stretchstart, $stretchstop, $type];
		}
		# update last values;
		$typeoccs{$chr}{$currtype}++;
		$typeoccs{$currtype}++;
		$stretchtype = $currtype; 
		$stretchstart = $pos;
		$stretchstop = $pos;
		$isofound = 0;
		$stretchpos = "$pos,";
		$stretchlength = 1;
	}
	if ($stretchlength >= $minsize) {
		if ($isofound > 0) {
			$type = 'i';
		}
		else {
			$type = 'h';
	
		}
		$stretchpos = substr($stretchpos,0,-1);
		$positions{$stretchstart."_".$stretchstop} = $stretchpos;
		push @{$segments{$chr}}, [$stretchtype, $stretchlength, $stretchstart, $stretchstop, $type]; 
	}
	else {
		# store as mismatch for merging?
		if ($isofound > 0) {
			$type = 'i';
		}
		else {
			$type = 'h';
		}
		$stretchpos = substr($stretchpos,0,-1);
		$positions{$stretchstart."_".$stretchstop} = $stretchpos;
		push @{$fails{$chr}}, [$stretchtype, $stretchlength, $stretchstart, $stretchstop,$type];
	}
}

#############################
## CHECK FOR WRONG PARENTS ##
#############################
if ($nrmi > 600) {
	# over 100 mendelian errors => wrong parents provided
	# scan upd types for most likely incorrect parent
	my $updm = 0;
	my $updp = 0;
	for ($chr = 1; $chr <= 22; $chr++) {
		my @segments = @{$segments{$chr}};
		my $nrsegs = scalar(@segments);
		for ($i = 0; $i < $nrsegs; $i++) {
			my $type = $segments[$i][0];
			next if ($type eq 'BPI');
			$updm++ if (substr($type,-1) eq 'M');
			$updp++ if (substr($type,-1) eq 'P');
		}
	}
	my $wrongparent = '';
	if ($updm > $updp) {
		$wrongparent = 'Non-Paternity';
	}
	else {
		$wrongparent = 'Non-Maternity';
	}
	print "$child => $wrongparent suspected due to $nrmi mendelian errors and $updm x UPD-M vs $updp x UPD-P . Not Storing Sample!\n";
	## load output file into string and store to database.
	open FILE, "/tmp/$filepid.$child.Mendelian.Inconsistensies.txt" or die "Couldn't open file: $!";
	my $string = join("", <FILE>);
	close FILE;
	$string = "$wrongparent@@@"."$nrmi@@@"."$updm@@@"."$updp@@@".$string;
	my $store = $dbh->prepare("UPDATE parents_relations SET `mendelian_errors` = ? WHERE id = '$sid'");
	$store->execute($string);
	$store->finish();
	## update status and exit
	$dbh->do("UPDATE `CNVanalysis-TMP`.`UPD` SET status = 1 WHERE pid = '$filepid' AND sid = '$sid'");
	$dbh->do("UPDATE projsamp SET checkedupd = 1 WHERE idsamp = '$sid' AND idproj = '$pid'"); # AND father = '$fid' AND mother = '$mid'");
	$now = time - $now;
	$dbh->do("INSERT INTO `GenomicBuilds`.`RuntimeStatistics` (process,chiptypeid,argument,time) VALUES ('UPD','$chiptypeid',' ','$now')");
	system("rm -f '/tmp/$filepid.$child.Mendelian.Inconsistensies.txt'");		
	exit;
}

#######################################################
## CALCULATE SIGNIFICANCE LEVELS FOR FOUND SEGMENTS  ##
#######################################################
#todo : add upd on X
for ($chr = 1; $chr <= 22; $chr++) {
	my @segments = @{$segments{$chr}};
	my $nrsegs = scalar(@segments);
	my %data;
	my %ppos; #probe positions sorted per sample.
	# read data for this chromosome if segments were found.
	# memory inefficient, but eliminates need to rescan the file for each segment.
	if ($nrsegs > 0) {
		print "Fetching data for chr$chr\n";
		#child
		open IN, $ofile;
		my $head = <IN>;
		while (<IN>) {
			chomp($_);
			my @pieces = split(/\t/,$_);
			if ($pieces[1] ne $chr) {
				next;
			}
			#if ($pieces[1] == $chr && $pieces[2] >= ($start - 250000) && $pieces[2] <= ($stop + 250000)) {
			#	$datastring = $datastring . "$pieces[2]"."_".sprintf("%.3f",$pieces[3])."_".$pieces[5]."_";
			#}
			# data->sampleID->position = datastring (logR_GT).
			$data{$sid}{$pieces[2]} = sprintf("%.3f",$pieces[3])."_".$pieces[5];
		}
		close IN;
		my @k = keys(%{$data{$sid}});
		@k = sort {$a <=> $b} @k;
		$ppos{$sid} = \@k;
		#father
		open IN, $ffile;
		my $head = <IN>;
		while (<IN>) {
			chomp($_);
			my @pieces = split(/\t/,$_);
			if ($pieces[1] ne $chr) {
				next;
			}
			#if ($pieces[1] == $chr && $pieces[2] >= ($start - 250000) && $pieces[2] <= ($stop + 250000)) {
			#	$datastring = $datastring . "$pieces[2]"."_".sprintf("%.3f",$pieces[3])."_".$pieces[5]."_";
			#}
			# data->sampleID->position = datastring (logR_GT).
			$data{$fid}{$pieces[2]} = sprintf("%.3f",$pieces[3])."_".$pieces[5];
		}
		close IN;
		my @l = keys(%{$data{$fid}});
		@l = sort{$a <=> $b} @l;
		$ppos{$fid} = \@l;
		#mother
		open IN, $mfile;
		my $head = <IN>;
		while (<IN>) {
			chomp($_);
			my @pieces = split(/\t/,$_);
			if ($pieces[1] ne $chr) {
				next;
			}
			#if ($pieces[1] == $chr && $pieces[2] >= ($start - 250000) && $pieces[2] <= ($stop + 250000)) {
			#	$datastring = $datastring . "$pieces[2]"."_".sprintf("%.3f",$pieces[3])."_".$pieces[5]."_";
			#}
			# data->sampleID->position = datastring (logR_GT).
			$data{$mid}{$pieces[2]} = sprintf("%.3f",$pieces[3])."_".$pieces[5];
		}
		close IN;
		my @m = keys(%{$data{$mid}});
		@m = sort{$a <=> $b} @m;
		$ppos{$mid} = \@m;
		
	}
	for ($i = 0; $i < $nrsegs; $i++) {
		my $type = $segments[$i][0];
		my $length = $segments[$i][1];
		my $start = $segments[$i][2];
		my $stop = $segments[$i][3];
		# skip normal regions
		if ($type eq 'BPI') {
			next;
		}
		# add iso or hetero
		if ($type =~ m/UPI/) {
			$stype = $segments[$i][4] . $type;
		}
		## get VALUES for formula
		$P = $typeoccs{$type} / $X;
		my $N = $length;
		my @pvals;
		# tabulate P for 1 to N
		for ($j = 1; $j <= $N ; $j++) {
			$pvals[$j] = 1 - ($P ** $j);		
		}
		#iteratively go to P(X)
		for ($j = ($N+1); $j <= $X; $j++) {
			my $pj = 0;
			for ($k = 0; $k < $N; $k++) {
				my $idx = $j - $k - 1; 
				$pj += ($P ** $k) * (1 - $P) * $pvals[$idx];
			}
			$pvals[$j] = $pj;
		}
		my $pv = 1 - $pvals[$X];
		if ($pv > 0.01) {
			# use strict significance leve, significant regions easily get E-5 or less.
			next;
		}
		# prepare to store
		$posstring = $positions{$start."_".$stop};
		my @pos = split(/,/,$posstring);
		my $avgLogR = 0;
		my $gtstring = ''; # store informative gt's as "position;child;father;mother@" trios.
		my $nrofprobes = scalar(@pos);
		foreach (@pos) {
			$avgLogR += $offspring{$chr}{$_}{'logR'}; 
			 $gtstring .= $_.";".$offspring{$chr}{$_}{'gt'}.";".$father{$chr}{$_}{'gt'}.";".$mother{$chr}{$_}{'gt'}."@";
		} 
		$gtstring = substr($gtstring,0,-1);
		$avgLogR = $avgLogR / $nrofprobes;
		my $store = $dbh->prepare("INSERT INTO parents_upd (sid, fid, mid, type, chr, start, stop, pval, nrprobes, logR, data,spid,fpid,mpid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		$store->execute($sid, $fid, $mid, $stype, $chr, $start, $stop, $pv, $nrofprobes, $avgLogR, $gtstring,$pid,$fatherpid,$motherpid);
		$store->finish();
		$lastinsert = $dbh->last_insert_id( undef, undef, undef, undef );
		print "UPD region : chr$chr:$start-$stop : stored under id $lastinsert\n";
			
		# store datapoints in format "pos_logR_gt"
		# child
		my $check = $dbh->prepare("SELECT id FROM parents_upd_datapoints WHERE id = ? AND sid = ? ");
		my $nrrows = $check->execute($lastinsert, $sid);
		$check->finish();
		if ($nrrows > 0) {
			# already in database. skip
		}
		else {
			# extract datapoints (250Kb up and downstream)
			my $datastring = '';
			foreach (@{$ppos{$sid}}) {
				if ($_ >= ($start - 250000) && $_ <= ($stop + 250000)) {
					$datastring = $datastring . "$_"."_".$data{$sid}{$_}."_";
				}
				if ($_ > ($stop + 250000)) {
					last;
				}
			}
			#open IN, $ofile;
			#my $head = <IN>;
			#while (<IN>) {
			#	chomp($_);
			#	my @pieces = split(/\t/,$_);
			#	if ($pieces[1] == $chr && $pieces[2] >= ($start - 250000) && $pieces[2] <= ($stop + 250000)) {
			#		$datastring = $datastring . "$pieces[2]"."_".sprintf("%.3f",$pieces[3])."_".$pieces[5]."_";
			#	}
			#}
			#close IN;
			$datastring = substr($datastring,0,-1);
			## obtain lock to store datapoints (large query)
			$dbh->do("LOCK TABLE `parents_upd_datapoints` LOW_PRIORITY WRITE");
			my $store = $dbh->prepare("INSERT INTO parents_upd_datapoints (id, sid, content) VALUES (?,?,?)");
			$store->execute($lastinsert, $sid,$datastring);
			$store->finish();
			$dbh->do("UNLOCK TABLES");
		}
		# father
		my $check = $dbh->prepare("SELECT id FROM parents_upd_datapoints WHERE id = ? AND sid =? ");
		my $nrrows = $check->execute($lastinsert, $fid);
		$check->finish();
		if ($nrrows > 0) {
			# already in database. skip
		}
		else {
			# extract datapoints (250Kb up and downstream)
			my $datastring = '';
			foreach (@{$ppos{$fid}}) {
				if ($_ >= ($start - 250000) && $_ <= ($stop + 250000)) {
					$datastring = $datastring . "$_"."_".$data{$fid}{$_}."_";
				}
				if ($_ > ($stop + 250000)) {
					last;
				}
			}

			# extract datapoints (250Kb up and downstream)
			#my $datastring = '';
			#open IN, $ffile;
			#my $head = <IN>;
			#while (<IN>){
			#	chomp($_);
			#	my @pieces = split(/\t/,$_);
			#	if ($pieces[1] == $chr && $pieces[2] >= ($start - 250000) && $pieces[2] <= ($stop + 250000)) {
			#		$datastring = $datastring . "$pieces[2]"."_".sprintf("%.3f",$pieces[3])."_".$pieces[5]."_";
			#	}
			#}
			#close IN;
			$datastring = substr($datastring,0,-1);
			$dbh->do("LOCK TABLE `parents_upd_datapoints` LOW_PRIORITY WRITE");
			my $store = $dbh->prepare("INSERT INTO parents_upd_datapoints (id, sid, content) VALUES (?,?,?)");
			$store->execute($lastinsert, $fid,$datastring);
			$store->finish();
			$dbh->do("UNLOCK TABLES");
		}
		# mother
		my $check = $dbh->prepare("SELECT id FROM parents_upd_datapoints WHERE id = ? AND sid =? ");
		my $nrrows = $check->execute($lastinsert, $mid);
		$check->finish();
		if ($nrrows > 0) {
			# already in database. skip
		}
		else {
			# extract datapoints (250Kb up and downstream)
			my $datastring = '';
			foreach (@{$ppos{$mid}}) {
				if ($_ >= ($start - 250000) && $_ <= ($stop + 250000)) {
					$datastring = $datastring . "$_"."_".$data{$mid}{$_}."_";
				}
				if ($_ > ($stop + 250000)) {
					last;
				}
			}

			# extract datapoints (250Kb up and downstream)
			#my $datastring = '';
			#open IN, $mfile;
			#my $head = <IN>;
			#while (<IN>){
			#	chomp($_);
			#	my @pieces = split(/\t/,$_);
			#	if ($pieces[1] == $chr && $pieces[2] >= ($start - 250000) && $pieces[2] <= ($stop + 250000)) {
			#		$datastring = $datastring . "$pieces[2]"."_".sprintf("%.3f",$pieces[3])."_".$pieces[5]."_";
			#	}
			#}
			#close IN;
			$datastring = substr($datastring,0,-1);
			$dbh->do("LOCK TABLE `parents_upd_datapoints` LOW_PRIORITY WRITE");
			my $store = $dbh->prepare("INSERT INTO parents_upd_datapoints (id, sid, content) VALUES (?,?,?)");
			$store->execute($lastinsert, $mid,$datastring);
			$store->finish();
			$dbh->do("UNLOCK TABLES");
		}
	}
}
#########################################
## SET STATUS TO COMPLETED IN DATABASE ##
#########################################
$dbh->do("UPDATE `CNVanalysis-TMP`.`UPD` SET status = 1 WHERE pid = '$filepid' AND sid = '$sid'");
$dbh->do("UPDATE projsamp SET checkedupd = 1 WHERE idsamp = '$sid' AND idproj = '$pid'"); # AND father = '$fid' AND mother = '$mid'");

$now = time - $now;
$dbh->do("INSERT INTO `GenomicBuilds`.`RuntimeStatistics` (process,chiptypeid,argument,time) VALUES ('UPD','$chiptypeid',' ','$now')");	

# clean up
system("rm -f '/tmp/$filepid.$child.Mendelian.Inconsistensies.txt'");
