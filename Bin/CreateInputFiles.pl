#!/usr/bin/perl
## keep track of runtime 
$globalnow = time;


################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;
#use Schedule::DRMAAc qw/ :all /;
use XML::Simple;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

# print the host running the job #
$hostname = `hostname`;
print "Running Job on $hostname";
##########################
# COMMAND LINE ARGUMENTS #
##########################
# D = Use specific database (mandatory if L is specified), otherwise use default.
# p = project ID 
# d = datafile (file location)
# z = is the input file in gz format
# r = random identifier
getopts('D:d:p:z:r:', \%opts);  # option are in %opts


###########################
# CONNECT TO THE DATABASE #
###########################
my $db ;
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbhgb)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass) ;
	}

	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "Assuming build ".$row[1]."\n";
	$gbsth->finish();
	$dbhgb->disconnect;	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

$dbh->{mysql_auto_reconnect} = 1;

#####################################
## CHANGE TO CNVanalysis DIRECTORY ##
#####################################
chdir($scriptdir);

####################
# ASSIGN ARGUMENTS #
####################
my $rand = $opts{'r'};
#my $settingsfile = "datafiles/CNV-WebStore/runtime/$rand.settings.xml";
my $settingsfile = "$datadir/$rand.settings.xml";
my $xml = new XML::Simple;
# this xml file contains all the parameter/method settings.
my $settings = $xml->XMLin($settingsfile);
my $datafile = $opts{'d'};
my $pid = $opts{'p'};
my $inputcols = $settings->{general}->{inputcols};

#############
# VARIABLES #
#############
my %chromhash; 
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";

#######################################
## GET PROJECT DETAILS FROM DATABASE ##
#######################################
my $query = "SELECT chiptype,chiptypeid FROM project WHERE id = '$pid'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @array = $sth->fetchrow_array();
my $chiptype = $array[0];
my $chipid = $array[1];
$sth->finish();
print "The project is using a $chiptype chip\n";

######################################
## GET SAMPLE DETAILS FROM DATABASE ##
######################################
$query = "SELECT s.chip_dnanr,s.id FROM sample s JOIN projsamp ps ON s.id = ps.idsamp WHERE idproj = '$pid'";
$sth = $dbh->prepare($query);
$sth->execute();
my %samplehash;
while (my @row = $sth->fetchrow_array()) {
	$samplehash{$row[0]} = $row[1];
}
$sth->finish();

## estimate wall time for genomic wave check & correct. (from database) 
my $walltime = '-l walltime=30:00';
if ($setwalltime == 1) {
	my $query = "SELECT AVG(time) FROM `GenomicBuilds`.`RuntimeStatistics` WHERE process = 'GenomicWave' AND chiptypeid = '$chipid'";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my ($seconds) = $sth->fetchrow_array();
	$sth->finish();
	if ($seconds > 0) {
		$walltime = '-l walltime=' . int($seconds);
	}
}
else {
	$walltime = '';
}	

#####################################
## dbh not needed anymore => close ##
#####################################
$dbh->disconnect();

# Filter variable probes if duochip)
if ($chiptype eq "HumanCNV370duo") {
	print "filtering variable probes\n";
	my %filter;
	open IN, "$scriptdir/cutoff_0.999.txt";
	while (<IN>) {
		chomp $_;
		$filter{ $_ } = "";
	}
	close IN;
	open IN, "$datafile";
	open OUT, ">/tmp/filtered.$pid.txt";
	my $header = <IN>;
	print OUT $header;
	while (<IN>) {
	my @pieces = split(/\t/,$_);
		# this assumes the probe name is first column !
		if (!exists($filter{ $pieces[0] })) {
			print OUT $_;
		}
	}
	$dump = system("cp /tmp/filtered.$pid.txt $datafile && rm -f /tmp/filtered.$pid.txt");
}

###################################
## COPY DATAFILE TO LOCAL SYSTEM ##
###################################
# copy (and extract) the file
my $localversion = "$pid.localdatafile.txt" ;
if ($opts{'z'} == 1) {
	print "Extracting datafile to /tmp/$localversion for performance\n";
	$dump = system("gunzip -c '$datafile' > '/tmp/$localversion'");
}
else {
	print "Copying datafile to /tmp/$localversion for performance\n";
	$dump = system("cp '$datafile' '/tmp/$localversion'");
}
# remove the original
$dump = system("rm -f '$datafile'");
# DOS TO UNIX 
$line = `head -n 1 /tmp/$localversion`;
my $convert = 0;
if( $line =~ m/\r\n$/ ) {
		print "Detected DOS file format: converting to UNIX\n";
	$dump = system("cd /tmp/ && dos2unix \"$localversion\" ");
}
elsif( $line =~ m/\n\r$/ ) {
		print "Detected DOS file format: converting to UNIX\n";
	$dump = system("cd /tmp/ && dos2unix \"$localversion\" ");
}
elsif( $line =~ m/\r$/ ) {
		print "Detected MAC file format: converting to UNIX\n";
		open IN, "/tmp/$localversion";
		open OUT, ">/tmp/tmpfile.txt";
		while (<IN>) {
				$_ =~ s/\r/\n/gi ;     # replace returns with newlines
				print OUT $_ ;
		}
		close OUT;
		close IN;
		$dump = `mv /tmp/tmpfile.txt /tmp/$localversion`;
}
else {
   print "Detected Unix file format\n";
}
# replace XY with X as chromosome entries, replace comma with dot as decimal seperator
$dump = system("cd /tmp/ && sed -i '1n; s/,/\./g ; s/XY/X/g' \"$localversion\" ");
## fix invalid sample names
$dump = system("cd /tmp/ && sed -i '1 s/[^-A-Za-z0-9\\.\\t]/_/g; s/Log_R_Ratio/Log R Ratio/g ; s/B_Allele_Freq/B Allele Freq/g' '$localversion'");

##########################################################
# FILL TEMPORARY PER-SAMPLE INPUT FILES  & QUEUE SAMPLES #
##########################################################
if ($localversion eq '') {
	die('No datafile specified');
}

my $headerline = `head -n 1 /tmp/$localversion`;
chomp($headerline);
my @headers = split(/\t/,$headerline);
$samples = 0;
my @fileorder ;
@fileorder =(0);
my @neededcols = split(/\|/,$inputcols);
my %datastructure;
foreach (@neededcols) {
	$datastructure{$_};
}
my %replace;
$replace{'%probename%'} = "Name";
$replace{'%chromosome%'} = "Chr";
$replace{'%position%'} = "Position";
$replace{'%samplelogr%'} = '([-\w]+).Log';
$replace{'%samplebaf%'} = '([-\w]+).B[\s\.]All';
$replace{'%samplegt%'} = '([-\w]+).GT';
# scan samples in data column headers
my $colidx = 1;
my %tmphash =();
foreach (@headers) {
	my $match = 0;
	my $colstring = $_;
	foreach (@neededcols) {
		my $thiscol = $_;
		my $regex = $replace{$thiscol};
		if ($thiscol !~ m/sample/) {
			if ($colstring =~ m/$regex/i) {
				$datastructure{$thiscol} = $colidx;
				##some of these are also set in seperate variables.
				if ($colstring =~ m/Name/i) {
					$NameCol = $colidx;
				}
				elsif ($colstring =~ m/Pos/i) {
					$PosCol = $colidx;
				}
				elsif ($colstring =~ m/chr/i) {
					$ChrCol = $colidx;
				}
				$colidx++;
				$match = 1;
				last;
			}
		}
		elsif ($colstring =~ m/^$regex/i) {
			my $s = $1;
			print "sample: $s ";
			$s =~ s/[^-A-Za-z0-9]/_/g;
			print "  => $s\n";
			if (!exists($tmphash{$s})) {
				push(@fileorder,$s);
				$samples++;
				$tmphash{$s} = 1;
			}
			$datastructure{$thiscol}{$s} = $colidx;
			$colidx++;
			$match = 1;
			last;
		}
	}	
	if ($match == 0) {
		$colidx++;
	}
}
%tmphash = ();
#################
## CHECK BUILD ##
#################
# build discriminating hash
print "Checking for the correct Genome Build using ChipID : $chipid\n";
open IN, "$scriptdir/Bin/DiscriminatingProbes.txt";
my %disc;
while (<IN>) {
	chomp($_);
	my $line = $_;
	my @p = split(/\|/,$line);
	if ($p[0] != $chipid) {
		next;
	}
	# extract current build coordinate
	$line =~ m/$db:(\d+):(\d+)(\||$)/;
	$disc{$p[1]}{'c'} = $1;
	$disc{$p[1]}{'p'} = $2;
}
close IN;
# check input file untill the first discriminating probe is found.
open IN, "/tmp/$localversion";
$dump = <IN>;
$convertbuild = 0;
while (<IN>) {
	$line = $_;
	chomp($line);
	my @p = split(/\t/,$line);
	if (exists($disc{$p[$NameCol-1]})) {
		if ($p[$PosCol-1] != $disc{$p[$NameCol-1]}{'p'} || $p[$ChrCol-1] != $disc{$p[$NameCol-1]}{'c'}) {
			$convertbuild = 1;
			last;
		}
	}
}
close IN;
if ($convertbuild == 1) {
	print "Detected incorrect build : Converting Genome Build to latest version.\n";
	`perl $scriptdir/Bin/ConvertBuild.pl -f '/tmp/$localversion' -n '$scriptdir/Bin/hg18.hg19.namelink.txt.gz' -i`;
}


# compose the pos != 0 &&  chr != 0  awk expression to discard missing mappings
my $discard = "( \$$ChrCol != \"0\" && \$$PosCol != \"0\" )";


# extract the samples and queue them for GenomicWave-estimation
my $subcols = '';
my %subcolhash;
my %samplefieldhash;
my $samplecounter = 0;
my $fieldcounter = 0;
my $totalcounter = 0;
for ($i = 1;$i<=$samples;$i++){
	my $samplename = $fileorder[$i];
	#my $sid = $samplehash{$samplename};
	#if (-e "$datadir/$pid.$sid.txt")
	#{
	#	  unlink "$datadir/$pid.$sid.txt";
	#}

		## process in batches of 30 samples.
	foreach (@neededcols) {
		my $thiscol = $_;
		if ($replace{$thiscol} =~ m/[\(\)]/) {
			## sample specific cols, unique per sample (but process as possible double anyway)
			my $colidx = $datastructure{$thiscol}{$samplename};
			if (!exists($subcolhash{$colidx})) {
				## first time seen, increase fieldcounter
				$fieldcounter++;
				## link between original col idx and tmp output col idx
				$subcolhash{$colidx} = $fieldcounter;
				## add to the cut fields for sample
				$samplefieldhash{$samplename} = $samplefieldhash{$samplename} . "$fieldcounter,";
				## add to awk extraction
				$subcols .= "\$$colidx,";
				#print "colidx : $colidx\n";
			}
			else {
				## seen before, get fieldcounter from hash, and add to sample cut fields
				$samplefieldhash{$samplename} = $samplefieldhash{$samplename} . $subcolhash{$colidx} . ",";
			}
		}
		else {
			## general shared columns. 
			my $colidx = $datastructure{$thiscol};
			if (!exists($subcolhash{$colidx})) {
				## first time seen, increase fieldcounter
				$fieldcounter++;
				## link between original col idx and tmp output col idx
				$subcolhash{$colidx} = $fieldcounter;
				## add to the cut fields for sample
				$samplefieldhash{$samplename} = $samplefieldhash{$samplename} . "$fieldcounter,";
				## add to awk extraction
				$subcols .= "\$$colidx,";
			}
			else {
				## seen before, get fieldcounter from hash, and add to sample cut fields
				$samplefieldhash{$samplename} = $samplefieldhash{$samplename} . $subcolhash{$colidx} . ",";
			}

		}
	}
	$samplecounter++;
	if ($samplecounter == 30) {
		print "Extracting subdatafile for 30 samples:";
		if (-e  "/tmp/$pid.subdata.txt") {
			unlink("/tmp/$pid.subdata.txt");
		}
		$now = time();
		$subcols = substr($subcols,0,-1);
		my $cmd = "awk ' BEGIN { FS = \"\\t\" ; OFS = \";\" } $discard {print $subcols} ' /tmp/$localversion > /tmp/$pid.subdata.txt";
		#print "$cmd\n";
		$dump = system($cmd);
		$now = time() - $now;
		printf(" %02d:%02d:%02d\n",int($now/3600),int(($now % 3600)/60),int($now % 60));	
		
		## extract seperate files and queue.
		foreach (keys(%samplefieldhash)) {
			$totalcounter++;
			my $samplename = $_;
			my $sid = $samplehash{$samplename};
			print "\tExtracting $samplename from subdata. (Sample $totalcounter of $samples)\n";
			if (-e "$datadir/$pid.$sid.txt")
			{
				  unlink "$datadir/$pid.$sid.txt";
			}
			my $cols = substr($samplefieldhash{$samplename},0,-1);
			my $cut = "cut --delimiter \";\" --output-delimiter \"	\" -f $cols /tmp/$pid.subdata.txt | fgrep --color=never -v -i nan > $datadir/$pid.$sid.txt";
			$dump = system($cut);

			## create the job script
			my $jobname = "$pid.$sid.GWAnalysis";
			my $jobfile = "$qsubscripts/$pid/$jobname.sh";
			open OUT, ">$jobfile";
			&PBSWriteHeader("OUT", $pid, $jobname, 1, 2, $walltime, $scriptdir);
			&PBSWriteCommand("OUT", "perl $scriptdir/Bin/GenomicWaveCorrection.pl -p $pid -s $sid -r $rand");
			&PBSWriteEnd("OUT");
			close OUT;

			## submit job
			my $jobid = `qsub $jobfile`;
			chomp($jobid);
			while ($jobid !~ m/^\d+\..*/) {
				sleep 1;
				$jobid = `qsub $jobfile`;
			}


		}
		
		## reset counter
		$subcols = '';
		%subcolhash = ();;
		%samplefieldhash = ();
		$samplecounter = 0;
		$fieldcounter = 0;

	}


}

## LAST BATCH !
print "Extracting subdatafile for last samples: ";
if (-e  "/tmp/$pid.subdata.txt") {
	unlink("/tmp/$pid.subdata.txt");
}
$now = time();
$subcols = substr($subcols,0,-1);
my $cmd = "awk ' BEGIN { FS = \"\\t\" ; OFS = \";\" } $discard {print $subcols} ' /tmp/$localversion > /tmp/$pid.subdata.txt";
$dump = system($cmd);
$now = time() - $now;
printf(" %02d:%02d:%02d\n",int($now/3600),int(($now % 3600)/60),int($now % 60));	
	
## extract seperate files
foreach (keys(%samplefieldhash)) {
	$totalcounter++;
	my $samplename = $_;
	my $sid = $samplehash{$samplename};
	if (-e "$datadir/$pid.$sid.txt")
	{
		  unlink "$datadir/$pid.$sid.txt";
	}
	print "\tExtracting $samplename from subdata. (Sample $totalcounter of $samples)\n";
	my $cols = substr($samplefieldhash{$samplename},0,-1);
	my $cut = "cut --delimiter \";\" --output-delimiter \"	\" -f $cols /tmp/$pid.subdata.txt | fgrep --color=never -v -i nan > $datadir/$pid.$sid.txt";
	$dump = system($cut);

	## create the job script
	my $jobname = "$pid.$sid.GWAnalysis";
	my $jobfile = "$qsubscripts/$pid/$jobname.sh";
	open OUT, ">$jobfile";
	&PBSWriteHeader("OUT", $pid, $jobname, 1, 2, $walltime, $scriptdir);
	&PBSWriteCommand("OUT", "perl $scriptdir/Bin/GenomicWaveCorrection.pl -p $pid -s $sid -r $rand");
	&PBSWriteEnd("OUT");
	close OUT;

	## submit job
	my $jobid = `qsub $jobfile`;
	chomp($jobid);
	while ($jobid !~ m/^\d+\..*/) {
		sleep 1;
		$jobid = `qsub $jobfile`;
	}

}

print "Preparing input files : Finished\n";

#######################################
## PRINT MAIN DATAFILE COL STRUCTURE ##
#######################################

## scan sample names to build column structure (upd/fam analysis)
my %extrasamples;
my $NameCol;
my $ChrCol;
my $PosCol;
open IN, "/tmp/$localversion";
my $header = <IN>;
close IN;
chomp($header);
my @pieces = split(/\t/,$header);
$j = 0;
my %idhash = ();
foreach (@pieces) {
	if ($_ =~ m/^([\w-]+).Log/ ) {
		my $sname = $1;
		my $r = $dbh->selectcol_arrayref("SELECT id FROM `sample` WHERE `chip_dnanr` = '$sname'");	
		$idhash{$sname}=$r->[0];
		$extrasamples{$r->[0]}{'LogR'} = $j +1;
		$j++;
	}
	elsif ($_ =~ m/^([\w-]+).B All/ ) {
		my $sname = $1;
		my $r = $dbh->selectcol_arrayref("SELECT id FROM `sample` WHERE `chip_dnanr` = '$sname'");	
		$idhash{$sname}=$r->[0];
		$extrasamples{$r->[0]}{'BAF'} = $j + 1;
		$j++;
	}
	elsif ($_ =~ m/^([\w-]+).GType/ ) {
		my $sname = $1;
		my $r = $dbh->selectcol_arrayref("SELECT id FROM `sample` WHERE `chip_dnanr` = '$sname'");	
		$idhash{$sname}=$r->[0];
		$extrasamples{$r->[0]}{'GType'} = $j +1;
		$j++;
	}
	elsif ($_ =~ m/^Name/ ) {
		$NameCol = $j+1;
		$j++;
	}
	elsif ($_ =~ m/^Chr/ ) {
		$ChrCol = $j+1;
		$j++;
	}
	elsif ($_ =~ m/^Position/ ) {
		$PosCol = $j+1;
		$j++;
	}
	else { $j++; }	
}	
## print out file structure
open OUT, ">$datadir/$pid.datafile.0.cols";
foreach (keys(%extrasamples)) {
	#$allsamples{$key} = "extra.$i";
	print OUT "$_=\$$NameCol,\$$ChrCol,\$$PosCol,\$".$extrasamples{$_}{'LogR'}.",\$".$extrasamples{$_}{'BAF'}.",\$".$extrasamples{$_}{'GType'}."\n";
}
close OUT;

##########################
## unlink main datafile ##
##########################
# since all samples have been extracted, file is obsolete now
unlink("/tmp/$localversion");
unlink("/tmp/$pid.subdata.txt");

## reconnect to db to update stats.
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

$dbh->{mysql_auto_reconnect} = 1;


$globalnow = time - $globalnow;
$dbh->do("INSERT INTO `GenomicBuilds`.`RuntimeStatistics` (process,chiptypeid,argument,time) VALUES ('CreateInput','$chipid','$samples','$globalnow')");
$dbh->disconnect();
exit();



###################
### SUBROUTINES ###
###################
sub PBSWriteCommand {
	my ($filehandle, $run_command) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	my $echo_command = $run_command;
	print $filehandle "echo 'Command:'\n";
	print $filehandle "echo '========'\n";
	$echo_command =~ s/"/\\"/g;
	print $filehandle "echo \"$echo_command\"\n";
	print $filehandle "$run_command\n";

}

sub PBSWriteHeader {
	my ($filehandle, $projectid, $job, $cpu, $mem, $time, $dir) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "#!/usr/bin/env bash\n";
	print $filehandle "#PBS -m a\n";
	print $filehandle "#PBS -M $adminemail\n";
	print $filehandle "#PBS -d $dir\n";
	print $filehandle "#PBS -l nodes=1:ppn=$cpu,mem=$mem"."g\n";
	print $filehandle "#PBS -N $job\n";
	print $filehandle "#PBS -o $cjo/$scriptuser/$projectid/$job.o.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -e $cjo/$scriptuser/$projectid/$job.e.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -V\n";
	if ($queuename ne '') {
		print $filehandle "#PBS -q $queuename\n";
	}
	if ($hpcaccount ne '') {
		print $filehandle "#PBS -A $hpcaccount\n";
	}
	if ($setwalltime == 1 && $time) {
		print $filehandle "#PBS $time\n";
	}
	#if ($afterok) {
	#	print $filehandle " #PBS -W depend=afterok:$afterok\n";
	#}

	print $filehandle "\necho 'Running on : ' `hostname`\n";
	print $filehandle "echo 'Start Time : ' `date`\n\n";
	print $filehandle "echo 'setting path: $path'\n";
	print $filehandle "export PATH=$path\n\n";

}

sub PBSWriteEnd {
	my $filehandle = shift;
	if (tell( $filehandle ) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "\n\necho 'End Time : ' `date`\n";
	print $filehandle "printf 'Execution Time = \%dh:\%dm:\%ds\\n' \$((\$SECONDS/3600)) \$((\$SECONDS%3600/60)) \$((\$SECONDS%60))\n";

}
