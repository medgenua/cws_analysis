#!/usr/bin/perl

## keep track of runtime 
$now = time;

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;
use XML::Simple;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

# print the host running the job #
$hostname = `hostname`;
print "Running Job on $hostname";

##########################
# COMMAND LINE ARGUMENTS #
##########################
# p = project id
# s = sample id
# l = level file qsnp
# a = method
# r = random identifier 
getopts('p:s:a:r:', \%opts);  # option are in %opts


###########################
# CONNECT TO THE DATABASE #
###########################
my $shortdb;
my $db;
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	$shortdb = $targetdb;
	$shortdb =~ s/^-//;
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbhgb)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass) ;
	}
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$shortdb = $row[0];
	$shortdb =~ s/^-//;
	print "Assuming build ".$row[1]."\n";
	$gbsth->finish();
	$dbhgb->disconnect;	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
$dbh->{mysql_auto_reconnect} = 1;

#####################################
## CHANGE TO CNVanalysis DIRECTORY ##
#####################################
chdir($scriptdir);

####################
# ASSIGN ARGUMENTS #
####################
my $sid = $opts{'s'};
my $pid = $opts{'p'};
my $method = $opts{'a'};
my $rand = $opts{'r'};

##################################
## READ IN METHOD CONFIGURATION ##
##################################
my $xml = new XML::Simple;
my $settingsfile = "$datadir/$rand.settings.xml";
my $settings = $xml->XMLin($settingsfile);
my $configuration = $settings->{$method}->{configuration};
#############
# VARIABLES #
#############
my %chromhash; 
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";

#######################################
## GET PROJECT DETAILS FROM DATABASE ##
#######################################
my $query = "SELECT chiptype,chiptypeid FROM project WHERE id = '$pid'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @array = $sth->fetchrow_array();
my $chiptype = $array[0];
my $chiptypeid = $array[1];
$sth->finish();

######################################
## GET SAMPLE DETAILS FROM DATABASE ##
######################################
$query = "SELECT chip_dnanr, gender FROM sample WHERE id = '$sid'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @row = $sth->fetchrow_array();
my $samplename = $row[0];
my $gender = $row[1];
$sth->finish();
print "Analysing sample '$samplename' with $method on chip $chiptype\n";

######################
## RUN SEGMENTATION ##
######################
$file = "$datadir/$pid".".$sid.txt";
$runtimedir = "$scriptdir/Analysis_Methods/Runtime/";

# replacement placeholders
$replace{'runtimedir'} = $runtimedir;
$replace{'samplename'} = $samplename;
$replace{'gender'} = $gender;
$replace{'inputfile'} = $file;
$replace{'build'} = $shortdb;
$replace{'pid'} = $pid;
$replace{'sid'} = $sid;
$replace{'chiptype'} = $chiptype;
$replace{'rand'} = $rand;
$replace{'datadir'} = $datadir;
$replace{'scriptdir'} = $scriptdir;
#####################
## compose command ##
#####################
my $command = "cd $scriptdir/Analysis_Methods/Runtime/$method/ && ";

my $launcher = $configuration->{general}->{launcher};
while ($launcher =~ m/%(.*?)%/) {
	$item = $1;
	$launcher =~ s/^(.*?)%($item)%(.*)/$1$replace{$item}$3/;
}
$command .= $launcher;
my $dash = $configuration->{general}->{dash};
# argument-value pairs
for my $key (keys(%{$configuration->{input_values}})) {
	my $val = $configuration->{input_values}->{$key};
	while ($val =~ m/%(.*?)%/) {
		$item = $1;
		$val =~ s/^(.*?)%($item)%(.*)/$1$replace{$item}$3/;
	}
	$command .= " $dash$key $val";
} 
# boolean arguments
for my $key (keys(%{$configuration->{input_boolean}})) {
	$command .= " $dash$key";
} 
# conditional arguments
for my $key (keys(%{$configuration->{input_conditional}})) {
	# each key is in %replace, see if it meets condition (in array) in "value" for index idx.
	foreach (@{$configuration->{input_conditional}->{$key}}) {
		if ($replace{$key} eq $_->{value}) {
			#condition met. add arguments from "arguments" field
			for my $subkey (keys(%{$_->{arguments}})) {
				# check for value
				my $subval = '';
				#if (scalar keys %{$_->{arguments}->{$subkey}}) {
				# this regex checks if the value is hash (true for boolean nodes (no value present, <node /> style)
				if ($_->{arguments}->{$subkey} !~ /hash/i) {
					$subval = $_->{arguments}->{$subkey};
				}
				# replace placeholders
				while ($subval =~ m/%(.*?)%/) {
					$item = $1;
					$subval =~ s/^(.*?)%($item)%(.*)/$1$replace{$item}$3/;
				}
				# add to command
				$command .= " $dash$subkey $subval";
			} 
		}
	} 
}
# additional arguments (input files...). if value=replace => replace by %replace{item}
for my $key (keys(%{$configuration->{input_nodash}})) {
	if ($configuration->{input_nodash}->{$key} eq 'replace') {
		$key = $replace{$key};
	}
	$command .= " $key";
} 
##############################
## disconnect from database ##
##############################
$dbh->disconnect();


# quantisnp mcr settings
if ($method eq 'QuantiSNPv2') {
	$command = 'export MCR_CACHE_ROOT=/tmp/mcr_$PBS_JOBID; mkdir /tmp/mcr_$PBS_JOBID; '.$command.'; rm -Rf /tmp/mcr_$PBS_JOBID';
}


##################
## RUN ANALYSIS ##
##################

print "Starting Analysis\n";
print "\n\n$command\n\n";

my $failed = 0;
system($command) == 0 or $failed = 1;

if (not $failed) {
	###########################################
	## COPY OUTPUT FILES TO TARGET LOCATIONS ##
	###########################################
	my $cnvfile = $configuration->{output}->{cnv_file};
	while ($cnvfile =~ m/%(.*?)%/) {
		$item = $1;
		$cnvfile =~ s/^(.*?)%($item)%(.*)/$1$replace{$item}$3/;
	}
	system("cp '$cnvfile' '$datadir/$pid.$sid.$method.cnv'");

	if (exists($configuration->{output}->{qc_file})) {
		$qcfile = $configuration->{output}->{qc_file};
		while ($qcfile =~ m/%(.*?)%/) {
			$item = $1;
			$qcfile =~ s/^(.*?)%($item)%(.*)/$1$replace{$item}$3/;
		}
		system("cp '$qcfile' '$datadir/$pid.$sid.$method.qc'");
	}
}


####################################
## INSERT COMPLETION NOTICE IN DB.##
####################################
$now = time - $now;
if (not $failed) {
	print "Analysis finished\n";
}
else {
	print "Analysis FAILED\n";
}
printf("\n\nTotal running time: %02d:%02d:%02d\n\n", int($now / 3600), int(($now % 3600) / 60), int($now % 60));


# first reconnect  
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
# retry few times if needed (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 5;
	$i++;
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
$dbh->{mysql_auto_reconnect} = 1;
if (not $failed) {
	$dbh->do("INSERT INTO `CNVanalysis-TMP`.`Segmentation` (pid, sid, algo) VALUES ('$pid', '$sid', '$method')");
	$dbh->do("INSERT INTO `GenomicBuilds`.`RuntimeStatistics` (process,chiptypeid,argument,time) VALUES ('Segmentation','$chiptypeid','$method','$now')");
}
else {
	$dbh->do("INSERT INTO `CNVanalysis-TMP`.`SegmentationErrors` (pid, sid, algo) VALUES ('$pid', '$sid', '$method')");
}

print "\n\nJob finished\n";

exit();

