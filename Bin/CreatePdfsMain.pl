#!/usr/bin/perl

################
# LOAD MODULES #
################
use Number::Format;
use DBI;
use Getopt::Std;
#use Schedule::DRMAAc qw/ :all /;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

# print the host running the job #
$hostname = `hostname`;
print "Running Job on $hostname";

##########################
# COMMAND LINE ARGUMENTS #
##########################
# D = Use specific database (mandatory if L is specified), otherwise use default.
# p = projectid
getopts('D:p:', \%opts);  # option are in %opts
my $pid;
my $db;
if ($opts{'p'}) {
	$pid = $opts{'p'};
}
else {
	die('No Project specified');
}
if ($opts{'D'}) {
	$db = $opts{'D'};
}
else {
	die('No Database specified');
}

#########################
## Connect to database ##
#########################
$connectionInfo="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfo,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfo,$userid,$userpass) ;
}

$dbh->{mysql_auto_reconnect} = 1;

#####################################
## CHANGE TO CNVanalysis DIRECTORY ##
#####################################
chdir($scriptdir);

###############
## VARIABLES ##
###############
@nrs=(1..22);
push(@nrs,'X');
my %chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y"; 

##########################
## DUMP CNVS IN PROJECT ##
##########################
# for usage in R
my $rowcache;
my $max;
$query = "SELECT sample, chr, start, stop, size, cn FROM aberration WHERE idproj = '$pid'";
$sth = $dbh->prepare($query);
$sth->execute();
my $rows = $sth->rows();
if ($rows < 10000) {
	$max = $rows;
}
else {
	$max = 10000;
}
print "Dumping $rows CNVs to a temporary file\n";
#open OUT, ">/var/tmp/datafiles/CNV-WebStore/runtime/$pid.cnvlist.txt";
open OUT, ">$datadir/$pid.cnvlist.txt";
print OUT "sid\tchr\tstart\tstop\tsize\tcn\n";
while (my $aref = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
	my $sid = $aref->[0];
	my $chr = $aref->[1];
	my $start = $aref->[2];
	my $stop = $aref->[3];
	my $size = $aref->[4];
	my $cn = $aref->[5];
	print OUT "$sid\t$chromhash{$chr}\t$start\t$stop\t$size\t$cn\n";
}
$sth->finish();
close OUT;

## get chiptypeid 
my $query = "SELECT chiptypeid FROM project WHERE id = '$pid'";
my $sth = $dbh->prepare($query);
$sth->execute();
my ($chipid) = $sth->fetchrow_array();
$sth->finish();

## estimate wall time for PDF 
my $walltime = '-l walltime=30:00';
if ($setwalltime == 1) {
	my $query = "SELECT AVG(time) FROM `GenomicBuilds`.`RuntimeStatistics` WHERE process = 'PDFsingle' AND chiptypeid = '$chipid'";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my ($seconds) = $sth->fetchrow_array();
	$sth->finish();
	if ($seconds > 0) {
		$walltime = '-l walltime=' . int($seconds);
		#print "Setting estimated wall time for INput file creation as $walltime 
	}
}
else {
	$walltime = '';
}	 

##############################
## Queue samples in project ##
##############################
print "Queueing samples for PDF generation\n";
$query = "SELECT idsamp FROM projsamp WHERE idproj = '$pid'";
$sth = $dbh->prepare($query);
$sth->execute(); 
while (my @res = $sth->fetchrow_array()) {
	my $sid = $res[0];
	$dbh->do("INSERT INTO `CNVanalysis-TMP`.`WholeGenomePlots` (pid, sid, status) VALUES ('$pid', '$sid', '0')");

	## create the job script
	my $jobname = "$pid.$sid.CreatePDF";
	my $jobfile = "$qsubscripts/$pid/$jobname.sh";
	open OUT, ">$jobfile";
	&PBSWriteHeader("OUT", $pid, $jobname, 1, 1, $walltime, $scriptdir);
	&PBSWriteCommand("OUT", "perl $scriptdir/Bin/CreatePdfsSingle.pl -D $db -p $pid -s $sid");
	&PBSWriteEnd("OUT");
	close OUT;

	## submit job
	my $jobid = `qsub $jobfile`;
	chomp($jobid);
	while ($jobid !~ m/^\d+\..*/) {
		sleep 1;
		$jobid = `qsub $jobfile`;
	}


}
$sth->finish();

#############################
## WAIT FOR PDFs TO FINISH ##
#############################
print "Waiting For PDFs, BAFSEG to finish\n";
## BAFSEG is queued from FormatResults. Can still be running. 
# could do this using the drmaa, but I need to update tables and mv files anyway.
$continue = 1;
while ($continue == 1) {
	my $todo = 0;
	# plots
	$sth = $dbh->prepare("SELECT sid, status FROM `CNVanalysis-TMP`.`WholeGenomePlots` WHERE pid = '$pid'");
	$sth->execute();
	while (my @row = $sth->fetchrow_array()) {
		if ($row[1] == 1) {
			## finished
			my $currsid = $row[0];
			#my $pdffile = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.$currsid.WGP.pdf";
			my $pdffile = "$datadir/$pid.$currsid.WGP.pdf";
			$fullfile = "WGP/WGP_$pid"."_$currsid.pdf";
			my $sql = "INSERT INTO plots (idsamp, idproj, filename) VALUES ('$currsid', '$pid', '$fullfile')";
			$dbh->do($sql);
			$copystring = "cp -f '$pdffile' '$plotdir/$fullfile' && chmod 777 '$plotdir/$fullfile'";
			my $dump = system("$copystring");
			unlink("$pdffile");
			print "Plots for sample $currsid are finished\n";
			## delete from todo table
			$dbh->do("DELETE FROM `CNVanalysis-TMP`.`WholeGenomePlots` WHERE pid = '$pid' AND sid = '$currsid'");
		}
		else {
			$todo++;
		}
	}
	$sth->finish();
	#bafseg 
	$sth = $dbh->prepare("SELECT sid, status FROM `CNVanalysis-TMP`.`BAFSEG` WHERE pid = '$pid'");
	$sth->execute();
	while (my @row = $sth->fetchrow_array()) {
		if ($row[1] == 1) {
			## finished
			my $currsid = $row[0];
			## delete from todo table
			$dbh->do("DELETE FROM `CNVanalysis-TMP`.`BAFSEG` WHERE pid = '$pid' AND sid = '$currsid'");
		}
		else {
			$todo++;
		}
	}
	$sth->finish();
	## items left?
	if ($todo > 0) {
		sleep 15;
	}
	else {
		$continue = 0;
	}
}

##############
## CLEAN UP ##
##############
system("rm -f $datadir/$pid.*");
$dbh->do("DELETE FROM `CNVanalysis-TMP`.`WholeGenomePlots` WHERE pid = '$pid'");
$dbh->do("DELETE FROM `CNVanalysis-TMP`.`BAFSEG` WHERE pid = '$pid'");
$dbh->do("DELETE FROM `CNVanalysis-TMP`.`FamilyAnalysis` WHERE pid = '$pid'");
$dbh->do("DELETE FROM `CNVanalysis-TMP`.`Formatting` WHERE pid = '$pid'");
$dbh->do("DELETE FROM `CNVanalysis-TMP`.`UPD` WHERE pid = '$pid'");
$dbh->do("DELETE FROM `CNVanalysis-TMP`.`Segmentation` WHERE pid = '$pid'");

############################################
## UPDATE OCCURENCES IN ABERRATIONS TABLE ##
############################################
my $pquery = "SELECT collection FROM project WHERE id = $pid";
my $psth = $dbh->prepare($pquery);
$psth->execute();
@row = $psth->fetchrow_array();
$collection = $row[0];

if ($collection eq 'control') {
	my $upcmd = "COLUMNS=256 && ps aux | grep update_control_occ.pl";
	my $upres = `$upcmd`;
	my @uplines = split("\\n",$upres);
	$run = 1;
	foreach(@uplines) {
		unless ($_ =~ m/grep|create_pdfs/){
			$run = 0;
		}
	}
	if ($run == 1 ) {
		my $command = $maintenancedir."update_control_occ.pl";
		my $dump = `$command`;
	}
}	
else {

	my $upcmd = "COLUMNS=256 && ps aux | grep update_occurences.pl";
	my $upres = `$upcmd`;
	my @uplines = split("\\n",$upres);
	$run = 1;
	foreach(@uplines) {
		unless ($_ =~ m/grep|create_pdfs/){
			$run = 0;
		}
	}
	if ($run == 1 ) {
		my $command = $maintenancedir."update_occurences.pl";
		my $dump = `$command`;
	}
}	

## SET THE RUNTIME OUTPUT TO READABLE BY ALL
system("chmod -R 755 '$cjo/$scriptuser/$pid'");

exit();



###################
### SUBROUTINES ###
###################
sub PBSWriteCommand {
	my ($filehandle, $run_command) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	my $echo_command = $run_command;
	print $filehandle "echo 'Command:'\n";
	print $filehandle "echo '========'\n";
	$echo_command =~ s/"/\\"/g;
	print $filehandle "echo \"$echo_command\"\n";
	print $filehandle "$run_command\n";

}

sub PBSWriteHeader {
	my ($filehandle, $projectid, $job, $cpu, $mem, $time, $dir) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "#!/usr/bin/env bash\n";
	print $filehandle "#PBS -m a\n";
	print $filehandle "#PBS -M $adminemail\n";
	print $filehandle "#PBS -d $dir\n";
	print $filehandle "#PBS -l nodes=1:ppn=$cpu,mem=$mem"."g\n";
	print $filehandle "#PBS -N $job\n";
	print $filehandle "#PBS -o $cjo/$scriptuser/$projectid/$job.o.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -e $cjo/$scriptuser/$projectid/$job.e.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -V\n";
	if ($queuename ne '') {
		print $filehandle "#PBS -q $queuename\n";
	}
	if ($hpcaccount ne '') {
		print $filehandle "#PBS -A $hpcaccount\n";
	}
	if ($setwalltime == 1 && $time) {
		print $filehandle "#PBS $time\n";
	}
	#if ($afterok) {
	#	print $filehandle " #PBS -W depend=afterok:$afterok\n";
	#}

	print $filehandle "\necho 'Running on : ' `hostname`\n";
	print $filehandle "echo 'Start Time : ' `date`\n\n";
	print $filehandle "echo 'setting path: $path'\n";
	print $filehandle "export PATH=$path\n\n";
	print $filehandle "echo $PATH\n\n";
}

sub PBSWriteEnd {
	my $filehandle = shift;
	if (tell( $filehandle ) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "\n\necho 'End Time : ' `date`\n";
	print $filehandle "printf 'Execution Time = \%dh:\%dm:\%ds\\n' \$((\$SECONDS/3600)) \$((\$SECONDS%3600/60)) \$((\$SECONDS%60))\n";

}
