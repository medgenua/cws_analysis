#!/usr/bin/perl

## keep track of runtime 
$now = time;


################
# LOAD MODULES #
################
use Number::Format;
use DBI;
use Getopt::Std;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

# print the host running the job #
$hostname = `hostname`;
print "Running Job on $hostname";

##########################
# COMMAND LINE ARGUMENTS #
##########################
# D = Use specific database (mandatory if L is specified), otherwise use default.
# p = projectid
getopts('D:p:s:', \%opts);  # option are in %opts
my $pid;
my $sid;
my $db;
if ($opts{'p'}) {
	$pid = $opts{'p'};
}
else {
	die('No Project specified');
}
if ($opts{'s'}) {
	$sid = $opts{'s'};
}
else {
	die('No Sample specified');
}
if ($opts{'D'}) {
	$db = $opts{'D'};
}
else {
	die('No Database specified');
}

#########################
## Connect to database ##
#########################
$connectionInfo="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfo,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfo,$userid,$userpass) ;
}

$dbh->{mysql_auto_reconnect} = 1;


########################
## GET SAMPLE DETAILS ##
########################
my $query = "SELECT chip_dnanr, gender FROM sample WHERE id = '$sid'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @row = $sth->fetchrow_array();
my $samplename = $row[0];
my $gender = $row[1];
$sth->finish();
$query = "SELECT idx FROM projsamp WHERE idproj = '$pid' AND idsamp = '$sid'";
$sth = $dbh->prepare($query);
$sth->execute();
@row = $sth->fetchrow_array();
$index = $row[0];
$sth->finish();
$query = "SELECT chiptypeid FROM project WHERE id = '$pid' ";
$sth = $dbh->prepare($query);
$sth->execute();
@row = $sth->fetchrow_array();
$chiptypeid = $row[0];

###########################
## GUESS PDFJOIN VERSION ##
###########################
my $lines = `pdfjoin --help | grep '\\\-\\\-orient' | wc -l`;
chomp($lines);
if ($lines > 0) {
        print "Using old style pdfjoin arguments\n";
	$join = "cd /tmp/ && pdfjoin --fitpaper false --paper a4paper --orient landscape --outfile $datadir/$pid.$sid.WGP.pdf";
}
else {
        print "Using new style pdfjoin/pdfjam arguments\n";
	$join = "cd /tmp/ && pdfjoin --paper a4paper --landscape --outfile $datadir/$pid.$sid.WGP.pdf -- ";
}

###############################
## CREATE WHOLE GENOME PLOTS ##
###############################
$filename = "$datadir/$pid.$sid.txt";
my $command = "cd $scriptdir && Rscript Bin/plots.R '$filename' '$samplename' '$pid' '$index' '$gender' '$sid' '$datadir'";
system($command);

##################################
## JOIN THE RESULTING PNG FILES ##
##################################
for ($i = 1; $i<=22; $i++) {
	$command = "cd /tmp && convert $pid.$sid"."_chr_$i.png $pid.$sid"."_chr_$i.pdf";
	#print "$command\n\n";
	system($command);
	$join .= " $pid.$sid"."_chr_$i.pdf";
}
# add X
$command = "cd /tmp/ && convert $pid.$sid"."_chr_X.png $pid.$sid"."_chr_X.pdf"; 
system($command);
$join .= " $pid.$sid"."_chr_X.pdf";
# add Y for male
if ($gender eq 'Male') {
	$command = "cd /tmp/ && convert $pid.$sid"."_chr_Y.png $pid.$sid"."_chr_Y.pdf"; 
	system($command);
	$join .= " $pid.$sid"."_chr_Y.pdf";
}
# run join command
$join .= " $outfile";
system($join);

##############
## CLEAN UP ##
##############
system("rm -f /tmp/$pid.$sid.*");
system("rm -f /tmp/$pid.$sid"."_chr*");

###############################
## UPDATE STATUS IN DATABASE ##
###############################
$dbh->do("UPDATE `CNVanalysis-TMP`.`WholeGenomePlots` SET status = 1 WHERE sid = '$sid' AND pid = '$pid'");

$now = time - $now;
print "Job finished\n";
printf("\n\nTotal running time: %02d:%02d:%02d\n\n", int($now / 3600), int(($now % 3600) / 60),
int($now % 60));
$dbh->do("INSERT INTO `GenomicBuilds`.`RuntimeStatistics` (process,chiptypeid,argument,time) VALUES ('PDFsingle','$chiptypeid',' ','$now')");

