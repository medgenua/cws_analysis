#!/usr/bin/perl

################
# LOAD MODULES #
################
use Getopt::Std;
use DBI;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

# connect to db
my $db ;
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbhgb)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass) ;
	}

	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "Converting input file to build ".$row[1]."\n";
	$gbsth->finish();
	$dbhgb->disconnect;	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

$dbh->{mysql_auto_reconnect} = 1;


##$|++; # disable output buffer

##################################
# SET EXPECTED ROWS PER CHIPTYPE #
##################################
my %nrs = ();
$nrs{1199188}{'id'} = 8;
$nrs{1199188}{'name'} = 'Human1M-DuoV3';
$nrs{370405}{'id'} = 1;
$nrs{370405}{'name'} = 'HumanCNV370duo';
$nrs{373398}{'id'} = 2;
$nrs{373398}{'name'} = 'HumanCNV370quad';
$nrs{294378}{'id'} = 3;
$nrs{294378}{'name'} = 'HumanCyto12v1.0';
$nrs{301233}{'id'} = 4;
$nrs{301233}{'name'} = 'HumanCyto12v2.0';
$nrs{620902}{'id'} = 5;
$nrs{620902}{'name'} = 'Human610-quad';
$nrs{657367}{'id'} = 6;
$nrs{657367}{'name'} = 'Human660W-quad';
$nrs{1140420}{'id'} = 9;
$nrs{1140420}{'name'} = 'HumanOmni1-QuadV1';
$nrs{299141}{'id'} = 10;
$nrs{299141}{'name'} = 'HumanCyto12v2.1';
$nrs{733203}{'id'} = 11;
$nrs{733203}{'name'} = 'HumanOmniExpress12v1.0';
$nrs{2449975}{'id'} = 12;
$nrs{2449975}{'name'} = 'HumanOmni2.5-4v1';

##########################
# COMMAND LINE ARGUMENTS #
##########################
#print "\n";
#print "Usage:\n";
#print "######\n";
#print "\n";
#print "\t-f = input datapoints file\n";
#print "\t-n = Name Link file (needed for HumanOmni2.5)\n";
#print "\t-p = Positions file (probelocations.csv.gz)\n";
#print "\t-o = Output file name.\n";
#print "\t-c = chiptype id from database ( optional ):\n";
#foreach (keys %nrs) {
#	print "\t\t$nrs{$_}{'name'} => $nrs{$_}{'id'}\n";
#}
#print "\n\n";

###########
## START ##
###########
my $infile;
# f : input file => only .txt files accepted!
# n : namelink file, needed for Omni chips.
# p : position file (obsolete, fetched from database) 
# c : chiptype (numeric id from db)
# o : output file
# i : inplace : overwrite the input file
getopts('f:n:p:c:o:i',\%opts);
if ($opts{'f'}) {
	$infile = $opts{'f'};
}
else {
	die('No input file specified');
}
if ($opts{'o'}) {
	$outfile = $opts{'o'};
}
elsif (!$opts{'i'}) {
	die('No output file specified');
}
else {
	print "Overwriting input file\n";
}
##########################
## INITIALISE VARIABLES ##
##########################
@nrs=(1..22);
push(@nrs,'X');
my %chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "MT"} = 25;
$chromhash{ "M" } = 25;
$chromhash{ "XY" } = 23;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "MT"; 
my %newprobes = ();
my %namelink = ();
my ($chipcol, $chrcol, $poscol, $namecol);
################################################
# CHECK FILE TYPE FOR LINE ENDINGS AND CONVERT #
################################################
print "Checking data file line-endings\n";
$line = `head -n 1 $infile`;
if( $line =~ m/\r\n$/ ) { 
	print " - Detected DOS file format: converting to UNIX\n"; 
	`dos2unix $infile`;
} 
elsif( $line =~ m/\n\r$/ ) { 
	print " - Detected DOS file format: converting to UNIX\n";
	`dos2unix $infile`;
} 
elsif( $line =~ m/\r$/ ) { 
	print " - Detected MAC file format: converting to UNIX\n"; 
	open IN, $infile;
	open OUT, ">tmpfile.txt";
	while (<IN>) {
        	$_ =~ s/\r/\n/gi ;     # replace returns with newlines
        	print OUT $_ ;
	}
	close OUT;
	close IN;
	my $dump = `mv tmpfile.txt $infile.txt`;
} 
else { 
   print " - Detected Unix file format\n"; 
}



##############
## CHIPTYPE ##
##############
print "Checking Chiptype\n";
if ($opts{'c'}) {
	$chiptype = $opts{'c'};
}
else {
	print " - Guessing Chiptype based on number of probes\n";
	$lines = `wc -l '$infile'`;
	chomp($lines);
	$lines =~ s/(\d+)\s$infile/$1/;
	print " - Found $lines lines\n";
	if ($nrs{$lines}) {
		print "\t=>using Chiptype $nrs{$lines}{'name'}\n";
		$chiptype = $nrs{$lines}{'id'};
	}
	else {
		$chiptype = 0;
	}
}
## human omni 2.5 specific part
if ($chiptype == 12) {
	print "Checking for probe name-link file type\n";
	if ($opts{'n'}) {
		$namefile = $opts{'n'};
		if(`gzip -t $namefile` == 0){
			open(IN,"gunzip -c $namefile | ") || die("Could not open NameLink file\n");
		}
		else {
			open(IN, $namefile) || die("Could not open NameLink file\n");
		}
		while (<IN>) {
			chomp($_);
			my @p = split(/\t|;|,/,$_);
			$namelink{$p[0]} = $p[1];
		}
		close IN;
	}
	else {
		die("NameLink file is mandatory for this chiptype!\n");
	}
}


#################################
## probelocations in new build ##
#################################
print "Loading probe locations from database\n";
if ($chiptype ne '' && $chiptype ne '0') {
	$query = "SELECT name, chromosome, position FROM probelocations WHERE chiptype = '$chiptype'";
}
else {
	$query = "SELECT name, chromosome, position FROM probelocations";
}	
if ($chipcol eq '' && $chiptype > 0) {
	$chiptype = 0;
	print "  NOTE : No Chiptype column found in positions file. Ignoring specified chiptype.\n";
} 
my $sth = $dbh->prepare($query);
$rows = $sth->execute();
if ($rows > 10000) {
	$max = 10000;
}
else {
	$max = $rows;
}
my $rowcache;
while (my $row = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
	
	$newprobes{$row->[0]}{'c'} = $row->[1];
	$newprobes{$row->[0]}{'p'} = $row->[2];
}
$sth->finish();
print " - Loaded " . keys(%newprobes) . " probe locations for new build\n";
######################
## GET Column names ##
######################
my $headerline = `head -n 1 $infile`;
chomp($headerline);
my @headers = split(/\t|;|,/,$headerline);
$i = 0;
foreach (@headers) {
	if ($_ =~ m/^Name/i ) {
		$NameCol = $i;
		$i++;
	}
	elsif ($_ =~ m/^Chr/i ) {
		$ChrCol = $i;
		$i++;
	}
	elsif ($_ =~ m/^Position/i ) {
		$PosCol = $i;
		$i++;
	}
	else { $i++; }	
}
################
## OPEN FILES ##
################
print "Converting input file coordinates\n";
my $rand = int(rand(10000));
while (-e "/tmp/$rand.tmp") {
	$rand = int(rand(10000));
}
open TMP, ">/tmp/$rand.tmp";
print TMP "$headerline\n";
open IN, $infile;
$failed = 0;
$head = <IN>;
while (<IN>) {
	chomp($_);
	my @p = split(/\t|;|,/,$_);
	my $name = $p[$NameCol];
	if ($namelink{$name}) {
		# get new name for HumanOmni2.5 in hg19 !
		$name = $namelink{$name};
	}
	if (!$newprobes{$name}) {
		print "\t => Probe $name is not present in new build.\n";
		$failed++;
		next;
	}
	$p[$ChrCol] = $chromhash{ $newprobes{$name}{'c'} };#$newchr;
	$p[$PosCol] = $newprobes{$name}{'p'};#$newpos;
	print TMP join("\t",@p) . "\n";
}
close IN;
close TMP; 
if ($opts{'i'}) {
	print " - Moving tmp output file to $infile\n";
	system("mv /tmp/$rand.tmp $infile"); 
}
else {
	print " - Moving output file to $outfile\n";
	system("mv /tmp/$rand.tmp $outfile"); 
}
print " - Done, $failed items were removed from the input file.\n";
exit();

