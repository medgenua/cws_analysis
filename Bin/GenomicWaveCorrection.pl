#!/usr/bin/perl

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;
#use Schedule::DRMAAc qw/ :all /;
use XML::Simple;

# keep track of runtime 
$now = time;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

# print the host running the job #
$hostname = `hostname`;
print "Running Job on $hostname";


##########################
# COMMAND LINE ARGUMENTS #
##########################
# p = project id
# s = sample id
# l = level file qsnp
# h = Detect homozygous stretches with plink
getopts('p:s:r:', \%opts);  # option are in %opts


###########################
# CONNECT TO THE DATABASE #
###########################
my $shortdb;
my $db ;
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	$shortdb = $targetdb;
	$shortdb =~ s/^-//;
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbhgb)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass) ;
	}

	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$shortdb = $row[0];
	$shortdb =~ s/^-//;
	print "Assuming build ".$row[1]."\n";
	$gbsth->finish();
	$dbhgb->disconnect;	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
$dbh->{mysql_auto_reconnect} = 1;

#####################################
## CHANGE TO CNVanalysis DIRECTORY ##
#####################################
chdir($scriptdir);

####################
# ASSIGN ARGUMENTS #
####################
my $pid = $opts{'p'};
my $sid = $opts{'s'};
my $rand = $opts{'r'};
#if ($opts{'l'} eq 'hd') {
#	$hmmfile = 'hhcustom.hmm';
#	$levelfile = 'levels-hd.dat';
#}
#else {
#	$hmmfile = 'hhall.hmm';
#	$levelfile = 'levels.dat';
#}

#############
# VARIABLES #
#############
my %chromhash; 
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";

######################################
## GET SAMPLE DETAILS FROM DATABASE ##
######################################
my $query = "SELECT chip_dnanr, gender FROM sample WHERE id = '$sid'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @row = $sth->fetchrow_array();
my $samplename = $row[0];
my $gender = $row[1];
$sth->finish();

################################
## GET CHIPTYPE FROM DATABASE ##
################################
$query = "SELECT chiptype,chiptypeid FROM project WHERE id = '$pid'";
$sth = $dbh->prepare($query);
$sth->execute();
@row = $sth->fetchrow_array();
my $chiptype = $row[0];
my $chipid = $row[1];
$sth->finish();

######################
## READ IN SETTINGS ##
######################
#my $settingsfile = "datafiles/CNV-WebStore/runtime/$rand.settings.xml";
my $settingsfile = "$datadir/$rand.settings.xml";
my $xml = new XML::Simple;
# this xml file contains all the parameter/method settings.
my $settings = $xml->XMLin($settingsfile);
my $inputcols = $settings->{general}->{inputcols};
my $methods = $settings->{general}->{methods};
my $plink = $settings->{general}->{plink};
###################################
## perform GC check & correction ##
###################################
if ($inputcols =~ m/%probename%/ && $inputcols =~ m/%chromosome%/ && $inputcols =~ m/%position%/ && $inputcols =~ m/%samplelogr%/ && $inputcols =~ m/%samplebaf%/) {
	#$file = "$scriptdir/datafiles/CNV-WebStore/runtime/$pid".".$sid.txt";
	$file = "$datadir/$pid".".$sid.txt";
	print "Command:\n";
	print "cd $scriptdir/Analysis_Methods/Runtime && ./PennCNVas/genomic_wave.pl -calwf $file 2>&1\n";
	$output = `cd $scriptdir/Analysis_Methods/Runtime && ./PennCNVas/genomic_wave.pl -calwf $file 2>&1`;
	print "output\n";
	print "$output\n";
	$output =~ m/\(GCWF\) is (.+), WF is (.+), GC correlation is (.+)$/;
	$GCWF = $1;
	$WF = $2;
	$CORR = $3;
	if ($WF >= 0.03 || $WF <= -0.03) {
		$WFprint = sprintf("%.4f", $WF );
		print "Sample $samplename has a WF of $WFprint, performing GC-correction.\n";
		`cd $scriptdir/Analysis_Methods/Runtime && ./PennCNVas/genomic_wave.pl -adjust -gcmodelfile PennCNVas/lib/$chiptype.$shortdb.gcmodel -suffix corrected $file`;
		unlink($file);
		`mv $file.corrected $file`;
		$query = "UPDATE projsamp SET INIWAVE = '$WFprint' WHERE idsamp = '$sid' AND idproj = '$pid'";
		#print "$query\n";
		$dbh->do($query);
	}
}
else {
	print "Not all columns needed for Genomic Wave Correction are present (see documentation of PennCNV). Skipping Normalisation.\n";
}

my @algonames = split(/;/,$methods); #qw(QuantiSNPv2 PennCNVas PennCNVx VanillaICE);
foreach $name (@algonames) { 
	## estimate wall time for Segmentation 
	my $walltime = ' -l walltime=30:00';
	if ($setwalltime == 1) {
		my $query = "SELECT AVG(time) FROM `GenomicBuilds`.`RuntimeStatistics` WHERE process = 'Segmentation' AND chiptypeid = '$chipid' AND argument = '$name'";
		my $sth = $dbh->prepare($query);
		$sth->execute();
		my ($seconds) = $sth->fetchrow_array();
		$sth->finish();
		if ($seconds > 0) {
			$walltime = ' -l walltime=' . int($seconds);
			#print "Setting estimated wall time for INput file creation as $walltime 
		}
	}
	else {
		$walltime = '';
	}

	print "Submitted sample '$samplename' for $name analysis\n";

	## create the job script
	my $jobname = "$pid.$sid.Segment.$name";
	my $jobfile = "$qsubscripts/$pid/$jobname.sh";
	open OUT, ">$jobfile";
	if ($name eq 'QuantiSNPv2') {
		&PBSWriteHeader( "OUT", $pid, $jobname, 2, 4, $walltime, $scriptdir );
	}
	else {
		&PBSWriteHeader("OUT", $pid, $jobname, 1, 2, $walltime, $scriptdir);
	}
	&PBSWriteCommand("OUT", "perl $scriptdir/Bin/RunSegmentation.pl -p $pid -s $sid  -a $name -r $rand");
	&PBSWriteEnd("OUT");
	close OUT;

	## submit job
	my $jobid = `qsub $jobfile`;
	chomp($jobid);
	while ($jobid !~ m/^\d+\..*/) {
		sleep 1;
		$jobid = `qsub $jobfile`;
	}


}
if ($plink == 1) {
	$name = "PlinkHomoz";
	## estimate wall time for Segmentation  
	my $walltime = ' -l walltime=30:00';
	if ($setwalltime == 1) {
		my $query = "SELECT AVG(time) FROM `GenomicBuilds`.`RuntimeStatistics` WHERE process = 'Segmentation' AND chiptypeid = '$chipid' AND argument = '$name'";
		my $sth = $dbh->prepare($query);
		$sth->execute();
		my ($seconds) = $sth->fetchrow_array();
		$sth->finish();
		if ($seconds > 0) {
			$walltime = ' -l walltime=' . int($seconds);
			#print "Setting estimated wall time for INput file creation as $walltime 
		}
	}
	else {
		$walltime = '';
	}

	## create the job script
	print "Submitted sample '$samplename' for $name analysis\n";
	my $jobname = "$pid.$sid.Segment.$name";
	my $jobfile = "$qsubscripts/$pid/$jobname.sh";
	open OUT, ">$jobfile";
	&PBSWriteHeader("OUT", $pid, $jobname, 1, 4, $walltime, $scriptdir);
	&PBSWriteCommand("OUT", "perl $scriptdir/Bin/RunSegmentation.pl -p $pid -s $sid -a $name -r $rand");
	&PBSWriteEnd("OUT");
	close OUT;

	## submit job
	my $jobid = `qsub $jobfile`;
	chomp($jobid);
	while ($jobid !~ m/^\d+\..*/) {
		sleep 1;
		$jobid = `qsub $jobfile`;
	}


}
print "finished, exiting\n";

## send runtime to database.
$now = time - $now;
$dbh->do("INSERT INTO `GenomicBuilds`.`RuntimeStatistics` (process,chiptypeid,argument,time) VALUES ('GenomicWave','$chipid','0','$now')");



exit();


###################
### SUBROUTINES ###
###################

sub PBSWriteCommand {
	my ($filehandle, $run_command) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	my $echo_command = $run_command;
	print $filehandle "echo 'Command:'\n";
	print $filehandle "echo '========'\n";
	$echo_command =~ s/"/\\"/g;
	print $filehandle "echo \"$echo_command\"\n";
	print $filehandle "$run_command\n";

}

sub PBSWriteHeader {
	my ($filehandle, $projectid, $job, $cpu, $mem, $time, $dir) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "#!/usr/bin/env bash\n";
	print $filehandle "#PBS -m a\n";
	print $filehandle "#PBS -M $adminemail\n";
	print $filehandle "#PBS -d $dir\n";
	print $filehandle "#PBS -l nodes=1:ppn=$cpu,mem=$mem"."g\n";
	print $filehandle "#PBS -N $job\n";
	print $filehandle "#PBS -o $cjo/$scriptuser/$projectid/$job.o.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -e $cjo/$scriptuser/$projectid/$job.e.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -V\n";
	if ($queuename ne '') {
		print $filehandle "#PBS -q $queuename\n";
	}
	if ($hpcaccount ne '') {
		print $filehandle "#PBS -A $hpcaccount\n";
	}
	if ($setwalltime == 1 && $time) {
		print $filehandle "#PBS $time\n";
	}
	#if ($afterok) {
	#	print $filehandle " #PBS -W depend=afterok:$afterok\n";
	#}

	print $filehandle "\necho 'Running on : ' `hostname`\n";
	print $filehandle "echo 'Start Time : ' `date`\n\n";
	print $filehandle "echo 'setting path: $path'\n";
	print $filehandle "export PATH=$path\n\n";

}

sub PBSWriteEnd {
	my $filehandle = shift;
	if (tell( $filehandle ) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "\n\necho 'End Time : ' `date`\n";
	print $filehandle "printf 'Execution Time = \%dh:\%dm:\%ds\\n' \$((\$SECONDS/3600)) \$((\$SECONDS%3600/60)) \$((\$SECONDS%60))\n";

}
