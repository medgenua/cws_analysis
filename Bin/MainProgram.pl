#!/usr/bin/perl

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;
use Data::Dumper;
use XML::Simple;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

#######################
# ENABLE AUTOFLUSHING #
#######################
$| = 1;	 	

#################
# GET STARTTIME #
#################
local $now = time;
@starttime = localtime(time);
##########################
# COMMAND LINE ARGUMENTS #
##########################
# D = Use specific *d*atabase,  otherwise use default.
# r = *r*andom number to identify the datafiles. 
getopts('D:r:', \%opts);  # option are in %opts


printf("\n\nStartup time:%04d-%02d-%02d %02d:%02d:%02d\n",($starttime[5] + 1900) ,($starttime[4]+1),$starttime[3],$starttime[2],$starttime[1],$starttime[0]);

###########################
# CONNECT TO THE DATABASE #
###########################
my $db ;
my $targetdb;
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbhgb)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass) ;
	}

	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$targetdb = $row[0];
	$newbuild = $row[1];
	print "Assuming build ".$row[1]."\n";
	$gbsth->finish();
	$dbhgb->disconnect;	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
$dbh->{mysql_auto_reconnect} = 1;

#####################################
## CHANGE TO CNVanalysis DIRECTORY ##
#####################################
chdir($scriptdir);

#######################
## RANDOM IDENTIFIER ##
#######################
my $rand = $opts{'r'};

####################
# GET SETTINGS XML #
####################
#my $settingsfile = "datafiles/CNV-WebStore/runtime/$rand.settings.xml";
my $settingsfile = "$datadir/$rand.settings.xml";

my $xml = new XML::Simple;
# this xml file contains all the parameter/method settings.
my $settings = $xml->XMLin($settingsfile);
my $baselink = $settings->{webhost};
my $usepedigree = $settings->{general}->{usepedigree};
# set project parameters
my $projectname = $settings->{general}->{projectname};
my $chiptype = $settings->{general}->{chiptype};
my $sharegroup = $settings->{general}->{sharegroup};
my $minsnp = $settings->{general}->{minsnp};
my $group = $settings->{general}->{group};
if ($group eq "") {
	print "No Experimental Group Specified, using 'Guest Data'\n";
	$group = 'Guest Data';
}
my $uname = $settings->{general}->{username};
# options for asymmetric filtering
my $asym = $settings->{general}->{asym};
my $asymminconf = $settings->{general}->{asymminconf};
my $asymmethod = $settings->{general}->{asymmethod};
my $asymtype = $settings->{general}->{asymtype};
if ($asym == 1) {
	print "High confidence (>$asymminconf) $asymmethod calls of $asymtype type will be considered as overlapping region\n";
}

# options for UPD calling
if ($settings->{general}->{checkupd} == 1) {
	$checkupd = 1;
}
else {
	$checkupd = 0;
}
## options for triPOD
if ($settings->{general}->{triPOD} == 1) {
	$triPOD = 1;
}
else {
	$triPOD = 0;
}

$minsize = $settings->{general}->{uminsnp};
my $plink = $settings->{general}->{plink};


##################################
## check / unzip gzipped files. ##
##################################
#open IN, "datafiles/CNV-WebStore/runtime/$rand.zipcoding.txt";
open IN, "$datadir/$rand.zipcoding.txt";
my %zipped;
while (<IN>) {
	chomp($_);
	my @p = split(/\t/,$_);
	$zipped{$p[0]} = $p[1];
}
close IN;
unlink("$datadir/$rand.zipcoding.txt");
if ($zipped{"data.0"} == 1){
	$datafile = "$datadir/$rand.datafile.0.gz";
}
else {
	$datafile = "$datadir/$rand.datafile.0.txt";		
}
if ($zipped{"gender"} == 1) {
	print "Extracting gzipped samples table\n";
	system("gunzip -c $datadir/$rand.genderfile.gz > $datadir/$rand.genderfile.txt");
}	
$genderfile = "$datadir/$rand.genderfile.txt";
if ($usepedigree == 1) {
	print "Found Family information. Storing parental information\n";
	if ($zipped{"pedigree"} == 1) {
		print "Extracting gzipped pedigree information\n";
		system("gunzip -c $datadir/$rand.pedigree.gz > $datadir/$rand.pedigree.txt");
	}	
	$pedfile = "$datadir/$rand.pedigree.txt";
}

#############
# VARIABLES #
#############
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";

my %allsamples; 	# hash to contain all sample names, including extra files
my %genderhash ;    	# links gender to chip_dnanr
my %indexhash ;		# links project index to chip_dnanr
my %newsamplehash;	# keeps track of new samples by chip_dnanr
my %fortrackhash ;	# add to track, based on chip_dnanr
my %callratehash ;	# callrates by chip_dnanr
my %idhash ;		# db_id by chip_dnanr
my %revidhash ;		# chip_dnanr by db_id ; ONLY FOR FILES IN THIS PROJECT !
my %samplenamehash ;	# samplename by file order (equals to line number in genderfile)
my %filehash ;		# file order by  chip_dnanr 
my %chipposhash ; 	# Sentrix Position, by chip_dnanr
my %chipcodehash ; 	# Sentrix ID (barcode) by chip_dnanr
###########################
## SET OUTPUT FILE NAMES ##
###########################
if ($settings->{general}->{methods} eq '') {
	die "No analysis methods specified. Program will exit\n";
}
my @algos = split(/;/,$settings->{general}->{methods});
print "Using the following methods: \n";
foreach (@algos) {
	print "\t$_\n";
}
if (scalar(@algos) > 1) {
	$targetname = "$sitedir/bookmarks/" . $projectname . "_multi.xml";
	$targetnamelist = "$sitedir/CNVlists/" . $projectname . "_multi.txt";
}
else {
	$targetname = "$sitedir/bookmarks/".$projectname . "_". $algos[0].".xml";
	$targetnamelist = "$sitedir/bookmarks/".$projectname . "_". $algos[0].".txt";
} 

###################################
## GET CHIPTYPE ID FROM DATABASE ##
###################################
$query = "SELECT ID FROM chiptypes WHERE name = '$chiptype'";
$sth = $dbh->prepare($query);
$sth->execute();
my @chipres = $sth->fetchrow_array();
my $chiptypeid = $chipres[0];
$sth->finish();

###############################
## GET USER ID FROM DATABASE ##
###############################
my $getuserid = "SELECT id, email FROM users WHERE username = '$uname'";
$sth = $dbh->prepare($getuserid);
$sth->execute();
my @userids = $sth->fetchrow_array();
my $uid = $userids[0];
my $email = $userids[1];
$sth->finish();

##################
# CREATE PROJECT #
##################
print "Creating database entry for this project\n";
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year = 1900+$year;
$mon = $mon +1;
$time = "$mday/$mon/$year - $hour:$min:$sec";
my $asymstring = "$asym|$asymmethod|$asymminconf|$asymtype";
$query = "INSERT INTO project (naam, chiptype, created, collection, userID, chiptypeid,asymFilter,minsnp,plink) values ('$projectname', '$chiptype', '$time', '$group', '$uid', '$chiptypeid','$asymstring','$minsnp','$plink') ";
$dbh->do($query);
my $getindexq = "SELECT id FROM project WHERE naam = '$projectname' ORDER BY id DESC;";
$sth = $dbh->prepare($getindexq);
$sth->execute();
my @result = $sth->fetchrow_array();
my $pid = $result[0];
$sth->finish();
print "\t=> Project-ID = $pid\n";

###################################
## SEND MESSAGE OF SCRIPT LAUNCH ##
###################################
# this is read by browser.
open OUT, ">$scriptdir/output/$rand.pid";
print OUT $pid;
close OUT; 

################################
## create a lock for this pid ##
################################
#if (-e "datafiles/CNV-WebStore/runtime/$pid.lock") {
if (-e "$datadir/$pid.lock") {
	print "CRITICAL PROBLEM : \n";
	print "==================\n";
	print "   Could not get lock on aquired project ID.\n";
	print "   The program is put on hold untill resolved.\n";  
	print "\n";
	#print "   Lock File: $scriptdir/datafiles/CNV-WebStore/runtime/$pid.lock\n";
	print "   Lock File: $datadir/$pid.lock\n";
}
#while (-e "datafiles/$CNV-WebStore/runtime/pid.lock") {
while (-e "$datadir/$pid.lock") {
	sleep 30;
}
#$dump = system("touch datafiles/CNV-WebStore/runtime/$pid.lock");
$dump = system("touch $datadir/$pid.lock");

###########################
## ASSIGN METHODS TO PID ##
###########################
my %versions; # needed later on
foreach (@algos) {
	my $method = $_;
	# get version
	my $configuration = $settings->{$method}->{configuration};
	my $version = $configuration->{general}->{version};
	$versions{$method} = $version;
	# get settings string
	my $methodstring = 'min_conf@'.$configuration->{general}->{min_conf} . '|';
	for my $key (keys %{$configuration->{input_values}}) {
		$methodstring .= "$key@".$configuration->{input_values}->{$key} .'|';
	}
	for my $key (keys %{$configuration->{input_boolean}}) {
		$methodstring .= "$key@".'boolean|';
	}
	for my $key (keys (%{$configuration->{input_conditional}})) {
		foreach (@{$configuration->{input_conditional}->{$key}}) {
			my $condition = $_->{value};
			for my $subkey (keys(%{$_->{arguments}})) {
				my $subval = '';
				# this regex checks if the value is hash (true for boolean nodes (no value present, <node /> style)
				if ($_->{arguments}->{$subkey} !~ /hash/i) {
					$subval = $_->{arguments}->{$subkey} ; 
					$methodstring .= "if_$key"."_equals_".$_->{value} . "_then_". $subkey .'@'.$subval."\n";
				}
				else {
					$methodstring .= "if_$key"."_equals_".$_->{value} . "_then_". $subkey .'@boolean|';
				}
			}
		}
	}
	$methodstring = substr($methodstring,0,-1);
	my $pmsth = $dbh->prepare("INSERT INTO `project_x_method` (pid, Method, Version, settings) VALUES (?, ?, ?, ?)");
	$pmsth->execute($pid, $method, $version, $methodstring);
	$pmsth->finish();
}

##############################
## SET PERMISSIONS FOR USER ##
##############################
my $setperm = "INSERT INTO projectpermission (userid, projectid, editsample,editclinic,editcnv) VALUES ('$uid', '$pid','1','1','1') ON DUPLICATE KEY UPDATE userid=userid";
$dbh->do($setperm);

################################
## DEFINE COMMON prefix AS pid##
################################
my $prefix = $pid;
open OUT, ">output/$rand.pid";
print OUT $pid;
close OUT;
#system("mv $genderfile datafiles/CNV-WebStore/runtime/$pid.genderfile.txt");
system("mv $genderfile $datadir/$pid.genderfile.txt");
#$genderfile = "datafiles/CNV-WebStore/runtime/$pid.genderfile.txt";
$genderfile = "$datadir/$pid.genderfile.txt";
if ($usepedigree == 1) {
	#system("mv $pedfile datafiles/CNV-WebStore/runtime/$pid.pedigree.txt");
	system("mv $pedfile $datadir/$pid.pedigree.txt");
	#$pedfile = "datafiles/CNV-WebStore/runtime/$pid.pedigree.txt";
	$pedfile = "$datadir/$pid.pedigree.txt";
}
if (!-e "$cjo/$scriptuser") {
	print "Creating output directory\n";
	system("mkdir '$cjo/$scriptuser'");
}
system("mkdir -p '$cjo/$scriptuser/$pid'");
system("mkdir -p '$qsubscripts/$pid'");


############################################
# INSERT/UPDATE sample details in database #
############################################
## check line endings in gender table
$line = `head -n 1 $genderfile`;
my $convert = 0;
if( $line =~ m/\r\n$/ ) {
		print "Samples Table : Detected DOS file format: converting to UNIX\n";
	$dump = system("cd /tmp/ && dos2unix \"$genderfile\" ");
}
elsif( $line =~ m/\n\r$/ ) {
		print "Samples Table : Detected DOS file format: converting to UNIX\n";
	$dump = system("cd /tmp/ && dos2unix \"$genderfile\" ");
}
elsif( $line =~ m/\r$/ ) {
		print "Samples Table : Detected MAC file format: converting to UNIX\n";
		open IN, "$genderfile";
		open OUT, ">/tmp/tmpfile.txt";
		while (<IN>) {
				$_ =~ s/\r/\n/gi ;     # replace returns with newlines
				print OUT $_ ;
		}
		close OUT;
		close IN;
		$dump = `mv  \"/tmp/tmpfile.txt \"$genderfile\"`;
}

# GENDERFILE: Get column names
print "Reading Samples Table : \n";
open INGender, "$genderfile" or die "Cannot open Genderfile";
my $header = <INGender>;
chomp($header);
my @headerparts = split(/\t/,$header);
my $gendercol;
my $namecol;
my $idcol;
my $chipposcol = -1;
my $chipcodecol = -1;
my $callratecol;
my $colidx = 0;
my $fileok = 1;
foreach(@headerparts) {
	if ($_ =~ m/Sample ID/i ) {
		$namecol = $colidx;
	}
	elsif ($_ =~ m/Gender/i ) {
		$gendercol = $colidx;
	}
	elsif ($_ =~ m/Call Rate/i ) {
		$callratecol = $colidx;
	}
	elsif ($_ =~ m/Index/i ) {
		$idcol = $colidx;
	}
	elsif ($_ =~ m/Sentrix ID/i) {
		$chipcodecol = $colidx;
	}
	elsif ($_ =~ m/Sentrix Position/i) {
		$chipposcol = $colidx;
	}
	$colidx++;
}

## Genderfile : scan rows
my $sampleidx = 1;
my $printcol = 1;
my $nrsamples ;
while (<INGender>) {
	chomp($_);
	my @gender = split(/\t/,$_);
	if ($printcol == 3) {
		$sep = "\n";
		$printcol = 0;
	}
	else {
		$sep = "\t\t";
	}
	$gender[$namecol] =~ s/[^a-zA-Z0-9\-_]/_/g;	
	print " Sample $gender[$namecol] => $gender[$gendercol] $sep";
	$printcol++;
	if ($gender[$gendercol] ne "Female" && $gender[$gendercol] ne "Male") {
		$fileok = 0;
	}
	$genderhash{$gender[$namecol]} = $gender[$gendercol];
	$indexhash{$gender[$namecol]} = $gender[$idcol];
	$fortrackhash{$gender[$namecol]} = 0;
	$gender[$callratecol] =~ s/,/\./;
	$callratehash{$gender[$namecol]} = $gender[$callratecol];
	$samplenamehash{$sampleidx} = $gender[$namecol];
	$sampleidx++;
	my $query = "SELECT id FROM sample WHERE chip_dnanr='". $gender[$namecol] ."';";
	my $sth = $dbh->prepare($query); 
	$sth->execute();
	$rv = $sth->rows();
	$sth->finish();
	if ($rv == "0") {
		$newsamplehash{$gender[$namecol]} = "1";
	}
	else {
		$newsamplehash{$gender[$namecol]} = "0";
	}
	if ($chipposcol != -1) {
		$chipposhash{$gender[$namecol]} = $gender[$chipposcol];
	}
	if ($chipcodecol != -1) {
		$chipcodehash{$gender[$namecol]} = $gender[$chipcodecol];
	}

}
print "\n";
close INGender;

## remove gender file
unlink($genderfile);

if ($fileok == 0) {
	die "Incorrect Gender Format detected\n";
}
$nrsamples = keys( %genderhash);
print "$nrsamples Samples found to analyse\n";
#_FORTRACK-FILE: Get names of samples not yet in CNV-WebStore
my $fortrack = $projectname . "_fortrack.txt";
open FORTRACK, "output/$fortrack";
while(<FORTRACK>) {
  chomp($_);
  $fortrackhash{$_} = 1;
}
close FORTRACK;

################################################
## insert/update sample details into database ##
################################################
print "Updating sample details in the database\n";
for my $key ( keys %genderhash ) {
	my $sid = '';
	if ($fortrackhash{$key} == 1 && $newsamplehash{$key} == 1) {
		my $query = "INSERT INTO sample (chip_dnanr, gender, intrack, trackfromproject) values ('$key', '". $genderhash{$key} ."', '1', '$pid');";
		$dbh->do($query);
		my $getindex = "SELECT id FROM sample where chip_dnanr='$key' ORDER BY id DESC;";
		$sth = $dbh->prepare($getindex);
		$sth->execute();
		my @result = $sth->fetchrow_array();
		my $id = $result[0];
		$sid = $id;
		$allsamples{$id} = $id;
		$idhash{$key} = $id;
		$revidhash{$id} = $key;
		$sth->finish();
		$callrate = $callratehash{$key};
		$index = $indexhash{$key};
		my $queryproj = "INSERT INTO projsamp (idsamp, idproj, callrate, idx) values ('$id', '$pid', '$callrate', '$index');";
		$dbh->do($queryproj);
		
	}
	elsif ($fortrackhash{$key} == 0 && $newsamplehash{$key} == 1) {
		my $query = "INSERT INTO sample (chip_dnanr, gender, intrack, trackfromproject) values ('$key', '". $genderhash{$key} . "', '0', '');";
		$dbh->do($query);
		my $getindex = "SELECT id FROM sample where chip_dnanr='$key' ORDER BY id DESC;";
		$sth = $dbh->prepare($getindex);
		$sth->execute();
		my @result = $sth->fetchrow_array();
		my $id = $result[0];
		$sid = $id;
		$allsamples{$id} = $id;
		$idhash{$key} = $id;
		$revidhash{$id} = $key;
		$sth->finish();
		$callrate = $callratehash{$key};
		$index = $indexhash{$key};
		my $queryproj = "INSERT INTO projsamp (idsamp, idproj, callrate, idx) values ('$id', '$pid', '$callrate', '$index');";
		$dbh->do($queryproj);
	}
	elsif ($fortrackhash{$key} == 1 && $newsamplehash{$key} == 0) {
		my $query = "SELECT id FROM sample WHERE chip_dnanr='" . $key . "';";
		my $sth = $dbh->prepare($query);
		$sth->execute();
		my @result = $sth->fetchrow_array();
		my $id = $result[0];
		$sid = $id;
		$allsamples{$id} = $id;
		$idhash{$key} = $id;
		$revidhash{$id} = $key;
		my $query = "UPDATE sample SET intrack='1',trackfromproject='$pid' WHERE id='$id';";
		$dbh->do($query);
		$callrate = $callratehash{$key};
		$index = $indexhash{$key};
		my $queryproj = "INSERT INTO projsamp (idsamp, idproj, callrate, idx) values ('$id', '$pid', '$callrate', '$index');";
		$dbh->do($queryproj);
	}
	else {
		my $query = "SELECT id FROM sample WHERE chip_dnanr='" . $key . "';";
		my $sth = $dbh->prepare($query);
		$sth->execute();
		my @result = $sth->fetchrow_array();
		my $id = $result[0];
		$sid = $id;
		$allsamples{$id} = $id;
		$idhash{$key} = $id;
		$revidhash{$id} = $key;
		$callrate = $callratehash{$key};
		$index = $indexhash{$key};
		my $queryproj = "INSERT INTO projsamp (idsamp, idproj, callrate, idx) values ('$id', '$pid', '$callrate', '$index');";
		$dbh->do($queryproj);
	}
	if ($chipposcol != -1) {
		$dbh->do("UPDATE projsamp SET SentrixPos = '".$chipposhash{$key}."' WHERE idsamp = '$sid' AND idproj = '$pid'");
	}
	if ($chipcodecol != -1) {
		$dbh->do("UPDATE projsamp SET SentrixID = '".$chipcodehash{$key}."' WHERE idsamp = '$sid' AND idproj = '$pid'");
	}	
}	
##########################################
# SEND FILE PREPARATION TO CLUSTER QUEUE #
##########################################
## estimate wall time for file creation. 
my $walltime = '-l walltime=30:00';

## create the job script
my $jobname = "$pid.CreateInputFiles";
my $jobfile = "$qsubscripts/$pid/$jobname.sh";
open OUT, ">$jobfile";
&PBSWriteHeader("OUT", $pid, $jobname, 1, 4, $walltime, $scriptdir);
&PBSWriteCommand("OUT", "perl $scriptdir/Bin/CreateInputFiles.pl -d $datafile -p $pid -r $rand -z $zipped{'data.0'}");
&PBSWriteEnd("OUT");
close OUT;

## submit job
my $jobid = `qsub $jobfile`;
chomp($jobid);
while ($jobid !~ m/^\d+\..*/) {
	sleep 1;
	$jobid = `qsub $jobfile`;
}
# copy the main datafile to the correct syntax for family analysis.
system("cp '$datafile' '$datadir/$pid.datafile.0.txt'");
#######################################
## RENAME EXTRA DATAFILES IF PRESENT ##
#######################################
# this is run from the main script, 
# as we have to wait for analysis to finish anyway
print "Checking for additional datafiles\n";
for ($i = 1; $i <= $settings->{general}->{datafiles}; $i++) {
	## set filenames
	my $target = '';
	if ($zipped{"data.$i"} == 1) {
		## extract the file
		#my $source = "/var/tmp/datafiles/CNV-WebStore/runtime/$rand.datafile.$i.gz";
		my $source = "$datadir/$rand.datafile.$i.gz";
		#my $target = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.datafile.$i.txt";
		$target = "$datadir/$pid.datafile.$i.txt";
		print "Extracting Supplementary File $i\n";
		$dump = system("gunzip -c $source >> $target && rm -f $source");
	}
	else {
		## move the file
		#my $source = "/var/tmp/datafiles/CNV-WebStore/runtime/$rand.datafile.$i.txt";
		my $source = "$datadir/$rand.datafile.$i.txt";
		#my $target = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.datafile.$i.txt";
		$target = "$datadir/$pid.datafile.$i.txt";
		$dump = system("mv '$source' '$target' ");
	}
	## convert to unix format
	my $line = `head -n 1 $target`;
	if( $line =~ m/\r\n$/ || $line =~ m/\n\r$/) {
		print "  => Supplementary File $i : Detected DOS file format: converting to UNIX\n";
		$dump = system("cd \"$datadir\" && dos2unix -n '$target' '$target.tmp' && mv '$target.tmp' '$target' ");
	}
	elsif( $line =~ m/\r$/ ) {
		print "  => Supplementary File $i : Detected MAC file format: converting to UNIX\n";
		open IN, "$target";
		open OUT, ">/tmp/$pid.datafile.$i.txt";
		while (<IN>) {
			$_ =~ s/\r/\n/gi ;     # replace returns with newlines
				print OUT $_ ;
		}
		close OUT;
		close IN;
		$dump = system("mv /tmp/$pid.datafile.$i.txt $target");
	}
	## pseudo-autosomal fix ; comma for decimal fix.
	$dump = system("cd '$datadir' && sed -i '1n; s/,/\./g ; s/XY/X/g' \"$target\" ");
	## fix invalid sample names
	$dump = system("cd '$datadir' && sed -i '1 s/[^A-Za-z0-9\\-\\_\\.\\t]/_/g; s/Log_R_Ratio/Log R Ratio/g ; s/B_Allele_Freq/B Allele Freq/g' '$target'");
	## scan sample names
	my %extrasamples;
	my $NameCol;
	my $ChrCol;
	my $PosCol;
	open IN, $target;
	my $header = <IN>;
	close IN;
	chomp($header);
	my @pieces = split(/\t/,$header);
	$j = 0;
	foreach (@pieces) {
		if ($_ =~ m/^([\w-]+).Log/ ) {
			my $sname = $1;
			if (!defined($idhash{$sname})) {
				## first occurence : get id from DB.
				$r = $dbh->selectcol_arrayref("SELECT id FROM sample WHERE chip_dnanr = '$sname'");
				$idhash{$sname} = $r->[0];
			}
			$extrasamples{$idhash{$sname}}{'LogR'} = $j +1;
			$j++;
		}
		elsif ($_ =~ m/^([\w-]+).B All/ ) {
			my $sname = $1;
			if (!defined($idhash{$sname})) {
				## first occurence : get id from DB.
				$r = $dbh->selectcol_arrayref("SELECT id FROM sample WHERE chip_dnanr = '$sname'");
				$idhash{$sname} = $r->[0];
			}
			$extrasamples{$idhash{$sname}}{'BAF'} = $j + 1;
			$j++;
		}
		elsif ($_ =~ m/^([\w-]+).GType/ ) {
			my $sname = $1;
			if (!defined($idhash{$sname})) {
				## first occurence : get id from DB.
				$r = $dbh->selectcol_arrayref("SELECT id FROM sample WHERE chip_dnanr = '$sname'");
				$idhash{$sname} = $r->[0];
			}
			$extrasamples{$idhash{$sname}}{'GType'} = $j +1;
			$j++;
		}
		elsif ($_ =~ m/^Name/ ) {
			$NameCol = $j+1;
			$j++;
		}
		elsif ($_ =~ m/^Chr/ ) {
			$ChrCol = $j+1;
			$j++;
		}
		elsif ($_ =~ m/^Position/ ) {
			$PosCol = $j+1;
			$j++;
		}
		else { $j++; }	
	}	
	## print out file structure
	open OUT, ">$datadir/$pid.datafile.$i.cols";
	foreach (keys(%extrasamples)) {
		$allsamples{$_} = "extra.$i";
		print OUT "$_=\$$NameCol,\$$ChrCol,\$$PosCol,\$".$extrasamples{$_}{'LogR'}.",\$".$extrasamples{$_}{'BAF'}.",\$".$extrasamples{$_}{'GType'}."\n";
	}
	close OUT;
}
print "All additional files processed\n";


########################
# PREPARE OUTPUT FILES #
########################
print "Preparing output files\n";
##################
## OUTPUT files ##
##################
# Main bookmarks
if (-e "results_xml/".$prefix."_Results_total.xml") {unlink "results_xml/".$prefix."_Results_total.xml";} 
open OUT, ">>results_xml/".$prefix."_Results_total.xml";
if (-e "results_list/$prefix"."_CNV_list.txt") {unlink "results_list/$prefix"."_multi.txt";}
open OUTRES, ">results_list/$prefix"."_multi.txt";
print OUT "<Project_Bookmarks><Version>2.0.0</Version><Name>$projectname Combined</Name><Author>$uname</Author><Comment>Used: ";
for my $method (keys(%version)) {
	print OUT "$method ($versions{$method}) ";
}
print OUT "</Comment><CreateDate>$time</CreateDate><Algorithm>Majority Vote</Algorithm><AlgorithmVersion>2.0</AlgorithmVersion><Bookmark_Templates><bookmark_template><type>CNV Bin: Min 0 To Max 0.5</type><fill_color>Red</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 0.5 To Max 1.5</type><fill_color>Purple</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 1.5 to Max 2.5</type><fill_color>Gold</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 2.5 To Max 3.5</type><fill_color>Green</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 3.5 To Max 4.5</type><fill_color>Blue</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>RECURRENT CNV</type><fill_color>Black</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template></Bookmark_Templates>\n<Bookmarks>";
print OUTRES "Sample ID\tChromosome\tStart (bp)\tStop (bp)\tSize (bp)\tCopy Number\tNr SNPs\tNr Genes\tAlgorithms and scores\n";
close OUT;
close OUTRES;
# if needed, seperate method bookmarks #
for my $method (keys(%versions)) {
	# xml
	my $version = $versions{$method};
	if (-e "results_xml/".$prefix."_Results_$method.xml") {unlink "results_xml/".$prefix."_Results_$method.xml";} 
	open OUTM, ">>results_xml/".$prefix."_Results_$method.xml";
	print OUTM "<Project_Bookmarks><Version>$version</Version><Name>$projectname $method</Name><Author>$uname</Author>";
	print OUTM "<Comment></Comment><CreateDate>$time</CreateDate><Algorithm>$method</Algorithm><AlgorithmVersion>$version</AlgorithmVersion>";
	print OUTM "<Bookmark_Templates><bookmark_template><type>CNV Bin: Min 0 To Max 0.5</type><fill_color>Red</fill_color><fill_style>Solid</fill_style>";
	print OUTM "<fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 0.5 To Max 1.5</type><fill_color>Purple</fill_color>";
	print OUTM "<fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 1.5 To Max 2.5</type>";
	print OUTM "<fill_color>Gold</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template>";
	print OUTM "<type>CNV Bin: Min 2.5 To Max 3.5</type><fill_color>Green</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template>";
	print OUTM "<bookmark_template><type>CNV Bin: Min 3.5 To Max 4.5</type><fill_color>Blue</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity>";
	print OUTM "</bookmark_template><bookmark_template><type>RECURRENT CNV</type><fill_color>Black</fill_color><fill_style>Solid</fill_style>";
	print OUTM "<fill_opacity>40</fill_opacity></bookmark_template></Bookmark_Templates>\n<Bookmarks>";
	close OUTM;
	# txt
	my $txtfile = "results_list/$prefix"."_$method"."_list.txt";
	if (-e "$txtfile") {
		unlink("$txtfile");
	}
	`touch "$txtfile" && chmod 777 "$txtfile"`;
	open TXT, ">>$txtfile";
	print TXT "Sample ID\tChr\tStart (bp)\tStop (bp)\tSize (bp)\tStart Probe\tStop Probe\tCopy Number\tNr.SNPs\tConfidence\n";
	close TXT;
}
if ($plink == 1) {
	$method = 'PlinkHomoz';
	my $xml = new XML::Simple;
	my $configuration = $xml->XMLin("$scriptdir/Analysis_Methods/Configuration/$method.xml");
	my $version = $configuration->{general}->{version};
	# xml
	if (-e "results_xml/".$prefix."_Results_$method.xml") {unlink "results_xml/".$prefix."_Results_$method.xml";} 
	open OUTM, ">>results_xml/".$prefix."_Results_$method.xml";
	print OUTM "<Project_Bookmarks><Version>$version</Version><Name>$projectname $method</Name><Author>$uname</Author>";
	print OUTM "<Comment></Comment><CreateDate>$time</CreateDate><Algorithm>$method</Algorithm><AlgorithmVersion>$version</AlgorithmVersion>";
	print OUTM "<Bookmark_Templates><bookmark_template><type>Homozygous Region: CN=2</type>";
	print OUTM "<fill_color>Orange</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template></Bookmark_Templates>\n<Bookmarks>";
	close OUTM;
	# txt
	my $txtfile = "results_list/$prefix"."_$method"."_list.txt";
	if (-e "$txtfile") {
		unlink("$txtfile");
	}
	`touch "$txtfile" && chmod 777 "$txtfile"`;
	open TXT, ">>$txtfile";
	print TXT "Sample ID\tChr\tStart (bp)\tStop (bp)\tSize (bp)\tStart Probe\tStop Probe\tCopy Number\tNr.SNPs\tConfidence\n";
	close TXT;
}


##################
## PROGRAM FLOW ##
##################
# sample input is prepared by CreateInputFiles (already started by now), and queued for  check/correcting of genomic wave.
# GenomicWaveCorrection checks&corrects Genomic Wave with PennCNV functions and queues them from Segmentation
# Finished segmentations are stored in a temporary database from RunSegmentation.pl
# this database is queried by the Main program. Samples with completed segmentation results are sent from here to FormatResults queue.
# FormatResults.pl compares results, stores them to the database and returns results for output to xml/table files to a temp database.
# the main program fetches these tmp data and outputs them to the results files.  

############################################################
## SCAN CNVanalysis-TMP.Segmentation for finished samples ##
############################################################
print "Waiting for complete segmentation processes...\n";
my %todo = %revidhash;
my %formattodo = %revidhash;
my %failed;
## estimate wall time. 
$walltime = '-l walltime=30:00';
if ($setwalltime == 1) {
	my $query = "SELECT AVG(time) FROM `GenomicBuilds`.`RuntimeStatistics` WHERE process = 'FormatResults' AND chiptypeid = '$chiptypeid'";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my ($seconds) = $sth->fetchrow_array();
	$sth->finish();
	if ($seconds > 0) {
		$walltime = '-l walltime=' . int($seconds);
	}
}	
else {
	$walltime = '';
}
while (keys(%todo) > 0) {
	## get finished samples to send to formatting
	$query = "SELECT COUNT(DISTINCT(algo)), sid FROM `CNVanalysis-TMP`.`Segmentation` WHERE pid = '$pid' GROUP BY sid ";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	while (my @row = $sth->fetchrow_array()) {
		## get current sample id
		my $currsid = $row[1];
		my $currsample = $revidhash{$currsid};
		## need scalar(@algos) results (+ plink if specified)
		if ($row[0] < (scalar(@algos) + $plink)) {
			my $sth2 = $dbh->prepare('SELECT algo FROM `CNVanalysis-TMP`.`SegmentationErrors` WHERE pid = ? and sid = ?');
			$sth2->execute($pid, $currsid);
			my $aref = $sth2->fetchall_arrayref;
			my $nrfailed = scalar(@$aref);
			## maybe an algorithm failed, count together and check
			if (($row[0] + $nrfailed) == (scalar(@algos) + $plink)) {
				my @failedalgos;
				foreach (@$aref) {
					push(@failedalgos, $_->[0]);
				}
				$failed{$currsid} = join(",", @failedalgos);
				## remove sample from Db and todo lists
				$dbh->do("DELETE FROM `CNVanalysis-TMP`.`Segmentation` WHERE pid = '$pid' AND sid = '$currsid'");
				$dbh->do("DELETE FROM `CNVanalysis-TMP`.`SegmentationErrors` WHERE pid = '$pid' AND sid = '$currsid'");
				$dbh->do("DELETE FROM `CNVanalysis-TMP`.`Formatting` WHERE pid = '$pid' AND sid = '$currsid'");
				delete($todo{$currsid});
				delete($formattodo{$currsid});
			}	
			next;
		}

		## create the job script
		$jobname = "$pid.$currsid.FormatResults";
		$jobfile = "$qsubscripts/$pid/$jobname.sh";
		open OUT, ">$jobfile";
		&PBSWriteHeader("OUT", $pid, $jobname, 1, 2, $walltime, $scriptdir);
		&PBSWriteCommand("OUT", "perl $scriptdir/Bin/FormatResults.pl -D $db -p $pid -s $currsid -r $rand");
		&PBSWriteEnd("OUT");
		close OUT;

		## submit job
		$jobid = `qsub $jobfile`;
		chomp($jobid);
		while ($jobid !~ m/^\d+\..*/) {
			sleep 1;
			$jobid = `qsub $jobfile`;
		}


		## and remove the sample from the todo list
		$dbh->do("DELETE FROM `CNVanalysis-TMP`.`Segmentation` WHERE pid = '$pid' AND sid = '$currsid'");
		delete($todo{$currsid});
	}
	$sth->finish();
	## get formatted sample to print out results
	$query = "SELECT sid FROM `CNVanalysis-TMP`.`Formatting` WHERE pid = '$pid' AND Method = 'Multi'";
	$sth = $dbh->prepare($query);
	$sth->execute();
	while (my @row = $sth->fetchrow_array()) {
		my $currsid = $row[0];
		&PrintOut($currsid);
		## delete from todo list
		$dbh->do("DELETE FROM `CNVanalysis-TMP`.`Formatting` WHERE pid = '$pid' AND sid = '$currsid'");
		delete($formattodo{$currsid});
	}
	$sth->finish();
	## sleep 30 seconds before requerying 
	sleep 30; 
}

## gently 'crash' the analysis and show which samples have failed
if (%failed) {
	print "These algorithms failed for the following samples:\n";
	foreach my $sample (keys %failed) {
		print "\t- " .$sample . ": " . $failed{$sample} . "\n";
	}

	print "The analysis is stopped. Before rerunning, please contact the administrator for troubleshooting\n";
	#######################
	## send mail to user ##
	#######################
	open OUT, ">$datadir/$pid.failedmail.txt";
	print OUT "to: $email\n";
	print OUT "subject: CNV-WebStore Project analysis FAILED\n";
	print OUT "from: no-reply\@cnv-webstore.uza.be\n\n";
	print OUT "Your Project '$projectname' has failed on segmentation of the following samples:\n";
	foreach my $sample (keys %failed) {
		print OUT "\t- " .$sample . ": " . $failed{$sample} . "\n";
	}
	print OUT "\nThe analysis is stopped.\nBefore rerunning, please contact the administrator for troubleshooting.\n\n";
	close OUT;
	system("sendmail $email < $datadir/$pid.failedmail.txt");
	exit();
}


## wait for the remaining items to finish & print them out
while (keys(%formattodo) > 0) {
	## get formatted sample to print out results
		$query = "SELECT sid FROM `CNVanalysis-TMP`.`Formatting` WHERE pid = '$pid' AND Method = 'Multi'";
		$sth = $dbh->prepare($query);
		$sth->execute();
		while (my @row = $sth->fetchrow_array()) {
				my $currsid = $row[0];
				## print out
				&PrintOut($currsid);
				## delete from todo list
				$dbh->do("DELETE FROM `CNVanalysis-TMP`.`Formatting` WHERE pid = '$pid' AND sid = '$currsid'");
				delete($formattodo{$currsid});
		}
		$sth->finish();
		## sleep 15 seconds before requerying
	sleep 15;
}


##############################
# FINISH & COPY RESULT FILES #
##############################
open OUT, ">>results_xml/".$prefix."_Results_total.xml";
print OUT "</Bookmarks>\n</Project_Bookmarks>\n";
close OUT;
$outname = "results_xml/" . $prefix . "_Results_total.xml";
`cp "$outname" "$targetname" && chmod a+rw "$targetname"`;

foreach (@algos) {
	#xml
	my $method = $_;
	open OUT, ">>results_xml/".$prefix."_Results_$method.xml";
	print OUT "</Bookmarks>\n</Project_Bookmarks>\n";
	close OUT;
	$outname = "results_xml/" . $prefix . "_Results_$method.xml";
	my $bookmarktargetname = "$sitedir/bookmarks/" . $projectname . "_$method.xml";
	`cp "$outname" "$bookmarktargetname" && chmod a+rw "$bookmarktargetname"`;
	#table
	my $target = "$sitedir/CNVlists/" . $projectname . "_$method.txt";
	my $src = "results_list/$prefix"."_$method"."_list.txt";
	`cp "$src" "$target" && chmod 755 "$target"`;	
}
if ($plink == 1) {
	my $method = 'PlinkHomoz';
	#xml
	open OUT, ">>results_xml/".$prefix."_Results_$method.xml";
	print OUT "</Bookmarks>\n</Project_Bookmarks>\n";
	close OUT;
	$outname = "results_xml/" . $prefix . "_Results_$method.xml";
	my $bookmarkname = "$sitedir/bookmarks/" . $projectname . "_$method.xml";
	`cp $outname "$bookmarkname" && chmod a+rw "$bookmarkname"`;
	#table
	my $target = "$sitedir/CNVlists/" . $projectname . "_$method.txt";
	my $src = "results_list/$prefix"."_$method"."_list.txt";
	`cp "$src" "$target" && chmod 755 "$target"`;	
}

print "\nAll files analysed and bookmark files created\n";
print "Extra analyses are now starting, but you can already browse the results.\n"; 




#################################################
## FAMILY ANALYSIS NEEDS ALL CNVS TO BE STORED ##
#################################################

#####################################################
# UPDATE FAMILY INFO IF PEDIGREE FILE IS AVAILABLE ##
#####################################################
if ($usepedigree == 1) {
	# new family information available, update the table first!
	print "Updating Family relations from pedigree file \n";
	## check for line endings
	my $pline = `head -n 1 $pedfile`;
	my $convert = 0;
	if( $pline =~ m/\r\n$/ ) {
		print "Pedigree Table : Detected DOS file format: converting to UNIX\n";
		$dump = system("cd /tmp/ && dos2unix -n '$pedfile' '$pedfile.tmp' && mv '$pedfile.tmp' '$pedfile' ");
	}
	elsif( $pline =~ m/\n\r$/ ) {
		print "Pedigree Table : Detected DOS file format: converting to UNIX\n";
		$dump = system("cd /tmp/ && dos2unix -n '$pedfile' '$pedfile.tmp' && mv '$pedfile.tmp' '$pedfile' ");
	}
	elsif( $pline =~ m/\r$/ ) {
		print "Pedigree Table : Detected MAC file format: converting to UNIX\n";
		open IN, "$pedfile";
		open OUT, ">/tmp/$rand.ped.tmpfile.txt";
		while (<IN>) {
				$_ =~ s/\r/\n/gi ;     # replace returns with newlines
				print OUT $_ ;
		}
		close OUT;
		close IN;
		$dump = `mv  \"/tmp/$rand.ped.tmpfile.txt \"$pedfile\"`;
	}
	
	# read the pedfile
	open PED, "$pedfile";
	$head = <PED>;
	while (<PED>) {
		## read pedigree file, syntax should be child;parent1;parent2
		chomp($_);
		if ($_ eq '') {
			next;
		}
		my @members = split(/\t|;|,/,$_);
		# reset variables
		for ($i = 0; $i < scalar(@members); $i++) {
			$members[$i] =~ s/[^A-Za-z0-9\-_]/_/g;
		}
		my $child;
		my $father;
		my $mother;
		if ($members[0] ne '') {  		# first entry must be child
			
			if (exists $idhash{$members[0]}) {	# check if included in this project
				$child = $idhash{$members[0]};	# 
			}
			else {					# else check if present in database
				my $sqsth = $dbh->prepare("SELECT id FROM sample WHERE chip_dnanr = '$members[0]'");
				$sqsth->execute();
				my $sqrows = $sqsth->rows();
				if ($sqrows == 0) {		# not present in db, ignoring (for now)
					$sqsth->finish();
					next;
				}
				else {				# in db, get id
					my @sqrow = $sqsth->fetchrow_array();
					$child = $sqrow[0];
				}
				$sqsth->finish();
			}
		}
		else {
			# child is needed as key
			next;
		}
		## parents are not mandatory.
		if ($members[1] ne '' && $members[1] !~ m/unknown/i){
			if (exists $idhash{$members[1]}) {	#check if included in project
				if ($genderhash{$members[1]} eq 'Male') {
					$father = $idhash{$members[1]};
				}
				else {
					$mother = $idhash{$members[1]};
				}
			}	
			else {
				my $sqsth = $dbh->prepare("SELECT id, gender FROM sample WHERE chip_dnanr = '$members[1]'");
				$sqsth->execute();
				my $sqrows = $sqsth->rows();
				if ($sqrows == 0) {		# not present in db, ignoring (for now)
				}
				else {				# in db, get id
					my @sqrow = $sqsth->fetchrow_array();
					if ($sqrow[1] eq 'Male') {
						$father = $sqrow[0];
					}
					else {
						$mother = $sqrow[0];
					}
				}
				$sqsth->finish();
			}
		}
		## parents are not mandatory.
		if ($members[2] ne '' && $members[1] !~ m/unknown/i){
			if (exists $idhash{$members[2]}) {	#check if included in project
				if ($genderhash{$members[2]} eq 'Male') {
					$father = $idhash{$members[2]};
				}
				else {
					$mother = $idhash{$members[2]};
				}
			}	
			else {
				my $sqsth = $dbh->prepare("SELECT id,gender FROM sample WHERE chip_dnanr = '$members[2]'");
				$sqsth->execute();
				my $sqrows = $sqsth->rows();
				if ($sqrows == 0) {		# not present in db, ignoring (for now)
				}
				else {				# in db, get id
					my @sqrow = $sqsth->fetchrow_array();
					if ($sqrow[1] eq 'Male') {
						$father = $sqrow[0];
					}
					else {
						$mother = $sqrow[0];
					}
				}
				$sqsth->finish();
			}

		}
		## check if info is available in the database
		$checksth = $dbh->prepare("SELECT id, father, mother, father_project, mother_project FROM parents_relations WHERE id = '$child'");
		$checksth->execute();
		$crv = $checksth->rows();
		if ($crv == 0) {
			## if not, insert new info
			$dbh->do("INSERT INTO parents_relations (id, father, mother) VALUES ('$child', '$father', '$mother')");
		}
		else {
			## else update
			my @crow = $checksth->fetchrow_array();
			my $oldfather = $crow[1];
			my $updated = 0;
			# update the father in relations only if different from listed (chip_dnanr) AND included in this project (benefit of WG-data)
			if ($oldfather != $father && $father != '' && exists $revidhash{$father} ) {
				$dbh->do("UPDATE parents_relations SET father = '$father', father_project = '0' WHERE id = '$child'");
				$updated = 1;
			}
			my $oldmother = $crow[2];
			# update the mother in relations only if different from listed (chip_dnanr) AND included in this project
			if ($oldmother != $mother && $mother != '' && exists $revidhash{$mother}) {
				$dbh->do("UPDATE parents_relations SET mother = '$mother', mother_project = '0' WHERE id = '$child'");
				$updated = 1;
			}
			if ($updated == 1) {
				print "REMARK : Parents of $members[0] were updated !\n";
			}
			# SHOULD DATA FROM PARENTS INFO BE DELETED ? no, if changed back afterwards, datapoints are still stored.  
			#$dbh->do("UPDATE parents_relations SET father, mother) VALUES ('$child', '$father', '$mother')");
		}
		$checksth->finish();
	}
}

#######################################################
## RUN FAMILY ANALYSIS ON ALL SAMPLES IN GENDER FILE ##
#######################################################
my %children;
my @parents; # these have to be processed last!
## estimate wall time. 
$walltime = '-l walltime=30:00';
if ($setwalltime == 1) {
	my $query = "SELECT AVG(time) FROM `GenomicBuilds`.`RuntimeStatistics` WHERE process = 'FamilyAnalysis' AND chiptypeid = '$chiptypeid'";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my ($seconds) = $sth->fetchrow_array();
	$sth->finish();
	if ($seconds > 0) {
		$walltime = '-l walltime=' . int($seconds);
	}
}	
else {
	$walltime = '';
}
foreach (keys(%idhash) ) {
	my $samplename = $_;
	my $currid = $idhash{$samplename};
	# check if family relations are available
	# this is done without a link to the provided pedigree file (if any)
	# everything in that file is now also in the db
	my $query = "SELECT father, mother FROM parents_relations WHERE id = '$currid'";
	my $prsth = $dbh->prepare($query);
	$prsth->execute();
	my $rv = $prsth->rows();
	if ($rv == "0") {
		# perhaps it's a parent?
		my $sq = "SELECT id, father, mother FROM parents_relations WHERE father = '$currid' OR mother = '$currid'";
		my $ssth = $dbh->prepare($sq);
		$ssth->execute();
		my $srv = $ssth->rows();
		if ($srv == "0") {
			#siblings? not supported (yet) or a sample without family info, so skipping
		}
		else {
			my @srow = $ssth->fetchrow_array();
			my $child = $srow[0];
			my $fatherid = $srow[1];
			my $motherid = $srow[2];
			push(@parents,"$child-$fatherid-$motherid"); 
		}
		$ssth->finish();		

	}
	else {
		# entry found, it's a child
		my @prrow = $prsth->fetchrow_array();
		my $fatherid = $prrow[0];
		my $motherid = $prrow[1];
		$children{$currid} =1;
		# queue by db_id of child
		$dbh->do("INSERT INTO `CNVanalysis-TMP`.`FamilyAnalysis` (pid, sid, status) VALUES ('$pid', '$currid', '0')");


		## create the job script
		$jobname = "$pid.$currid.FamilyAnalysis";
		$jobfile = "$qsubscripts/$pid/$jobname.sh";
		open OUT, ">$jobfile";
		&PBSWriteHeader("OUT", $pid, $jobname, 1, 2, $walltime, $scriptdir);
		&PBSWriteCommand("OUT", "perl $scriptdir/Bin/FamilyAnalysis.pl -D $db -p $pid -s $currid");
		&PBSWriteEnd("OUT");
		close OUT;

		## submit job
		$jobid = `qsub $jobfile`;
		chomp($jobid);
		while ($jobid !~ m/^\d+\..*/) {
			sleep 1;
			$jobid = `qsub $jobfile`;
		}


	}
	$prsth->finish();
}
# next check parents that were present in the project 
foreach (@parents) {
	my @parts = split(/-/,$_); # childID,parent1ID,parent2ID
	if (exists $children{$parts[0]}) {
		# child already done, skipping
		next;
	}
	else {	# child was not present in this project, so enqueing for analysis (always based on the childid)
		# queue by db_id of child 
		my $currid = $parts[0];
		$children{$currid} = 1;
		my $parentname = $revidhash{$currid};
		$dbh->do("INSERT INTO `CNVanalysis-TMP`.`FamilyAnalysis` (pid, sid, status) VALUES ('$pid', '$currid', '0')");

		## create the job script
		$jobname = "$pid.$currid.FamilyAnalysis";
		$jobfile = "$qsubscripts/$pid/$jobname.sh";
		open OUT, ">$jobfile";
		&PBSWriteHeader("OUT", $pid, $jobname, 1, 2, $walltime, $scriptdir);
		&PBSWriteCommand("OUT", "perl $scriptdir/Bin/FamilyAnalysis.pl -D $db -p $pid -s $currid");
		&PBSWriteEnd("OUT");
		close OUT;

		## submit job
		$jobid = `qsub $jobfile`;
		chomp($jobid);
		while ($jobid !~ m/^\d+\..*/) {
			sleep 1;
			$jobid = `qsub $jobfile`;
		}


	}

}
# finally check if there are new family relations for samples not included in the project

###################
## CHECK FOR UPD ##
###################  
if ($checkupd == 1) {
	## estimate wall time. 
	$walltime = '-l walltime=30:00';
	if ($setwalltime == 1) {
		my $query = "SELECT AVG(time) FROM `GenomicBuilds`.`RuntimeStatistics` WHERE process = 'UPD' AND chiptypeid = '$chiptypeid'";
		my $sth = $dbh->prepare($query);
		$sth->execute();
		my ($seconds) = $sth->fetchrow_array();
		$sth->finish();
		if ($seconds > 0) {
			$walltime = '-l walltime=' . int($seconds);
		}
	}
	else {
		$walltime = '';
	}	
	# all pedigree relations are read in by now, so no need to re-update family relations. 
	# CheckUPD.pl can extract needed info based on pid, sid and db info
	# 
	# Here, we check if all data is available before submitting jobs. 
	# Actual data-extraction is done in the jobs themselves if needed.
	open PED, "$pedfile";
	my $headline = <PED>;
	while (<PED>) {
		chomp($_);
		if ($_ eq '') {
			## skip empty lines
			next;
		}
		my @members = split(/\t|;|,/,$_);
		## check if all columns are filled.
		if ($members[0] eq '' || $members[1] eq '' || $members[2] eq '') {
			print "Skipping UPD analysis for $members[0] due to missing values\n";
			next;
		}
		for ($i = 0; $i < scalar(@members); $i++) {
			$members[$i] =~ s/[^A-Za-z0-9\-_]/_/g;
		}

		## reset variables
		my $childname;
		my $childid;
		#my $fathername;
		#my $fatherid;
		#my $mothername;
		#my $motherid;
		my $parentid;
		## first column is child, get details
		$childname = $members[0];
		my $sqsth = $dbh->prepare("SELECT id FROM sample WHERE chip_dnanr = '".$members[0]."'");
		$sqsth->execute();
		my $sqrows = $sqsth->rows();
		if ($sqrows == 0) {		
			# not present in db, needs to be !
			print "Offspring '".$members[0] . "' is not present in the database, UPD-check not possible\n";
			next;
		}
		else {	
			# in db, get id
			my @sqrow = $sqsth->fetchrow_array();
			$childid = $sqrow[0];
		}
		$sqsth->finish();
		if (!$allsamples{$childid}) {
			# data not available, skip !
			print "No data found for offspring '$childname', UPD-analysis not possible\n";
			next;
		}

		## second column: check ID and datafile
		my $sqsth = $dbh->prepare("SELECT id FROM sample WHERE chip_dnanr = '".$members[1]."'");
		$sqsth->execute();
		my $sqrows = $sqsth->rows();
		if ($sqrows == 0) {		
			# not present in db, needs to be !
			print "Parent '".$members[1] . "' of '$childname' is not present in the database, UPD-analysis not possible \n";
			next;
		}
                else {
                        # in db, get id
                        my @sqrow = $sqsth->fetchrow_array();
                        $parentid = $sqrow[0];
                }
		$sqsth->finish();
		if (!$allsamples{$parentid}) {
			# no datafile found for parent 1
			print "No data found for Parent '".$members[1]."' of '$childname', UPD-analysis not possible\n";
			next;
		}

		# third column: check ID and datafile
		my $sqsth = $dbh->prepare("SELECT id FROM sample WHERE chip_dnanr = '".$members[2]."'");
		$sqsth->execute();
		my $sqrows = $sqsth->rows();
		if ($sqrows == 0) {		
			# not present in db, needs to be !
			print "Parent '".$members[2] . "' of '$childname' is not present in the database, UPD-analysis not possible\n";
			next;
		}
                else {
                        # in db, get id
                        my @sqrow = $sqsth->fetchrow_array();
                        $parentid = $sqrow[0];
                }
		$sqsth->finish();
		if (!$allsamples{$parentid}) {
			print "No data found for Parent '".$members[2]."' of '$childname', UPD-analysis not possible\n";
			next;
		}
		# all data present, submit job to queue.
		$dbh->do("INSERT INTO `CNVanalysis-TMP`.`UPD` (pid, sid, status) VALUES ('$pid', '$childid', '0')");

		## create the job script
		$jobname = "pid.$childid.UPDAnalysis";
		$jobfile = "$qsubscripts/$pid/$jobname.sh";
		open OUT, ">$jobfile";
		&PBSWriteHeader("OUT", $pid, $jobname, 1, 2, $walltime, $scriptdir);
		&PBSWriteCommand("OUT", "perl $scriptdir/Bin/CheckUPD.pl -D $db -p $pid -s $childid");
		&PBSWriteEnd("OUT");
		close OUT;

		## submit job
		$jobid = `qsub $jobfile`;
		chomp($jobid);
		while ($jobid !~ m/^\d+\..*/) {
			sleep 1;
			$jobid = `qsub $jobfile`;
		}


	}
}
##################################
## triPOD (trio based mosaicism ##
##################################  
if ($triPOD == 1) {
	## estimate wall time. 
	$walltime = ' -l walltime=30:00';
	if ($setwalltime == 1) {
		my $query = "SELECT AVG(time) FROM `GenomicBuilds`.`RuntimeStatistics` WHERE process = 'triPOD' AND chiptypeid = '$chiptypeid'";
		my $sth = $dbh->prepare($query);
		$sth->execute();
		my ($seconds) = $sth->fetchrow_array();
		$sth->finish();
		if ($seconds > 0) {
			$walltime = ' -l walltime=' . int($seconds);
		}
	}
	else {
		$walltime = '';
	}	
	# all pedigree relations are read in by now, so no need to re-update family relations. 
	# Run_triPOD.pl can extract needed info based on pid, sid and db info
	# 
	# Here, we check if all data is available before submitting jobs. 
	# Actual data-extraction is done in the jobs themselves if needed.
	open PED, "$pedfile";
	my $headline = <PED>;
	while (<PED>) {
		chomp($_);
		if ($_ eq '') {
			## skip empty lines
			next;
		}
		my @members = split(/\t|;|,/,$_);
		## check if all columns are filled.
		if ($members[0] eq '' || $members[1] eq '' || $members[2] eq '') {
			print "Skipping triPOD analysis for $members[0] due to missing values\n";
			next;
		}
		for ($i = 0; $i < scalar(@members); $i++) {
			$members[$i] =~ s/[^A-Za-z0-9\-_]/_/g;
		}

		## reset variables
		my $childname;
		my $childid;
		my $fatherid;
		my $fathername;
		my $motherid;
		my $mothername;
		my $parentid;
		## first column is child, get details
		$childname = $members[0];
		my $sqsth = $dbh->prepare("SELECT id FROM sample WHERE chip_dnanr = '".$members[0]."'");
		$sqsth->execute();
		my $sqrows = $sqsth->rows();
		if ($sqrows == 0) {		
			# not present in db, needs to be !
			print "Offspring '".$members[0] . "' is not present in the database, triPOD-analysis not possible\n";
			next;
		}
		else {	
			# in db, get id
			my @sqrow = $sqsth->fetchrow_array();
			$childid = $sqrow[0];
		}
		$sqsth->finish();
		if (!$allsamples{$childname}) {
			# data not available, skip !
			print "No data found for offspring '$childname', triPOD-analysis not possible\n";
			next;
		}

		## second column: check ID and datafile
		my $sqsth = $dbh->prepare("SELECT id FROM sample WHERE chip_dnanr = '".$members[1]."'");
		$sqsth->execute();
		my $sqrows = $sqsth->rows();
		if ($sqrows == 0) {		
			# not present in db, needs to be !
			print "Parent '".$members[1] . "' of '$childname' is not present in the database, triPOD-analysis not possible \n";
			next;
		}
                else {
                        # in db, get id
                        my @sqrow = $sqsth->fetchrow_array();
                        $parentid = $sqrow[0];
                }
		$sqsth->finish();
		if (!$allsamples{$parentid}) {
			# no datafile found for parent 1
			print "No data found for Parent '".$members[1]."' of '$childname', triPOD-analysis not possible\n";
			next;
		}

		# third column: check ID and datafile
		my $sqsth = $dbh->prepare("SELECT id FROM sample WHERE chip_dnanr = '".$members[2]."'");
		$sqsth->execute();
		my $sqrows = $sqsth->rows();
		if ($sqrows == 0) {		
			# not present in db, needs to be !
			print "Parent '".$members[2] . "' of '$childname' is not present in the database, triPOD-analysis not possible\n";
			next;
		}
                else {
                        # in db, get id
                        my @sqrow = $sqsth->fetchrow_array();
                        $parentid = $sqrow[0];
                }
		$sqsth->finish();
		if (!$allsamples{$parentid}) {
			print "No data found for Parent '".$members[2]."' of '$childname', triPOD-analysis not possible\n";
			next;
		}

		# all data present, submit job to queue.
		$dbh->do("INSERT INTO `CNVanalysis-TMP`.`triPOD` (pid, sid, status) VALUES ('$pid', '$childid', '0')");

		## create the job script
		$jobname = "$pid.$childid.triPOD";
		$jobfile = "$qsubscripts/$pid/$jobname.sh";
		open OUT, ">$jobfile";
		&PBSWriteHeader("OUT", $pid, $jobname, 1, 6, $walltime, $scriptdir);
		&PBSWriteCommand("OUT", "perl $scriptdir/Bin/Run_triPOD.pl -D $targetdb -p $pid -s $childid -r $rand");
		&PBSWriteEnd("OUT");
		close OUT;

		## submit job
		$jobid = `qsub $jobfile`;
		chomp($jobid);
		while ($jobid !~ m/^\d+\..*/) {
			sleep 1;
			$jobid = `qsub $jobfile`;
		}


	}
}

############################################
## WAIT FOR THE FAMILY/UPD JOBS TO FINISH ##
############################################
$continue = 1;
while ($continue == 1) {
	my $todo = 0;
	$sth = $dbh->prepare("SELECT sid, status FROM `CNVanalysis-TMP`.`FamilyAnalysis` WHERE pid = '$pid'");
	$sth->execute();
	## check family analysis
	while (my @row = $sth->fetchrow_array()) {
		if ($row[1] == 1) {
			my $currsid = $row[0];
			$dbh->do("DELETE FROM `CNVanalysis-TMP`.`FamilyAnalysis` WHERE pid = '$pid' AND sid = '$currsid'");
		}
		else {
			$todo++;
		}
	}
	$sth->finish();
	## check upd
	$sth = $dbh->prepare("SELECT sid, status FROM `CNVanalysis-TMP`.`UPD` WHERE pid = '$pid'");
	$sth->execute();
	while (my @row = $sth->fetchrow_array()) {
		if ($row[1] == 1) {
			my $currsid = $row[0];
			$dbh->do("DELETE FROM `CNVanalysis-TMP`.`UPD` WHERE pid = '$pid' AND sid = '$currsid'");
		}
		else {
			$todo++;
		}
	}
	$sth->finish();
	## check triPOD 
	$sth = $dbh->prepare("SELECT sid, status FROM `CNVanalysis-TMP`.`triPOD` WHERE pid = '$pid'");
	$sth->execute();
	while (my @row = $sth->fetchrow_array()) {
		if ($row[1] == 1) {
			my $currsid = $row[0];
			$dbh->do("DELETE FROM `CNVanalysis-TMP`.`triPOD` WHERE pid = '$pid' AND sid = '$currsid'");
		}
		else {
			$todo++;
		}
	}
	$sth->finish();

	## items left?
	if ($todo > 0) {
		sleep 30;
	}
	else {
		$continue = 0;
	}
}

#######################
## update user stats ##
#######################
#unlink("$fortrack");
my $query = "UPDATE users SET projectsrun = (projectsrun + 1), samplesrun = (samplesrun + $nrsamples) WHERE id = '$uid'";
$dbh->do($query); 


########################
## START PDF CREATION ##
########################
print "\n\n => Analysis Complete !\n";
print " => Starting up Whole Genome Plot creation in the background.\n";
## is run on local system, distributes jobs and waits to clean up all tmp files.
$dump = system("$scriptdir/Bin/CreatePdfsMain.pl -p \"$pid\" -D \"$db\" > $cjo/$scriptuser/$pid/$pid.CreatePfsMain.out.txt 2>&1 &");

##################
# PRINT RUN-TIME #
##################
$now = time - $now;
printf("\n\nRunning time:%02d:%02d:%02d\n",int($now/3600),int(($now % 3600)/60),int($now % 60));
$dump = system("echo 0 > $sitedir/status/status.$projectname");

## update project to finished
$query = "UPDATE project SET finished = 1 WHERE id = $pid";
$query = $dbh->do($query);

#######################
## SHARE THE PROJECT ##
#######################
if ($sharegroup != 0) {
	# get permissions of usergroup
	my $sth = $dbh->prepare("SELECT editcnv,editclinic,editsample FROM `usergroups` WHERE id = '$sharegroup'");
	$sth->execute();
	my ($cnv,$clin,$sample) = $sth->fetchrow_array();
	print "Usergroup permissions: cnv: $cnv ; clin: $clin ; sample: $sample\n";
	$sth->finish();
	## get users in usergroup
	$sth = $dbh->prepare("SELECT uid FROM `usergroupuser` WHERE gid = '$sharegroup'");
	$sth->execute();
	## share with individual users
	while (my ($nuid) = $sth->fetchrow_array()) {
		## increase/add permissions 
		$dbh->do("INSERT INTO projectpermission (projectid, userid, editcnv, editclinic,editsample) VALUES ('$pid','$nuid', '$cnv', '$clin','$sample') ON DUPLICATE KEY UPDATE editcnv = if(editcnv >= '$cnv', editcnv,$cnv), editclinic = if(editclinic >= '$clin',editclinic,'$clin'),editsample = if(editsample >= '$sample', editsample,'$sample')");
	}
	## share with usergroup
	$dbh->do("INSERT INTO `projectpermissiongroup` (projectid,groupid,sharedby) VALUES ('$pid','$sharegroup','$uid')");
}

#######################
## send mail to user ##
#######################
# first compose message.
open OUT, ">$datadir/$pid.mail.txt";
print OUT "to: $email\n";
print OUT "subject: CNV-WebStore Project Finished\n";
print OUT "from: no-reply\@cnv-webstore.uza.be\n\n";
print OUT "Your Project '$projectname' is finished. Use the following links to access the results (after logging in)\n\n";
print OUT "- Runtime Overview : $baselink"."index.php?page=result&type=multiple&r=$rand\n";
print OUT "- Runtime Details : $baselink"."index.php?page=browseruntimeoutput&p=$pid\n";
close OUT;
system("sendmail $email < $datadir/$pid.mail.txt");

exit();

###################
### SUBROUTINES ###
###################


###############################################
## PRINT OUT FORMATTED RESULTS FROM DATABASE ##
###############################################
sub PrintOut {
	my $currsid = shift;
	## get finished samples => table contains results from FormatResults Jobs
	my $query = "SELECT sid,nrabs,xml,tab FROM `CNVanalysis-TMP`.`Formatting` WHERE pid = '$pid' AND sid = '$currsid' AND Method = 'Multi'";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my @row = $sth->fetchrow_array();
	$sth->finish();
	my $nrabs = $row[1];
	my $mxml = $row[2];
	my $mtab = $row[3];
	my $sname = $revidhash{$currsid};
	## check if mosaicism is suspected
	my $subquery = "SELECT indications FROM `CNVanalysis-TMP`.`BAFSEG` WHERE pid = '$pid' AND sid = '$currsid'";
	my $ssth = $dbh->prepare($subquery);
	$ssth->execute();
	my $rows = $ssth->rows();
	my @row = $ssth->fetchrow_array();
	my @inds;
	if ($row[0] != 'no indication') {
		my @p = split(/\n/, $row[0]);
		foreach (@p) {
			my @q = split(/\t/, $_);
			push(@inds, $q[0]);
		}
	}
	$ssth->finish();
	
	my $ind;
	if (@inds) {
		$ind = " and indications of mosaicism on chromosomes ". join(",", @inds);
	}
	else {
		$ind = " and no indications of mosaicism";
	}

	print "  => Sample $sname : $nrabs aberrations $ind\n";
	## write out xml files and multi list (from database)
	open OUT, ">>results_xml/".$prefix."_Results_total.xml";
	open OUTRES, ">>results_list/$prefix"."_multi.txt";
	print OUT $mxml;
	print OUTRES $mtab;
	close OUT;
	close OUTRES;
	
	# seperate methods
	for my $method (keys(%versions)) {
		my $version = $versions{$method};
		my $query = "SELECT xml,tab FROM `CNVanalysis-TMP`.`Formatting` WHERE pid = '$pid' AND sid = '$currsid' AND Method = '$method' AND Version = '$version'";
		my $sth = $dbh->prepare($query);
		$sth->execute();
		my @row = $sth->fetchrow_array();
		$sth->finish();
		my $xml = $row[0];
		my $tab = $row[1];
		#xml
		open OUT, ">>results_xml/".$prefix."_Results_$method.xml";
		print OUT $xml;
		close OUT; 
		#tabular
		open OUT, ">>results_list/$prefix"."_$method"."_list.txt";	
		print OUT $tab;
		close OUT;
	}
	#plink if specified
	if ($plink == 1) {
		my $method = 'PlinkHomoz';
		my $query = "SELECT xml,tab,Version FROM `CNVanalysis-TMP`.`Formatting` WHERE pid = '$pid' AND sid = '$currsid' AND Method = '$method'";
		my $sth = $dbh->prepare($query);
		$sth->execute();
		my @row = $sth->fetchrow_array();
		$sth->finish();
		my $xml = $row[0];
		my $tab = $row[1];
		my $version = $row[2];
		#xml
		open OUT, ">>results_xml/".$prefix."_Results_$method.xml";
		print OUT $xml;
		close OUT; 
		#tabular
		open OUT, ">>results_list/$prefix"."_$method"."_list.txt";	
		print OUT $tab;
		close OUT;


	}
}

sub PBSWriteCommand {
	my ($filehandle, $run_command) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	my $echo_command = $run_command;
	print $filehandle "echo 'Command:'\n";
	print $filehandle "echo '========'\n";
	$echo_command =~ s/"/\\"/g;
	print $filehandle "echo \"$echo_command\"\n";
	print $filehandle "$run_command\n";

}

sub PBSWriteHeader {
	my ($filehandle, $projectid, $job, $cpu, $mem, $time, $dir) = @_;
	if (tell($filehandle) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "#!/usr/bin/env bash\n";
	print $filehandle "#PBS -m a\n";
	print $filehandle "#PBS -M $adminemail\n";
	print $filehandle "#PBS -d $dir\n";
	print $filehandle "#PBS -l nodes=1:ppn=$cpu,mem=$mem"."g\n";
	print $filehandle "#PBS -N $job\n";
	print $filehandle "#PBS -o $cjo/$scriptuser/$projectid/$job.o.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -e $cjo/$scriptuser/$projectid/$job.e.txt.\$PBS_JOBID\n";
	print $filehandle "#PBS -V\n";
	if ($queuename ne '') {
		print $filehandle "#PBS -q $queuename\n";
	}
	if ($hpcaccount ne '') {
		print $filehandle "#PBS -A $hpcaccount\n";
	}
	if ($setwalltime == 1 && $time) {
		print $filehandle "#PBS $time\n";
	}
	#if ($afterok) {
	#	print $filehandle " #PBS -W depend=afterok:$afterok\n";
	#}

	print $filehandle "\necho 'Running on : ' `hostname`\n";
	print $filehandle "echo 'Start Time : ' `date`\n\n";
	print $filehandle "echo 'setting path: $path'\n";
	print $filehandle "export PATH=$path\n\n";

}

sub PBSWriteEnd {
	my $filehandle = shift;
	if (tell( $filehandle ) == -1) {
		print "Outputfile '$filehandle' is not open for writing! Exit script\n";
		exit;
	}
	print $filehandle "\n\necho 'End Time : ' `date`\n";
	print $filehandle "printf 'Execution Time = \%dh:\%dm:\%ds\\n' \$((\$SECONDS/3600)) \$((\$SECONDS%3600/60)) \$((\$SECONDS%60))\n";

}

