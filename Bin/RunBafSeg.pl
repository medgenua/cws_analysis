#!/usr/bin/perl

## keep track of runtime 
$now = time;

################
# LOAD MODULES #
################
use Number::Format;
use DBI;
use Getopt::Std;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

# print the host running the job #
$hostname = `hostname`;
print "Running Job on $hostname";

###############################
# SET NUMBER FORMATTING STYLE #
###############################
my $de = new Number::Format(-thousands_sep =>',',-decimal_point => '.');

##########################
# COMMAND LINE ARGUMENTS #
##########################
# p = project id
# s = sample id
# b = baf standard deviation
getopts('p:s::b:', \%opts);  # option are in %opts
$sid = $opts{'s'};
$pid = $opts{'p'};
$bafsd = $opts{'b'};
###########################
# CONNECT TO THE DATABASE #
###########################

my $db;
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbhgb)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass) ;
	}

	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "Assuming build ".$row[1]."\n";
	$gbsth->finish();
	$dbhgb->disconnect;	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

$dbh->{mysql_auto_reconnect} = 1;
##############################
## change working directory ##
##############################
chdir($scriptdir);

########################
## GET SAMPLE DETAILS ##
########################
my $query = "SELECT chip_dnanr, gender FROM sample WHERE id = '$sid'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @row = $sth->fetchrow_array();
$CurrSample = $row[0];
$gender = $row[1];
$sth->finish();
$query = "SELECT idx FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'";
$sth = $dbh->prepare($query);
$sth->execute();
@row = $sth->fetchrow_array();
$CurrIndex = $row[0];
$sth->finish();
$query = "SELECT chiptype,chiptypeid FROM project WHERE id = '$pid'";
$sth = $dbh->prepare($query);
$sth->execute();
@row = $sth->fetchrow_array();
$chiptype = $row[0];
$chiptypeid = $row[1];
$sth->finish();
$query = "SELECT indications FROM `CNVanalysis-TMP`.`BAFSEG` WHERE pid = '$pid' AND sid = '$sid'";
$sth = $dbh->prepare($query);
$sth->execute();
@row = $sth->fetchrow_array();
$indication = $row[0];
$sth->finish();
# disconnect from db
$dbh->disconnect();

###########################
## GUESS PDFJOIN VERSION ##
###########################
my $lines = `pdfjoin --help | grep '\\\-\\\-orient' | wc -l`;
chomp($lines);
if ($lines > 0) {
	print "Using pdfjoin with --orient landscape option\n";
	$args = '--orient landscape';
}
else {
	print "Using pdfjoin with --landscape option\n";
	$args = '--landscape';
}
#########################
## RUN BAFsegmentation ##
#########################
my $bafdir = "/tmp/BAF_$pid.$sid";
my $filename = "$datadir/$pid.$sid.txt";
my $bafcp = "cp -Rf $scriptdir/Analysis_Methods/Runtime/BAFSegment $bafdir && cp $filename $bafdir/extracted/ ";
system($bafcp);
open SOUT, ">>$bafdir/extracted/sample_names.txt";
print SOUT "$CurrSample\t$pid.$sid.txt\t$CurrIndex\n";
close SOUT;
#$bafcommand = "cd $bafdir && perl BAFSegment_default.pl --chiptype $chiptype --sid $sid --pid $pid --sname $CurrSample";
$bafcommand = "cd $bafdir && perl BAF_segment_samples.pl $chiptype $bafsd";
$dump = system($bafcommand);
$join = "cd $bafdir && pdfjoin --fitpaper false --paper a4paper $args";
for ($i = 1; $i<=24; $i++) {
	$command = "cd $bafdir && convert plots/$CurrSample"."_chr_$i.png pdfs/$CurrSample"."_chr_$i.pdf";
	#print "$command\n\n";
	$dump = system($command);
	$join .= " pdfs/$CurrSample"."_chr_$i.pdf";
}
$join .= " --outfile pdfs/$CurrSample.pdf ";
#print "$join\n\n";
$dump = system($join);
my $pdffile = "$bafdir/pdfs/".$CurrSample.'.pdf';
$filesize = -s $pdffile;
open MYFILE, "$bafdir/segmented/AI_regions.txt";
my $resfile;
while (<MYFILE>) {
	$resfile .= $_;
}
close MYFILE;

###################
## STORE RESULTS ##
###################
## reconnect to dbh
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
$dbh->{mysql_auto_reconnect} = 1;
#store
my $fullfile = "BAFSEG/BAFSEG_$pid"."_$sid.pdf";
my $sql = "INSERT INTO BAFSEG (idsamp, idproj, filename, resultlist,indications) VALUES ('$sid', '$pid', '$fullfile', ?, ?)";
my $sth = $dbh->prepare($sql);
my $numrows = $sth->execute($resfile,$indication);
$sth->finish;
$copystring = "cp '$pdffile' '$plotdir/$fullfile' && chmod 777 '$plotdir/$fullfile'";
$dump = system("$copystring");
my $bafdel = "rm -Rf $bafdir";
$dump = system($bafdel);

################################
## UPDATE STATUS IN DATABASE  ##
################################
$dbh->do("UPDATE `CNVanalysis-TMP`.`BAFSEG` SET status = 1 WHERE pid = '$pid' AND sid = '$sid'");

$now = time - $now;
print "Analysis finished\n";
printf("\n\nTotal running time: %02d:%02d:%02d\n\n", int($now / 3600), int(($now % 3600) / 60),
int($now % 60));
$dbh->do("INSERT INTO `GenomicBuilds`.`RuntimeStatistics` (process,chiptypeid,argument,time) VALUES ('BAFSeg','$chiptypeid',' ','$now')");


