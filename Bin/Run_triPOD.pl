#!/usr/bin/perl

## keep track of runtime 
$now = time;

################
# LOAD MODULES #
################
use Number::Format;
use DBI;
use Getopt::Std;
use XML::Simple;
# read credentials
use Cwd 'abs_path';
$credpath = abs_path(".LoadCredentials.pl");
print "credpath : $credpath\n";
require($credpath);
print "userid: $userid\n";

# print the host running the job #
$hostname = `hostname`;
print "Running Job on $hostname";

###############################
# SET NUMBER FORMATTING STYLE #
###############################
my $de = new Number::Format(-thousands_sep =>',',-decimal_point => '.');

##########################
# COMMAND LINE ARGUMENTS #
##########################
# p = project id
# s = sample id
# b = baf standard deviation
# r = random ID of project.
getopts('p:s:D:r:', \%opts);  # option are in %opts
$sid = $opts{'s'};
$pid = $opts{'p'};
$rand = $opts{'r'};
###########################
# CONNECT TO THE DATABASE #
###########################

my $db;
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
}
else {
	die('No database specified');
}
$shortdb = $targetdb;
$shortdb =~ s/^-//;

$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

$dbh->{mysql_auto_reconnect} = 1;

## if coming from addped.pl, the pid does not correspond to a real pid, so get and compare from db. 
$filepid = $pid;
$sth = $dbh->prepare("SELECT COUNT(idsamp) FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
$sth->execute();
#$sth->finish();
my ($count) = $sth->fetchrow_array();
$sth->finish();
if ($count == 0) {
	print "sample '$sid' not found in provided project '$pid'. Fetching other project id from database\n";
	$sth = $dbh->prepare("SELECT trackfromproject FROM sample WHERE id = '$sid'");
	$sth->execute();
	($pid) = $sth->fetchrow_array();
	$sth->finish();
	if ($pid == 0) {
		$sth = $dbh->prepare("SELECT idproj FROM projsamp WHERE idsamp = '$sid' ORDER BY idproj ASC LIMIT 1");
		$sth->execute();
		($pid) = $sth->fetchrow_array();
		$sth->finish();
	}
}
else {
	print "Sample '$sid' is present in provided project '$pid'. Assuming this is a valid hit.\n";
}

#######################################
## GET PROJECT DETAILS FROM DATABASE ##
#######################################
my $query = "SELECT chiptype,chiptypeid FROM project WHERE id = '$pid'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @array = $sth->fetchrow_array();
my $chiptype = $array[0];
my $chiptypeid = $array[1];
$sth->finish();


##############################
## change working directory ##
##############################
chdir($scriptdir);


##########################
## INITIALISE VARIABLES ##
##########################
my %samples;
my %extrasamples;
my $mfile = 0;
my $ffile = 0;
my $ofile = 0;
my $minsize = 3; # keep fragments of at least 3 informative probes
@nrs=(1..22);
push(@nrs,'X');
my %chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 


##########################################
## GET ALL SAMPLES FROM CURRENT PROJECT ##
##########################################
## in project itself
$query = "SELECT idsamp FROM projsamp WHERE idproj = '$pid'";
my $sth = $dbh->prepare($query);
$sth->execute();
while (my @row = $sth->fetchrow_array()) {
	$samples{$row[0]} = 1;
}
$sth->finish();
## in structure of datafiles (0 = main during CNVanalysis)
for ($i = 0; $i<= 99; $i++) {
	#if (-e "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extra.$i.cols") {
	if (-e "$datadir/$filepid.datafile.$i.cols") {
		print "Reading column structure from $datadir/$filepid.datafile.$i.cols\n";
		#open IN, "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extra.$i.cols";
		open IN, "$datadir/$filepid.datafile.$i.cols";
		while (<IN>) {
			chomp($_);
			my @p = split(/=/,$_);
			$extrasamples{$p[0]}{'cols'} = $p[1];
			#print "Storing : $p[0] ==> $p[1]\n";
			#$extrasamples{$p[0]}{'file'} = "/var/tmp/datafiles/CNV-WebStore/runtime/$pid.extra.$i.txt";
			$extrasamples{$p[0]}{'file'} = "$datadir/$filepid.datafile.$i.txt";
		} 
		close IN;
	}
}

###################################
## GET FAMILY INFO FROM DATABASE ##
###################################
## family relations
my $query = "SELECT father, father_project, mother, mother_project FROM parents_relations WHERE id = '$sid'";
my $prsth = $dbh->prepare($query);
$prsth->execute();
my @res = $prsth->fetchrow_array();
my $fid = $res[0];
my $fatherpid = $res[1];
my $mid = $res[2];
my $motherpid = $res[3];
$prsth->finish();
## sample names
$query = "SELECT chip_dnanr,gender FROM sample WHERE id = ?";
$sth = $dbh->prepare($query);
$sth->execute($sid);
@res = $sth->fetchrow_array();
$child = $res[0];
$gender = $res[1];
$sth->execute($fid);
@res = $sth->fetchrow_array();
$father = $res[0];
$sth->execute($mid);
@res = $sth->fetchrow_array();
$mother = $res[0];
$sth->finish();
my %datahash = ();
$dbh->disconnect();
################################
## CREATE INPUT FILES ##
################################
## offspring
if (exists($samples{$sid})) {
	## in current project, so file exists
	$ofile = "$datadir/$filepid.$sid.txt";
	print "Using datafile from provided project: $ofile\n";
	#print "using samples-ofile: $ofile\n";
	unless (-e $ofile) {
		print "Creating datafile for offspring (in samples, could happen if pid got updated to sample not in track.):\n";
		## create file lock while creating the datafile
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$sid}{'cols'}; ## also contains samples from main project.
		my $datafile = $extrasamples{$sid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$sid.txt && cp /tmp/$filepid.extrasample.$sid.txt $ofile && rm -f /tmp/$filepid.extrasample.$sid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}
}
elsif (exists($extrasamples{$sid})) {
	## in extra files
	$ofile = "$datadir/$filepid.extrasample.$sid.txt";
	unless (-e $ofile) {
		## create file lock while creating the datafile
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$sid}{'cols'};
		my $datafile = $extrasamples{$sid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$sid.txt && cp /tmp/$filepid.extrasample.$sid.txt $ofile && rm -f /tmp/$filepid.extrasample.$sid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}
}
else {
	print "Could not determine the correct file for the child\n";
	print "  - pid: $pid\n";
	print "  - filepid : $filepid\n";
	print "  - mid : $sid\n";
	print "  - name: $child\n";
}
## father
if (exists($samples{$fid})) {
	## in current project, so file exists
	$ffile = "$datadir/$filepid.$fid.txt";
	unless (-e $ffile) {
		## create file lock while creating the datafile
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$fid}{'cols'};
		my $datafile = $extrasamples{$fid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$fid.txt && cp /tmp/$filepid.extrasample.$fid.txt $ffile && rm -f /tmp/$filepid.extrasample.$fid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}
}
elsif (exists($extrasamples{$fid})) {
	## in extra files
	$ffile = "$datadir/$filepid.extrasample.$fid.txt";
	unless (-e $ffile) {
		## create file lock while creating the datafile
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$fid}{'cols'};
		my $datafile = $extrasamples{$fid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$fid.txt && cp /tmp/$filepid.extrasample.$fid.txt $ffile && rm -f /tmp/$filepid.extrasample.$fid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}
}
else {
	print "Could not determine the correct file for the father\n";
	print "  - pid: $pid\n";
	print "  - filepid : $filepid\n";
	print "  - mid : $fid\n";
	print "  - name: $father\n";
}
## mother
if (exists($samples{$mid})) {
	## in current project, so file exists
	$mfile = "$datadir/$filepid.$mid.txt";
	unless (-e $mfile) {
		## create file lock while creating the datafile
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$mid}{'cols'};
		my $datafile = $extrasamples{$mid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$mid.txt && cp /tmp/$filepid.extrasample.$mid.txt $mfile && rm -f /tmp/$filepid.extrasample.$mid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}
}
elsif (exists($extrasamples{$mid})) {
	## in extra files
	$mfile = "$datadir/$filepid.extrasample.$mid.txt";
	unless (-e $mfile) {
		## create file lock while creating the datafile
		open DUMMY, ">>$datadir/dummy";
		flock DUMMY,2;
		my $cols = $extrasamples{$mid}{'cols'};
		my $datafile = $extrasamples{$mid}{'file'};
		my $command = "awk ' BEGIN { FS = \"\\t\" ; OFS = \"\\t\" } {print $cols} ' $datafile | grep -v -i nan > /tmp/$filepid.extrasample.$mid.txt && cp /tmp/$filepid.extrasample.$mid.txt $mfile && rm -f /tmp/$filepid.extrasample.$mid.txt";
		system($command);
		flock DUMMY,8;
		close DUMMY;
	}
}
else {
	print "Could not determine the correct file for the mother\n";
	print "  - pid: $pid\n";
	print "  - filepid : $filepid\n";
	print "  - mid : $mid\n";
	print "  - name: $mother\n";
}
## read in to hash.
open IN, $ffile or die("Could not open file $ffile\n");
$head = <IN>;
chomp($head);
my @c = split(/\t/,$head);
$chrcol = $poscol = $namecol = $gtcol = $bafcol = $lrrcol = -1;
$idx = -1;
my %headlines = ();
foreach(@c) {
	$idx++;
	if ($_  eq 'Name') {
		$namecol = $idx;
	}
	elsif($_ eq 'Chr') {
		$chrcol = $idx;
	}
	elsif($_ eq 'Position') {
		$poscol = $idx;
	}
	elsif($_ =~ m/GType/) {
		$gtcol = $idx;
		$headlines{'fgt'} = $_;
	}
	elsif ($_ =~ m/B\sAllele\sFreq/) {
		$bafcol = $idx;
		$headlines{'fbaf'} = $_;
	}
	elsif($_ =~ m/Log\sR\sRatio/) {
		$lrrcol = $idx;
		$headlines {$flrr} = $_;
	}
}
while (<IN>) {
	chomp;
	my @p = split(/\t/,$_);
	$datahash{$p[$chrcol]}{$p[$poscol]}{'string'} = "$p[$namecol]\t$p[$chrcol]\t$p[$poscol]\t$p[$gtcol]\t$p[$bafcol]\t$p[$lrrcol]";
	$datahash{$p[$chrcol]}{$p[$poscol]}{'count'} = 1;
}
close IN;
open IN, $mfile  or die("Could not open file $mfile\n");
$head = <IN>;
chomp($head);
my @c = split(/\t/,$head);
$chrcol = $poscol = $namecol = $gtcol = $bafcol = $lrrcol = -1;
$idx = -1;

foreach(@c) {
	$idx++;
	if ($_  eq 'Name') {
		$namecol = $idx;
	}
	elsif($_ eq 'Chr') {
		$chrcol = $idx;
	}
	elsif($_ eq 'Position') {
		$poscol = $idx;
	}
	elsif($_ =~ m/GType/) {
		$gtcol = $idx;
		$headlines{'mgt'} = $_;
	}
	elsif ($_ =~ m/B\sAllele\sFreq/) {
		$bafcol = $idx;
		$headlines{'mbaf'} = $_;
	}
	elsif($_ =~ m/Log\sR\sRatio/) {
		$lrrcol = $idx;
		$headlines{'mlrr'} = $_;
	}
}
while (<IN>) {
	chomp;
	my @p = split(/\t/,$_);
	$datahash{$p[$chrcol]}{$p[$poscol]}{'string'} = $datahash{$p[$chrcol]}{$p[$poscol]}{'string'} . "\t$p[$gtcol]\t$p[$bafcol]\t$p[$lrrcol]";
	$datahash{$p[$chrcol]}{$p[$poscol]}{'count'} = $datahash{$p[$chrcol]}{$p[$poscol]}{'count'} + 1;
}
close IN;
open IN, $ofile  or die("Could not open file $ofile\n");
$head = <IN>;
chomp($head);
my @c = split(/\t/,$head);
$chrcol = $poscol = $namecol = $gtcol = $bafcol = $lrrcol = -1;
$idx = -1;

foreach(@c) {
	$idx++;
	if ($_  eq 'Name') {
		$namecol = $idx;
	}
	elsif($_ eq 'Chr') {
		$chrcol = $idx;
	}
	elsif($_ eq 'Position') {
		$poscol = $idx;
	}
	elsif($_ =~ m/GType/) {
		$gtcol = $idx;
		$headlines{'ogt'} = $_;
	}
	elsif ($_ =~ m/B\sAllele\sFreq/) {
		$bafcol = $idx;
		$headlines{'obaf'} = $_;
	}
	elsif($_ =~ m/Log\sR\sRatio/) {
		$lrrcol = $idx;
		$headlines{'olrr'} = $_;
	}
}
while (<IN>) {
	chomp;
	my @p = split(/\t/,$_);
	$datahash{$p[$chrcol]}{$p[$poscol]}{'string'} = $datahash{$p[$chrcol]}{$p[$poscol]}{'string'} . "\t$p[$gtcol]\t$p[$bafcol]\t$p[$lrrcol]";
	$datahash{$p[$chrcol]}{$p[$poscol]}{'count'} = $datahash{$p[$chrcol]}{$p[$poscol]}{'count'} + 1;
}
close IN;

## make runtime dir & infile
my $tripoddir = "/tmp/triPOD_$pid.$sid";
my $filename = "$tripoddir/$pid.$sid.triPOD.txt";
my $cp = "cp -Rf $scriptdir/Analysis_Methods/Runtime/triPOD $tripoddir";
system($cp);

open OUT, ">$filename";
print OUT "Name\tChr\tPosition\t$headlines{'fgt'}\t$headlines{'fbaf'}\t$headlines{'flrr'}\t$headlines{'mgt'}\t$headlines{'mbaf'}\t$headlines{'mlrr'}\t$headlines{'ogt'}\t$headlines{'obaf'}\t$headlines{'olrr'}\n";
foreach(@nrs) {
	my $chr = $_;
	my @pos = sort { $a <=> $b } keys(%{$datahash{$chr}});
	foreach(@pos) {
		if ($datahash{$chr}{$_}{'count'} == 3) {
			print OUT $datahash{$chr}{$_}{'string'}."\n";
		}
	}
}
close OUT;

###########################
## GUESS PDFJOIN VERSION ##
###########################
my $lines = `pdfjoin --help | grep '\\\-\\\-orient' | wc -l`;
chomp($lines);
if ($lines > 0) {
	print "Using pdfjoin with --orient landscape option\n";
	$args = '--orient landscape';
}
else {
	print "Using pdfjoin with --landscape option\n";
	$args = '--landscape';
}
##################################
## READ IN METHOD CONFIGURATION ##
##################################
my $xml = new XML::Simple;
my $settingsfile = "$datadir/$rand.settings.xml";
my $settings = $xml->XMLin($settingsfile);
my $configuration = $settings->{'triPOD'}->{configuration};

# replacement placeholders
$replace{'gender'} = $gender;
$replace{'inputfile'} = $filename;
$replace{'build'} = $shortdb;

################
## RUN triPOD ##
################
$command = "cd $tripoddir && ";

my $launcher = $configuration->{general}->{launcher};
while ($launcher =~ m/%(.*?)%/) {
	$item = $1;
	$launcher =~ s/^(.*?)%($item)%(.*)/$1$replace{$item}$3/;
}

$command .= $launcher;
my $dash = $configuration->{general}->{dash};
for my $key (keys(%{$configuration->{input_values}})) {
	my $val = $configuration->{input_values}->{$key};
	while ($val =~ m/%(.*?)%/) {
		$item = $1;
		$val =~ s/^(.*?)%($item)%(.*)/$1$replace{$item}$3/;
	}
	$command .= " $dash$key $val";
} 
for my $key (keys(%{$configuration->{input_boolean}})) {
	$command .= " $dash$key";
} 

print "Starting triPOD analysis\n";
print $command."\n";
system($command);

# join plots.
$join = "cd $tripoddir && pdfjoin --fitpaper false --paper a4paper $args";
for ($i = 1; $i<=22; $i++) {
	print "$tripoddir/".$configuration->{input_values}->{out}."/$child"."_Chr_$i.pdf\n";
	if (-e "$tripoddir/".$configuration->{input_values}->{out}."/$child"."_Chr_$i.pdf") {
		$join .= " $tripoddir/".$configuration->{input_values}->{out}."/$child"."_Chr_$i.pdf";
	}
}
if (-e "$tripoddir/".$configuration->{input_values}->{out}."/$child"."_Chr_X.pdf") {;
	$join .= " $tripoddir/".$configuration->{input_values}->{out}."/$child"."_Chr_X.pdf";
}
$join .= " --outfile $tripoddir/".$configuration->{input_values}->{out}."/$child.pdf";
print "$join\n";
$dump = system($join);
my $pdffile = "$tripoddir/".$configuration->{input_values}->{out}."/$child.pdf";
$filesize = -s $pdffile;

open MYFILE, "$tripoddir/".$configuration->{input_values}->{out}."/$pid.$sid.triPOD_triPOD_Results.txt";
for ($i = 1; $i <= $configuration->{output}->{cnv_header}; $i++) {
	$header = <MYFILE>;
}
my $resfile;
while (<MYFILE>) {
	chomp;
	if ($_ eq '') {
		last;
	}
	$resfile .= $_."\n";
}
close MYFILE;

###################
## STORE RESULTS ##
###################
## reconnect to dbh
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
$dbh->{mysql_auto_reconnect} = 1;
#store
my $fullfile = "triPOD/triPOD_$pid"."_$sid.pdf";
my $sql = "INSERT INTO triPOD (idsamp, idproj, filename, resultlist) VALUES ('$sid', '$pid', '$fullfile', ?)";
my $sth = $dbh->prepare($sql);
my $numrows = $sth->execute($resfile);
$sth->finish;
$copystring = "mkdir -p '$plotdir/triPOD/' && cp '$pdffile' '$plotdir/$fullfile' && chmod 777 '$plotdir/$fullfile'";
$dump = system("$copystring");


my $del = "rm -Rf '$tripoddir'";
#print "$del\n";
$dump = system($del);

################################
## UPDATE STATUS IN DATABASE  ##
################################
$dbh->do("UPDATE `CNVanalysis-TMP`.`triPOD` SET status = 1 WHERE pid = '$pid' AND sid = '$sid'");

$now = time - $now;
print "Analysis finished\n";
printf("\n\nTotal running time: %02d:%02d:%02d\n\n", int($now / 3600), int(($now % 3600) / 60),
int($now % 60));
$dbh->do("INSERT INTO `GenomicBuilds`.`RuntimeStatistics` (process,chiptypeid,argument,time) VALUES ('triPOD','$chiptypeid',' ','$now')");


