message("loading libraries and functions")
#sessionInfo()
library("VanillaICE")
library("genefilter")
library("SNPchip")
args <- commandArgs(TRUE)
datafile <- args[1]
gender <- args[2]
prefix <- args[3]
sampleID <- args[4]
taufactor <- args[5]
taufactor <- as.numeric(taufactor)
hmm <- args[6]
variance <- args[7]
chiptype <- args[8]

if (hmm == "regular" ) { 
  mu <- log2(c(0, 1, 2, 2, 3, 4)/2)
}
if (hmm == "experimental") { 
  #if (chiptype != 'HumanCNV370quad' && chiptype != 'HumanCyto12v2.0') { 
#	  mu <- c(0, -0.45, 0, 0, 0.3, 0.75)  
  #}
  #else {
	  mu <- c(0, -0.44036, 0, 0, 0.2281, 0.75)
 # }
}
if (chiptype == "") {
	zeroedfile = "lib/zeroedsnps.txt"
}
if (chiptype != "") {
	zeroedfile <- paste("lib/",chiptype, ".zeroed.txt", sep = "")
}

message("reading in zeroed datapoints list")   
zeroed<- read.table(zeroedfile, as.is = TRUE, header = TRUE, sep = "\t", comment.char="")
zero<-zeroed$Name
message(paste("Loaded ",length(zero)," zero'ed probes.",sep=""))
message("reading in full data table")
classes <- c('character', 'character', 'integer', 'numeric', 'numeric', 'character')
sample <- read.table(datafile, as.is = TRUE, header = TRUE, sep = "\t", comment.char="",colClasses = classes)
message(paste("Loaded ",length(sample$Chr), " datapoints.",sep=""))
logrcol <- grep("Log.R",colnames(sample))
bafcol <- grep("B.Allele",colnames(sample))
## clean up file
message('Cleaning up data table (empty rows, chromosome or positions equal to zero, no LogR values)')
inisize <- length(sample$Chr)
nologR<-sample[sample[,logrcol]=="NaN","Name"]
message(paste("Discarded ",length(nologR)," datapoints by lack of LogR value.",sep=""))
sample<-sample[!sample$Name %in% nologR,]
sample<-sample[sample$Chr != "",]
sample<-sample[sample$Chr != 0,]
sample<-sample[sample$Position != 0,]
postsize <- length(sample$Chr)
disc <- inisize - postsize
message(paste("Discarded ", disc, " probes during data-table cleanup",sep=""));
inisize <- length(sample$Chr)
sample<-sample[sample$Chr!="MT",]
postsize <- length(sample$Chr)
disc <- inisize - postsize
message(paste("Discarded ", disc, " probes from the Mitochondrial chip content.", sep=""))
sample[sample$Name %in% zero,grep(".GType", colnames(sample))] <- 5
sample[sample$Chr == "XY","Chr"] <- "X"
GT <- sample[, grep(".GType", colnames(sample)), drop = FALSE]
GT[GT == "AA"] <- 1
GT[GT == "AB"] <- 2
GT[GT == "BB"] <- 3
GT[GT == "NC"] <- 4
GT <- as.matrix(as.integer(GT[[1]]))
CN <- as.matrix(as.numeric(sample[, grep("Log.R.Ratio", colnames(sample))]))
colnames(GT) <- colnames(CN) <- sampleID  
rownames(GT) <- rownames(CN) <- sample[, "Name"]


fD <- new("AnnotatedDataFrame",
	  data = data.frame(position = sample[, "Position"],
	  chromosome = integer2chromosome(sample[, "Chr"]), stringsAsFactors=FALSE),
	  varMetadata = data.frame(labelDescription = c("position","chromosome")))
featureNames(fD) <- sample[, "Name"]

myObject <- new("oligoSnpSet", copyNumber = CN,
		calls = GT,
		phenoData = annotatedDataFrameFrom(CN, byrow = FALSE),
		featureData = fD, annotation = "Illumina")


message("Sorting data")
myObject <- myObject[order(chromosome(myObject), position(myObject)), ]

states <- c("homozygousDeletion", "hemizygousDeletion", "normal", "LOH", "3copyAmp", "4copyAmp")
#mu <- c(0, -0.53, 0, 0, 0.35, 0.54)
mu[1] <- log2(0.05/2)

##############################################################
# Calculate robust estimate on standard devation (all but X) #
############################################################## 
message("Calculating Variation estimation")
NoXY <- sample[sample[,"Chr"]!="X",c("Name","Chr" )]
NoXY <- NoXY[NoXY[,"Chr"]!="Y","Name"]
sddata<-sample[sample$Name %in% NoXY,grep("Log.R",colnames(sample)) ]
#cn.sds <- VanillaICE:::robustSds(as.matrix(sddata), takeLog = FALSE)



message(paste("Variance model in use: ",variance, sep = ""))
if (variance == "insample") {
  robustSD <- function(X) (diff(quantile(X, probs=c(0.16, (1-0.16)), na.rm=TRUE))/2)[[1]] 
  uncertainty <- robustSD(sddata)
  uncertainty <- matrix(uncertainty, nrow=nrow(myObject), ncol=ncol(myObject))
  
}
if (variance == "robustref") {
  robustSD <- read.table("robust.txt", as.is = TRUE, sep = "\t", header = TRUE)
  uncertainty <- robustSD$Variance
  uncertainty <- as.array(uncertainty)
  rownames(uncertainty) <- robustSD$Probe
  uncertainty <- as.matrix(uncertainty)
  #uncertainty<-uncertainty[!rownames(uncertainty) %in% nologR,]
}
if (variance == "stdevref") {
  stdev <- read.table("stdev.txt", as.is = TRUE, sep = "\t", header = TRUE)
  uncertainty <- stdev$Variance
  uncertainty <- as.array(uncertainty)
  rownames(uncertainty) <- stdev$Probe
  uncertainty <- as.matrix(uncertainty)
  #uncertainty<-uncertainty[!rownames(uncertainty) %in% nologR,]
}

logemission.logCT <- copynumberEmission(copynumber=copyNumber(myObject),
					states=states,
					mu=mu,
					uncertainty=uncertainty,
					takeLog=FALSE,
					verbose=FALSE)

logemission.logCT[logemission.logCT < -10] <- -10
##probability of a homozygous genotype call
probs <- c(0.99, 0.9999, 0.99, 0.9999, 0.99, 0.99)
probMissing <- c(0.999, rep(0.01, 5))
names(probs) <- names(probMissing) <- states
GT <- calls(myObject)
#load custom genotypeEmission Function (set p(NC | zeroed snp) = 1 )
source("genotypeEmission.R")
logemission.gt <- genotypeEmission(genotypes = GT,
				   states = states,
				   probHomCall = probs,
				   probMissing = probMissing,
				   verbose = TRUE)

logemission <- logemission.gt + logemission.logCT

tau <- exp(-2*diff(position(myObject))/(100*taufactor))
#tau.scale <- matrix(1, length(states), length(states))
#dimnames(tau.scale) <- list(states, states)
#factor <- 1e-04
#tau.scale["normal", c("LOH", "homozygousDeletion")] <- factor
#S <- length(states)
#scale <- (S-1)/(S - 2 + factor)
#tau.scale["normal", c("hemizygousDeletion", "3copyAmp", "4copyAmp")] <- scale

initialStateProb <- rep(1e-04, length(states))
initialStateProb[states == "normal"] <- 1 - (length(states) - 1) * 1e-04

##Lets specify the chromosomal arm as well  (fits a separate HMM to each arm)
data(chromosomeAnnotation, package="SNPchip", envir=environment())
chrAnn <- as.matrix(chromosomeAnnotation)
chromosomeArm <- as.character(position(myObject) <= chromosomeAnnotation[chromosome(myObject), "centromereStart"])
chromosomeArm[chromosomeArm == "TRUE"] <- "p"
chromosomeArm[chromosomeArm == "FALSE"] <- "q"


message("Fitting the HMM")
fit <- viterbi(initialStateProbs=log(initialStateProb),
	       emission=logemission[, 1, ],
	       arm=chromosomeArm,
	       tau=tau,
	       #returnLikelihood = TRUE,
)
#		tau.scale=tau.scale)
message("Searching Breakpoints")

#hopefully load custom function...

source("findBreaks.R")

results <- findBreaks(x = fit, states = states, position = position(myObject), chromosome = chromosome(myObject), sample = sampleNames(myObject))
altered <- results[results$state != "normal", ]
#altered <- altered[-grep(",", altered$chr),]
altered[,"chr"] <- unlist(altered[,"chr"])
#message(nrows)
nrows <- nrow(altered)
filename <- paste("rawcnv/",prefix,".rawcnv",sep="")
headers <- c("Chr", "Start", "End", "Size", "StartProbe", "EndProbe", "NrSNPs", "State")
write(headers, file=filename, append = FALSE, sep = "\t", ncolumns = 8)
for(i in 1:nrows) {
  startprobe <- sample[sample$Position == altered[i, "start"],"Name"]
  endprobe <- sample[sample$Position == altered[i, "end"], "Name"]
  line <- c(altered[i, "chr"][1], altered[i, "start"][1], altered[i, "end"][1], altered[i, "nbases"][1], startprobe, endprobe, altered[i, "nprobes"][1], altered[i, "state"][1]) 
  write(line, file = filename, append = TRUE, sep= "\t", ncolumns = 8)
}
message("Done. Found ", nrows, " CNV's",sep="")  





