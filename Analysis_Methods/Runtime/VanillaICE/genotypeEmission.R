genotypeEmission <- function (genotypes, states, probHomCall, probMissing, verbose = TRUE)
{
    if (!is.numeric(genotypes))
        stop("genotypes must be integers (1=AA, 2=AB, 3=BB, 4=missing, 5=zeroed")
    emissionForGenotypes <- function(probHomGenotype, genotypes) {
        isHom <- which(as.vector(genotypes) == 1 | as.vector(genotypes) ==
            3)
        isHet <- which(as.vector(genotypes) == 2)
        isMissing <- which(as.vector(genotypes) == 4 | is.na(as.vector(genotypes)))
	isZeroed <- which(as.vector(genotypes) == 5 )
        emission.gt <- rep(NA, length(genotypes))
        emission.gt[isHom] <- probHomGenotype
        emission.gt[isHet] <- 1 - probHomGenotype
        emission.gt[isMissing] <- NA
	emission.gt[isZeroed] <- rep(1,length(probHomGenotype))
        emission.gt
    }
    emission.gt <- array(NA, dim = c(nrow(GT), ncol(GT), length(states)))
    for (j in 1:ncol(GT)) {
        emission.gt[, j, ] <- sapply(probs, emissionForGenotypes, 
            genotypes = GT[, j])
        if (any(is.na(emission.gt[, j, 1]))) {
            missing <- is.na(emission.gt[, j, 1])
            if (!missing(probMissing)) {
                if (length(probMissing) != length(states))
                  stop("probMissing must be a numeric vector equal to the number of states")
                emission.gt[missing, j, ] <- matrix(probMissing,
                  sum(missing), length(states), byrow = TRUE)
            }
            else {
                if (verbose)
                  message("Argument probMissing is not specified. Assume that missing genotype calls are independent of the underling hidden state")
                emission.gt[missing, j, ] <- 1
            }
        }
    }
    dimnames(emission.gt) <- list(rownames(genotypes), colnames(genotypes),
        states)
    return(suppressWarnings(log(emission.gt)))
}

