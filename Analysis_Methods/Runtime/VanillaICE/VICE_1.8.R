datafile <- opt$inputfile
gender <- opt$gender
prefix <- opt$prefix
sampleID <- opt$samplename
taufactor <- opt$tau
taufactor <- as.numeric(taufactor)
hmm <- opt$levels
variance <- opt$variance
chiptype <- opt$chiptype

if (hmm == "regular" ) { 
  #mu <- log2(c(0, 1, 2, 2, 3, 4)/2)
   mu <- log2(c(0.025, 1, 2, 3, 4)/2)
}
if (hmm == "experimental") { 
  #if (chiptype != 'HumanCNV370quad' && chiptype != 'HumanCyto12v2.0') { 
	  #mu <- c(0, -0.45, 0, 0, 0.3, 0.75) 
	  mu <- c(0, -0.44036, 0, 0.2281, 0.75)
  #}
  #else {
#	  mu <- c(0, -0.44036, 0, 0, 0.2281, 0.75)
 # }
}
#if (hmm == "chrY") {
#	# cn on normalized intensities: 0, 2 (normal), 3 (dup in PAR), 4 (dup outside PAR & higher)
#	mu <- c(0,0,0.2281,0.75)
#}
if (chiptype == "") {
	zeroedfile = "lib/zeroedsnps.txt"
}
if (chiptype != "") {
	zeroedfile <- paste("lib/",chiptype, ".zeroed.txt", sep = "")
}


message("reading in datafile")   
zeroed<- read.table(zeroedfile, as.is = TRUE, header = TRUE, sep = "\t", comment.char="")
zero<-zeroed$Name
message(paste("Loaded ",length(zero)," zero'ed probes.",sep=""))
#################
# READ DATAFILE #
#################
sample <- read.table(datafile, as.is = TRUE, header = TRUE, sep = "\t", comment.char="")
message(paste("Loaded ",length(sample$Chr), " datapoints.",sep=""))
nologR<-sample[sample[,grep("Log.R",colnames(sample))]=="NaN","Name"]
message(paste("Discarded ",length(nologR)," datapoints by lack of LogR value.",sep=""))
sample<-sample[!sample$Name %in% nologR,]
inisize <- length(sample$Chr)
sample<-sample[sample$Chr!="MT",]
postsize <- length(sample$Chr)
disc <- inisize - postsize
message( "Discarded ", disc, " probes from the Mitochondrial chip content.", sep="")
sample[sample$Chr == "XY","Chr"] <- "X"
#sample[sample$Chr == "X" , "Chr"] <- 23
#sample[sample$Chr == "Y" , "Chr"] <- 24
#sample[sample$Chr == "MT", "Chr"] <- 25
## order
sample$Chr <- chromosome2integer(sample$Chr) 
sample <- sample[order(sample$Chr, sample$Position), ]
sample$Chr <- integer2chromosome(sample$Chr)
#######################
# EXTRACT INFORMATION #
#######################
sample[sample$Name %in% zero,grep(".GType", colnames(sample))] <- 5
chrom <- sample[,"Chr"]
pos <- sample[,"Position"]
names(chrom) <- names(position) <- sample[,"Name"]
#ordering <- order(chrom, pos)
#chrom <- chrom[ordering]
#pos <- pos[ordering]
GT <- sample[, grep(".GType", colnames(sample)), drop = FALSE]
GT[GT == "AA"] <- 1
GT[GT == "AB"] <- 2
GT[GT == "BB"] <- 3
GT[GT == "NC"] <- 4
GT <- as.matrix(as.integer(GT[[1]]))
CN <- as.matrix(as.numeric(sample[, grep("Log.R.Ratio", colnames(sample))]))
colnames(GT) <- colnames(CN) <- sampleID  
rownames(GT) <- rownames(CN) <- sample[, "Name"]
#GT <- GT[ordering, , drop = FALSE]
#CN <- CN[ordering, , drop = FALSE]

######################
# CREATE DATA FRAMES #
######################
locusAnnotation <- data.frame(list(chromosome = chrom, position = pos),row.names = names(chrom))
fD <- new("AnnotatedDataFrame",data = locusAnnotation,varMetadata = data.frame(labelDescription = colnames(locusAnnotation)))
message("annotation frame succesfully constructed")
myObject <- new("oligoSnpSet", copyNumber = CN,
		calls = GT,
		phenoData = annotatedDataFrameFrom(CN, byrow = FALSE),
		featureData = fD, annotation = "Illumina")
message("Sorting data")
myObject <- myObject[order(chromosome(myObject), position(myObject)), ]
chrom <- as.vector(chromosome(myObject))
pos <- position(myObject)
message("Sorting succeeded")
#################
# DEFINE STATES #
#################
#states <- c("homozygousDeletion", "hemizygousDeletion", "normal", "LOH", "3copyAmp", "4copyAmp")
states <- c("0", "1", "2", "3", "4")

mu[1] <- log2(0.05/2)
copynumberStates <- mu
#probs <- c(0.9, 0.9999, 0.9, 0.9999, 0.9, 0.9)
probs <- c(0.9, 0.9999, 0.9, 0.9, 0.9)

names(probs) <- states
#probMissing <- c(0.999, rep(0.01, 5))
probMissing <- c(0.999, rep(0.01, 4))

names(probMissing)<-states
probNonInf <- (rep(1, length(states)))/length(states)
initialStateProb <- rep(1e-04, length(states))
initialStateProb[states == "normal"] <- 1 - (length(states) - 1) * 1e-04
#initialP <- (rep(1, length(states)))/length(states)
initialP <- initialStateProb
message("assigning tau")
source('transProb_1.8.R')
if (taufactor < as.numeric(1e09) && length(pos) > 600000) {
	message('Adjusting smoothing factor to 1e09 for high density chips')
	taufactor <- as.numeric(1e09)
}
tau <- transProb(chromosome = chrom, position = pos, TAUP = taufactor)

## standard transProb values give errors on fitting, replace with simpler estimation
#taubis <- c(exp(-2*diff(position(myObject))/(100*taufactor)),1)
#tau[,"transitionPr"] <- taubis

##############################################################
# Calculate robust estimate on standard devation (all but X) #
############################################################## 
message("Calculating Variation estimation")
NoXY <- sample[sample[,"Chr"]!="X",c("Name","Chr" )]
NoXY <- NoXY[NoXY[,"Chr"]!="Y","Name"]
sddata<-sample[sample$Name %in% NoXY,grep("Log.R",colnames(sample)) ]
#cn.sds <- VanillaICE:::robustSds(as.matrix(sddata), takeLog = FALSE)
robustSD <- function(X) (diff(quantile(X, probs=c(0.16, (1-0.16)), na.rm=TRUE))/2)[[1]]
uncertainty <- robustSD(sddata)
## now fill for all (including X/Y)
cn.sds <- rep(uncertainty,length(pos))

#######################
# CopyNumber Emission #
#######################
message ("Calculating copy number emissions")
emission.cn <- copynumberEmission(copynumber = copyNumber(myObject), sds = cn.sds, states = states, mu = mu, takeLog = FALSE, verbose = TRUE)
emission.cn[emission.cn < -10] <- -10


#######################################
# GenoType Emission (custom function) #
#######################################
message("Calculating genotype emissions")
source("genotypeEmission.R")
emission.gt <- genotypeEmission(genotypes = GT, states = states, probHomCall = probs, probMissing = probMissing, verbose = TRUE)
emission.gt[emission.gt < -10] <- -10


###############
# Fit the HMM #
###############
message("Fitting the model")
emission <- emission.gt + emission.cn
source('newviterbi.R')
fit <- newviterbi(initialStateProbs = log(initialP), emission = emission, tau = tau[, "transitionPr"], arm = tau[, "arm"],normalIndex = 3 , returnLikelihood = TRUE, verbose = TRUE)
message("Defining breakpoints")
result <- breaks(x = fit[[1]], states = states, position = tau[, "position"], chromosome = tau[, "chromosome"])
message("printing results file")


altered <-result
#message(nrows)
nrows <- nrow(altered)
filename <- paste("rawcnv/",prefix,".rawcnv",sep="")
headers <- c("Chr", "Start", "End", "Size", "StartProbe", "EndProbe","NrSNPs", "State", "Confidence")
write(headers, file=filename, append = FALSE, sep = "\t", ncolumns = 9)
for(i in 1:nrows) {
  if (altered[i,"state"] != 'normal') {
        startprobe <- sample[(sample$Position == altered[i, "start"] & sample$Chr == altered[i,"chr"]),"Name"]
	# on Omni2.5, still multiple probes possible, same chr, same pos ! ==> arbitrarily use first !
  	startprobe <- startprobe[1]
	endprobe <- sample[(sample$Position == altered[i, "end"] & sample$Chr == altered[i,"chr"]), "Name"]
	# on Omni2.5, still multiple probes possible, same chr, same pos ! ==> arbitrarily use first !
	endprobe <- endprobe[1]
	# compose line.
 	line <- c(altered[i, "chr"], altered[i, "start"], altered[i, "end"], altered[i, "nbases"], startprobe, endprobe, altered[i, "nprobes"], altered[i, "state"], fit[[2]][[1]][i]) 
 	write(line, file = filename, append = TRUE, sep= "\t", ncolumns = 9)
  }
}
  






