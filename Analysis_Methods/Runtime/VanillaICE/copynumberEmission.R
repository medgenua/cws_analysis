copynumberEmission <- function (copynumber, states, mu, uncertainty, takeLog, verbose = TRUE)
{
    cne <- copynumber
    location <- mu
    if (missing(takeLog))
        stop("must specify whether to take the log2 of the copy number matrix")
    if (missing(uncertainty))
        stop("must supply uncertainty estimates")
    if (takeLog) {
        cne <- log2(cne)
        location <- log2(location)
    }
    else {
        if (verbose)
            message("no transformation of copy number")
    }
    fn <- rownames(cne)
    S <- length(states)
    cne <- array(cne, dim = c(nrow(cne), ncol(cne), S))
    dimnames(cne) <- list(rownames(copynumber), colnames(copynumber),
        states)
    location <- aperm(array(location, dim = c(S, ncol(cne), nrow(cne))))
    i <- which(!(is.na(as.vector(1/uncertainty))) & !is.na(as.vector(cne)))
    if (all(1/uncertainty > 0, na.rm = TRUE)) {
        if (verbose)
            message("Using uncertainty as standard deviation for the copy number")
        scale <- array(uncertainty, dim = dim(cne))
    }
    else {
        stop("confidence scores in slot cnConfidence must be positive")
    }
    k <- which(!is.na(as.vector(cne)))
    if (!identical(dim(cne), dim(location)))
        stop("dimensions must be the same")
    emission.cn <- rep(NA, length(as.vector(location)))
    emission.cn[k] <- dnorm(as.vector(cne)[k], as.vector(location)[k],
        as.vector(scale)[k])
    emission.cn <- array(emission.cn, dim = dim(cne))
    dimnames(emission.cn) <- list(rownames(cne), colnames(cne),
        states)
    if (verbose)
        message("returning the emission probability on the log scale")
    return(log(emission.cn))
}

