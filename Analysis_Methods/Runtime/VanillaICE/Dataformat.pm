package Dataformat; 

sub Format_data {
my $datadir = "$scriptdir/datafiles/";
my $datafile = shift;
my $target = shift;
if    ($target =~ m/penncnv/i ) { $AlgoDir = "$scriptdir/PennCNV/datafiles/";}
elsif ($target =~ m/multi/i)	{ $AlgoDir = "$scriptdir/datafiles/";}
elsif ($target =~ m/faseg/i)    { $AlgoDir = "$scriptdir/FASeg/datafiles/";}
elsif ($target =~ m/quantisnp/i){ $AlgoDir = "$scriptdir/QuantiSNP/datafiles/";}
elsif ($target =~ m/dnacopy/i)  { $AlgoDir = "$scriptdir/DNAcopy/datafiles/";}
elsif ($target =~ m/vanillaice/i) { $AlgoDir = "$scriptdir/VanillaICE/datafiles/";}

print "files will be written to $AlgoDir\n";

$infile = $datafile;
$infile =~ m/datafiles\/(.+)\.txt$/;
$prefix = $1;
print "Preparing input files from $infile\n";

# Create files
#open IN, $infile;
my $headerline = `head -n 1 $datafile`;
chomp($headerline);
my @headers = split(/\t/,$headerline);
#print "prefix: $prefix\n";
#my $nrsamples = @headers;
$samples = 0;
$i = 0;
my @LogRcol = (0);
my @BafCol = (0);
my @GtCol = (0);
my @order =(0);
foreach (@headers) {
	if ($_ =~ m/^(\w+).Log/ ) {
		$samples++;
		push(@LogRcol, $i);
		$i++;
		push(@order, $1);
	}
	elsif ($_ =~ m/^(\w+).B All/ ) {
		push(@BafCol, $i);
		$i++;
	}
	elsif ($_ =~ m/^(\w+).GType/ ) {
		push(@GtCol, $i);
		$i++;
	}
	elsif ($_ =~ m/^Name/ ) {
		$NameCol = $i;
		$i++;
	}
	elsif ($_ =~ m/^Chr/ ) {
		$ChrCol = $i;
		$i++;
	}
	elsif ($_ =~ m/^Position/ ) {
		$PosCol = $i;
		$i++;
	}
	else { $i++; }	
}

#OPEN FILES
for ($i = 1;$i<=$samples;$i++){
	if (-e $AlgoDir.$prefix."_$i.txt")
  	  {
		  unlink $AlgoDir.$prefix."_$i.txt";
	  }
	open "OUT$i" , ">>".$AlgoDir.$prefix."_$i.txt";
}
print "target format: $target \n";
#FILL FILES
open IN, $datafile;
while (<IN>) {
	chomp($_);
	my @line = split(/\t/, $_); 
	
	for ($i = 1;$i<=$samples; $i++) {
	   if ($target =~ m/quantisnp/i || $target =~ m/penncnv/i) {
		if (($line[$LogRcol[$i]] !~ m/NaN/ ) && ($line[$BafCol[$i]] !~ m/NaN/)){
	        	print {"OUT$i"} "$line[$NameCol]\t$line[$ChrCol]\t$line[$PosCol]\t$line[$LogRcol[$i]]\t$line[$BafCol[$i]]\n";
		}
	   }
	   elsif ($target =~ m/multi/i) {
		if ($line[$logRcol[$i]] !~ m/NaN/){
			print {"OUT$i"} "$line[$NameCol]\t$line[$ChrCol]\t$line[$PosCol]\t$line[$LogRcol[$i]]\t$line[$BafCol[$i]]\t$line[$GtCol[$i]]\n";
		}
	   }
	   elsif ($target =~ m/FASeg/i || $target =~ m/dnacopy/i) {
		if ($line[$LogRcol[$i]] !~ m/NaN/ ){
			print {"OUT$i"} "$line[$NameCol]\t$line[$ChrCol]\t$line[$PosCol]\t$line[$LogRcol[$i]]\n";
		}
	   }
	   elsif ($target =~ m/vanillaice/i) {
		if ($line[$LogRcol[$i]] !~ m/NaN/ ) {
			print {"OUT$i"} "$line[$NameCol]\t$line[$ChrCol]\t$line[$PosCol]\t$line[$LogRcol[$i]]\t$line[$GtCol[$i]]\n";
		}
	   }
	

	}
}
for ($i = 1;$i<=$nrsamples;$i++){
	close "OUT$i";
}
close IN;
return @order;
}

1;
