datafile <- opt$inputfile
gender <- opt$gender
prefix <- opt$prefix
sampleID <- opt$samplename
taufactor <- opt$tau
taufactor <- as.numeric(taufactor)
hmm <- opt$levels
variance <- opt$variance
chiptype <- opt$chiptype

#datafile <- '/home/webstoreadmin/2010-11-05_14u34m14s_12.demotrio.data_1.txt'
#gender <- 'Male' 
#prefix <- 'prefix'
#sampleID <-  'Demo_Data_Child12'
#taufactor <- as.numeric('1E06')
#hmm <- 'experimental'
#variance<-'insample'
#chiptype<-'HumanCyto12v2.0'

if (hmm == "regular" ) { 
  mu <- log2(c(0, 1, 2, 2, 3, 4)/2)
}
if (hmm == "experimental") { 
#  if (chiptype != 'HumanCNV370quad' && chiptype != 'HumanCyto12v2.0') { 
#	  mu <- c(0, -0.45, 0, 0, 0.3, 0.75)  
#  }
 # else {
	  mu <- c(0, -0.44036, 0, 0, 0.2281, 0.75)
#  }
}

if (chiptype == "") {
	zeroedfile = "lib/zeroedsnps.txt"
}
if (chiptype != "") {
	zeroedfile <- paste("lib/",chiptype, ".zeroed.txt", sep = "")
}


message("reading in datafile")   
zeroed<- read.table(zeroedfile, as.is = TRUE, header = TRUE, sep = "\t", comment.char="")
zero<-zeroed$Name
message(paste("Loaded ",length(zero)," zero'ed probes.",sep=""))
#################
# READ DATAFILE #
#################
sample <- read.table(datafile, as.is = TRUE, header = TRUE, sep = "\t", comment.char="")
message(paste("Loaded ",length(sample$Chr), " datapoints.",sep=""))
nologR<-sample[sample[,grep("Log.R",colnames(sample))]=="NaN","Name"]
message(paste("Discarded ",length(nologR)," datapoints by lack of LogR value.",sep=""))
sample<-sample[!sample$Name %in% nologR,]
inisize <- length(sample$Chr)
sample<-sample[sample$Chr!="MT",]
postsize <- length(sample$Chr)
disc <- inisize - postsize
message( "Discarded ", disc, " probes from the Mitochondrial chip content.", sep="")
sample[sample$Chr == "XY","Chr"] <- "X"
sample[sample$Chr == "X" , "Chr"] <- 23
sample[sample$Chr == "Y" , "Chr"] <- 24
sample[sample$Chr == "MT", "Chr"] <- 25

#######################
# EXTRACT INFORMATION #
#######################
sample[sample$Name %in% zero,grep(".GType", colnames(sample))] <- 5
chromosome <- as.numeric(sample[,"Chr"])
position <- sample[,"Position"]
names(chromosome) <- names(position) <- sample[,"Name"]
ordering <- order(chromosome, position)
chromosome <- chromosome[ordering]
position <- position[ordering]
GT <- sample[, grep(".GType", colnames(sample)), drop = FALSE]
GT[GT == "AA"] <- 1
GT[GT == "AB"] <- 2
GT[GT == "BB"] <- 3
GT[GT == "NC"] <- 4
GT <- as.matrix(as.integer(GT[[1]]))
CN <- as.matrix(as.numeric(sample[, grep("Log.R.Ratio", colnames(sample))]))
colnames(GT) <- colnames(CN) <- sampleID  
rownames(GT) <- rownames(CN) <- sample[, "Name"]
GT <- GT[ordering, , drop = FALSE]
CN <- CN[ordering, , drop = FALSE]

######################
# CREATE DATA FRAMES #
######################
locusAnnotation <- data.frame(list(chromosome = chromosome, position = position),row.names = names(chromosome))
fD <- new("AnnotatedDataFrame",data = locusAnnotation,varMetadata = data.frame(labelDescription = colnames(locusAnnotation)))
message("annotation frame succesfully constructed")
myObject <- new("oligoSnpSet", copyNumber = CN,
		call = GT,
		phenoData = annotatedDataFrameFrom(CN, byrow = FALSE),
		featureData = fD, annotation = "Illumina")
message("Sorting data")
myObject <- myObject[order(chromosome(myObject), position(myObject)), ]

#################
# DEFINE STATES #
#################
states <- c("homozygousDeletion", "hemizygousDeletion", "normal", "LOH", "3copyAmp", "4copyAmp")
mu[1] <- log2(0.05/2)
copynumberStates <- mu
probs <- c(0.9, 0.9999, 0.9, 0.9999, 0.9, 0.9)
names(probs) <- states
probMissing <- c(0.999, rep(0.01, 5))
names(probMissing)<-states
probNonInf <- (rep(1, length(states)))/length(states)
initialStateProb <- rep(1e-04, length(states))
initialStateProb[states == "normal"] <- 1 - (length(states) - 1) * 1e-04
initialP <- initialStateProb

##############################################################
# Calculate robust estimate on standard devation (all but X) #
############################################################## 
message("Calculating Variation estimation")
NoXY <- sample[sample[,"Chr"]!="X",c("Name","Chr" )]
NoXY <- NoXY[NoXY[,"Chr"]!="Y","Name"]
sddata<-sample[sample$Name %in% NoXY,grep("Log.R",colnames(sample)) ]
cn.sds = robustSds(as.matrix(sddata), takeLog = FALSE)


## create hmm
message('setting up hmm')
#source('newhmmsetup.R')
#chromosome(myObject) <- as.numeric(chromosome(myObject))
hmmOpts <- hmm.setup(myObject, ICE = FALSE, copynumberStates = mu, states = states, normalIndex = 3, log.initial = initialP, prGenotypeHomozygous = probs, prGenotypeMissing = probMissing, verbose = TRUE)
#######################
# CopyNumber Emission #
#######################
#message ("Calculating copy number emissions")
#emission.cn <- copynumberEmission(copynumber = copyNumber(myObject), sds = cn.sds, states = states, mu = mu, takeLog = FALSE, verbose = TRUE)
#emission.cn[emission.cn < -10] <- -10


#######################################
# GenoType Emission (custom function) #
#######################################
#message("Calculating genotype emissions")
#source("genotypeEmission.R")
#emission.gt <- newgenotypeEmission(genotypes = GT, states = states, probHomCall = probs, probMissing = probMissing, verbose = TRUE)
#emission.gt[emission.gt < -10] <- -10


###############
# Fit the HMM #
###############
message("Fitting the model")
#emission <- emission.gt + emission.cn
fit <- hmm(myObject, hmmOpts)
#source('newviterbi.R')
#fit <- viterbi(object = myObject, hmm.params = hmmOpts, verbose = TRUE )
#message("Defining breakpoints")
#source("breaks.R")
#result <- breaks(x = fit[[1]], states = states, position = tau[, "position"], chromosome = tau[, "chromosome"])
#result <- breaks(x = unlist(fit[1]), states = states, position = tau[, "position"], chromosome = tau[, "chromosome"], sampleNames = colnames(CN))

message("printing results file")


#altered <- result[results$state != "normal", ]
#altered <- altered[-grep(",", altered$chr),]
#altered <-result
#message(nrows)
nrows <- nrow(fit)
message(nrows)
filename <- paste("rawcnv/",prefix,".rawcnv",sep="")
headers <- c("Chr", "Start", "End", "Size", "StartProbe", "EndProbe", "NrSNPs", "State", "Confidence")
write(headers, file=filename, append = FALSE, sep = "\t", ncolumns = 9)
# ranges: first = start, second = size in bp
mat.ranges <- as.matrix(fit$ranges)
for(i in 1:nrows) {
  if (fit$state[i] != 3) {
	 startpos <- mat.ranges[i, 1]
	 stoppos <- (mat.ranges[i,2]  + mat.ranges[i,1] -1)
	startprobe <- sample[(sample$Position == altered[i, "start"] & sample$Chr == altered[i,"chr"]),"Name"]
	# on Omni2.5, still multiple probes possible, same chr, same pos ! ==> arbitrarily use first !
  	startprobe <- startprobe[1]
	endprobe <- sample[(sample$Position == altered[i, "end"] & sample$Chr == altered[i,"chr"]), "Name"]
	# on Omni2.5, still multiple probes possible, same chr, same pos ! ==> arbitrarily use first !
	endprobe <- endprobe[1]
	# compose line.

 	 line <- c(fit$chrom[i], mat.ranges[i, 1], (mat.ranges[i,2]  + mat.ranges[i,1] -1), mat.ranges[i, 2], startprobe, endprobe, fit$numMarkers[i], states[fit$state[i]], fit$LLR[i]) 
 	 write(line, file = filename, append = TRUE, sep= "\t", ncolumns = 9)
  }
}
  






