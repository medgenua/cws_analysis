findBreaks <- function(x, states, position, chromosome, sample,
		       lik1, lik2, chromosomeAnnotation){
	if(is.matrix(x)) if(ncol(x) > 1) stop("x should be a vector or matrix with 1 column")	
	if(!is.integer(chromosome)) {
		chromosome <- chromosome2integer(chromosome)
	}
	#chromosome[chromosome == "X"] <- 23
	#chromosome[chromosome == "Y"] <- 24
	#chromosome[chromosome == "XY"] <- "X"
	chromosome[chromosome == "X"]
	if(!all(chromosome %in% 1:24)){
			message("Chromosome annotation is currently available for chromosomes 1-22, X and Y")
			message("Please add/modify data(chromosomeAnnotation, package='SNPchip') to accomodate special chromosomes")
			stop()
	}
	if(!is.integer(position)) {
		message("Coerced position to an integer.")
		position <- as.integer(position)
	}	
	##ensure that the reported breaks do not span the centromere
	if(missing(chromosomeAnnotation)){
		data(chromosomeAnnotation, package="SNPchip", envir=environment())
		chrAnn <- as.matrix(chromosomeAnnotation)
	}
	chromosome <- integer2chromosome(chromosome)
	uchrom <- unique(chromosome)
	positionList <- split(position, chromosome)
	positionList <- positionList[match(uchrom, names(positionList))]
	arm <- list()
	for(i in seq(along=uchrom)){
		arm[[i]] <- as.integer(ifelse(positionList[[i]] <= chrAnn[uchrom[i], "centromereStart"], 0, 1))	
	}		
	arm <- unlist(arm)
	if(length(chromosome)==1) chromosome <- rep(chromosome, length(position))
	splitby <- factor(cumsum(c(1, diff(x) != 0 | diff(arm) != 0)))
	indices <- split(1:length(x), splitby)
	len <- sapply(indices, length)
	S <- states[sapply(split(x, splitby), unique)]
	pos <- t(sapply(split(position, splitby), range))
	size <- apply(t(sapply(split(position, splitby), range)), 1, diff)
	chr <- sapply(split(chromosome, splitby), unique)
	breaks <- data.frame(matrix(NA, length(chr), 7))
	colnames(breaks) <- c("sample", "chr", "start", "end", "nbases", "nprobes", "state")
	breaks$sample <- rep(sample, length(chr))
	breaks$chr <- chr
	breaks$start <- pos[, 1]
	breaks$end <- pos[, 2]
	breaks$nbases <- size
	breaks$nprobes <- len
	breaks$state <- S
	if(!missing(lik1) & !missing(lik2)){
		likdiff <- function(index, lik1, lik2, state){
			state <- unique(state[index])
			i <- range(index)
			if(min(i) > 1) i[1] <- i[1]-1
			if(max(x) < nrow(lik1)) i[2] <- i[2]+1
			##the more positive the better
			d1 <- diff(lik1[i, state])
			d2 <- diff(lik2[i, "N"])
			LR <- d1-d2
			return(LR)
		}
		LR <- as.numeric(sapply(indices, likdiff, lik1=lik1, lik2=lik2, state=x))
	}
	breaks <- breaks[sapply(chr, length) == 1, ]
	breaks$chr <- unlist(breaks$chr)
	return(breaks)
}
