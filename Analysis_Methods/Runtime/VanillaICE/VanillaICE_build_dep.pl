#!/usr/bin/perl

################
# LOAD MODULES #
################
use Sys::CPU;
use threads;
use Thread::Queue;
use threads::shared;
use Sys::CpuLoad;
use Number::Format;
use DBI;
use Dataformat;
use Getopt::Std;

##########################
# COMMAND LINE ARGUMENTS #
##########################
# D = Use specific database (mandatory if L is specified), otherwise use default.
# u = username (string)
# d = datafile (file location)
# g = genderfile (file location)
# p = project name (string)
# c = chiptype (string)
# s = Minimal number of snps
# t = Tau factor
# m = Hidden Markov Model
# v = Variance Model
getopts('D:u:d:g:p:c:s:t:m:v:', \%opts);  # option are in %opts

#######################
# ENABLE AUTOFLUSHING #
#######################
$| = 1;	 	

###########################
# CONNECT TO THE DATABASE #
###########################
my $db :shared;
require("/opt/ServerMaintenance/PerlDatabaseCredentials.pl");
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "Assuming build ".$row[1]."\n";
	$gbsth->finish();
	$dbhgb->disconnect;	
}


$connectionInfo="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfo,$userid,$userpass);
$dbh->{mysql_auto_reconnect} = 1;

#################
# GET STARTTIME #
#################
local $now = time;


###############################
# SET NUMBER FORMATTING STYLE #
###############################
my $de = new Number::Format(-thousands_sep =>',',-decimal_point => '.');


#############################
# GET COMMANDLINE ARGUMENTS #
#############################
my $basedir = "$scriptdir/";
my $datafile = $opts{'d'};
my $datadir = $basedir . "datafiles/"; 
$datafile =~ m/datafiles\/(.+)\.txt$/;
$prefix = $1;
my $genderfile = $opts{'g'};
my $projectname = $opts{'p'};
my $statusfile = "status/$projectname.status";
my $targetname = "$sitedir/bookmarks/" . $projectname . "_VanillaICE.xml";
my $xmlfile = $projectname . "_VanillaICE.xml";
my $minsnp = $opts{'s'};
my $taufactor = $opts{'t'};
my $hmm = $opts{'m'};
my $variance = $opts{'v'};
my $chiptype :shared;
$chiptype = $opts{'c'};
print "Considering Chiptype: $chiptype\n";
if ($chiptype eq "") {
	$chiptype = "HumanCNV370quad";
} 
if ($chiptype eq "HumanCNV370duo") {
	print "filtering variable probes\n";
	my %filter;
	open IN, "$scriptdir/cutoff_0.999.txt";
	while (<IN>) {
		chomp $_;
		$filter{ $_ } = "";
	}
	close IN;
	open IN, "$datafile";
	open OUT, ">$scriptdir/datafiles/filtered.txt";
	my $header = <IN>;
	print OUT $header;
	while (<IN>) {
	my @pieces = split(/\t/,$_);
		if (!exists($filter{ $pieces[0] })) {
			print OUT $_;
		}
	}
	system("mv $scriptdir/datafiles/filtered.txt $datafile");
}
my $chiptypeid :shared;
$query = "SELECT ID FROM chiptypes WHERE name = '$chiptype'";
$sth = $dbh->prepare($query);
$sth->execute();
my @chipres = $sth->fetchrow_array();
$chiptypeid = $chipres[0];
$sth->finish();

my $username = $opts{'u'};
my $getuserid = "SELECT id FROM users WHERE username = '$username'";
$sth = $dbh->prepare($getuserid);
$sth->execute();
my @userids = $sth->fetchrow_array();
my $userid = $userids[0];
$sth->finish();

##################
# CREATE PROJECT #
##################
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year = 1900+$year;
$mon = $mon +1;
$time = "$mday/$mon/$year - $hour:$min:$sec";

##########################
# PREPARE MULTITHREADING #
##########################
$nrcpus = Sys::CPU::cpu_count() ;
my $qsnpqueue = Thread::Queue->new();
`echo 1 > $statusfile`;

##################################
# PREPARE PER-SAMPLE INPUT FILES #
##################################
my @fileorder = Dataformat::Format_data( $datafile, "VanillaICE");


############################################
# INSERT/UPDATE sample details in database #
############################################

# GENDERFILE: Get sample genders & names
print "Reading Gender File\n";
open INGender, "$genderfile" or die "Cannot open Genderfile";
print "Following genders settings will be used:\n";
my $header = <INGender>;
chomp($header);
my @headerparts = split(/\t/,$header);
my $gendercol;
my $namecol;
my $idcol;
my $callratecol;
my $colidx = 0;
my $fileok = 1;
foreach(@headerparts) {
	if ($_ =~ m/Sample ID/i ) {
		$namecol = $colidx;
	}
	elsif ($_ =~ m/Gender/i ) {
		$gendercol = $colidx;
	}
	elsif ($_ =~ m/Call Rate/i ) {
		$callratecol = $colidx;
	}
	elsif ($_ =~ m/Index/i ) {
		$idcol = $colidx;
	}
	$colidx++;
}

my %genderhash;
my %indexhash;
my %newsamplehash;
my %fortrackhash;
my %callratehash;
my %idhash;
while (<INGender>) {
	chomp($_);
	my @gender = split(/\t/,$_);
	if ($printcol == 3) {
		$sep = "\n";
		$printcol = 0;
	}
	else {
		$sep = "\t\t";
	}

	print " Sample $gender[0] => $gender[1] $sep";
	$printcol++;
	if ($gender[$gendercol] ne "Female" && $gender[$gendercol] ne "Male") {
		$fileok = 0;
	}
	$genderhash{$gender[$namecol]} = $gender[$gendercol];
	$indexhash{$gender[$namecol]} = $gender[$idcol];
	$callratehash{$gender[$namecol]} = $gender[$callratecol];
}
print "\n";

close INGender;
if ($fileok == 0) {
	die "Incorrect Gender Format detected\n";
}

$nrsamples = keys( %genderhash) ; 

#########################################
# COMPOSE ARGUMENT LISTS AND QUEUE THEM #
#########################################
for ($i = 1;$i<=$nrsamples;$i++){
	my $headcommand = "head -n 1 datafiles/$prefix"."_$i.txt";
	my $line = `$headcommand`;
	$line =~ m/\t(\w+)\.Log/;
	$sampleID = $1;
	$gender = $genderhash{$sampleID};
	my $command = "cd $scriptdir/VanillaICE && Rscript VICE_Commands.R datafiles/$prefix"."_$i.txt $genderhash{$sampleID} $prefix"."_$i $sampleID $taufactor $hmm $variance $chiptype &> output/VanillaICE_output_$prefix"."_$i.txt";
	$qsnpqueue->enqueue("$command");
}


#######################
# CREATE VICE THREADS #
#######################

for ($i = 1;$i<=$nrcpus;$i++) {
	${qnspthr."$i"} = threads->create('runalgo');	
}

#########################################
# Make the treads end before continuing #
#########################################
for ($i=1;$i<=$nrcpus;$i++) {
	$qsnpqueue->enqueue(undef);
}
for ($i=1;$i<=$nrcpus;$i++) {
	${qnspthr."$i"}->join();
	print "Thread $i of $nrcpus ended succesfully\n";
	`echo 0 > $statusfile`;
}
unlink($statusfile);

############################
# FORMAT AND STORE RESULTS #
############################
print "Formatting resultsfile: $prefix"."_VanillaICE.xml\n";
if (-e "results_list/$prefix"."_CNV_list.txt") {unlink "results_list/$prefix"."_CNV_list.txt";}
open OUTLIST, ">>results_list/$prefix"."_CNV_list.txt";
open OUTXML, ">results_xml/".$prefix."_VanillaICE.xml";

my @types = ("CNV Bin: Min 0 to Max 0.5" , "CNV Bin: Min 0.5 to Max 1.5" , "CNV Bin: Loss of Heterozygosity" , "CNV Bin: Min 2.5 to Max 3.5", "CNV Bin: Min 3.5 to Max 4.5", "RECURRENT CNV");

print OUTXML "<Project_Bookmarks><Version>2.0.0</Version><Name>$projectname</Name><Author></Author><Comment>Tau: $taufactor - Hmm: $hmm - minsnp: $minsnp</Comment><CreateDate>$time</CreateDate><Algorithm>VanillaICE</Algorithm><AlgorithmVersion>unknown</AlgorithmVersion><Bookmark_Templates><bookmark_template><type>CNV Bin: Min 0 To Max 0.5</type><fill_color>Red</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 0.5 To Max 1.5</type><fill_color>Purple</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Loss of Heterozygosity</type><fill_color>Orange</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 2.5 To Max 3.5</type><fill_color>Green</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 3.5 To Max 4.5</type><fill_color>Blue</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>RECURRENT CNV</type><fill_color>Black</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template></Bookmark_Templates>\n<Bookmarks>";



print OUTLIST "SampleID\tChrNum\tBase_Start\tBase_Stop\tLength\tStart_Probe\tStop_Probe\tNr_SNPs\tCNV\tScore\n";

for ($n=1;$n<=$nrsamples;$n++) {
	my $CurrSample = $fileorder[$n];
	#print "Formatting results for sample $n\n";
	open IN, "rawcnv/$prefix"."_".$n.".rawcnv";
	my $line = <IN>;
	while (<IN>) {
		chomp($_);
		my @line = split(/\t/, $_);
		my $CurrChr = $line[0];
		my $CurrStart = $line[1];
		my $CurrEnd = $line[2];
		my $CurrSize = $line[3];
		my $CurrSProbe = $line[4];
		my $CurrEProbe = $line[5];
		my $CurrNrsnps = $line[6];
		my $CurrState = $line[7];
		my $CurrCN = 2;
		my $CurrGender = $genderhash{$CurrSample};
		if ($CurrGender eq "Female" && $CurrChr =~ m/Y/i) {
			next;
		}
		if ($CurrGender eq "Male" && $CurrChr =~ m/X/i){
			if ($CurrState =~ m/homozygous/) {
				$CurrCN = 0;
			}
			elsif ($CurrState =~ m/copyAmp/) {
				$CurrCN = 3;
			}
			elsif ($CurrState =~ m/hemizygous/) {
				next;
			}	
		}
	
		if ($CurrState =~ m/homozygous/) { 
			$CurrCN = 0; 
		}		
		elsif ($CurrState =~ m/hemizygous/) { 
			$CurrCN = 1; 
		}
		elsif ($CurrState =~ m/LOH/) {
			$CurrCN = 2;
		}
		elsif ($CurrState =~ m/3copyAmp/) {
			$CurrCN = 3;
		}
		elsif ($CurrState =~ m/4copyAmp/) {
			$CurrCN = 4;
		}
		#if ($CurrGender == "Male" && $CurrCN == 1) {
		#	next;
		#}
		my $score = '';
		if (scalar(@line) == 9) {
			$score = "\t$line[8]";
		}
	
		if ($CurrNrsnps >= $minsnp) {
		   print OUTLIST "$CurrSample\t$CurrChr\t$CurrStart\t$CurrEnd\t$CurrSize\t$CurrSProbe\t$CurrEProbe\t$CurrNrsnps\t$CurrCN\t$CurrState$score\n";
		   print OUTXML "<bookmark>\n<sample_id>$CurrSample \[$indexhash{$CurrSample}\]</sample_id>\n<bookmark_type>$types[$CurrCN]</bookmark_type>\n<entry_date></entry_date>\n<chr_num>$CurrChr</chr_num>\n<base_start_pos>$CurrStart</base_start_pos>\n<base_end_pos>$CurrEnd</base_end_pos>\n<author />\n<value>$CurrCN</value>\n<comment><![CDATA[ State: $CurrState, N.SNP: $CurrNrsnps]]></comment>\n</bookmark>\n";
		}
	}
}
print OUTXML "</Bookmarks>\n</Project_Bookmarks>\n";
close OUTXML;
close OUTLIST;

$outname = "results_xml/$prefix"."_VanillaICE.xml";
`cp $outname $targetname && chmod a+rw $targetname`;

for ($i=1;$i<=$nrsamples;$i++) {
	my $splitted = "datafiles/$prefix"."_".$i.".txt";
        unlink("$splitted");
}


# Insert project into database
$query = "INSERT INTO nonmulti (userID, algo, project, stalen, bookmarks, date, chiptype) values ('$userid', 'VanillaICE', '$projectname', '$nrsamples', '$xmlfile','$time', '$chiptypeid') ";
$sth = $dbh->prepare($query);
$sth->execute();
$sth->finish();




##################
# PRINT RUN-TIME #
##################
$now = time - $now;
printf("\n\nVanillaICE running time:%02d:%02d:%02d\n",int($now/3600),int(($now % 3600)/60),int($now % 60));


###############
# SUBROUTINES #
###############
sub searcharray {
  my $found = 0;
  $cn = $_[8];
  $start = $_[2];
  $end = $_[3];
  $chr = $_[1];
  $mean = ($start + $end)/2;
  $size = $end - $start;
  for ($i=0; $i<=$filtersize;$i++) {
	if ($chr != $filter[$i][0] ) {
	  next;
        }
	elsif (($cn != $filter[$i][3]) && (($filter[$i][3] > 2 && $cn < 2 ) || ($filter[$i][3] < 2 && $cn > 2))) {
	  next;
        }
        else {
	  if ($start >= $filter[$i][1] && $end <= $filter[$i][2]) {
 		#Recurrent abberation
 		return "1 $filter[$i][1] $filter[$i][2] $filter[$i][3] $filter[$i][4]";
		$found = 1;
		last;
 	  }
	  elsif ($mean >= $filter[$i][1] && $mean <= $filter[$i][2] && $size <= 1.2*($filter[$i][2]-$filter[$i][1])) {
		#Recurrent abberation extending on at least one side, and not too far
		return "1 $filter[$i][1] $filter[$i][2] $filter[$i][3] $filter[$i][4]";
		$found = 1;
		last;
          }
	  else{
           next;
          }
        } 
  }
  if ($found !=1){return 0;}
}

sub runalgo {
	my $thrid = threads->tid();
        CHECKLOAD: {
		my @cpuload = Sys::CpuLoad::load();
		my $currload = int($cpuload[0]);
		my $running = `cat $statusfile`;
		chomp $running;
		if (($thrid > $nrcpus - $currload) && ($running == 1)) {
			my $randtime = int(120 + rand()*60); 
			#print "Overload protection, waiting $randtime"."s before starting thread $thrid\n";
			sleep $randtime; 
			redo CHECKLOAD;
		}
	}
	while ( defined( $comm = $qsnpqueue->dequeue)) {
	       RUNPCNV:{
			 my @cpuload = Sys::CpuLoad::load();
			 my $currload = int($cpuload[0]);
 			 if ($currload < $nrcpus) {
				#$comm =~ m/output_\w+_(\w+)\..{1,2}\.txt$/;
				$comm =~ m/(data\/.+\.txt) \w/i;
				my $head = `head -n 1 $1`;
				$head =~ m/\t(\w+)\.Log/i;
				print "Starting VanillaICE for sample $1\n";
				system("$comm");
				sleep 10;
				print "VanillaICE analysis for sample $1 done\n";
			 }
			 else {
				print "Optimal load reached, retry in 1 min\n";
				sleep 60;
				redo RUNPCNV;
			 } 
		}
	}
}

