#!/usr/bin/perl
use DBI;
$|++;
########################################
# CONNECT TO DATABASE AND CLEAR TABLES #
########################################
$db = "CNVanalysis";
$host = "localhost";
$userid = "";
$userpass = "";
$connectionInfo = "dbi:mysql:$db:$host";
$dbh = DBI->connect($connectionInfo, $userid, $userpass);

# READ IN current pfb file
print "Read In current pfb file\n";
open IN, "hhall.hg18.pfb";
$head = <IN>;
my %hash;
while (<IN>) {
	chomp($_);
	my @pieces = split(/\t/,$_);
	$hash{ $pieces[0] } = 1;
}
close IN;
# CHECK intensity only probes for non-cytochips";
@types = (1, 2, 5, 6);
my @newprobes;
foreach (@types) {
	$chiptype = $_;
	print "Checking chiptype : $chiptype\n";
	$snpquery = "SELECT name FROM probelocations WHERE chiptype = $chiptype AND zeroed = 1";
	my $ary_ref = $dbh->selectcol_arrayref($snpquery);
	#my $cn = $array[3];
	foreach (@$ary_ref) {
		#print OUT "-$_-,-p-\n";
		if (!exists $hash{$_}) {
			$hash{$_} = 1;
			push (@newprobes, $_);
		}
	}

}
$nrnew = scalar(@newprobes);
print "printing $nrnew new probe information lines to outfile.txt";
open OUT, ">outfile.txt";
foreach (@newprobes) {
	$name = $_;
	$snpquery = "SELECT chromosome, position FROM probelocations WHERE name = '$name' LIMIT 1";
	$sth = $dbh->prepare($snpquery);
	$sth->execute();
	@row = $sth->fetchrow_array();
	$sth->finish();
	$chr = $row[0];
	$pos = $row[1];
	print OUT "$name\t$chr\t$pos\t2\n";
}
close OUT;
print "Done !\n";

