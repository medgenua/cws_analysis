### 
# This script performs segmentation of B allele frequency data
# according to the publication

#  Segmentation-based detection of allelic imbalance and
#  loss-of-heterozygosity in cancer cells using whole genome SNP
#  arrays 
#  Johan Staaf, David Lindgren, Johan Vallon-Christersson,
#  Anders Isaksson, Hanna Goransson, Gunnar Juliusson, Richard
#  Rosenquist, Mattias Hoglund, Ake Borg and Markus Ringner 
#  Genome Biology 2008, 9:R136


breakpoints<-function(x){
	# IN: vector with values. Breakpoints are found between segments of identical values
	# OUT: two vectors, a start index vector and an end index vector
	pos<-1:length(x)
	jj<-which(is.na(x))
	if(length(jj)>0){
		x<-x[-jj]
		pos<-pos[-jj]
	}
	
	x1<-c(-99,x)
	x2<-c(x,-99)
	z<- x2-x1 #all non-zero values in z corresponds to the breakpoints in the profile
	non.zero.z<-which(z !=0) #all breakpoints as c(start1,start2,start3,...,length(data.subset)+1)
	start<-non.zero.z[1:length(non.zero.z)-1]
	stop<-non.zero.z-1
	stop<-stop[-c(1)]
			
	return(cbind(pos[start],pos[stop]))
}


trim.func<-function(x,chr,position){
	# Performs triplet filtering
	# IN:
	# x corresponds to mBAF vector
	# chr corresponds to chromosome assingment. Length and index order should match x
	# position corresponds genomic position of SNPs. Length and index order should match x
	# OUT:
	# 
	
	uu<-order(chr,position)
	x<-x[uu]
	chr<-chr[uu]
	position<-position[uu]
		
	triplet.values<-rep(0,length(x))
		
	my.chr<-unique(chr)
	for(u in 1:length(my.chr)){
		chr.u<-which(chr==my.chr[u])
		my.x<-x[chr.u]
			
		triplet.chr<-rep(0,length(chr.u))
		for(w in 1:length(chr.u)){
			#for each value compute its absolute triplet value
			st<-w-1
			en<-w+1
			if(st<1){
				st<-1
			}
			if(en>length(chr.u)){
				en<-length(chr.u)
			}
			triplet.chr[w]<-sum(abs(my.x[st:en]-my.x[w]))+my.x[w]-0.5
		}
		triplet.values[chr.u]<-triplet.chr
	}
	return(triplet.values)
}#end triplet function


#### Read and prepare cytoband object ####
# File cytoBand.txt should be present in the same directory as BAF_segment.R
# The file can be obtained from UCSC Genome Browser. Make sure that it matches the genome build
# that SNPs are mapped to.
cytoband<-read.table("cytoBand.txt",header=FALSE,na.strings=c(NA))
colnames(cytoband)<-c("chromosome","start","end","name","plotstatus")
cytoband$name<-as.character(cytoband$name)
cytoband$plotstatus<-as.character(cytoband$plotstatus)
cytoband$color<-rep("",length(cytoband$name)) #holder of plotcolor
cytoband$chromosome<-as.character(cytoband$chromosome)
my.blacks<-grep("gpos\\d*",cytoband$plotstatus)
my.whites<-grep("gneg\\d*",cytoband$plotstatus)
my.gvar<-grep("gvar\\d*",cytoband$plotstatus)
my.acen<-grep("acen\\d*",cytoband$plotstatus)
my.stalk<-grep("stalk\\d*",cytoband$plotstatus)
cytoband$color[my.stalk]<-"white"
cytoband$color[my.acen]<-"black"
cytoband$color[my.blacks]<-"black"
cytoband$color[my.gvar]<-"black"
cytoband$color[my.whites]<-"white"

# Find centromer limits for p-and q arm
cyto.chromosomes<-seq(1,24)
centromer.limit.p<-rep(0,length(cyto.chromosomes))
centromer.limit.q<-rep(0,length(cyto.chromosomes))
centromer.limit.q.max<-rep(0,length(cyto.chromosomes))
for(j in 1:length(cyto.chromosomes)){
	cChr<-j
	if(j==23){ # X chrom
		cChr<-"X"
	}
	if(j==24){ # Y chrom
		cChr<-"Y"
	}
	cChr<-paste("chr",cChr,sep="")
	c.Chr.index<-which(cytoband$chromosome==cChr)
	cytoChr<-cytoband[c.Chr.index,] #index 5 is the plotstatus, 6, the plot color
	centromer.limit.p[j]<-cytoChr$end[max(grep("p",cytoChr$name))]
	centromer.limit.q[j]<-cytoChr$end[min(grep("q",cytoChr$name))]
	centromer.limit.q.max[j]<-cytoChr$end[max(grep("q",cytoChr$name))]
	rm(cytoChr,cChr,c.Chr.index)
}
centromer.limit<-data.frame(chromosome=cyto.chromosomes,maxParm=centromer.limit.p,minQarm=centromer.limit.q,maxQarm=centromer.limit.q.max)
rm(centromer.limit.p,centromer.limit.q,centromer.limit.q.max)
gc();
####


### File paths and other parameters
#input.path<-"/Users/johans/Documents/LOHsegmentering/programDevelopment/extracted/"
#output.path<-"/Users/johans/Documents/LOHsegmentering/programDevelopment/extracted/"
#non.informative.mBAF.thr<-0.97
#triplet.thr<-0.8
#ai.thr<-0.56
#ai.size<-4
source("BAF_segment_parameters.txt")
###
#if (chiptype != "") {
zeroedfile <- paste("zeroedfiles/",chiptype, ".zeroed.txt", sep = "")
#}


### Read the file with assays to process ####
# file name should be 'sample_names.txt' 
# format: [Assay] [Filename] [IGV_index]
# headers should be Assay, Filename and IGV_index. Format should be tab separated. IGV_index is necessary afterwards for creating bookmarks to Illumina BeadStudio. If Beadstudio is not
# used, then the IGV_index column can be anything. Only 1s or sample index e.g.
assaysToRow<-read.table(paste(input.path,"sample_names.txt",sep=""),colClasses=c("character"),header=TRUE,na.strings=c(NA))
nbrAssays<-length(assaysToRow$Assay)
###


### Require DNAcopy package
library(DNAcopy)
###


### List of regions in AI
ai.list<-list()
###

chromosome.data<-rep(0,22) #tells whether an autosomal chromosome has been present in at least one sample. Used for cross assay plotting.

### Process each sample ###
for(r in 1:nbrAssays){
	sampleName<-assaysToRow$Assay[r]
	print(sampleName)
	#my.file<-paste(input.path,sampleName,"_extracted.txt",sep="")
	my.file<-paste(input.path,assaysToRow$Filename[r],sep="")
	#### Read sample data file ####
	# Format:
	# Name	Chr	Position	B Allele Frequency	Log R Ratio
	aa<-scan(my.file,nlines=1,what="character",sep="\t")
	nbr.data.col<-length(aa)-3
	my.colClasses<-c("character","character","numeric")
	# This is custom for our data!
	my.colClasses<-c(my.colClasses,rep("numeric",2),"character")
	sim.data<-read.table(my.file,as.is=TRUE, header=TRUE, sep="\t", comment.char="")
	colnames(sim.data) <- c("Name", "Chr", "Position", "Log.R.Ratio", "B.Allele.Frequency", "GT")
	#sim.data<-read.delim(my.file,colClasses=my.colClasses,header=TRUE)
	sim.data$Log.R.Ratio<-as.numeric(sim.data$Log.R.Ratio)
	sim.data$B.Allele.Frequency<-as.numeric(sim.data$B.Allele.Frequency)
	#####
	
	
	### Keep only chromosomes 1-24 if present
	sim.data$Chr<-as.character(sim.data$Chr)
	sim.data$Chr[sim.data$Chr=='XY'] = 'X'
	uu<-which(sim.data$Chr=="X")
	if(length(uu)>0){
		sim.data$Chr[uu]<-23
	}
	uu<-which(sim.data$Chr=="Y")
	if(length(uu)>0){
		sim.data$Chr[uu]<-24
	}
	sim.data$Chr<-as.integer(sim.data$Chr)
	oo<-which(is.na(sim.data$Chr))
	if(length(oo)>0){
		sim.data<-sim.data[-oo,]
	}
	rm(oo,uu)
	gc();
	####
	
	
	### Removing missing values
	oo<-which( (is.na(sim.data$Log.R.Ratio)) | ( is.na(sim.data$B.Allele.Frequency) ) )
	if(length(oo)>0){
		sim.data<-sim.data[-oo,]
	}
	rm(oo)
	###
	
	
	### Remove CNV if option specified ###  CHANGED TO REMOVE =ALL= NON POLYMORPHIC PROBES
	#if(removeCNV){
	#	uu<-grep("cnv",sim.data$Name)
	#	if(length(uu)>0){
	#		sim.data<-sim.data[-uu,]
	#	}
	#}
	###
	
	# NEW CODE
	if(removeCNV) {
		zeroed<- read.table(zeroedfile, as.is = TRUE, header = TRUE, sep = "\t", comment.char="")
		zero<-zeroed$Name
		inisize <- length(sim.data$Name)
		sim.data<-sim.data[!sim.data$Name %in% zero,]
		postsize <- length(sim.data$Name)
		disc <- inisize - postsize
		message("Discarded ",disc, " non polymorphic probes",sep="")
		rm(zeroed)
		rm(zero)
	}

	
	#### Order data ###
	uu<-order(sim.data$Chr,sim.data$Position)
	sim.data<-sim.data[uu,]
	chromosomes<-sort(unique(sim.data$Chr))
	for(q in 1:22){
		jj<-which(sim.data$Chr==q)
		if(length(jj)>0){
			#this chromosome is present in at least this sample, then cross assay plot this chromosome
			chromosome.data[q]<-1
		}
	}
	##
	
	
	#### Select SNPs to segment on using strict mBAF cut-off##
	mBAF<-abs(sim.data$B.Allele.Frequency-0.5)+0.5
	remove<-which(mBAF>non.informative.mBAF.thr)
	my.sim.data<-sim.data
	if(length(remove)>0){
		mBAF<-mBAF[-remove]
		my.sim.data<-sim.data[-remove,]
	
		ii<-which(is.na(mBAF))
		if(length(ii)>0){
			mBAF<-mBAF[-ii]
			my.sim.data<-my.sim.data[-ii,]
		}
	}
	####
	
	#### smoothing / focal aberration for mBAF ####
	sm.window<-20
	sm.mbaf.sd<-rep(NA,length(mBAF))
	
	for(j in 1:length(chromosomes)){
		#print(j)
		chr.i<-which(my.sim.data$Chr==chromosomes[j])
		if(length(chr.i)>0) {
			for(q in 1:length(chr.i)){
				q.pre<-q-sm.window
				q.post<-q+sm.window
				if(q.pre<1) q.pre<-1
				if(q.post>length(chr.i)) q.post<-length(chr.i)
				mean.window<-mean(mBAF[chr.i[q.pre]:chr.i[q.post]])
				my.sd<-sd(mBAF[chr.i[q.pre]:chr.i[q.post]])
				sm.mbaf.sd[chr.i[q]]<-abs(mBAF[chr.i[q]]-mean.window)/my.sd
			}
		}
	}
	ww<-which(sm.mbaf.sd>2)
	print(paste("removing", length(ww), "SNPs using smoothing window"))
	if(length(ww)>0){
		mBAF<-mBAF[-ww]
		my.sim.data<-my.sim.data[-ww,]
	}
	rm(sm.mbaf.sd)
	####
	
			
	#### Run triplet filtering on mBAF and remove SNPs failing filter threshold ####
	triplet.info<-trim.func(mBAF,my.sim.data$Chr,my.sim.data$Position)
	ii<-which(triplet.info>triplet.thr)
	print(paste("removing", length(ii), "SNPs using triplet"))
	if(length(ii)>0){
		mBAF<-mBAF[-ii]
		my.sim.data<-my.sim.data[-ii,]
	}
	####
	
		
	##### Segment using CBS ###
	CNA.object<-CNA(mBAF, my.sim.data$Chr, my.sim.data$Position, data.type=c("logratio"),sampleid=sampleName)
	segment.CNA.object <- segment(CNA.object, verbose=2,alpha=0.001,nperm=200)
		
	vvv.end<-cumsum(as.numeric(segment.CNA.object$output$num.mark))
	vvv.start<-vvv.end+1
	vvv.start<-c(1,vvv.start[-length(vvv.start)])
	segmented.profile<-mBAF
	for(k in 1:length(vvv.end)){
		#mean
		#segmented.profile[vvv.start[k]:vvv.end[k]]<-segment.CNA.object$output$seg.mean[k]
		#using median instead of mean
		segmented.profile[vvv.start[k]:vvv.end[k]]<-median(segment.CNA.object$data[vvv.start[k]:vvv.end[k],3],na.rm=TRUE)
	}
	rm(CNA.object,segment.CNA.object)
	gc();
	#####
	
	
	##### Assign AI ####
	ai<-rep(0,length(segmented.profile))
	uu<-which(segmented.profile>ai.thr)
	if(length(uu)>0){
		ai[uu]<-1
	}
	rm(uu)
	gc();
	#####
	
	
	##### Identify regions of allelic imbalance ###
	print("starting chromosome screen of regions of allelic imbalance")
	for(j in 1:length(chromosomes)){
		workingChrom<-chromosomes[j]
		#print(paste("chr:",workingChrom))
		
		# Filtered tumor informative data
		chr.i<-which(my.sim.data$Chr==workingChrom)
		my.ai<-ai[chr.i]
		my.mBAF<-mBAF[chr.i]
		chr.data<-my.sim.data[chr.i,]
		my.segmented.profile<-segmented.profile[chr.i]
		
		# Original data
		chr.i.org<-which(sim.data$Chr==workingChrom)
		org.chr.data<-sim.data[chr.i.org,]
		org.chr.data$tumInformative<-rep(0,length(org.chr.data[,1]))
		org.chr.data$tumInformativeMBAF<-rep(NA,length(org.chr.data[,1]))
		uu<-match(chr.data$Name,org.chr.data$Name)
		if(length(uu)>0){
			org.chr.data$tumInformative[uu]<-1
			org.chr.data$tumInformativeMBAF[uu]<-my.mBAF
		}
		rm(uu)
		
		# breakpoints of the segmented data	
		bps<-breakpoints(my.segmented.profile)
		size.ai<-bps[,2]-bps[,1]+1
		
		status.i<-my.ai[bps[,1]] #1 if allelic imbalance
		# step through each segment. If it is AI and larger in size than ai.size, then record it
		# AI calls between the matched start ends from calling.frame
		if(length(bps[,1])>0) {
			for(q in 1:length(bps[,1])){
				if((status.i[q]==1) & (size.ai[q]>=ai.size) ){
					start.snp<-chr.data$Name[bps[q,1]]
					end.snp<- chr.data$Name[bps[q,2]]
					org.start<-match(start.snp,org.chr.data$Name)
					org.end<-match(end.snp,org.chr.data$Name)
					# Assay, Chr, Start, End, StartSNP, EndSNP mBAF Het.Rate Average.LogR BpSize
					# EventType is in the form: [Somatic] [Germline]  ADD To different forms!!
					#event.type<-"Germline"
					#if(my.segmented.profile[bps[q,1]]<non.informative.mBAF.thr){
					#	event.type<-"Somatic"
					#}
					mbaf.e<-round(my.segmented.profile[bps[q,1]],2)
					log.r<-round(median(org.chr.data$Log.R.Ratio[org.start:org.end],na.rm=TRUE),2)
					het.rate<-round(length(which(org.chr.data$tumInformative[org.start:org.end]==1)) / length(org.start:org.end),2)
					line<-c(sampleName,workingChrom,chr.data$Position[bps[q,1]],chr.data$Position[bps[q,2]],start.snp,end.snp,mbaf.e,het.rate,log.r,chr.data$Position[bps[q,2]]-chr.data$Position[bps[q,1]]+1)
				
					ai.list[[length(ai.list)+1]]<-line
					rm(mbaf.e,log.r,het.rate,line,org.start,org.end)
					gc();
				}
			}
		}
		rm(chr.i.org,org.chr.data,chr.i,chr.data,my.segmented.profile,my.ai,bps,size.ai,status.i)
		gc();
	}#end chromosomes to test
	print("completed chromosome screen of regions of allelic imbalance")
	#####
	
	if(doPlot){
		print("starting plotting")
		global.logR.vec<-c()
		global.logR.pos<-c()
		global.mbaf.vec<-c()
		global.mbaf.pos<-c()
		local.chromosome.data<-matrix(ncol=3,nrow=length(chromosomes),0)
		#postscript(paste(plot.path,sampleName,"_Segmented.ps",sep=""))
		for(j in 1:length(chromosomes)){
			png(file=paste("plots/",sampleName, "_chr_",j,".png",sep=""), bg="white", width=2040, height=1460)
			chr.i<-which(my.sim.data$Chr==chromosomes[j])
			if(length(chr.i)>0) {
				#print(paste("length(chr.i)",length(chr.i),j,chromosomes[j]))
				op<-par(mfrow=c(3,1),mar=c(4, 4, 2, 1))
				
				### Extract the appropiate cytobands for this chromosome
				cChr<-chromosomes[j]
				if(cChr==23){ # X chrom
					cChr<-"X"
				}
				if(cChr==24){ # Y chrom
					cChr<-"Y"
				}
				cChr<-paste("chr",cChr,sep="")
				cytoChr<-cytoband[which(cytoband$chromosome==cChr),] 
				cytoChr$chromosome<-as.character(cytoChr$chromosome)
				cytoChr$chromosome<-as.character(cytoChr$chromosome)
				###	
			
			
				# 1: Mosaic informative BAF
				plot(my.sim.data$Position[chr.i]/1000,my.sim.data$B.Allele.Frequency[chr.i],xlab="kb",xlim=c(min(cytoChr$start),max(cytoChr$end))/1000,ylab="BAF",ylim=c(0,1),cex=0.3,pch=19,las=1,main=paste(sampleName,"chr",chromosomes[j],"Mosaic informative BAF"))
				abline(h=seq(0,1,0.1),lty=2)
			
				# 2: Mosaic informative mBAF with segmentation and allelic imbalance
				my.ylim<-c(0.45,1)
				my.width<-0.05*(my.ylim[2]-my.ylim[1])
				my.ylim[1]<-my.ylim[1]-my.width
		
				bps<-breakpoints(segmented.profile[chr.i])
				bps<-cbind(bps,bps[,2]-bps[,1]+1,segmented.profile[chr.i][bps[,1]])
				uu<-which((bps[,3]<ai.size) & (bps[,4]>ai.thr) )
				if(length(uu)>0){
					#to short segments in allelic imbalance exists, do not plot them as they have not been reported.
					bps<-bps[-uu,]
					if(!is.matrix(bps)){
						bps<-matrix(nrow=1,byrow=TRUE,bps)
					}
				}
			
				inds<-sort(c(bps[,1],bps[,2]))
				plot(my.sim.data$Position[chr.i],mBAF[chr.i],ylab="mBAF",xlab="",ylim=my.ylim,xlim=c(min(cytoChr$start),max(cytoChr$end)),yaxt="n",xaxt="n",cex=0.3,pch=19,las=1,main=paste(sampleName,"chr",chromosomes[j],"Mosaic informative mBAF with regions of allelic imbalance"))
				lines(my.sim.data$Position[chr.i][inds],segmented.profile[chr.i][inds],col="green",lwd=2)
				if(length(bps[,1])>0) {
					for(q in 1:length(bps[,1])){
						if(bps[q,4]>ai.thr){
							rect(my.sim.data$Position[chr.i][bps[q,1]],0.45,my.sim.data$Position[chr.i][bps[q,2]],0.5,col="red",border=FALSE)
						}
					}
				}
				abline(h=seq(0.5,1,0.1),lty=2)
				abline(h=ai.thr,lty=2,col="red")
				axis(2,at=seq(0.5,1,0.1),labels=seq(0.5,1,0.1),las=1)
				## Adds Cytobands
				at.tics<-matrix(ncol=length(cytoChr$start)/2,nrow=1,0)
				as.lab<-as.character(cytoChr$name)
				as.lab.real<-matrix(ncol=length(cytoChr$start)/2,nrow=1,0)
				k<-0
				for(t in 1:length(cytoChr$start)){
					if(t%%2){
						if(cytoChr$plotstatus[t] =="acen"){
							increment.from.side<-my.width*0.3 / 2
							rect(cytoChr$start[t],(my.ylim[1] + increment.from.side),cytoChr$end[t],(my.ylim[1] + my.width - increment.from.side),col=cytoChr$color[t])
						}else{
							rect(cytoChr$start[t],(my.ylim[1]),cytoChr$end[t],(my.ylim[1] + my.width),col=cytoChr$color[t])
						}
						k<-k+1
						at.tics[k]<- mean(c(cytoChr$start[t],cytoChr$end[t]))
						as.lab.real[k]<-as.lab[t]
					}else{
						if(cytoChr$plotstatus[t]=="acen"){
							increment.from.side<-my.width*0.3 / 2
							rect(cytoChr$start[t],(my.ylim[1] +increment.from.side),cytoChr$end[t],(my.ylim[1]+my.width - increment.from.side),col=cytoChr$color[t])			
						}else{
							rect(cytoChr$start[t],(my.ylim[1]),cytoChr$end[t],(my.ylim[1] + my.width),col=cytoChr$color[t])
						}
					}
				}#end for		
				####
				axis(side=1,labels=as.lab.real,at=at.tics,cex.axis=0.7,las=2) # bottom x axis, Cytobands
			
				if(length(bps[,1])>0) {
					uu1<-match(my.sim.data$Name[chr.i][bps[,1]],sim.data$Name)
					uu2<-match(my.sim.data$Name[chr.i][bps[,2]],sim.data$Name)
					global.mbaf.vec<-c(global.mbaf.vec,rep(bps[,4],each=2))
					global.mbaf.pos<-c(global.mbaf.pos,uu1,uu2)
				}
			
				# 3: Log R Ratio plot with average log R ratios for mbaf segments.
				p.start<-match(my.sim.data$Name[chr.i][bps[,1]],sim.data$Name)
				p.end<-match(my.sim.data$Name[chr.i][bps[,2]],sim.data$Name)
				mean.logR.vec<-c()
				plot(sim.data$Position[which(sim.data$Chr==chromosomes[j])]/1000,sim.data$Log.R.Ratio[which(sim.data$Chr==chromosomes[j])],xlim=c(min(cytoChr$start),max(cytoChr$end))/1000,xlab="kb",ylab="Log R Ratio",ylim=c(-2,2),main=paste(sampleName,"chr",chromosomes[j],"Log R Ratio with means for mBAF segments"),cex=0.3,pch=19,las=1)
				abline(h=c(0,0.5,-0.5,1,-1),lty=2)
				if(length(bps[,1])>0) {
					for(q in 1:length(p.start)){
						mean.logR<-mean(sim.data$Log.R.Ratio[p.start[q]:p.end[q]],na.rm=TRUE)
						mean.logR.vec<-c(mean.logR.vec,mean.logR,mean.logR)
						#segments(sim.data$Position[which(sim.data$Chr==chromosomes[j])][p.start[q]]/1000,mean.logR,sim.data$Position[which(sim.data$Chr==chromosomes[j])][p.end[q]]/1000,mean.logR,col="green")
					}
					lines(sim.data$Position[sort(c(p.start,p.end))]/1000,mean.logR.vec,col="green",lwd=2)
					global.logR.vec<-c(global.logR.vec,mean.logR.vec)
					global.logR.pos<-c(global.logR.pos,p.start,p.end)
				}
				par(op)
				
				local.chromosome.data[j,1]<-chromosomes[j]
				local.chromosome.data[j,2]<-min(which(sim.data$Chr==chromosomes[j]))
				local.chromosome.data[j,3]<-max(which(sim.data$Chr==chromosomes[j]))
				
			}
			else {
				plot.new()
				plot.window(c(1,10),c(1,10))
				text(5,5,labels=paste(sampleName,"chr",chromosomes[j]),cex=0.8)				
				text(5,3,labels="No probes remained after filtering",cex=0.8)
			}
			dev.off()	
		}
		
		postscript(paste(plot.path,sampleName,"_WholeGenomeSegmented.ps",sep=""))
		global.logR.pos<-sort(global.logR.pos)
		global.mbaf.pos<-sort(global.mbaf.pos)
		center.chrom<-(local.chromosome.data[,3]+local.chromosome.data[,2])/2
		op<-par(mfrow=c(2,1))
		plot(global.mbaf.pos,global.mbaf.vec,type="l",las=1,xaxt="n",ylim=c(0.5,1),xlab="",main=paste("Global segmented mBAF profile",sampleName),ylab="mBAF")
		abline(v=local.chromosome.data[1,2],lty=2)
		abline(v=local.chromosome.data[,3],lty=2)
		axis(1,at=center.chrom,labels=chromosomes,las=1,cex.axis=0.7)
		abline(h=ai.thr,col="red")
		abline(h=seq(0.55,1,0.05),lty=3)
		plot(global.logR.pos,global.logR.vec,type="l",las=1,xaxt="n",ylim=c(-2,2),xlab="",main=paste("Global segmented Log R Ratio profile based on mBAF segments",sampleName),ylab="Log R Ratio")
		abline(h=seq(-1,1,0.5),lty=3)
		abline(v=local.chromosome.data[1,2],lty=2)
		abline(v=local.chromosome.data[,3],lty=2)
		axis(1,at=center.chrom,labels=chromosomes,las=1,cex.axis=0.7)
		par(op)
		dev.off()
		print("Finished plotting")
		rm(global.logR.vec,global.logR.pos,global.mbaf.vec,global.mbaf.pos,local.chromosome.data)
		gc();
	}#end do plot
}#end sample


##### Print segmented data ####
my.file<-paste(output.path,"AI_regions.txt",sep="")
header.names<-c("Assay","Chr","Start","End","StartSNP","EndSNP","mBAF","HetRate","MedianLogRratio","BpSize")
ww<-unlist(ai.list)
ww.mat<-matrix(ncol=length(header.names),ww,byrow=TRUE)
write.table(ww.mat, file = my.file, quote = FALSE,append=FALSE, sep = "\t", row.names = FALSE,col.names = header.names)
######	


