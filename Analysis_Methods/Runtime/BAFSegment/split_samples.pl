#!/usr/bin/perl -w

# Copyright (C) 2008,2009 Markus Ringner
# 
# This file is part of tQN and BAFsegmentation
# http://baseplugins.thep.lu.se/wiki/se.lu.onk.IlluminaSNPNormalization
# http://baseplugins.thep.lu.se/wiki/se.lu.onk.BAFsegmentation
# 
# tQN/BAFsegmentation is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
# 
# tQN/BAFsegmentation is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Class Discoverer; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307 USA

use strict;
use warnings;

use File::Spec;
use Getopt::Long;
use IO::File;
use Pod::Usage;

# initialize user-definable parameters to default values

my $extract_dir='extracted';
my $beadstudio_file='';
my $prefix='';

# set user defined parameters

GetOptions('data_file=s' => \$beadstudio_file,
	   'output_directory=s' => \$extract_dir,
	   'prefix=s' => \$prefix) or pod2usage(1);

$beadstudio_file=File::Spec->canonpath($beadstudio_file);
$extract_dir=File::Spec->canonpath($extract_dir);
if($beadstudio_file ne '') {
    unless((-r $beadstudio_file)) {
	print STDERR "Error: Cannot read data file: $beadstudio_file\n";
	exit(0);
    }
}
else {
    print STDERR "Error: No file with data specified!\n";
    pod2usage(1);
}

unless((-d $extract_dir)) {
    print STDERR "Error: Cannot access directory for storing output: " . 
	"$extract_dir\n";
    exit(0);
}

# Main program - Split multiple sample file into one file per sample

# Read header

open(BEADSTUDIO,"$beadstudio_file");
my $beadstudio_error="Error: BeadStudio data in $beadstudio_file " . 
    "is not in the correct format";
my $header=<BEADSTUDIO>;
chomp($header);
my @header=split(/\t/,$header,-1);
if($header[0] ne 'Name' || $header[1] ne 'Chr' || $header[2] ne 'Position') {
    print STDERR "$beadstudio_error\n";
    print STDERR "The first three column headers should be: " .
	"Name, Chr, Position\n";
    exit(0);
}

my @samples;
my $beadstudio_format='normalization';
for(my $i=3;$i<scalar(@header);$i+=2) {    
    if($header[$i]!~/\.X/ && $header[$i]!~/\.B Allele Freq/) {
	print STDERR "$beadstudio_error\n";
	print STDERR "Column " . ($i+1) . " header, $header[$i]," .
	    " should be <sample>.X or <sample>.B Allele Freq\n";
	exit(0);
    } 
    if($header[$i]=~/\.B Allele Freq/) {
	$beadstudio_format='segmentation';
    }
    my $sample_name=$header[$i];
    $sample_name=~s/\.X//;
    $sample_name=~s/\.B Allele Freq//;
    if($header[$i+1]!~/$sample_name\.Y/  && $header[$i+1]!~/\.Log R Ratio/) {
	print STDERR "$beadstudio_error\n";
	print STDERR "Column " . ($i+2) . " header, $header[$i+1]," .
	    " should be ${sample_name}.Y or ${sample_name}.Log R Ratio\n";
	exit(0);
    } 
    push(@samples,$sample_name);
}

my $name="sample_names.txt";
if($prefix ne '') {
    $name="${prefix}_sample_names.txt";
}
my $filename=File::Spec->canonpath("$extract_dir/$name");
my $fh1=IO::File->new(">$filename");
if(!defined($fh1)) {
    print STDERR "Error: Cannot open file $filename for writing\n";
    exit(0);
}

print $fh1 "Assay\tFilename\tIGV_index\n";

my $igv_index=1;
foreach my $sample (@samples) {
    print $fh1 "$sample" .
	"\t${sample}_extracted.txt" .
	"\t$igv_index" .
	"\n";
    $igv_index++;
}
close($fh1);

my @file_handles;
foreach my $sample (@samples) {
    my $filename=File::Spec->canonpath("$extract_dir/${sample}_extracted.txt");
    my $fh=IO::File->new(">$filename");
    if(!defined($fh)) {
	print STDERR "Error: Cannot open file $filename for writing\n";
	exit(0);
    }
    my $out_header="Name\tChr\tPosition\tX\tY";
    if($beadstudio_format eq 'segmentation') {
	$out_header="Name\tChr\tPosition\tB Allele Frequency\tLog R Ratio";
    }
    print $fh "$out_header\n";
    push(@file_handles,$fh);
}

my $decimal_error=0;
while(my $line=<BEADSTUDIO>) {
    chomp($line);
    my @line=split(/\t/,$line,-1);
    if(scalar(@header) != scalar(@line)) {
	print STDERR "$beadstudio_error\n";
	print STDERR "Not the same number of columns in all rows\n";
	exit(0);
    }
    for(my $i=3;$i<scalar(@header);$i+=2) {    
	my $fh=$file_handles[($i-3)/2];
	my $data_line="$line[$i]\t$line[$i+1]";
	if($data_line =~ /,/) {
	    $decimal_error=1;
	    $data_line =~ s/,/./g;
	}
	print $fh "$line[0]\t$line[1]\t$line[2]\t$data_line\n";
    }
}

foreach my $fh (@file_handles) {
    close($fh);
}

if($decimal_error) {
    print "Warning: commas (,) were found in you BeadStudio data columns. " . 
	"These have been interpreted as decimal points and " . 
	"have been replaced with points (.) in the generated files. " . 
	"BAFsegmentation and tQN requires decimal points to be points (.) and not commas (,).\n"
}

# Documentation

=pod

=head1 NAME

split_samples.pl - split multiple samples in a data file into separate files for use with BAFsegmentation or tQN

=head1 SYNOPSIS

split_samples.pl --data_file=<name> [--output_directory=<name>] [--prefix=<name>]

=head1 OPTIONS

=over 8

=item B<--data_file=<name>>

Specify a file with data from BeadStudio.

=item B<--output_directory=<name>>

Specify a directory to store the generated files in. Default is "extracted".

=item B<--prefix=<name>>

Specify a prefix for the generated file with sample names: <prefix>_sample_names.txt. Default is to use no prefix.


=back

=head1 AUTHORS 

Markus Ringner

Please report bugs to markus.ringner@med.lu.se

=cut

# End of documentation
