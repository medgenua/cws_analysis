#!/usr/bin/perl 

# Copyright (C) 2008 Markus Ringner
# 
# This file is part of BAFsegmentation
# http://baseplugins.thep.lu.se/wiki/se.lu.onk.BAFsegmentation
# 
# BAFsegmentation is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
# 
# BAFsegmentation is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Class Discoverer; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307 USA

use strict;
use warnings;

use File::Spec;
use Getopt::Long;
use IO::File;
use Pod::Usage;

######################################################

# NOTE!!! Change here depending on how you run R on your system

###
# Mac OS X and Linux
my $R_command="Rscript BAF_segment.R";
###
# Windows
# Note that we are using Rscript, which is a part of the R distribution.
#my $R_windows=File::Spec->canonpath('C:/"Program Files"/R/R-2.7.0/bin/Rscript');
#my $R_command="$R_windows --vanilla BAF_segment.R";

#######################################################




sub write_bookmarks($$$$);
sub write_head($$);
sub write_foot($);

# initialize user-definable parameters to default values

my $input_dir='extracted';
my $output_dir='segmented';
my $plot_dir='plots';
my $noninformative=0.97;
my $triplet=0.8;
my $aithreshold=0.56;
my $aisize=4;
my $removecnv='TRUE';
my $doplot='TRUE';
my $help=0;
my $sid='';
my $pid='';
my $sname='';
my $chiptype='';
#my $chiptype = $ARGV[0];
# set user defined parameters

GetOptions('sid=s' => \$sid,
	    'pid=s' => \$pid,
	    'sname=s' => \$sname,
	   #'input_directory=s' => \$input_dir,
	   #'output_directory=s' => \$output_dir,
	   #'plot_directory=s' => \$plot_dir,
	   #'non_informative=f' => \$noninformative,
	   #'triplet=f' => \$tripletthreshold,
	   #'ai_threshold=f' => \$aithreshold,
	   #'ai_size=i' => \$aisize,
	   #'remove_CNV=s' => \$removecnv,
	   #'do_plot=s' => \$doplot,
	   'chiptype' => \$chiptype,
	   'help|?' => \$help
	   ) or pod2usage(1);

	       pod2usage(1) if $help;

$input_dir=File::Spec->canonpath($input_dir);
$output_dir=File::Spec->canonpath($output_dir);

unless((-d $input_dir)) {
    print STDERR "Error: Cannot access directory for reading input: " . 
	"$input_dir\n";
    exit(0);
}

unless((-d $output_dir)) {
    print STDERR "Error: Cannot access directory for storing output: " . 
	"$output_dir\n";
    exit(0);
}

unless((-d $plot_dir)) {
    print STDERR "Error: Cannot access directory for plots: " . 
	"$plot_dir\n";
    exit(0);
}


# Generate parameter file for the R-script performing the normalization
#open(FILE,">BAF_segment_parameters.txt");
#print FILE "input.path<-\"$input_dir/\"\n";
#print FILE "output.path<-\"$output_dir/\"\n";
#print FILE "non.informative.mBAF.thr<-$informative_threshold\n";
#print FILE "triplet.thr<-$triplet_threshold\n";
#print FILE "ai.thr<-$ai_threshold\n";
#print FILE "ai.size<-$ai_size\n";
#print FILE "plot.path<-\"$plot_dir/\"\n";
#print FILE "removeCNV<-$removeCNV\n";
#print FILE "doPlot<-$doPlot\n";
#print FILE "chiptype<-\"$chiptype\"\n";
#print FILE "samplename<-\$$samplename\"\n";
#close(FILE);

# add parameters to command.
$R_command .= " --pid $pid --sid $sid --sname $sname --triplet $triplet --non_informative $noninformative --ai_threshold $aithreshold --ai_size $aisize --remove_CNV $removecnv --do_plot $doplot --chiptype $chiptype";
# Run R script to generate segmented data
system("$R_command");

# Generate bookmark file with regions for import into BeadStudio

my $sample_file=File::Spec->canonpath("$input_dir/sample_names.txt");
open(SAMPLES,"$sample_file") || 
    die "Error: Cannot open sample list file: $sample_file\n";
my $header=<SAMPLES>;
my $ok_header="Assay\tFilename\tIGV_index";
chomp($header);
if($header ne $ok_header) {
    print STDERR "Error: Unexpected file format in sample list file $sample_file: " .
	"The header is not $ok_header\n";
    exit(0);
}

my %samples;
while(my $line=<SAMPLES>) {
    chomp($line);
    my @tmp=split(/\t/,$line);
    $samples{$tmp[0]}=$tmp[2];
}
close(SAMPLES);

my @time = localtime(time);
my $month=$time[4]+1;
my $hour=$time[2];
my $min=$time[1];
my $sec=$time[0];
my $day=$time[3];
if(length($month)==1) {
    $month='0' . $month;
}
if(length($day)==1) {
    $day='0' . $day;
}
if(length($hour)==1) {
    $hour='0' . $hour;
}
if(length($min)==1) {
    $min='0' . $min;
}
if(length($sec)==1) {
    $sec='0' . $sec;
}
my $date=$time[5]+1900 . '-' . "$month-$day $hour:$min:$sec";

my $outfile=File::Spec->canonpath("${output_dir}/AI_regions.xml");
open(OUTPUT,">$outfile") || die "Error: Cannot open result file: $outfile\n";
my $infile=File::Spec->canonpath("${output_dir}/AI_regions.txt");
open(INPUT,"$infile") || die "Error: Cannot open result file: $infile\n";
write_head(\*OUTPUT,$date);
write_bookmarks(\*OUTPUT,\*INPUT,\%samples,$date);
write_foot(\*OUTPUT);
close(OUTPUT);
close(INPUT);


# End of main program

# Subroutines

sub write_bookmarks($$$$) {
    my $fh_out=shift;
    my $fh_in=shift;
    my $samples=shift;
    my $date=shift;
    my $header=<$fh_in>;
    while (my $line=<$fh_in>) {
	chomp($line);
	my @items=split(/\t/,$line,-1);
	my $igv_index=$$samples{$items[0]};
	print $fh_out "    <bookmark>\n" .
	    "      <sample_id>$items[0] [$igv_index]</sample_id>\n" .  
	    "      <bookmark_type>AI</bookmark_type>\n" .
	    "      <entry_date>$date</entry_date>\n" .
	    "      <chr_num>$items[1]</chr_num>\n" .
            "      <base_start_pos>$items[2]</base_start_pos>\n" .
	    "      <base_end_pos>$items[3]</base_end_pos>\n" .
	    "      <author></author>\n" .
            "      <comment>\n" .
            "        <![CDATA[ Segmented BAF: $items[6]\n" .
	    "Median Log R Ratio: $items[8]\n" .
	    "Heterozygosity rate: $items[7]\n" .
	    "First SNP: $items[4]\n" .
	    "Last SNP: $items[5]\n" .
	    "Region: chr".$items[1].":".$items[2]."-".$items[3]."\n" .
	    " ]]>\n" . 
	    "      </comment>\n" .
	    "    </bookmark>\n";
	}
}


sub write_head($$) {
    my $fh=shift;
    my $date=shift;
    print $fh "<Project_Bookmarks>\n" .
	"  <Version>2.0.0</Version>\n" .
	"  <Name>BAFsegmentation 0.1</Name>\n" .
	"  <Author></Author>\n" .
	"  <Comment><![CDATA[  ]]></Comment>\n" .
	"  <CreateDate>$date</CreateDate>\n" .
	"  <Algorithm></Algorithm>\n" .
	"  <AlgorithmVersion></AlgorithmVersion>\n" .
	"  <Module></Module>\n" .
	"  <GenomeSpecies></GenomeSpecies>\n" .
	"  <GenomeBuild ></GenomeBuild >\n" .
	"  <TableSource>GT Samples</TableSource>\n" .
	"  <Bookmark_Templates>\n" .
	"   <bookmark_template>\n" .
	"     <type>AI</type>\n" .
	"     <fill_color>DarkRed</fill_color>\n" .
	"     <fill_style>Solid</fill_style>\n" .
	"     <fill_opacity>20</fill_opacity>\n" .
	"   </bookmark_template>\n" .
	"  </Bookmark_Templates>\n" .
	"  <Bookmarks>\n";
}

sub write_foot($) {
    my $fh=shift;
    print $fh "  </Bookmarks>\n" . 
	"</Project_Bookmarks>\n";
}



# Documentation

=pod

=head1 NAME

BAF_segment_samples.pl - Segment samples for detection of allelic imbalance

=head1 SYNOPSIS

BAF_segment_samples.pl [--input_directory=<name>] [--output_directory=<name>] [--non_informative=<value>] [--triplet=<value>] [--ai_threshold=<value>] [--ai_size=<value>] [--plot_directory=<name>] [--remove_CNV=<name>] [--do_plot=<name>] [--help]

=head1 OPTIONS

=over 8

=item B<--input_directory=<name>>

Specify a directory with files for individual samples with BeadStudio
data. The directory should contain a file "sample_names.txt" with a
list of samples to analyse. Files in this directory can be generated
using "split_samples.pl". Default is "extracted".

=item B<--output_directory=<name>>

Specify a directory to store the generated files in. Default is "segmented".

=item B<--non_informative=<value>>

Specify a threshold in mBAF for removing putatively non-informative
homozygous SNPs. Default value is 0.97.

=item B<--triplet=<value>>

Specify a threshold for thriplet filtering used to improve removal of
putatively non-informative homozygous SNPs. Default value is 0.8.

=item B<--ai_threshold=<value>>

Specify a threshold in mBAF for calling regions af allelic imbalance
based on segmented mBAF values. Default is 0.56.

=item B<--ai_size=<value>>

Specify the minimal number of SNPs a segmented region should contain
to be allowed to be called as allelic imbalance. Default is 4.

=item B<--plot_directory=<name>>

Specify a directory to store any generated plot files in. Default is "plots".

=item B<--remove_CNV=<name>>

Specify whether to remove probes with name starting with cnv_ (TRUE) or 
not (FALSE). Default is FALSE.

=item B<--do_plot=<name>>

Specify whether to do individual plots for each sample (TRUE) or not (FALSE). Default is TRUE.

=item B<--help>

Prints a help message and exits



=back

=head1 AUTHORS 

Markus Ringner

Please report bugs to markus.ringner@med.lu.se

=cut

# End of documentation
