
###################################################
## PLINK HOMOZYGOSITY MAPPING : Plink_Overlap.pl ##
###################################################

1 Runtime arguments
===================
	- h : print this help file and exit
	- u : User name for report
	- c : Use Cluster (if not specified, run on local machine)
	- w : sliding window size [50]
	- H : heterozygous calls per window allowed [1] 
	- m : missing calls per window allowed [5]
	- o : overlap for joining [0.95]

2 Input Files
=============
	- rawdata/samples.txt : 
	    - Mandatory Headerline 
	    - syntax example: 
		plink_FID	Sample_Name	Gender	Affected
		1		sample_1	Male	1
		1		sample_2	Female	0
	- rawdata/datafile.[0-inf].txt/gz : 
	    - Exported GenomeStudio Full.Data.Table files
	    - Mandatory columns : SNP name, SNP chr, SNP pos, Sample Genotype
	    - Allowed formats : Plain text (.txt) or Gzip'ed (.gz)

3 OUTPUT
=======
	- Regular plink files for homozygous region analysis
	- PDF report containing Genes in the overlapping regions unique to the cases : report/Overlap.Report.pdf

