#!/usr/bin/perl


################
# LOAD MODULES #
################
use Getopt::Std;
use Number::Format;
use DBI;
use Cwd;
use XML::Simple;

# read credentials
use Cwd 'abs_path';
## credentials loading is severals directories up, but working directory is set to $scriptdir from caller. !
$credpath = abs_path(".LoadCredentials.pl");
require($credpath);

use warnings;

$|++;

###################
## GET ARGUMENTS ##
###################
# h : print help file and exit
# w : sliding window size
# H : heterozygous calls per window allowed
# m : missing calls per window allowed
# o : overlap for joining
# i : input file
# p : project id
# s : sample id
getopts('hH:g:i:m:n:p:s:S:w:"', \%opts);  # option are in %opts

if ($opts{'h'}) {
	open IN, "README.txt";
	while (<IN>) {
		print $_;
	}
	close IN;
	print "\n\n";
	exit();
}

my $homozygopts = "--homozyg ";
if ($opts{'w'}) {
	$homozygopts .= "--homozyg-window-snp $opts{'w'} ";
}
if ($opts{'H'}) {
	$homozygopts .= "--homozyg-window-het $opts{'H'} ";
}
if ($opts{'m'}) {
	$homozygopts .= "--homozyg-window-missing $opts{'m'} ";
}
if ($opts{'g'}) {
	$homozygopts .= "--homozyg-gap $opts{'g'} ";
}
if ($opts{'n'}) {
	$homozygopts .= "--homozyg-snp $opts{'n'} ";
}
if ($opts{'S'}) {
	$homozygopts .= "--homozyg-kb $opts{'S'} ";
}

## pid
our $pid = $opts{'p'};
my $sid = $opts{'s'};

###############################
# SET NUMBER FORMATTING STYLE #
###############################
my $de = new Number::Format(-thousands_sep =>',',-decimal_point => '.');

####################
## CHOMOSOME HASH ##
####################
my %chromhash = {};
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";

###########################
# CONNECT TO THE DATABASE #
###########################
my $db ;
# connect to GenomicBuilds DB to get the current Genomic Build Database. 
my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$db = "CNVanalysis".$row[0];
#print "Assuming build ".$row[1]."\n";
my $BuildName = $row[1];
$gbsth->finish();
$dbhgb->disconnect;	
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
$dbh->{mysql_auto_reconnect} = 1;

###############################
## GET HASH OF ZEROED PROBES ##
###############################
$sth = $dbh->prepare("SELECT chiptypeid FROM project WHERE id = '$pid'");
$sth->execute();
my ($chipid) = $sth->fetchrow_array();
$sth->finish();
$sth = $dbh->prepare("SELECT name FROM probelocations WHERE zeroed = 1 AND chiptype = '$chipid'");
$nrzero = $sth->execute();
$sth->{RaiseError} = 1;
my $ref = $sth->fetchall_arrayref;
my %zero = ();
foreach $r (@{$ref})
{
	$zero{$r->[0]} = 1;
}
$sth->finish();
print "The used chiptype contains ". keys(%zero) . " zeroed probes that will be discarded.\n";

########################
## GET SAMPLE DETAILS ##
########################
$sth = $dbh->prepare("SELECT chip_dnanr, gender FROM sample WHERE id = '$sid'");
$sth->execute();
my ($sample,$gender) = $sth->fetchrow_array();
$sth->finish();

###########################
## CHECK FOR INPUT FILES ##
###########################
#print "Checking for presence of datafiles\n";
my $infile = $opts{'i'};
if (!-e "$infile" || $infile eq '') {
	print "No sample information found in file '$infile'!\n";
	print "Exiting.\n";
	exit();
}

# create output directory
if ($sid eq '' || $pid eq '') {
	print "Project/sample information incomplete. Aborting\n";
	exit();
}
my $tmpdir = "/tmp/$pid.$sid.plink";
system("mkdir '$tmpdir'");


#################
## GLOBAL VARS ##
#################
my %samplehash = {};
my %fambyid = {};
my $samplestring = '';
my %sidlinks = {};
my $nrcases = 0;
my $cwd = getcwd;
my %genders;
$genders{'Male'} = 1;
$genders{'Female'} = 2;

######################
## CREATE TFAM FILE ##
######################
open OUT, ">$tmpdir/$pid.$sid.tfam";
print OUT "1\t$sid\t0\t0\t$genders{$gender}\t1\n";
close OUT;

##################################
## CREATE INDIVIDUAL TPED FILES ##
##################################
print "Extracting data for $sample\n";
my $head = `head -n 1 '$infile'`;
# get needed columns (chr snpid, pos and gt).
chomp($head);
my @p = split(/\t/,$head);
my $namecol = -1;
my $poscol = -1;
my $chrcol = -1;
my $gtcol = -1;
my $counter = -1;
foreach (@p) {
	$counter++;
	if ($_ eq 'Name') {
		$namecol = $counter;
		next;
	}
	if ($_ eq 'Chr') {
		$chrcol = $counter;
		next;
	}
	if ($_ eq 'Position') {
		$poscol = $counter;
		next;
	}
	if ($_ eq "$sample.GType") {
		$gtcol = $counter;
		next;
	}
}
# create outfile name 
$outfile = "$tmpdir/$pid.$sid.tped";
# fill file
open OUT , ">$outfile";
open IN, "$infile";
my $ignored = 0;
my $missing = 0;
my $posignored = 0;
my $skip = <IN>;
my $total = 0;
while (<IN>) {
	$total++;
	$_ =~ s/\r\n/\n/;
	chomp($_);
	my @p = split(/\t/,$_);
	if (exists $zero{$p[$namecol]} && $zero{$p[$namecol]} == 1) {
		$ignored++;
		next;
	}
	if ($p[$chrcol] eq '0' || $p[$poscol] eq '0') {
		$posignored++;
		next;
	}
	my $gtpart = '';
	if ($p[$gtcol] eq 'NC') {
		$missing++;
		$gtpart = "0 0";
	}
	else {
		($gtpart = $p[$gtcol]) =~ s/(\S)(\S)/$1\ $2/;
	} 
	print OUT "$p[$chrcol]\t$p[$namecol]\t0\t$p[$poscol]\t$gtpart\n"; 
}
#print "\tResults for $sample\n\t\t$ignored probes were ignored due to zeroed status (discarded), \n\t\t$missing were set to missing GT (kept), \n\t\t$posignored had bad position info (discarded).\n";
print "\tResults for $sample\n\t\t$ignored probes were ignored due to zeroed status (discarded), \n\t\t$missing were set to missing GT (kept), \n\t\t$posignored had bad position info (discarded).\n\t\t$total probes were checked in total\n";
close IN;
close OUT;

##################
## RUN ANALYSIS ##
##################
$binary = "$scriptdir/Analysis_Methods/Runtime/PlinkHomoz/Bin/plink";
#print "Running Plink analysis\n";
$command = "cd $tmpdir && $binary --noweb --tped $pid.$sid.tped --tfam $pid.$sid.tfam $homozygopts --1 > $tmpdir/run.output.txt 2> $tmpdir/run.erroroutput.txt";

system($command);

## copy results file to target file ##
my $outfile = "$scriptdir/Analysis_Methods/Runtime/PlinkHomoz/output/$pid.$sid.hom";
system("cp '$tmpdir/plink.hom' '$outfile'");

##############
## CLEAN UP ##
##############
system("rm -Rf '$tmpdir'");


		
