#!/usr/bin/perl

################
# LOAD MODULES #
################
use Sys::CPU;
use threads;
use Thread::Queue;
use threads::shared;
use Sys::CpuLoad;
use Number::Format;
use DBI;
use Dataformat;
use Getopt::Std;

##########################
# COMMAND LINE ARGUMENTS #
##########################
# D = Use specific database (mandatory if L is specified), otherwise use default.
# u = username (string)
# d = datafile (file location)
# g = genderfile (file location)
# p = project name (string)
# c = chiptype (string)
# s = Minimal number of snps
# S = Minimal Score (Log Bayes Factor)
# C = Configuration file
# l = Location of GC-content descriptions
# e = EM iterations
# L = Lenght setting
# M = max copy number value
# G = Do GC correction
# R = printRS: print out probe info
getopts('D:u:d:g:p:c:s:S:C:l:e:L:M:G:R:', \%opts);  # option are in %opts

#######################
# ENABLE AUTOFLUSHING #
#######################
$| = 1;	 	


#################
# GET STARTTIME #
#################
local $now = time;


###############################
# SET NUMBER FORMATTING STYLE #
###############################
my $de = new Number::Format(-thousands_sep =>',',-decimal_point => '.');



###########################
# CONNECT TO THE DATABASE #
###########################
$db = "";
require("/opt/ServerMaintenance/PerlDatabaseCredentials.pl");
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "Assuming build ".$row[1]."\n";
	$gbsth->finish();
	$dbhgb->disconnect;	
}
$connectionInfo="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfo,$userid,$userpass);
$dbh->{mysql_auto_reconnect} = 1;

################################
# ASSIGN COMMANDLINE ARGUMENTS #
################################
my $datafile = $opts{'d'};
my $genderfile = $opts{'g'};
$datafile =~ m/datafiles\/(.+)\.txt$/;
$prefix = $1;
my $projectname = $opts{'p'};
my $targetname = "$sitedir/bookmarks/" . $projectname . "_QuantiSNP.xml";
my $xmlfile = $projectname . "_QuantiSNP.xml";
my $configfile = $opts{'C'};
my $emiters = $opts{'e'};
my $lsetting = $opts{'L'};
my $maxcopy = $opts{'M'};
my $dogccorrect = $opts{'G'};
if ($dogccorrect =~ m/(Ja|ja|JA|yes|Yes|YES|1)/) {
	$dogccorrect = " --doGCcorrect";
	$dgcc = "1";
}
else {
	$dgcc = "0";
}
my $gcdir = $opts{'l'};
my $printrs = $opts{'R'};
if ($printrs =~ m/(Ja|ja|JA|yes|Yes|YES|1)/) {
	$printrs = " --printRS";
	$prs = "1";
}
else {
	$prs = "0";
}
my $minsnp = $opts{'s'};
my $minlbf = $opts{'S'};
my $statusfile = "status/$projectname.status";
my $username = $opts{'u'};
my $getuserid = "SELECT id FROM users WHERE username = '$username'";
$sth = $dbh->prepare($getuserid);
$sth->execute();
my @userids = $sth->fetchrow_array();
my $userid = $userids[0];
$sth->finish();
my $chiptype = $opts{'c'};
print "chiptype: $chiptype\n";
if ($chiptype eq "HumanCNV370duo") {
	print "filtering variable probes\n";
	my %filter;
	open IN, "$scriptdir/cutoff_0.999.txt";
	while (<IN>) {
		chomp $_;
		$filter{ $_ } = "";
	}
	close IN;
	open IN, "$datafile";
	open OUT, ">$scriptdir/datafiles/filtered.txt";
	my $header = <IN>;
	print OUT $header;
	while (<IN>) {
	my @pieces = split(/\t/,$_);
		if (!exists($filter{ $pieces[0] })) {
			print OUT $_;
		}
	}
	system("mv $scriptdir/datafiles/filtered.txt $datafile");
}
my $chiptypeid :shared;
$query = "SELECT ID FROM chiptypes WHERE name = '$chiptype'";
$sth = $dbh->prepare($query);
$sth->execute();
my @chipres = $sth->fetchrow_array();
$chiptypeid = $chipres[0];
$sth->finish();

##################
# CREATE PROJECT #
##################
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year = 1900+$year;
$mon = $mon +1;
$time = "$mday/$mon/$year - $hour:$min:$sec";

##########################
# PREPARE MULTITHREADING #
##########################
$nrcpus = Sys::CPU::cpu_count() ;
#print "$nrcpus CPU's detected\n";
my $nr_running : shared;
my $nr_running = 0;
my $qsnpqueue = Thread::Queue->new();
`echo 1 > $statusfile`;

##################################
# PREPARE PER-SAMPLE INPUT FILES #
##################################
my @fileorder = Dataformat::Format_data( $datafile, "quantisnp");

############################################
# INSERT/UPDATE sample details in database #
############################################

# GENDERFILE: Get sample genders & names
print "Reading Gender File\n";
open INGender, "$genderfile" or die "Cannot open Genderfile";
print "Following genders settings will be used:\n";
my $header = <INGender>;
chomp($header);
my @headerparts = split(/\t/,$header);
my $gendercol;
my $namecol;
my $idcol;
my $callratecol;
my $colidx = 0;
my $fileok = 1;
foreach(@headerparts) {
	if ($_ =~ m/Sample ID/i ) {
		$namecol = $colidx;
	}
	elsif ($_ =~ m/Gender/i ) {
		$gendercol = $colidx;
	}
	elsif ($_ =~ m/Call Rate/i ) {
		$callratecol = $colidx;
	}
	elsif ($_ =~ m/Index/i ) {
		$idcol = $colidx;
	}
	$colidx++;
}
my %genderhash;
my %indexhash;
my %newsamplehash;
my %fortrackhash;
my %callratehash;
my %idhash;
while (<INGender>) {
	chomp($_);
	my @gender = split(/\t/,$_);
	if ($printcol == 3) {
		$sep = "\n";
		$printcol = 0;
	}
	else {
		$sep = "\t\t";
	}

	print " Sample $gender[0] => $gender[1] $sep";
	$printcol++;
	if ($gender[$gendercol] ne "Female" && $gender[$gendercol] ne "Male") {
		$fileok = 0;
	}
	$genderhash{$gender[$namecol]} = $gender[$gendercol];
	$indexhash{$gender[$namecol]} = $gender[$idcol];
	$callratehash{$gender[$namecol]} = $gender[$callratecol];
}
print "\n";
close INGender;
if ($fileok == 0) {
	die "Incorrect Gender Format detected\n";
}
$nrsamples = keys( %genderhash);
print "Preparing input files : Finished\n";
print "Starting analysis\n";

#########################################
# COMPOSE ARGUMENT LISTS AND QUEUE THEM #
#########################################
my %samplehash;
for ($i = 1;$i<=$nrsamples;$i++){
	my $headcommand = "head -n 1 datafiles/$prefix"."_$i.txt";
	my $line = `$headcommand`;
	$line =~ m/\t(\w+)\.Log/;
	$sampleID = $1;
	$samplehash{$i} = $sampleID;
	#print "Running QuantiSNP for sample $sampleID\n";
	if (-e "QS_output/QS_output_$prefix"."_$sampleID.txt") {unlink "QS_output/QS_output_$prefix"."_$sampleID.txt";}
	my $command = "cd $scriptdir/QuantiSNPv2/ && run_quantisnp2.sh --levels /opt/QuantiSNP2/config/levels-hd.dat --config /opt/QuantiSNP2/config/params.dat --output rawcnv --sampleid $sampleID --gender $genderhash{$sampleID} --emiters $emiters --Lsetting $lsetting --maxcopy $maxcopy $dogccorrect $printrs --gcdir /opt/QuantiSNP2/b36/ --input-files datafiles/".$prefix."_$i.txt >> QS_output/QS_output_$prefix"."_$sampleID.txt";
	#print "$command\n";
        #system("$command");
	$qsnpqueue->enqueue("$command");
}
my $left = $qsnpqueue->pending;
print "there are $left items on the queue\n";

#######################
# CREATE QSNP THREADS #
#######################
for ($i = 1;$i<=$nrcpus;$i++) {
	${qnspthr."$i"} = threads->create('qsnprun');	
}

#########################################
# Make the treads end before continuing #
#########################################
for ($i=1;$i<=$nrcpus;$i++) {
	$qsnpqueue->enqueue(undef);
}
for ($i=1;$i<=$nrcpus;$i++) {
	${qnspthr."$i"}->join();
	print "Thread $i of $nrcpus ended succesfully\n";
        `echo 0 > $statusfile`; 
}

############################
# FORMAT AND STORE RESULTS #
############################
print "Formatting resultsfile: $prefix"."_Results_total.xml\n";
open IN, "filter.txt";
my @filter = ();
$headerline = <IN>;
while (<IN>) {
	my $line = $_;
	chomp($line);
	my @values = split(/\t/,$line);	
	push(@filter, [@values]);
}
close IN;
$filtersize = scalar(@filter) -1;
if (-e "results_xml/".$prefix."_Results_total.xml") {unlink "results_xml/".$prefix."_Results_total.xml";} 
open OUT, ">>results_xml/".$prefix."_Results_total.xml";
#open OUT, ">>$targetname";
if (-e "results_list/$prefix"."_CNV_list.txt") {unlink "results_list/$prefix"."_CNV_list.txt";}
open OUTRES, ">>results_list/$prefix"."_CNV_list.txt";
print OUTRES "SampleID\tChrNum\tBase_Start\tBase_Stop\tLength\tStart_Probe\tStop_Probe\tNr_SNPs\tCNV\tLBF\n";
my @types = ("CNV Bin: Min 0 to Max 0.5" , "CNV Bin: Min 0.5 to Max 1.5" , "CNV Bin: Min 1.5 to Max 2.5" , "CNV Bin: Min 2.5 to Max 3.5", "CNV Bin: Min 3.5 to Max 4.5", "RECURRENT CNV");

print OUT "<Project_Bookmarks><Version>2.0.0</Version><Name>$projectname</Name><Author></Author><Comment>em: $emiters - Ls: $lsetting - mc: $maxcopy - dogcc: ".$opts{'G'}." - pRS: ".$opts{'R'}." - minSNP: $minsnp - minLBF: $minlbf</Comment><CreateDate>$time</CreateDate><Algorithm>QuantiSNP</Algorithm><AlgorithmVersion>1.1</AlgorithmVersion><Bookmark_Templates><bookmark_template><type>CNV Bin: Min 0 To Max 0.5</type><fill_color>Red</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 0.5 To Max 1.5</type><fill_color>Purple</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 1.5 to Max 2.5</type><fill_color>Gold</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 2.5 To Max 3.5</type><fill_color>Green</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 3.5 To Max 4.5</type><fill_color>Blue</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template><bookmark_template><type>RECURRENT CNV</type><fill_color>Black</fill_color><fill_style>Solid</fill_style><fill_opacity>40</fill_opacity></bookmark_template></Bookmark_Templates>\n<Bookmarks>";
for ($n=1;$n<=$nrsamples;$n++) {
	my $currsample = $samplehash{$n};
	open IN, "rawcnv/$currsample.cnv";
	my $line = <IN>;
	while (<IN>) {
		my $found = 0;
		chomp($_);
		my @line = split(/\t/, $_);
		my $CurrChr = $line[1];
		$CurrChr =~ s/^\s+//;
		$CurrChr = $chromhash{ $CurrChr };
		my $CurrStart = $line[2];
		$CurrStart =~ s/^\s+//;
		my $CurrEnd = $line[3];
		$CurrEnd =~ s/^\s+//;
		my $CurrCN = $line[8];
		$CurrCN =~ s/^\s+//;
		my $CurrSNP = $line[7];
		$CurrSNP =~s/^\s+//;
		my $CurrScore = $line[9];
		$CurrScore =~ s/^\s+//;

		if (($CurrSNP >= $minsnp) && ($CurrScore >=$minlbf)) { #skip all too short/unreliable calls (cf manual & Collella 2007) 
			#$result = searcharray(@line);
			#@found = split(/ /, $result);
			#$recstart = $found[1];
			#$recend = $found[2];
			#$recstart = $de->format_number($recstart);
			#$recend = $de->format_number($recend);
			#$reccn = $found[3];
			#$recocc = $found[4];
			#if ($found[0] == 1 ) {
		  	#  print OUTRES "$_\n";
		        #  print OUT "<bookmark>\n<sample_id>$line[0] \[$indexhash{$line[0]}\]</sample_id>\n<bookmark_type>$types[5]</bookmark_type>\n<entry_date></entry_date>\n<chr_num>$line[1]</chr_num>\n<base_start_pos>$line[2]</base_start_pos>\n<base_end_pos>$line[3]</base_end_pos>\n<author />\n<value>$line[8]</value>\n<comment><![CDATA[ RECURRENT ($recocc"."x); MeanCNV: $reccn\nMeanPos: $recstart - $recend\nLBF: $line[9], N.SNP: $line[7]]]></comment>\n</bookmark>\n";
			#}
			#else {
			  print OUTRES "$_\n";
			print OUT "<bookmark>\n<sample_id>$currsample \[$indexhash{$currsample}\]</sample_id>\n<bookmark_type>$types[$CurrCN]</bookmark_type>\n<entry_date></entry_date>\n<chr_num>$CurrChr</chr_num>\n<base_start_pos>$CurrStart</base_start_pos>\n<base_end_pos>$CurrStop</base_end_pos>\n<author />\n<value>$CurrCN</value>\n<comment><![CDATA[ LBF: $CurrScore, N.SNP: $CurrSNP]]></comment>\n</bookmark>\n";
		        }
		}
	}
	close IN;
}
print OUT "</Bookmarks>\n</Project_Bookmarks>\n";
close OUT;
$outname = "results_xml/" . $prefix . "_Results_total.xml";
`cp $outname $targetname && chmod a+rw $targetname`;
print "Cleaning up temporary files\n";
for ($i=1;$i<=$nrsamples;$i++) {
	my $cs = $samplehash{$i};
	my $splitted = "datafiles/$prefix"."_".$i.".txt";
	my $partresult = "rawcnv/$cs.cnv";
	my $qc = "results_$prefix"."_$i"."_QC.txt";
        `mv $qc QS_QualityControl/$qc`; 
        unlink("$splitted");
	unlink("$partresult");
	
}
unlink("$statusfile");
# Insert project into database
$query = "INSERT INTO nonmulti (userID, algo, project, stalen, bookmarks, date, chiptype) values ('$userid', 'QuantiSNP', '$projectname', '$nrsamples', '$xmlfile','$time', '$chiptypeid') ";
$sth = $dbh->prepare($query);
$sth->execute();
$sth->finish();

##################
# PRINT RUN-TIME #
##################
$now = time - $now;
printf("\n\nQuantiSNP running time:%02d:%02d:%02d\n",int($now/3600),int(($now % 3600)/60),int($now % 60));

###############
# SUBROUTINES #
###############
sub searcharray {
  my $found = 0;
  $cn = $_[8];
  $start = $_[2];
  $end = $_[3];
  $chr = $_[1];
  $mean = ($start + $end)/2;
  $size = $end - $start;
  for ($i=0; $i<=$filtersize;$i++) {
	if ($chr != $filter[$i][0] ) {
	  next;
        }
	elsif (($cn != $filter[$i][3]) && (($filter[$i][3] > 2 && $cn < 2 ) || ($filter[$i][3] < 2 && $cn > 2))) {
	  next;
        }
        else {
	  if ($start >= $filter[$i][1] && $end <= $filter[$i][2]) {
 		#Recurrent abberation
 		return "1 $filter[$i][1] $filter[$i][2] $filter[$i][3] $filter[$i][4]";
		$found = 1;
		last;
 	  }
	  elsif ($mean >= $filter[$i][1] && $mean <= $filter[$i][2] && $size <= 1.2*($filter[$i][2]-$filter[$i][1])) {
		#Recurrent abberation extending on at least one side, and not too far
		return "1 $filter[$i][1] $filter[$i][2] $filter[$i][3] $filter[$i][4]";
		$found = 1;
		last;
          }
	  else{
           next;
          }
        } 
  }
  if ($found !=1){return 0;}
}

sub qsnprun {
	my $thrid = threads->tid();
        CHECKLOAD: {
		my @cpuload = Sys::CpuLoad::load();
		my $currload = int($cpuload[0]);
		my $running = `cat $statusfile`;
		chomp($running);
		if (($thrid > $nrcpus - $currload) && ($running == 1) ) {
			my $randtime = int(120 + rand()*60); 
			#print "Overload protection, waiting $randtime"."s before starting thread $thrid\n";
			sleep $randtime; 
			redo CHECKLOAD;
		}
	}
	while ( defined( $comm = $qsnpqueue->dequeue)) {
	       RUNQSNP:{
			 my @cpuload = Sys::CpuLoad::load();
			 my $currload = int($cpuload[0]);
			 #print "$nrcpus CPU's found in subroutine with a load of $currload\n";
 			 if ($currload < $nrcpus) {
				$comm =~ m/sampleid (\w+) --/;
				print "Starting QuantiSNP for sample $1\n";
				#print "$comm\n";
				system("$comm");
				sleep 10;
				print "QuantiSNP analysis for sample $1 done\n";
			 }
			 else {
				print "Optimal load reached, retry in 1 min\n";
				sleep 60;
				redo RUNQSNP;
			 } 
			}
	}
}

