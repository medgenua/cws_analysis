
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.1"
	>

<xsl:output method="text"/>

<xsl:template match="HDF5">
/** 
 * This file has been automatically generated from a XML file,
 * which in turn was generated from the HDF5 header files.
 * Copyright 2005-2006 The MathWorks, Inc.
 */

<xsl:call-template name="add_ENUMS"/>
<xsl:call-template name="add_DEFINES"/>
<xsl:call-template name="add_FUNCTIONS"/>
</xsl:template>


<xsl:template name="add_ENUMS">
  <xsl:for-each select="ENUMS">
void HDF5lib::initializeConstants() {
  <xsl:for-each select="enum">
  <xsl:for-each select="value">
    addConstant("<xsl:value-of select="."/>", <xsl:value-of select="."/>);</xsl:for-each>
  </xsl:for-each>
}

</xsl:for-each>
</xsl:template>


<xsl:template name="add_DEFINES">
  <xsl:for-each select="DEFINES">
void HDF5lib::initializeDefines() {
  <xsl:for-each select="name">
    addConstant("<xsl:value-of select="."/>", <xsl:value-of select="."/>);</xsl:for-each>
}

</xsl:for-each>
</xsl:template>


<xsl:template name="add_STRUCTS">
  <xsl:for-each select="STRUCTS">
  <xsl:for-each select="struct">
  </xsl:for-each>
  </xsl:for-each>
</xsl:template>


<xsl:template name="add_TYPEDEFS">
  <xsl:for-each select="TYPEDEFS">
  <xsl:for-each select="typedef">
  </xsl:for-each>
  </xsl:for-each>
</xsl:template>


<xsl:template name="add_GLOBALS">
  <xsl:for-each select="GLOBALS">
  <xsl:for-each select="global">
    //addGlobal();
  </xsl:for-each>
  </xsl:for-each>
</xsl:template>


<xsl:template name="add_FUNCTIONS">
  <xsl:for-each select="FUNCTIONS">
  <xsl:text>void HDF5lib::initializeAutoFunctions() {
    </xsl:text>
  <xsl:for-each select="function">
    <xml:text>atts.init(<xsl:value-of select="@lmin"/>,<xsl:value-of select="@lmax"/>,<xsl:value-of select="@rmin"/>,<xsl:value-of select="@rmax"/>);
    </xml:text>
    <xsl:for-each select="output">
    <xml:text>atts.setParamFlags(0, <xsl:value-of select="@inout"/>, <xsl:value-of select="@minLength"/>);
    </xml:text>
    </xsl:for-each>
    <xsl:for-each select="input">
    <xml:text>atts.setParamFlags(<xsl:number count="input"/>, <xsl:value-of select="@inout"/>, <xsl:value-of select="@minLength"/>);
    </xml:text>
    </xsl:for-each>
    <xml:text>ADD_PROCEDURE_<xsl:value-of select="@cnargout"/>_<xsl:value-of select="@cnargin"/>("<xsl:value-of select="@name"/>", <xsl:value-of select="@name"/>, <xsl:value-of select="output"/>
    <xsl:for-each select="input">, <xsl:value-of select="."/></xsl:for-each>);
    </xml:text>
  </xsl:for-each>
}

</xsl:for-each>
</xsl:template>


</xsl:stylesheet>
