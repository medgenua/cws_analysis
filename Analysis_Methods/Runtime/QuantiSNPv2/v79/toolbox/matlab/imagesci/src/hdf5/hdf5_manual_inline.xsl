
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.1"
	>

<xsl:output method="text"/>

<xsl:template match="HDF5">
/** 
 * This file has been automatically generated from a XML file,
 * which in turn was generated from the HDF5 header files.
 * Copyright 2005-2006 The MathWorks, Inc.
 */

<xsl:call-template name="add_FUNCTIONS"/>
</xsl:template>


<xsl:template name="add_FUNCTIONS">
  <xsl:for-each select="FUNCTIONS">
  <xsl:text>void HDF5lib::initializeManualFunctions() {
    </xsl:text>
  <xsl:for-each select="function">
    <xml:text>atts.init(<xsl:value-of select="@lmin"/>,<xsl:value-of select="@lmax"/>,<xsl:value-of select="@rmin"/>,<xsl:value-of select="@rmax"/>);
    </xml:text>
    <xsl:for-each select="output">
    <xml:text>atts.setParamFlags(0, <xsl:value-of select="@inout"/>, <xsl:value-of select="@minLength"/>);
    </xml:text>
    </xsl:for-each>
    <xsl:for-each select="input">
    <xml:text>atts.setParamFlags(<xsl:number count="input"/>, <xsl:value-of select="@inout"/>, <xsl:value-of select="@minLength"/>);
    </xml:text>
    </xsl:for-each>
    <xml:text>addMethod(new HDF5lib_<xsl:value-of select="@name"/>("<xsl:value-of select="@name"/>", atts));
    </xml:text>
  </xsl:for-each>
}

</xsl:for-each>
</xsl:template>


</xsl:stylesheet>
