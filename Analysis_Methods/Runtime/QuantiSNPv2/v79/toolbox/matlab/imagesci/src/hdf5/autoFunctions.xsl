        
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.1">
        
<xsl:output method="text"/>

<xsl:template match="HDF5">

<xsl:call-template name="add_FUNCTIONS"/>
</xsl:template>


<xsl:template name="add_FUNCTIONS">
  <xsl:for-each select="FUNCTIONS">
    <xsl:for-each select="function">
<xsl:value-of select="@name"/><xsl:text>
</xsl:text>
    </xsl:for-each>
  </xsl:for-each>
</xsl:template>


</xsl:stylesheet>
