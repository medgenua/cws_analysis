#! /usr/local/bin/perl -w
# Make a template version of functionDoc.txt
# which must be hand-edited.

use lib "../";
use isdf;
use XML::Parser;

# read the list of functions
my @autoFunctions = &readFileIntoArray("autoFunctions.txt");
my @manualFunctions = &readFileIntoArray("manualFunctions.txt");
my @functions = (@autoFunctions, @manualFunctions);
@functions = sort(@functions);

# read the list of functions that will be stripped of an output parameter
my @missingOutput = &readFileIntoArray("returnsError.txt");

# read the parameter names
my $parser = new XML::Parser(ErrorContext => 2);
$parser->setHandlers(Start => \&start_handler);

$parser->parsefile("hdf5_auto.xml");
$parser->parsefile("hdf5_manual.xml");


# finally, print the documentation file
foreach $func (@functions) {
    # print out the function name
    print($func."\n");
    # print out the output argument names
    for $i ( 0 .. $#{ $param{$func."_output"} } ) {
        if($i==0 && grep { $_ eq $func } @missingOutput ) {
            #continue
        }
        else {
            print $param{$func."_output"}[$i] . " ";
        }
    }
    print "\n";
    # print out the input argument names
    for $i ( 0 .. $#{ $param{$func."_input"} } ) { 
        print $param{$func."_input"}[$i] . " ";
    }
    print "\n";
    # print out the function documentation
    print("Call the HDF5 library $func function.\n");
}


# The handler for a given XML tag.
sub start_handler
{
    my ($exp, $elem, @attr) = @_;
    # set global information about the function
    if($elem eq "function") {
        $currFunc = $attr[1];
        $currOutputNum = 0;
        $currInputNum  = 0;
    }
    # Add the parameter to the input and/or output list.
    if($elem eq "output" || $elem eq "input") {
        if($attr[1] =~ m/OUTPUT/) {
            $param{$currFunc."_output"}[$currOutputNum++] = $attr[3];
        }
        if($attr[1] =~ m/INPUT/) {
            $param{$currFunc."_input"}[$currInputNum++] = $attr[3];
        }
    }
}
