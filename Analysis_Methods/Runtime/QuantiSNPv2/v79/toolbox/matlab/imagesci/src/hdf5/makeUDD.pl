#! /usr/local/bin/perl -w
# Make a bunch of @H5 UDD packages
# Write the M-files that are contained within them.

use lib "../";
use isdf;

use File::Find;
use File::Basename;


# The interfaces that we operate on (H5ML is maintained manually).
my $cwd = `pwd`;
my @interfaces = ("H5", "H5A", "H5D", "H5E", "H5F", "H5G", "H5I", "H5P", "H5R", "H5S", "H5T", "H5Z");

# Read the required template and data files
my %functions = &readFunctionDoc("functionDoc.txt");
my $mTemplate = &readFileIntoString("functionTemplate.m");
my $sTemplate = &readFileIntoString("schemaTemplate.m");

# Write all of the functions
foreach $fname (keys(%functions)) {
    $fname =~ m/^(H5[A-Z]*)(.*)/;
    my $iface = $1;
    my $dname = '../../@'.$iface;
    my $fileName = $2;
    my $fpath = $dname.'/'.$fileName.'.m';
    makeMFile($fpath, $mTemplate, $iface, $fileName, %functions);
}    

# Write the contents and schema files
foreach $iface (@interfaces) {
    my $dname = '../../@'.$iface;
    makeContentsFile($dname, $iface);
    makeSchemaFile($dname, $sTemplate, $iface);
}

exit(0);


sub makeMFile {
# func is a structure with several fields.
    my ($filename, $text, $iface, $fname, %functions) = @_;

    # Fill in a hash of the long interface names.
    my %ifacename;
    $ifacename->{'H5'}  = 'Library';
    $ifacename->{'H5A'} = 'Attribute';
    $ifacename->{'H5D'} = 'Dataset';
    $ifacename->{'H5E'} = 'Error';
    $ifacename->{'H5F'} = 'File';
    $ifacename->{'H5G'} = 'Group';
    $ifacename->{'H5I'} = 'Identifier';
    $ifacename->{'H5P'} = 'Property List';
    $ifacename->{'H5R'} = 'Reference';
    $ifacename->{'H5S'} = 'Dataspace';
    $ifacename->{'H5T'} = 'Datatype';
    $ifacename->{'H5Z'} = 'Compression';

    # Read the input parameter names
    my $input = trim($functions{$iface.$fname}{'input'});
    if ($input eq "") {
        $inputList = "";
        $inputListBottom = "";
    }
    else {
        $input =~ s/ /, /g;
        $inputList = "(" . $input . ")";
        $inputListBottom = ", " . $input;
    }

    # Read the output parameter names
    my $output = trim($functions{$iface.$fname}{'output'});
    if ($output eq "") {
        $outputList = "";
    }
    else {
        if ($output !~ m/ /) {
            $outputList = $output . " = ";
        }
        else {
            $output =~ s/ /, /g;
            $outputList = "[" . $output . "] = ";
        }
    }
    
    # Read the function description
    my $desc = $functions{$iface.$fname}{'desc'};

    # Substitute template text.
    $text =~ s/\#INAME\#/$iface/g;
    $text =~ s/\#FNAME\#/$fname/g;
    $text =~ s/\#IFACE\#/$ifacename->{$iface}/g;
    $text =~ s/\#ARGINTOP\#/$inputList/g;
    $text =~ s/\#ARGINBOTTOM\#/$inputListBottom/g;
    $text =~ s/\#ARGOUT\#/$outputList/g;
    $text =~ s/\#FUNCTIONDESC\#/$desc/g;

    # Write the file
    open(FID, ">$filename");
    print(FID $text);
    close(FID);
}

# Create the schema file for a package.
sub makeSchemaFile {
    my ($dirname, $text, $iface) = @_;
    $filename = $dirname.'/schema.m';
    $text =~ s/\#INAME\#/$iface/g;
    open(FID, ">$filename");
    print(FID $text);
    close(FID);
}

# Create the contents file for a package.
sub makeContentsFile {
    my $dir   = shift(@_);
    my $iface = shift(@_);
    my $filename = $dir."/Contents.m";
    
    open(CONT_FID, ">$filename");
    
    $text = "% Contents for " . $iface .":\n%\n";
    print(CONT_FID $text);
    
    $fNameLength = 4;
    find(\&getLongestFunctionName, $dir."/");
    find(\&writeH1Line, $dir."/");
    close(CONT_FID);
}

# Return the longest function name in the directory.
sub getLongestFunctionName {
    my $filename = $File::Find::name;
    # M-files starting with lowercase letters.
    if( $filename =~ m/(.*)\/([a-z]+\w*\.m$)/ ) {
        my $length = length($2) +2; # Add two
        $fNameLength = &max($fNameLength, $length);
    }
}
    
sub max {
    my $a = shift(@_);
    my $b = shift(@_);
    if ($a>$b) {return $a} else {return $b};
} 
sub min {
    my $a = shift(@_);
    my $b = shift(@_);
    if ($a<$b) {return $a} else {return $b};
} 

# Write the H1 line of a file to the contents File Identifier
sub writeH1Line {
    my $filename = $File::Find::name;
    # M-files starting with lowercase letters.
    if( $filename =~ m/(.*)\/([a-z]+\w*\.m$)/ ) {
        my @lines = &readFileIntoArray($2);
        my $text = $lines[1];
        $text =~ m/%(\w*.\w*)\s+(.*)/;
        my $fmtStr = sprintf("%%-%ds ", $fNameLength);
        $text = "%   ". sprintf($fmtStr, $1).$2."  \n";
        print(CONT_FID $text);
    }
}

# Read the documentation data file.  It is a repeating format consisting
# of the following four lines:
# 1) function name
# 2) function output arguments (space seperated)
# 3) function input arguments (space seperated)
# 4) The function description
sub readFunctionDoc {
    my $filename = shift(@_);
    open(FID, "<$filename") or die "Could not open file: $filename";
    my $fNum = 0;
    my %output;
    my $desc;

    while(<FID>) {
        chomp($_);
        my $line = $_;
        if($fNum==0) {
            $func = $_;
            $desc = "";
            $fNum++;
        }
        elsif($fNum==1) {
            $output{$func}->{'output'} = $_;
            $fNum++;
        }
        elsif($fNum==2) {
            $output{$func}->{'input'} = $_;
            $fNum++;
        }
        elsif($fNum==3) {
            $output{$func}->{'h1'} = $_;
            $fNum++;
        }
        elsif($_ =~ m/^%/) {
            $desc = $desc . $line ."\n";
        }
        else {
            # Remove the extra line break
            chomp($desc);
            $output{$func}->{'desc'} = $desc;
            $fNum=0;
        }
    }
    close(FID);

    return %output;
}
