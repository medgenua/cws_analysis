#! /usr/local/bin/perl -w


#######################
# Parse the input options

use Getopt::Long;

# include isdf.pm for readFileIntoArray.
use lib "../";
use isdf;

my $unsFunctionsFile = "N/A";
my $unsDefinesFile = "N/A";

GetOptions("include=s"      =>  \$HDF5_INC,
           "header=s"       =>  \$HDF5_H,
           "unsFunctions=s" =>  \$unsFunctionsFile,
           "defines=s"      =>  \$unsDefinesFile,
           "convert=s"      =>  \$stringConvertFile);

#######################
# Preprocess the header file

my $interfaceFile = "hdf5.i";
# Create a version without #DEFINES
system("cpp -P -nostdinc -I $HDF5_INC $HDF5_INC/$HDF5_H > $interfaceFile");
# Create a version with #DEFINES
my $definesFile = $interfaceFile . ".d";
system("cpp -P -dM -nostdinc -I $HDF5_INC $HDF5_INC/$HDF5_H > $definesFile");

#######################
# Read the list of disallowed functions/defines

my @unsupportedFunctions = &readFileIntoArray($unsFunctionsFile);
my @unsupportedDefines = &readFileIntoArray($unsDefinesFile);


#######################
# Read the list of types which may require conversion from a MATLAB string.
# All of the enumerations, as well as several types which are #DEFINED
# We add the later here.
my @stringConvertParams = &readFileIntoArray($stringConvertFile);

#######################
# Match three types of things:
# 1) ENUM * enumname;
# 2) TYPEDEF * type;
# 3) output function(*);

# Read the file into a variable
my $file = &readFileIntoString($interfaceFile);

# Break apart the interface into declarations
$file =~ s/\n//g;
$file =~ s/\s+/ /g;
my @chunks = split(/;/, $file);


# Since we may have split structure definitions, 
# reunite chunks with unmatched curly braces {}
my $nesting = 0;
my $chunk;
my @decl;
my $declNum = 0;

foreach $chunk (@chunks) {
    $decl[$declNum] = $decl[$declNum] . $chunk;
    
    my $lbrace = $chunk;
    if( $lbrace =~ /{/ && $lbrace =~ /}/ ) {
    }
    elsif( $lbrace =~ /{/ && $lbrace !~ /}/ ) {
        $nesting = $nesting+1;
    }
    elsif ( $lbrace !~ /{/ && $lbrace =~ /}/ ) {
        $nesting = $nesting-1;
    }
    
    if ($nesting==0) {
        # Trim WS
        if($decl[$declNum]) {
            $decl[$declNum] = trim($decl[$declNum]);
        }
        $declNum++;
    }
    else {
        $decl[$declNum] = $decl[$declNum] . ";";
    }
};

# OK, time to print the HDF5 file.
# All of the top-level stuff is here, but we delegate to specific 
# routines later on in the file.
print "<HDF5 version=\"1.6.5\">\n";
print "<ENUMS>\n";
my @enums;
my $numEnum = 0;
foreach $chunk (@decl) {
    if( $chunk =~ / enum/) {
        $enums[$numEnum++] = &processEnum($chunk);
    }
}
print "</ENUMS>\n\n";
push(@stringConvertParams, @enums);
print "<STRUCTS>\n";
foreach $chunk (@decl) {
    if( $chunk =~ /struct/) {
        &processStructure($chunk);
    }
}
print "</STRUCTS>\n\n";
print "<TYPEDEFS>\n";
foreach $chunk (@decl) {
    if( $chunk =~ /typedef/ && $chunk !~ / enum/ && $chunk !~ /struct/) {
        &processTypedef($chunk);
    }
}
print "</TYPEDEFS>\n\n";
print "<GLOBALS>\n";
foreach $chunk (@decl) {
    if( $chunk =~ /extern / && $chunk !~ /typedef/ && $chunk !~ / enum/ && $chunk !~ /struct/) {
        &processGlobal($chunk);
    }
}
print "</GLOBALS>\n\n";
print "<FUNCTIONS>\n";
foreach $chunk (@decl) {
    if( $chunk !~ /extern / && $chunk !~ /typedef/ && $chunk !~ / enum/ && $chunk !~ /struct/) {
        &processFunction($chunk, $unsupportedFunctions);
    }
}
print "</FUNCTIONS>\n\n";
print "<DEFINES>\n";
&processDefines($definesFile,@unsupportedDefines);
print "</DEFINES>\n";
print "</HDF5>\n";


# cleanup intermediate files
system("rm $interfaceFile");
system("rm $definesFile");

exit(0);



#######################
# Read the ENUMs
sub processEnum() {
    my $str = shift;
    # e.g. (typedef enum) { (a=1, b=2, ...) } (typename_t)
    if($str =~ /^(.*)\{(.*)\} *(\w+)/ ) {
        my $fn = $3;
        my $enums = $2;
        print "<enum name=\"" . $3 ."\">\n";
        my @args = split(",", $enums);
        
        foreach $arg (@args) {
            # strip off the names
            $arg =~ /[ ]*(\w*).*/;
            print "\t<value>" . trim($1) . "</value>\n";
        }
        print "</enum>\n";
        return $fn;
    }
    else {
        print(<STDERR>, "UNHANDLED ENUM: " . $str ."\n");
    }
}


#######################
# Read the STRUCTs
sub processStructure() {
    my $str = shift;
    print "<struct>".$str.";</struct>\n";
}

#######################
# Read the TYPEDEFs
sub processTypedef() {
    my $str = shift;
    print "<typedef>".$str.";</typedef>\n";
}

#######################
# Read the GLOBALs
sub processGlobal() {
    my $str = shift;
    $str =~ /(\w+) (\w+) (\w+)/;
    print "<global name=\"" . $3 . "\" type=\"" . $2 . "\" />\n";
}

#######################
# Read the FUNCTIONs
sub processFunction() {
    my $str = shift(@_);
    my $unsupportedFunctions = shift;
    my $cnargin  = 0;
    my $cnargout = 0;
    # e.g. function_name ( parameter_list ) 
    if($str =~ /^(.*)\((.*)\)/ ) {
        my $fn = $1;
        my $params = $2;
        # e.g. const type function_name
        $fn =~ /^(.*[ *]+)(\w+)/;
        my $output = trim($1);
        if($output ne "void") {
            $cnargout++;
        }
        
        my $name = $2;
        my @args = split(",", $params);

        # If this function is not supported, just return.
        if ( grep { $_ eq $name } @unsupportedFunctions) {
            return;
        }
        
        my $numArgs = 0;
        my @minLength;
        foreach $arg (@args) {
            # strip off the names of the formal parameters.  
            # The parameter might be unnamed, if its void.
            # e.g. (const int) (foobar)
            if( $arg =~ /^(.*[ *]+)(\w+){1}[ ]*$/ ) {
                $args[$cnargin] = trim($1);
                $argNames[$cnargin] = trim($2);
            }
            # Turn [] into *
            # This will not strip names ending in []
            if( $arg =~ /^(.*[ *]+)(\w+){1}\s*\[\s*\]\s*$/ ) {
                $args[$cnargin] = trim($1)."*";
            }
            
            # Set the minimum length of non-const pointer arguments.
            if ($arg =~ /\*/ && $arg !~ /const/) {
                $minLength[$cnargin] = $name."_".$cnargin."_MIN_LENGTH";
            }
            else {
                $minLength[$cnargin] = "1";
            }
            
            if($arg ne "void") {
                $cnargin++;
            }
        }

        # Write the C output tag
        my $argString = "\t<output";
        my $inout = "ParameterAttributes::OUTPUT";
        if( $output =~ m/const/ ) {
            $inout = "ParameterAttributes::OUTPUT|ParameterAttributes::NODELETE";
        }
        $argString = $argString . " inout=\"".$inout."\"";
        $argString = $argString . " name=\"output\"";
        $argString = $argString . " minLength=\"1\"";
        $argString = $argString . ">".$output."</output>\n";

        # Write the C input tags
        my $lmin = 0;
        my $lmax = 1;
        my $rmin = 0;
        my $rmax = 0;
        $argNum = 0;
        foreach $arg (@args) {
            if($arg ne "void") {

                if( $arg =~ m/\*/ && $arg !~ m/const/) {
                    $inout = "ParameterAttributes::OUTPUT";
                    $lmax++;
                }
                else {
                    $inout = "ParameterAttributes::INPUT";
                    $rmax++;
                    $rmin++;
                }
                # If this parameter is an enumeration, we should convert it from a string.
                if ( grep { $_ eq $arg } @stringConvertParams) {
                    $inout = $inout . "|ParameterAttributes::STRING_CONVERT";
                }
                # Remove const
                # $arg =~ s/^const\s*//;
                $argString = $argString . "\t<input";
                $argString = $argString . " inout=\"".$inout."\"";
                $argString = $argString . " name=\"".$argNames[$argNum]."\"";
                $argString = $argString . " minLength=\"".$minLength[$argNum]."\"";
                $argString = $argString . ">".$arg."</input>\n";
            }
            $argNum++;
        }
        # Print the function tag
        my $nargs = $cnargin + $cnargout;
        print "<function name=\"".$name."\" nargs=\"".$nargs."\" ";
        print "cnargin=\"".$cnargin."\" cnargout=\"".$cnargout."\" ";
        print "lmin=\"".$lmin."\" lmax=\"".$lmax."\" rmin=\"".$rmin."\" rmax=\"".$rmax."\" >\n";
        print $argString;
        print "</function>\n";
    }
    else {
        print "UNHANDLED FUNCTION: " . $str ."\n";
    }
}

#######################
# Read the DEFINES
sub processDefines() {
    my $definesFile = shift;
    my $unsupportedDefines = shift;
    open INFILE, "<$definesFile" or die "Can't open the #DEFINEs file";
        while (<INFILE>) {
            if ($_ =~ m/^(\#define H)([^ (]*)/) {
                # If this define is not supported, just return.
                my $name = "H".$2;
                if ( grep { $_ =~ m/$name/ } @unsupportedDefines ) {
                    ;# It's unsupported: do nothing
                }
                else {
                    print "\t<name>". $name ."</name>\n" ;
                }
            }
        }
    close INFILE;
}
