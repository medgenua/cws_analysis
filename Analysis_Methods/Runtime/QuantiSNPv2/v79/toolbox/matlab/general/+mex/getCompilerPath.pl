#   getCompilerPath.pl is a tool used by CompilerConfiguration tool.
#
#   Copyright 2007-2008 The MathWorks, Inc. 
#   $Revision: 1.1.6.3 $  $Date: 2008/06/24 17:12:23 $#
#

BEGIN{
use Getopt::Long;
# Parse inputs
GetOptions('matlabroot=s'       => \$MATLAB,
           'storageLocation=s'  => \$storageLocation,
           'outputType=s'       => \$outputType);
push(@INC, $MATLAB . "/bin" );
}

use mexsetup;
use File::Basename;

#Reorganize storage locatioin into something that callstpfile understands
$storageLocation =~ s|\.bat|\.stp|;
my ($name,$path,$suffix);
($name,$path,$suffix) = fileparse( "$storageLocation", qr{\.stp});
$record = callstpfile($path, $name . $suffix);

# If compiler is too old or too new for the OS then skip it.
if (!isCompilerSupportedOnArchitecture($record)) {
    print("compilerNotSupportedOnOS");
    exit(1);
}

# Print appropriate return value
if ($outputType eq 'environmentVariable') {
    print($record->{"root_var"});
} else {
    my $locate_fcn = $record->{"locate"};
    my @locations = &$locate_fcn;
    my $path = @locations[0];
    print($path);
}

exit(1);
