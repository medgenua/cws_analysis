/* Copyright 1996-2004 The MathWorks, Inc. */

/* $Revision: 1.12.4.2 $ */
#ifndef VERSION

#ifdef V5_COMPAT
#define VERSION "MATLAB 5 compatible"
#else
#define VERSION "MATLAB R14 native"
#endif /* V5_COMPAT */

#endif /* VERSION */

#ifdef ARRAY_ACCESS_INLINING
#define INLINE " (inlined)"
#else
#define INLINE
#endif /* ARRAY_ACCESS_INLINING */

#ifdef MCC_GENERATED
#define COMPILERGEN " (MATLAB Compiler Generated)"
#else
#define COMPILERGEN
#endif /* MCC_GENERATED */

static const char *version = VERSION INLINE COMPILERGEN;

#ifdef __cplusplus
extern "C" {
#endif

const char *mexVersion (void);

const char *mexVersion (void) {
    /* mex version information */
    return version;
}

#ifdef __cplusplus
}
#endif
