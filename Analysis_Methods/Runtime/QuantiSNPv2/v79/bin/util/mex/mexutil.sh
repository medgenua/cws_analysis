#!/bin/sh
#
# mexutil.sh contains assorted utility functions for MEX and MBUILD.
#     
# Copyright 2008 The MathWorks, Inc.
# $Revision: 1.1.8.2 $  $Date: 2008/03/17 19:36:46 $
#____________________________________________________________________________
#

# Validate version of Sun Studio
  warn_about_SunStudio11()
  {
    if [ "$Arch" = "sol64" ]; then
      if [ `expr match "/$FOUND_FILE" '/.*bin/gccopts.sh$'` -eq 0 ]; then 

        versionNumber="`cc -V 2>&1 | sed -n -e's/.*Sun C \(5\.8\).*/\1/p' `"

        if [ "$versionNumber" = "5.8" ]; then
          echo
          echo "**************************************************************************"
          echo "  Warning: Sun Studio 11 is being phased out and will not be supported in"
          echo "           a future release."
          echo "           For a list of currently supported compilers see:"
          echo "           http://www.mathworks.com/support/tech-notes/1600/1601.html"
          echo "**************************************************************************"
          echo
        fi
      fi
    fi
  }
# end validate_SunSolaris11
#
#****************************************************************************
