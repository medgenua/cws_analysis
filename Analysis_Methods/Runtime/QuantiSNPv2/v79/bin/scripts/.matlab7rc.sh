#
# usage:        .matlab7rc.sh
#
# abstract:     This Bourne Shell script is sourced by the matlab script
#		to obtain certain site/user dependent information
#		as explained below. The first occurrence of this file
#		in the directory list:
#
#			. 		(current)
#			$HOME		(home)
#			matlab/bin	(default location)
#
#		is used. Most of the time this file in the default location
#		need not be modified at all and nothing needs to be done.
#		However, if MATLAB does not work properly with the default
#		values then this file may need to be modified and different
#		values inserted.
#
#		Currently, the following variables appear in the code below.
#
#		ARCH			(machine architecture)
#		AUTOMOUNT_MAP		(Path prefix map for automounting)
#		DISPLAY			(DISPLAY variable for X Window System)
#		LDPATH_PREFIX		(path(s) that appear at the start of
#					 LD_LIBRARY_PATH)
#		LDPATH_SUFFIX		(path(s) that appear at the end of
#					 LD_LIBRARY_PATH)
#		LD_LIBRARY_PATH		(load library path - the name
#					 LD_LIBRARY_PATH is platform dependent)
#		MATLAB			(MATLAB root directory)
#		MATLABPATH		(MATLAB search path)
#		SHELL			(which shell to use for ! and unix
#					 command in MATLAB)
#		TOOLBOX			(toolbox path)
#    		XAPPLRESDIR		(X Application Resource Directory)
#		XKEYSYMDB		(X keysym Database file)
#
#		NOTE: Run matlab -n to get the values used to run MATLAB.
#		      MATLAB is NOT executed.
#
#		Additional variables are used in the MATLAB script, but
#		to affect their behavior from this file requires an
#		understanding first of how they are determined in the
#		MATLAB script and then making code modifications to this
#		file.
#
#		The strategy behind the use of this file is to keep
#		the site wide changes in the matlab/bin/.matlab7rc.sh version
#		and have the individual user start with a copy in their
#		$HOME directory and modify it for their special circumstances.
#
#		IMPORTANT: Please understand that The MathWorks cannot
#			   anticipate every possible installation. If
#			   your situation does not fit into the current
#			   model of using this .matlab7rc.sh file then
#			   we would like to hear from you. Please
#			   contact The MathWorks Technical Support.
#
# note(s):	1. The default values are
#
#		   ARCH			(machine architecture)
#
#			This is the machine architecture determined by
#			the arch utility script.
#
#		   AUTOMOUNT_MAP	(Path prefix map for automounting)
#
#			This is set by install_matlab if possible. This is
#			null unless you are running the automounter. This
#			may have to be set manually if installation is done
#			on a machine that doesn't have automouting, yet
#			machines that execute MATLAB do.
#
#			For the case that the MATLAB directory is moved
#			intact and no reinstallation is done either set the
#			value of the AUTOMOUNT_MAP at the beginning of the
#			script below or set it in the environment before
#			MATLAB is called.
#
#		   DISPLAY		(DISPLAY variable for X Window System)
#
#			This is set to "$DISPLAY" where DISPLAY is
#		        taken from the environment.
#		        
#		   LDPATH_PREFIX	(path(s) that appear at the
#					 start of LD_LIBRARY_PATH)
#
#			Enclose in single quotes to defer evaluation
#			to the MATLAB script.
#
#		   LDPATH_SUFFIX	(path(s) that appear at the
#					  end of LD_LIBRARY_PATH)
#
#			Enclose in single quotes to defer evaluation
#			to the MATLAB script.
#
#		   LD_LIBRARY_PATH	(load library path - the name
#					 LD_LIBRARY_PATH is platform
#					 dependent)
#
#			TABLE:
#
#			      platform          variable name
#			      --------          -------------
#				sol2		LD_LIBRARY_PATH
#				hpux		SHLIB_PATH
#				glnx86		LD_LIBRARY_PATH
#                               mac             DYLD_LIBRARY_PATH       
#
#		   NOTE: The final load library path determined
#			 in the MATLAB startup script is composed
#			 of:
#			
#       ------------------------------------------------------------
#       LDPATH_PREFIX:<matlab_additions>:LD_LIBRARY_PATH:\
#		      <system_additions>:LDPATH_SUFFIX
#       ------------------------------------------------------------
#
#			 This means to add paths between:
#			 1. <matlab_additions> and LD_LIBRARY_PATH
#			    put them in front of LD_LIBRARY_PATH
#			 2. LD_LIBRARY_PATH and <system_additions>
#			    put them at the end of LD_LIBRARY_PATH
#
#		   MATLAB		(MATLAB root directory)
#
#			This is set by install_matlab. The MATLAB script
#			fixes this directory using the automount map
#			specified by AUTOMOUNT_MAP if it is not null.
#
#		        NOTE: The value of MATLAB called MATLABdefault is
#			      determined first by the MATLAB script before
#			      this file is sourced. If the value of MATLAB
#			      placed in this script by install_matlab is
#			      different than MATLABdefault then
#			      MATLABdefault is used as the value of MATLAB
#			      then the AUTOMOUNT_MAP value in the environment
#			      is used.
#
#		   MATLABPATH		(MATLAB search path)
#
#			This is set to "$MATLABPATH" where MATLABPATH is
#			taken from the environment.
#
#		   SHELL		(which shell to use for ! or
#					 unix command in MATLAB)
#
#		        This is set to "$SHELL" where SHELL is taken from
#			the environment. If SHELL is empty or not defined
#			then MATLAB uses /bin/sh internally.
#
#		   TOOLBOX		(toolbox path)
#
#			This is set to "$TOOLBOX" where TOOLBOX is
#			taken from the environment.
#			
#    		   XAPPLRESDIR		(X Application Resource Directory)
#
#			This is set to '$MATLAB/X11/app-defaults'.
#			
#		        WATCH OUT! Those are single quotes. Because this
#				   will evaluated later! 
#
#		   XKEYSYMDB		(X keysym Database file)
#
#			This is set to '$MATLAB/X11/app-defaults/XKeysymDB'.
#
#		        WATCH OUT! Those are single quotes. Because this
#				   will evaluated later! 
#
# Copyright 1986-2007 The MathWorks, Inc.
# $Revision: 1.1.8.7 $  $Date: 2007/11/12 22:52:33 $
#----------------------------------------------------------------------------
#
# If you move the MATLAB tree, do not reinstall, and an automount map is
# required then replace AUTOMOUNT_MAP by the map in the right hand side
# of the next statement. Then AUTOMOUNT_MAP does not have to be set 
# in the environment before MATLAB is called.
#
    AUTOMOUNT_MAPenv="$AUTOMOUNT_MAP"
#
# Determine the arch.
#
#   -------------------------------------------------------------
#
    MATLAB_UTIL_DIR=
#
#   -------------------------------------------------------------
#
    if [ ! "$MATLAB_UTIL_DIR" ]; then
	MATLAB_UTIL_DIR=$MATLAB_UTIL_DIRdefault
    fi
#
# arch.sh requires MATLAB - save temporarily
#
    MATLABsave="$MATLAB"
    MATLAB="$MATLABdefault"
#
    . $MATLAB_UTIL_DIR/arch.sh
    if [ "$ARCH" = "unknown" ]; then
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    echo ''
    echo '    Sorry! We could not determine the machine architecture for your'
    echo '           host. Please contact:'
    echo ''
    echo '               MathWorks Technical Support'
    echo ''
    echo '           for further assistance.'
    echo ''
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        trap ""
        exit 1
    fi
    MATLAB="$MATLABsave"
#
#	IMPORTANT! Modify ONLY if you don't like the defaults after running
#		   MATLAB.
#
    case "$ARCH" in
	sol2)
#----------------------------------------------------------------------------
# MATLAB and AUTOMOUNT_MAP originally set by install_matlab
#
	    MATLAB=|>MATLAB<|
	    AUTOMOUNT_MAP=|>AUTOMOUNT_MAP<|
#
	    if [ "$MATLAB" != "$MATLABdefault" ]; then
		MATLAB=$MATLABdefault
	        AUTOMOUNT_MAP=$AUTOMOUNT_MAPenv
	    fi
#
	    DISPLAY="$DISPLAY"
	    ARCH="$ARCH"
	    TOOLBOX="$TOOLBOX"
	    MATLABPATH="$MATLABPATH"
	    XAPPLRESDIR='$MATLAB/X11/app-defaults'
	    XKEYSYMDB='$MATLAB/X11/app-defaults/XKeysymDB'
	    SHELL="$SHELL"
	    LDPATH_PREFIX=''
#
# To always use the OpenGL libraries shipped with MATLAB uncomment the next
# line.
#
#           LDPATH_PREFIX='$MATLAB/sys/opengl/lib/$ARCH'
#
	    LDPATH_SUFFIX=''
#
	    if [ "$LD_LIBRARY_PATH" != "" ]; then
                LD_LIBRARY_PATH=$LD_LIBRARY_PATH
	    else
                LD_LIBRARY_PATH=
	    fi
#----------------------------------------------------------------------------
	    ;;
	hpux)
#----------------------------------------------------------------------------
# MATLAB and AUTOMOUNT_MAP originally set by install_matlab
#
	    MATLAB=|>MATLAB<|
	    AUTOMOUNT_MAP=|>AUTOMOUNT_MAP<|
#
	    if [ "$MATLAB" != "$MATLABdefault" ]; then
		MATLAB=$MATLABdefault
	        AUTOMOUNT_MAP=$AUTOMOUNT_MAPenv
	    fi
#
	    DISPLAY="$DISPLAY"
	    ARCH="$ARCH"
	    TOOLBOX="$TOOLBOX"
	    MATLABPATH="$MATLABPATH"
	    XAPPLRESDIR='$MATLAB/X11/app-defaults'
	    XKEYSYMDB='$MATLAB/X11/app-defaults/XKeysymDB'
	    SHELL="$SHELL"
	    LDPATH_PREFIX=''
#
# To always use the OpenGL libraries shipped with MATLAB uncomment the next
# line.
#
#           LDPATH_PREFIX='$MATLAB/sys/opengl/lib/$ARCH'
#
	    LDPATH_SUFFIX=''
#
	    if [ "$SHLIB_PATH" != "" ]; then
                SHLIB_PATH=$SHLIB_PATH
	    else
                SHLIB_PATH=
	    fi

# Eliminating Incorrect Message Catalog Warning on HP-UX
# When you compile an M-file on HP-UX, you may see a series of warnings including
# cc: 487: warning : Possibly incorrect message catalog. 
#
# To eliminate the warnings, either unset NLSPATH or set NLSPATH to
# /opt/ansic/lib/nls/msg/%L/%N.cat:/opt/ansic/lib/nls/msg/C/%N.cat 
#
# New Bang environment syncronization sends value of the environment variable NLSPATH to Agent. 
# The variable NLSPATH is set by JVM. To eliminate the warnings we set NLSPATH here and
# JVM will append it's own value later.
#
	mw_nlspath="/opt/ansic/lib/nls/msg/%L/%N.cat:/opt/ansic/lib/nls/msg/C/%N.cat" 
        if [ "$NLSPATH" = "" ]; then
	    NLSPATH="${mw_nlspath}"
	else    
	    NLSPATH="${NLSPATH}:${mw_nlspath}"
	fi
	export NLSPATH
#----------------------------------------------------------------------------
	    ;;
	glnx86)
#----------------------------------------------------------------------------
# MATLAB and AUTOMOUNT_MAP originally set by install_matlab
#
	    MATLAB=|>MATLAB<|
	    AUTOMOUNT_MAP=|>AUTOMOUNT_MAP<|
#
	    if [ "$MATLAB" != "$MATLABdefault" ]; then
		MATLAB=$MATLABdefault
	        AUTOMOUNT_MAP=$AUTOMOUNT_MAPenv
	    fi
#
	    DISPLAY="$DISPLAY"
	    ARCH="$ARCH"
	    TOOLBOX="$TOOLBOX"
	    MATLABPATH="$MATLABPATH"
	    XAPPLRESDIR='$MATLAB/X11/app-defaults'
	    XKEYSYMDB='$MATLAB/X11/app-defaults/XKeysymDB'
	    SHELL="$SHELL"
	    LDPATH_PREFIX=''
#
# To always use the OpenGL libraries shipped with MATLAB uncomment the next
# line.
#
#           LDPATH_PREFIX='$MATLAB/sys/opengl/lib/$ARCH'
#
	    LDPATH_SUFFIX=''
#
	    if [ "$LD_LIBRARY_PATH" != "" ]; then
                LD_LIBRARY_PATH=$LD_LIBRARY_PATH
	    else
                LD_LIBRARY_PATH=
	    fi
#----------------------------------------------------------------------------
	    ;;
	glnxi64)
#----------------------------------------------------------------------------
# MATLAB and AUTOMOUNT_MAP originally set by install_matlab
#
	    MATLAB=|>MATLAB<|
	    AUTOMOUNT_MAP=|>AUTOMOUNT_MAP<|
#
	    if [ "$MATLAB" != "$MATLABdefault" ]; then
		MATLAB=$MATLABdefault
	        AUTOMOUNT_MAP=$AUTOMOUNT_MAPenv
	    fi
#
	    DISPLAY="$DISPLAY"
	    ARCH="$ARCH"
	    TOOLBOX="$TOOLBOX"
	    MATLABPATH="$MATLABPATH"
	    XAPPLRESDIR='$MATLAB/X11/app-defaults'
	    XKEYSYMDB='$MATLAB/X11/app-defaults/XKeysymDB'
	    SHELL="$SHELL"
	    LDPATH_PREFIX=''
#
# To always use the OpenGL libraries shipped with MATLAB uncomment the next
# line.
#
#           LDPATH_PREFIX='$MATLAB/sys/opengl/lib/$ARCH'
#
	    LDPATH_SUFFIX=''
#
	    if [ "$LD_LIBRARY_PATH" != "" ]; then
                LD_LIBRARY_PATH=$LD_LIBRARY_PATH
	    else
                LD_LIBRARY_PATH=
	    fi
#----------------------------------------------------------------------------
	    ;;
	mac|maci|maci64)
#----------------------------------------------------------------------------
# MATLAB originally set by install_matlab
#
	    MATLAB=|>MATLAB<|
	    AUTOMOUNT_MAP='/private ->'
#
	    if [ "$MATLAB" != "$MATLABdefault" ]; then
		MATLAB=$MATLABdefault
	        AUTOMOUNT_MAP=$AUTOMOUNT_MAPenv
	    fi
#
	    DISPLAY="$DISPLAY"
	    ARCH="$ARCH"
	    TOOLBOX="$TOOLBOX"
	    MATLABPATH="$MATLABPATH"
	    XAPPLRESDIR='$MATLAB/X11/app-defaults'
	    XKEYSYMDB='$MATLAB/X11/app-defaults/XKeysymDB'
	    SHELL="$SHELL"
	    LDPATH_PREFIX=''
#
# To always use the OpenGL libraries shipped with MATLAB uncomment the next
# line.
#
#           LDPATH_PREFIX='$MATLAB/sys/opengl/lib/$ARCH'
#
	    LDPATH_SUFFIX=''
#
	    if [ "$DYLD_LIBRARY_PATH" != "" ]; then
                DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH
	    else
                DYLD_LIBRARY_PATH=
	    fi
#----------------------------------------------------------------------------
	    ;;
	*)
#----------------------------------------------------------------------------
# MATLAB and AUTOMOUNT_MAP originally set by install_matlab
#
	    MATLAB=|>MATLAB<|
	    AUTOMOUNT_MAP=|>AUTOMOUNT_MAP<|
#
	    if [ "$MATLAB" != "$MATLABdefault" ]; then
		MATLAB=$MATLABdefault
	        AUTOMOUNT_MAP=$AUTOMOUNT_MAPenv
	    fi
#
	    DISPLAY="$DISPLAY"
	    ARCH="$ARCH"
	    TOOLBOX="$TOOLBOX"
	    MATLABPATH="$MATLABPATH"
	    XAPPLRESDIR='$MATLAB/X11/app-defaults'
	    XKEYSYMDB='$MATLAB/X11/app-defaults/XKeysymDB'
	    SHELL="$SHELL"
	    LDPATH_PREFIX=''
#
# To always use the OpenGL libraries shipped with MATLAB uncomment the next
# line.
#
#           LDPATH_PREFIX='$MATLAB/sys/opengl/lib/$ARCH'
#
	    LDPATH_SUFFIX=''
#
	    if [ "$LD_LIBRARY_PATH" != "" ]; then
                LD_LIBRARY_PATH=$LD_LIBRARY_PATH
	    else
                LD_LIBRARY_PATH=
	    fi
#----------------------------------------------------------------------------
	    ;;
    esac
